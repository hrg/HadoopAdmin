<%@ page import="org.apache.shiro.subject.Subject" %>
<%@ page import="org.apache.shiro.SecurityUtils" %>
<%@ page import="com.ideal.hadoopadmin.security.ShiroDbRealm" %>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>

<%
    request.setAttribute("decorator", "none");
    response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
    response.setHeader("Pragma","no-cache"); //HTTP 1.0
    response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>

<%
    Subject subject = SecurityUtils.getSubject();
    ShiroDbRealm.ShiroUser shiroUser = (ShiroDbRealm.ShiroUser)subject.getPrincipal();
    if(shiroUser!=null){
        String uri = request.getContextPath();
        response.sendRedirect(uri+ "/main/home_page.do");
    }
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<link href="${ctxRoot}/static/css/login.css" rel="stylesheet" type="text/css" />
<html>
<head>
    <title>Login</title>
    <script src="${ctxRoot}/static/js/scriptLib.js"></script>
    <script>
        function checkLogin(){
            with(document){
                var userName = getElementById("userName");
                var password = getElementById("password");
                if(isEmpty(userName.value)){
                    alert("用户名不能为空！");
                    userName.focus();
                    return false;
                }
                if(isEmpty(password.value)){
                    alert("密码不能为空！");
                    password.focus();
                    return false;
                }
                return true;
            }
        }
//        window.onload=function(){document.demoForm.submit();}
    </script>
</head>
<body>
        <form action="${ctxRoot}/jsp/login.jsp" name="demoForm" method="post" onsubmit="return checkLogin();">
        <div class="bg_login">
            <img src="${ctxRoot}/static/images/bg.jpg" alt="" width="1200" height="680"
                 lowsrc="${ctxRoot}/static/images/bg.jpg" class="bgbgs">
            <div class="login_box">
                <table width="90%" border="0" cellspacing="0" class="login_tab">
                    <tr>
                        <td>
                            <div class="lgk">
                                <span class="user_name">用户名：</span>
                                <input type="text" id="userName" name="username" value=""/>
                                <div class="errorMessage"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="lgk">
                                <span class="user_name">密　码：</span>
                                <input type="password" id="password" name="password" value=""/>
                                <div class="errorMessage"></div>
                                <%
                                    Subject subject1 = SecurityUtils.getSubject();
                                    ShiroDbRealm.ShiroUser shiroUser1 = (ShiroDbRealm.ShiroUser) subject.getPrincipal();
                                    if (shiroUser == null) {
                                        String error = (String) request.getAttribute(FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);
                                        if (error != null) {
                                            out.print("<span style='color: red'>用户名或密码错误。</span>");
                                        }
                                    }

                                %>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="lgk lgk_b" style="background-color: rgb(250,250,250)">
                                <input name="" type="submit" value="登　录"/>
                            </div>

                        </td>
                    </tr>
                </table>

            </div>


        </div>
        </form>
</body>
</html>
