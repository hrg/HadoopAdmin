<%--ace BEGIN--%>
<!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="${ctxRoot}/static/ace/assets/js/jquery.2.1.1.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="${ctxRoot}/static/ace/assets/js/jquery.1.11.1.min.js"></script>
<![endif]-->

<!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='${ctxRoot}/static/ace/assets/js/jquery.min.js'>"+"<"+"/script>");
</script>

<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
window.jQuery || document.write("<script src='${ctxRoot}/static/ace/assets/js/jquery1x.min.js'>"+"<"+"/script>");
</script>
<![endif]-->
<script type="text/javascript">
    if('ontouchstart' in document.documentElement) document.write("<script src='${ctxRoot}/static/ace/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="${ctxRoot}/static/ace/assets/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="${ctxRoot}/static/ace/assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="${ctxRoot}/static/ace/assets/js/jquery-ui.custom.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/jquery-ui.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/chosen.jquery.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/fuelux.spinner.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/bootstrap-datepicker.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/bootstrap-timepicker.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/moment.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/daterangepicker.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/bootstrap-colorpicker.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/jquery.knob.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/jquery.autosize.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/jquery.inputlimiter.1.3.1.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/jquery.maskedinput.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/bootstrap-tag.min.js"></script>
<script type="text/javascript" src="${ctxRoot}/static/framework/My97DatePicker/WdatePicker.js"></script>
<script src="${ctxRoot}/static/js_project/select.jquery.min.js"></script>

<!-- ace scripts -->
<script src="${ctxRoot}/static/ace/assets/js/ace.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/ace-elements.min.js"></script>

<!-- inline scripts related to this page -->

<%--ace END--%>
<%--ace CSS-BEGIN--%>
<!-- page specific plugin styles -->
<link rel="stylesheet" href="${ctxRoot}/static/ace/assets/css/jquery-ui.custom.min.css" />
<link rel="stylesheet" href="${ctxRoot}/static/ace/assets/css/chosen.min.css" />
<link rel="stylesheet" href="${ctxRoot}/static/ace/assets/css/datepicker.min.css" />
<link rel="stylesheet" href="${ctxRoot}/static/ace/assets/css/bootstrap-timepicker.min.css" />
<link rel="stylesheet" href="${ctxRoot}/static/ace/assets/css/daterangepicker.min.css" />
<link rel="stylesheet" href="${ctxRoot}/static/ace/assets/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" href="${ctxRoot}/static/ace/assets/css/colorpicker.min.css" />




<!--
///---------------------------------

ace  lib

----------------------------------//

-->

<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="${ctxRoot}/static/ace/assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="${ctxRoot}/static/ace/assets/font-awesome/4.2.0/css/font-awesome.min.css" />
<!-- page specific plugin styles -->
<!-- text fonts -->
<link rel="stylesheet" href="${ctxRoot}/static/ace/assets/fonts/fonts.googleapis.com.css" />
<!-- ace styles -->
<link rel="stylesheet" href="${ctxRoot}/static/ace/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<!--[if lte IE 9]>
<link rel="stylesheet" href="${ctxRoot}/static/ace/assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
<![endif]-->
<!--[if lte IE 9]>
<link rel="stylesheet" href="${ctxRoot}/static/ace/assets/css/ace-ie.min.css" />
<![endif]-->
<!-- inline styles related to this page -->
<!-- ace settings handler -->
<script src="${ctxRoot}/static/ace/assets/js/ace-extra.min.js"></script>
<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
<!--[if lte IE 8]>
<script src="${ctxRoot}/static/ace/assets/js/html5shiv.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/respond.min.js"></script>
<![endif]-->




<%--js--%>
<script src="${ctxRoot}/static/js/extra.ajax.js"></script>
<script src="${ctxRoot}/static/js/extra.tips.js"></script>
<script src="${ctxRoot}/static/js/extra.win.js"></script>
<script src="${ctxRoot}/static/js/extra.util.js"></script>
<script src="${ctxRoot}/static/js/extra.jquery.js"></script>
<script src="${ctxRoot}/static/js/extra.regional.js"></script>

<link rel="stylesheet" href="${ctxRoot}/static/css/style.css"/>

<%--Highcharts--%>
<%--<script type="text/javascript" src="${ctxRoot}/static/js/Highcharts-4.1.8/js/highcharts.js"></script>--%>
<%--<script type="text/javascript" src="${ctxRoot}/static/js/Highcharts-4.1.8/js/modules/exporting.js"></script>--%>
<%--<script type="text/javascript" src="${ctxRoot}/static/js/Highcharts-4.1.8/js/highcharts-more.js"></script>--%>
<%--layer--%>

<%--ztree--%>
<link href="${ctxRoot}/static/ztree/zTreeStyle.css" rel="stylesheet" type="text/css" />
<script src="${ctxRoot}/static/ztree/jquery.ztree.core-3.5.js"></script>
<script src="${ctxRoot}/static/ztree/jquery.ztree.excheck-3.5.js"></script>

<%--jquery_multiple--%>
<link rel="stylesheet" href="${ctxRoot}/static/js/jquery_multiple/multiple-select.css" />
<script src="${ctxRoot}/static/js/jquery_multiple/jquery.multiple.select.js"></script>

<script src="${ctxRoot}/static/js/layer2.0/layer/layer.js"></script>

<script src="${ctxRoot}/static/js/common.js.jsp"></script>
<!-- jquery.treeTable-->
<script src="${ctxRoot}/static/treeTable1.4.2/script/treeTable/jquery.treeTable.js"></script>
<link href="${ctxRoot}/static/treeTable1.4.2/script/treeTable/vsStyle/jquery.treeTable.css" rel="stylesheet" type="text/css" />

