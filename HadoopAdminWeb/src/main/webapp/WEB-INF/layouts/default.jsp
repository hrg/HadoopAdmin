<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <sitemesh:head/>
    <title>数据管理权限</title>
    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="shortcut icon" href="${ctxRoot}/static/images/manager.ico" type="image/x-icon"/>
    <%@include file="/WEB-INF/layouts/meta.jsp" %>
</head>

<body class="no-skin">
<iframe id='callbackframe' name='callbackframe' src='about:blank' style='display:none'></iframe>
<div id='background' style="display:block;" class='background'></div>
<div id='progressBar' style="display:block;" class='progressBar'>数据加载中，请稍等...</div>
<!--<![endif]-->
<%@ include file="/WEB-INF/layouts/header.jsp" %>

<div class="main-container" id="main-container">
    <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
            <span class="menu-text"></span>
        </a>

        <%--<jsp:include page="menu.jsp"/>--%>

        <div class="main-content" id="mainId">
            <sitemesh:body/>
        </div>

    </div>
</div>
<div id="_winId" style="display:none">
    <div style="top:0;left:0;position:fixed;z-index:1000;" class="bg"></div>
    <div class="beian_winBG">
        <span class="ui-dialog-title">
            <div class="widget-header widget-header_height">
                <h4 class="smaller font_y"><i class="icon-plus-sign green"></i><span id="_winTitle"></span></h4>
            </div>
        </span>
        <div id="_winContentId">

        </div>
    </div>
</div>
<div id="_thirdId" style="display:none">
    <div style="top:0;left:0;position:fixed;z-index:1050;" class="bg"></div>
    <div class="beian_winBG2" style="z-index: 1100;">
        <span class="ui-dialog-title">
            <div class="widget-header widget-header_height">
                <h4 class="smaller font_y"><i class="icon-plus-sign green"></i><span id="_thirdTitle"></span>
                    <span style="float:right;margin-right: 5px" id="span_tool">
                    <a data-action="close" href="#" onclick="thirdWin.close();"><i class="ace-icon fa fa-times"></i></a></span></h4>
            </div>
        </span>
        <div id="_thirdContentId">

        </div>
    </div>
</div>
</body>
</html>
