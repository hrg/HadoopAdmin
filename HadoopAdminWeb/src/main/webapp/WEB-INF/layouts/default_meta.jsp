<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="/WEB-INF/layouts/meta.jsp" %>
    <sitemesh:head/>
    <title>数据管理权限</title>
    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="shortcut icon" href="${ctxRoot}/static/images/manager.ico" type="image/x-icon"/>
</head>

<body class="no-skin">
<sitemesh:body/>
<div id="_winId" style="display:none">
    <div style="top:0;left:0;position:fixed;z-index:1000;" class="bg"></div>
    <div class="beian_winBG2">
        <span class="ui-dialog-title">
            <div class="widget-header widget-header_height">
                <h4 class="smaller font_y"><i class="icon-plus-sign green"></i><span id="_winTitle"></span></h4>
            </div>
        </span>
        <div id="_winContentId">

        </div>
    </div>
</div>
</body>
</html>
