<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<div class="navbar navbar-default" id="navbar">
    <div class="navbar-container" id="navbar-container">
        <div class="navbar-header pull-left">
            <a href="#" class="navbar-brand">
                <small>
                    <i class="icon-leaf"></i>
                    多租户管理平台
                </small>
            </a><!-- /.brand -->
        </div>
        <!-- /.navbar-header -->

        <div class="navbar-header pull-right" role="navigation">

            <ul class="nav ace-nav">
                <li class="purple">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" onclick="getCountHDFS();" id="countHDFSId">
                        <i class="ace-icon fa fa-bell icon-animated-bell"></i>
                        <span class="badge badge-important" flag="flag_countHDFS">${sessionScope.countHDFS}</span>
                    </a>

                    <ul class="dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
                        <li class="dropdown-header">
                            <i class="ace-icon fa fa-exclamation-triangle"></i>
                            <span flag="flag_countHDFS">${sessionScope.countHDFS}</span> Notifications
                        </li>
                        <li class="dropdown-content">
                            <ul class="dropdown-menu dropdown-navbar navbar-pink">
                                <%--<li>--%>
                                    <%--<a href="${ctxRoot}/system/eventInfo_list.do">--%>
                                        <%--<div class="clearfix">--%>
                                <%--<span class="pull-left">--%>
                                <%--<i class="btn btn-xs no-hover btn-pink fa fa-comment"></i>--%>
                                  <%--事件信息--%>
								<%--</span>--%>
                                            <%--<c:choose>--%>
                                                <%--<c:when test="${count!=null}">--%>
                                                    <%--<span class="pull-right badge badge-info">+${count}</span>--%>
                                                <%--</c:when>--%>
                                                <%--<c:otherwise>--%>
                                                    <%--<span class="pull-right badge badge-info"></span>--%>
                                                <%--</c:otherwise>--%>
                                            <%--</c:choose>--%>
                                        <%--</div>--%>
                                    <%--</a>--%>
                                <%--</li>--%>
                                <%--<li>--%>
                                    <%--<a href="${ctxRoot}/report/userApprove_list.do">--%>
                                        <%--<div class="clearfix">--%>
                                <%--<span class="pull-left">--%>
                                    <%--<i class="btn btn-xs no-hover btn-success glyphicon glyphicon-gift"></i>--%>
									<%--Kerberos 事件--%>
								<%--</span>--%>
                                            <%--<c:choose>--%>
                                                <%--<c:when test="${countKerberos!=null}">--%>
                                                    <%--<span class="pull-right badge badge-success">+${countKerberos}</span>--%>
                                                <%--</c:when>--%>
                                                <%--<c:otherwise>--%>
                                                    <%--<span class="pull-right badge badge-success"></span>--%>
                                                <%--</c:otherwise>--%>
                                            <%--</c:choose>--%>
                                        <%--</div>--%>
                                    <%--</a>--%>
                                <%--</li>--%>
                                <li>
                                    <a href="${ctxRoot}/meta/hdfsCompare/compareHdfs.do">
                                        <div class="clearfix">
                                <span class="pull-left">
                                    <i class="btn btn-xs no-hover btn-info glyphicon glyphicon-warning-sign"></i>
									HDFS 警报
							    </span>
                                            <c:choose>
                                                <c:when test="${countHDFS!=null}">
                                                    <span class="pull-right badge badge-info">+${countHDFS}</span>
                                                </c:when>
                                                <c:otherwise>
                                                    <span class="pull-right badge badge-info"></span>
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="green" title="使用说明">
                    <a class="dropdown-toggle" target="_blank" href="${ctxRoot}/static/guide/quick/inform.html">
                        <i class="ace-icon fa fa-tasks"></i>
                    </a>
                </li>
                <li class="light-blue">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <img class="nav-user-photo" src="${ctxRoot}/static/ace/assets/avatars/user.jpg"
                             alt="Jason's Photo"/>
        <span class="user-info">
            <small>欢迎您：</small>
            <shiro:principal></shiro:principal>
        </span>

                        <i class="icon-caret-down"></i>
                    </a>

                    <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <%--暂时注释掉--%>
                        <%--<li>--%>
                        <%--<a href="${ctxRoot}/user/modify_user_password.do">--%>
                        <%--<i class="icon-user"></i>--%>
                        <%--修改密码--%>
                        <%--</a>--%>
                        <%--</li>--%>
                        <%--<li class="divider"></li>--%>
                        <li>
                            <a href="${ctxRoot}/logout.do" onclick="javascript:return confirm('确定退出系统？')">
                                <i class="icon-off"></i>
                                退出登陆
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- /.ace-nav -->
        </div>
        <!-- /.navbar-header -->
    </div>
    <!-- /.container -->
</div>
<script>
    function getCountHDFS(){
        $("#countHDFSId").attr("disable","disable");
        $.ajax({
            url: "${ctxRoot}/main/countHDFS.do",
            data: "",
            type: "post",
            dataType: "json",
            async:false,
            success: function (data) {
                console.log(data);
                console.log(data.data);
                $("span[flag='flag_countHDFS']").each(function() {
                    $(this).html(data["data"]);
                });
                $("#countHDFSId").removeAttr("disable");
            },
            error: function () {

            }

        })
    }

</script>