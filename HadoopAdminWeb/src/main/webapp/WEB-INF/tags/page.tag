<%@tag pageEncoding="UTF-8"%>
<%@ attribute name="page" type="com.github.pagehelper.PageInfo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${not empty page && page.size!=0}">
    <%
        //begin从哪里开始？
        int current = page.getPageNum() ;
        int paginationSize = page.getPageSize();
//        int begin = 1;
        int begin = Math.max(1, current - paginationSize / 2);
//        int end = page.getPages();
        int end = Math.min(begin + (paginationSize - 1),  page.getPages());
        int totalPages = page.getPages();
        Long totalSizes = page.getTotal();
        request.setAttribute("current", current);
        request.setAttribute("begin", begin);
        request.setAttribute("end", end);
        request.setAttribute("totalPages", totalPages);
        request.setAttribute("totalSizes", totalSizes);
    %>

    <div class="pull-right">
        <ul class="pagination middle pull-left">
            <% if (page.isHasPreviousPage()) {%>
            <li><a href="javascript:void(0);" onclick="page(1,'${searchParams}','${divId}');"><span> <i class="fa fa-angle-double-left bigger-150"></i></span></a></li>
            <li><a href="javascript:void(0);" onclick="page(${current-1},'${searchParams}','${divId}');"><span><i class="fa fa-angle-left bigger-150"></i></span></a></li>
            <%} else {%>
            <li class="disabled"><span> <i class="fa fa-angle-double-left bigger-150"></i></span></li>
            <li class="disabled"><span><i class="fa fa-angle-left bigger-150"></i></span></li>
            <%} %>

            <c:forEach var="i" begin="${begin}" end="${end}">
                <c:choose>
                    <c:when test="${i == current}">
                        <%--<li class="active"><a href="javascript:void(0);" onclick="page(${i},'${searchParams}','${divId}');">${i}</a></li>--%>
                        <li class="active"><a href="javascript:void(0);" style="background-color: #6faed9;border-color: #6faed9;color: #fff;" >${i}</a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="javascript:void(0);" onclick="page(${i},'${searchParams}','${divId}');">${i}</a></li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>

            <% if (page.isHasNextPage()) {%>
            <li><a href="javascript:void(0);" onclick="page(${current+1},'${searchParams}','${divId}');"><i class="fa fa-angle-right bigger-140"></i></a></li>
            <li><a href="javascript:void(0);" onclick="page(${page.pages},'${searchParams}','${divId}');"><i class="fa fa-angle-double-right bigger-150"></i></a></li>
            <%} else {%>
            <li class="disabled"><span><i class="fa fa-angle-right bigger-150"></i></span></li>
            <li class="disabled"><span><i class="fa fa-angle-double-right bigger-150"></i></span></li>
            <%} %>
            <span class="pagination middle">&nbsp;&nbsp;&nbsp;${current} / ${totalPages}　共 ${totalSizes} 条记录</span>
        </ul>
    </div>
</c:if>