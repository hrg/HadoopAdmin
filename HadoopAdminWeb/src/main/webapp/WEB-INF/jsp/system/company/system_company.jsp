<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <title>租户管理</title>
</head>
<body>
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="${ctxRoot}">首页</a>
        </li>
        <li>
            集群管理
        </li>
        <li class="active">
            租户管理
        </li>
    </ul>
</div>
<div class="page-content">
    <form action="${ctx}/system_company.do" method="post" onsubmit="return search(this);">
        <div class="row_height_s">
            <div class="form-group_li">
                <input type="text"  placeholder="租户名称" class="col_d" name="Q_LIKE_companyName" value="${param.Q_LIKE_companyName}"/>
            </div>
            <%--<div class="form-group_li">--%>
            <%--<select class="form-control" name="Q_EQ_companyType.id">--%>
            <%--<option value="">---请选择租户类型---</option>--%>
            <%--<c:forEach var="item" items="${parameterList}">--%>
            <%--<option value="${item.id}" <c:if test="${item.id==param['Q_EQ_companyType.id']}">selected</c:if>>${item.name}</option>--%>
            <%--</c:forEach>--%>
            <%--</select>--%>
            <%--</div>--%>
            <button class="btn btn-primary btn-sm" type="submit">搜索
                <i class="icon-search icon-on-right bigger-110"></i>
            </button>
        </div>
    </form>
    <div class="row row_height_y">
        <shiro:hasPermission name="customerAdd">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-primary"
                        onClick="win.init({title:'添加租户',rel:''}).show('${ctx}/edit_customer_pop.do',{})">新增
                </button>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="customer_modify">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-info"
                        onClick="win.init({title:'修改租户'}).show('${ctx}/edit_customer_pop.do?id={id}',{})">修改
                </button>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="customer_delete">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-danger"
                        onClick="mywin.init({title:'确定要删除吗？'}).confirm('${ctx}/delete_customer.do?id={id}','确定要删除吗')">删除
                </button>
            </div>
        </shiro:hasPermission>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTable">
            <thead>
            <tr>
                <th class="center">
                    <label>
                        <input type="checkbox" class="ace" onclick="checkAll(this);">
                        <span class="lbl"></span>
                    </label>
                </th>
                <th>租户名称</th>
                <th>联系人</th>
                <th>联系人手机</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${page.list}" var="item">
                <tr>
                    <td class="center">
                        <label>
                            <input type="checkbox" class="ace" name="id" value="${item.id}"/>
                            <span class="lbl"></span>
                        </label>
                    </td>
                    <td >${item.companyName}</td>
                    <td>${item.contactName}</td>
                    <td>${item.contactMobile}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="row row_height_x message-footer">
            <tags:page page="${page}"/>
        </div>
    </div><!-- /.table-responsive -->
</div>
</body>
</html>
