<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<form action="${ctx}/save_systemCompany.do" method="post" onsubmit="return validate(this)">
    <input type="hidden" name="id" value="${customerInfo.id}">
    <input type="hidden" name="clusterCompanyQuotaId" value="${clusterCompanyQuota.id}">
    <div class="clearfix">
        <div class="form_g_li">
            <label class="col_left_li"> <span>*</span> 租户名称 </label>
            <div class="col_right_li">
                <input type="text" class="inp required" name="companyName" value="${customerInfo.companyName}"/>
            </div>
        </div>

        <div class="form_g_li">
            <label class="col_left_li"> <span>*</span> 联系人 </label>
            <div class="col_right_li">
                <input type="text" class="inp required" name="contactName" value="${customerInfo.contactName}"/>
            </div>
        </div>
        <div class="form_g_li">
            <label class="col_left_li"> <span>*</span> 联系人手机 </label>
            <div class="col_right_li">
                <input type="text" class="inp required mobile" name="contactMobile"
                       value="${customerInfo.contactMobile}"/>
            </div>
        </div>
        <div class="form_g_li">
            <label class="col_left_li"> <span>*</span> 最大用户数 </label>
            <div class="col_right_li">
                <input type="text" class="inp required digits" name="maxUserNum"
                       value="${clusterCompanyQuota.maxUserNum}"/>
            </div>
        </div>
        <div class="form_g_li">
            <label class="col_left_li"> <span>*</span> 最大内存空间(M) </label>
            <div class="col_right_li">
                <input type="text" class="inp required digits" name="maxResource"
                       value="${clusterCompanyQuota.maxResource}"/>
            </div>
        </div>
        <div class="form_g_li">
            <label class="col_left_li"> <span>*</span> 最大CPU数(核) </label>
            <div class="col_right_li">
                <input type="text" class="inp required digits" name="maxCpu"
                       value="${clusterCompanyQuota.maxCpu}"/>
            </div>
        </div>
        <div class="form_g_li">
            <label class="col_left_li"> <span>*</span> HDFS配额空间(T) </label>
            <div class="col_right_li">
                <input type="text" class="inp required digits" name="hdfsSpace"
                       value="${clusterCompanyQuota.hdfsSpace}"/>
                <span></span>
            </div>
        </div>
        <div class="form_g_li">
            <label class="col_left_li"> <span>*</span> HDFS文件配额数 </label>
            <div class="col_right_li">
                <input type="text" class="inp required digits" name="hdfsFileCount"
                       value="${clusterCompanyQuota.hdfsFileCount}"/>
            </div>
        </div>
        <div class="form_g_li form_g_li1">
            <label class="col_left_li"> <span>*</span>描述 </label>
            <div class="col_right_li">
                <textarea name="note" class="required" cols="75" rows="3" class="inp_l">${customerInfo.note}</textarea>
            </div>
        </div>
    </div>
    <div class="ui_t_foot">
        <button class="btn" type="button" onclick="win.close();">取消</button>
        <button class="btn btn-primary" type="submit">确定</button>
    </div>
</form>