<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib uri="/tags" prefix="date"%>

<html>
<head>
    <title>系统日志</title>
</head>
<body>
<%-- 页面标题头--%>
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="${ctxRoot}">首页</a>
        </li>
        <li>
            系统管理
        </li>
        <li class="active">
            日志管理
        </li>
    </ul>
</div>
<div class="page-content">


    <%--查询条件--%>
    <form action="${ctx}/view_daily.do" method="post" onsubmit="return search(this);">
        <div class="row_height_s">


            <%--用户名--%>
            <div class="form-group_li">
                <input type="text" placeholder="用户名" class="col_d" value="${userName}"
                       name="Q_LIKE_log.userName"/>
            </div>

            <%--执行时间查询--%>
            <div class="form-group_li">
                <input type="text" placeholder="操作" class="col_d" value="${val}"
                       name="Q_LIKE_op.operationName"/>
            </div>

            <button class="btn btn-primary btn-sm" type="submit">搜索
                <i class="ace-icon glyphicon glyphicon-search"></i>
            </button>
        </div>
    </form>


    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTable table-responsive" style="table-layout: fixed;">
            <thead>
            <tr>

                <th width="10%" class="center">用户名</th>
                <th width="10%" class="center">Url</th>
                <th width="10%" class="center">执行时间</th>
                <th width="15%" class="center">参数</th>
                <th width="13%" class="center">操作</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${infoWebappLogPage.list}" var="item">
                <tr>
                    <td class="center">${item.userName}</td>
                    <td class="center" title="${item.webOperations.url}" style="text-overflow:ellipsis; overflow: hidden;white-space: nowrap">${item.webOperations.url}</td>
                    <td class="center">
                        <date:date value="${item.createtime}" pattern="yyyy-MM-dd HH:mm:ss"/>

                    </td>
                    <td class="center" title="${item.parameter}" style="text-overflow:ellipsis; overflow: hidden;white-space: nowrap">${item.parameter}</td>
                    <td class="center">${item.webOperations.operationName}</td>
                </tr>


            </c:forEach>
            </tbody>

        </table>

        <div class="row row_height_x message-footer">
            <tags:page page="${infoWebappLogPage}"/>
        </div>
    </div>
</div>

</body>
</html>
