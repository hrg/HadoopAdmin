<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<form action="${ctx}/save_user.do" method="post" onsubmit="return validate(this)">
    <input type="hidden" name="id" value="${userInfo.id}">
    <div class="clearfix">
        <div class="form_g_li">
            <label class="col_left_li"> <span>*</span>用户名 </label>
            <div class="col_right_li">
                <c:choose>
                    <c:when test="${userInfo.id!=null}">
                        <input type="text" class="inp" name="login_name" readonly value="${userInfo.userName}"/>
                    </c:when>
                    <c:otherwise>
                        <input type="text" class="inp required" name="login_name"/>
                    </c:otherwise>
                </c:choose>
            </div>

        </div>

        <div class="form_g_li">
            <label class="col_left_li"> <span>*</span>登录密码 </label>
            <div class="col_right_li">
                <c:choose>
                    <c:when test="${userInfo.id!=null}">
                        <input type="password" class="inp" name="password" readonly value="${userInfo.passWord}"/>
                    </c:when>
                    <c:otherwise>
                        <input type="text" class="inp required password" minlength="6" name="password"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>

        <div class="form_g_li">
            <label class="col_left_li"> 手机号 </label>
            <div class="col_right_li">
                <input type="text" class="inp mobile" name="mobile" value="${userInfo.phoneNumber}"/>
            </div>
        </div>
        <div class="form_g_li">
            <label class="col_left_li"> email </label>
            <div class="col_right_li">
                <input type="text" class="inp mail" name="email" value="${userInfo.email}"/>
            </div>
        </div>

        <div class="form_g_li">
            <label class="col_left_li"> <span>*</span>角色 </label>
            <div class="col_right_li">
                <select id="roleId" name="role_id" multiple="multiple">
                    <c:forEach var="row" items="${roleList}">
                        <%--<option value="${row.id}" <c:if test="${row.id==userInfo.role.id}">selected</c:if>>${row.cnName}</option>--%>
                        <option value="${row.id}" >${row.name}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="form_g_li" style="width: 480px">
            <label class="col_left_li"> <span>*</span> 租户 </label>
            <div class="col_right_li">
                <select name="customer_id">
                    <c:forEach var="row" items="${customerList}">
                        <option value="${row.id}" <c:if test="${row.id==userInfo.systemCompanyId}">selected</c:if>>${row.companyName}</option>
                        <%--<option value="${row.id}" >${row.companyName}</option>--%>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="form_g_li form_g_li1">
            <label class="col_left_li"> 简介 </label>
            <div class="col_right_li">
                <textarea name="remark" style="max-width: 570px" cols="" rows=""
                          class="inp_l">${userInfo.remark}</textarea>
            </div>
        </div>
    </div>
    <div class="ui_t_foot">
        <button class="btn" type="button" onclick="win.close();">取消</button>
        <button class="btn btn-primary" type="submit">确定</button>
    </div>
</form>
<script>
    $('#roleId').multipleSelect({
        width: 215
    }).multipleSelect('setSelects',${roleIds});;
</script>