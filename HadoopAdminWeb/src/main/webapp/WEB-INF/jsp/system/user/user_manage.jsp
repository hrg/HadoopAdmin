<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <title>账户管理</title>
</head>
<body>
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="${ctxRoot}">首页</a>
        </li>
        <li>
            系统管理
        </li>
        <li class="active">
            账户管理
        </li>
    </ul>
</div>
<div class="page-content">
    <form action="${ctx}/user_manage.do" method="post" onsubmit="return search(this);">
        <div class="row_height_s">
            <div class="form-group_li">
                <input type="text"  placeholder="用户名" name="Q_LIKE_su.userName" class="col_d" value="${userName}"/>
            </div>
            <div class="form-group_li">
                <div class="col_right_li">
                    <select  class="chosen-select form-control" name="Q_EQ_sc.id" style="width:160px" data-placeholder="租户">
                        <option></option>
                        <c:forEach items="${customerList}" var="row">
                            <option value="${row.id}" <c:if test="${row.id==param['Q_EQ_sc.id']}">selected</c:if>>${row.companyName}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group_li">
                <div class="col_right_li">
                    <select  class="chosen-select form-control" name="Q_EQ_sr.id" style="width:160px" data-placeholder="角色">
                        <option></option>
                        <c:forEach items="${roleList}" var="row">
                            <option value="${row.id}" <c:if test="${row.id==param['Q_EQ_sr.id']}">selected</c:if>>${row.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <button class="btn btn-primary btn-sm" type="submit">搜索
                <i class="ace-icon glyphicon glyphicon-search"></i>
            </button>

        </div>
    </form>
    <div class="row row_height_y">
        <shiro:hasPermission name="user_add">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-primary"
                        onClick="win.init({title:'添加用户',rel:''}).show('${ctx}/edit_user_pop.do',{})">新增
                </button>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="user_modify">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-info"
                        onClick="win.init({title:'修改用户'}).show('${ctx}/edit_user_pop.do?id={id}',{})">修改
                </button>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="user_delete">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-danger"
                        onClick="mywin.init({title:'确定要删除吗？'}).confirm('${ctx}/delete_user.do?id={id}','确定要删除吗?')">删除
                </button>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="user_audit">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-success"
                        onclick="win.init({title:'审核'}).show('${ctx}/check_user_pop.do?id={id}')">审核
                </button>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="user_resetPassword">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-info"
                        onclick="win.init({title:'重置密码'}).show('${ctx}/reset_password_pop.do?id={id}')">重置密码
                </button>
            </div>
        </shiro:hasPermission>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTable">
            <thead>
            <tr>
                <th class="center">
                    <label>
                        <input type="checkbox" class="ace" onclick="checkAll(this);">
                        <span class="lbl"></span>
                    </label>
                </th>
                <th>用户名</th>
                <th>租户</th>
                <th>角色</th>
                <th>电话</th>
                <th>email</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${userListRole}" var="item">
            <tr>
                <td class="center">
                    <label>
                        <input type="checkbox" class="ace" name="id" value="${item.id}"/>
                        <span class="lbl"></span>
                    </label>
                </td>
                <td>${item.userName}</td>
                <td>${item.systemCompany.companyName}</td>
                <td>${item.role}</td>
                <td>${item.phoneNumber}</td>
                <td>${item.email}</td>
                </td>
            </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="row row_height_x message-footer">
            <tags:page page="${page}"/>
        </div>
    </div><!-- /.table-responsive -->
</div>
<script src="${ctxRoot}/static/js_project/select.jquery.min.js"></script>
</body>
</html>
