<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <title>菜单管理</title>
</head>
<style type="text/css">
    .ztree li a.level0 {
        width: 200px;
        height: 20px;
        text-align: center;
        display: block;
        background-color: #0B61A4;
        border: 1px silver solid;
    }

    .ztree li a.level0.cur {
        background-color: #66A3D2;
    }

    .ztree li a.level0 span {
        display: block;
        color: white;
        padding-top: 3px;
        font-size: 12px;
        font-weight: bold;
        word-spacing: 2px;
    }

    .ztree li a.level0 span.button {
        float: right;
        margin-left: 10px;
        visibility: visible;
        display: none;
    }

    .ztree li span.button.switch.level0 {
        display: none;
    }

    /* 两列右侧自适应布局 */
    .g-bd1 {
        margin: 0 0 10px;
    }

    .g-sd1 {
        position: relative;
        float: left;
        width: 220px;
        margin-right: -220px;
    }

    .g-mn1 {
        float: right;
        width: 100%;
    }

    .g-mn1c {
        margin-left: 230px;
    }

    .zTreeDemoBackground {
        background-color: #f2f2f2;
        height: 650px;
        overflow: auto;
        width: 220px;
    }

</style>
<body>
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="${ctxRoot}">首页</a>
        </li>
        <li>
            系统管理
        </li>
        <li class="active">
            菜单管理
        </li>
    </ul>
</div>
<div class="page-content">
    <div class="row row_height_y">
        <div class="ui-pg-div icon_left">
            <button class="btn btn-minier btn-primary"
                    <%--onClick="win.init({title:'添加客户',rel:''}).show('${ctx}/edit_customer_pop.do',{})">新增--%>
                    onClick="addMenu()">新增
            </button>
        </div>
        <div class="ui-pg-div icon_left">
            <button class="btn btn-minier btn-info"
                    <%--onClick="win.init({title:'修改客户'}).show('${ctx}/edit_customer_pop.do?id={id}',{})">修改--%>
                    onClick="editMenu()">修改
            </button>
        </div>
        <div class="ui-pg-div icon_left">
            <button class="btn btn-minier btn-danger"
                    <%--onClick="win.init({title:'确定要删除吗？'}).confirm('${ctx}/delete_customer.do?id={id}')">删除--%>
                    onClick="deleteMenu()">删除
            </button>
        </div>
    </div>
    <div class="table-responsive">
        <div id="menuContent" class="menuContent" >
            <ul id="treeDemo" class="ztree" style="margin-top:0; width:180px; height: 100%;"></ul>
        </div>
    </div>
</div>
<div class="f-dn" style="display:none;" id="menuTreeData">${menuTreesData}</div>

<script type="text/javascript" src="${ctxRoot}/static/js_project/system/menu/menu.js"></script>

</body>
</html>
