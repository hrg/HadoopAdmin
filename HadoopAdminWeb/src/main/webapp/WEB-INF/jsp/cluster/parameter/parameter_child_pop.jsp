<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<script type="text/javascript" src="${ctxRoot}/static/js/xbb.js"></script>
<div class="unit">
    <div class="widget-header header-color-blue2">
        <h5 class="lighter smaller">子参数管理</h5>
    </div>
    <div class="row row_height_y">
        <c:if test="${not empty parentId}">
            <div class="pull-left">
                <shiro:hasPermission name="parameter_children_add">
                    <div class="ui-pg-div icon_left">
                        <button class="btn btn-minier btn-primary"
                                onClick="win.init({title:'添加参数',rel:''}).show('${ctx}/add_parameterNew_pop.do?rel=childrenFormId&parentId=${parentId}')">
                            新增参数
                        </button>
                    </div>
                </shiro:hasPermission>
            </div>
        </c:if>
        <div class="pull-right1">
            <form id="childrenFormId" rel="childrenId" action="${ctx}/parameter_child_pop.do?parentId=${parentId}"
                  onsubmit="return search(this)">
                <%--<select  class="chosen-select form-control" name="Q_EQ_clusterType.clusterTypeId" style="width:200px" data-placeholder="集群类型">
                    <option ></option>
                    <c:forEach items="${clusterTypes}" var="clusterType">
                        <option value="${clusterType.clusterTypeId}" <c:if test="${clusterType.clusterTypeId==param['Q_EQ_clusterType.clusterTypeId']}">selected</c:if>>${clusterType.clusterTypeName}</option>
                    </c:forEach>
                </select>--%>
                <input hidden="hidden" name="Q_EQ_id" value="${param['Q_EQ_id']}"/>
                <input type="text" placeholder="key" name="Q_LIKE_parameterKey" value="${param['Q_LIKE_parameterKey']}"/>
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-minier" type="submit">
                            <i class="ace-icon glyphicon glyphicon-search"></i>
                        </button>
                    </span>
            </form>
        </div>
    </div>
    <div class="table-responsive td_o table-responsive1" id="parentId">
        <table class="table table-striped table-bordered table-hover dataTable">
            <thead>
            <tr>
                <th class="center" width="10%">key</th>
                <th class="center" width="15%">val</th>
                <th class="center" width="11%">集群类型</th>
                <th class="center" width="20%">描述</th>
                <th class="center" width="5%">操作</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${parameterNewPageChild.list}" var="item">
                <tr >
                    <td class="center">${item.parameterKey}</td>
                    <td class="center">${item.parameterVal}</td>
                    <td class="center">${item.clusterType.clusterTypeName}</td>
                    <td class="center">${item.note}</td>
                    <td class="center">
                        <div class="action-buttons">
                            <a title="编辑"
                               onClick="win.init({title:'修改参数',rel:''}).show('${ctx}/add_parameterNew_pop.do?rel=childrenFormId&id=${item.id}',{})">
                                <i class="ace-icon fa fa-pencil bigger-130"></i>
                            </a>
                            <a title="删除" rel="childrenFormId" url="${ctx}/delete_parameterNew.do?id=${item.id}"
                                <%--onClick="win.init({title:'确定要删除吗？', rel:'childrenId'}).confirm('${ctx}/delete_parameterNew.do?id=${item.id}')"--%>
                               onclick="delConfirm(this)">
                                <i class="ace-icon fa fa-trash-o bigger-130"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <!-- /.table-responsive -->
    <div class="row row_height_x message-footer">
        <tags:page page="${parameterNewPageChild}"/>
    </div>
</div>
<%--<script>--%>
    <%--function delConfirm(obj) {--%>
        <%--if (confirm("确定要删除吗？")) {--%>
            <%--$.ajax({--%>
                <%--type: 'GET',--%>
                <%--url: $(obj).attr("url").toString(),--%>
                <%--dataType: "json",--%>
                <%--cache: false,--%>
                <%--success: function () {--%>
                    <%--var rel = $(obj).attr("rel");--%>
                    <%--if (rel) {--%>
                        <%--$("#"+rel).form().action = "parameter_child_pop.do?parentId=${parentId}&&${parameterNewPageChild.pageNum}";--%>
                        <%--$("#" + rel).submit();--%>
                    <%--}--%>
                <%--},--%>
                <%--error: function () {--%>
                    <%--alert("error")--%>
                <%--}--%>
            <%--});--%>
        <%--}--%>
    <%--}--%>
<%--</script>--%>