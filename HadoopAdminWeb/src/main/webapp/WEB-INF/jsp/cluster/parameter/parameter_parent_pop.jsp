<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<style>
    .actives {
        color: #0000C0;
    }


</style>
<script>
    <%--function flush() {--%>
        <%--$.ajax({--%>
            <%--url: "${ctx}/flush.do",--%>
            <%--success: function () {--%>
                <%--alert("刷新成功")--%>
            <%--},--%>
            <%--error: function () {--%>
                <%--alert("刷新失败")--%>
            <%--}--%>
        <%--})--%>
    <%--}--%>
</script>
<div class="unit">
    <div class="widget-header header-color-blue2">
        <h5 class="lighter smaller">父参数管理</h5>
    </div>
    <div class="row row_height_y">
        <div class="pull-left">
            <shiro:hasPermission name="parameter_parent_add">
                <div class="ui-pg-div icon_left">
                    <button class="btn btn-minier btn-primary"
                            onClick="win.init({title:'添加参数',rel:''}).show('${ctx}/add_parameterNew_pop.do?parentId=0&rel=parentFormId')">
                        新增参数
                    </button>
                    <%--<button class="btn btn-minier btn-primary"--%>
                            <%--onClick="flush()">--%>
                        <%--刷新参数--%>
                    <%--</button>--%>
                </div>
            </shiro:hasPermission>
        </div>
        <div class="pull-right1">
            <form id="parentFormId" rel="parentId" action="${ctx}/parameter_parent_pop.do?rel=parentFormId"
                  onsubmit="return search(this)">
                <%--<select  class="chosen-select form-control" name="Q_EQ_clusterType.clusterTypeId" style="width:200px" data-placeholder="集群类型">
                    <option ></option>
                    <c:forEach items="${clusterTypes}" var="clusterType">
                        <option value="${clusterType.clusterTypeId}" <c:if test="${clusterType.clusterTypeId==param['Q_EQ_clusterType.clusterTypeId']}">selected</c:if>>${clusterType.clusterTypeName}</option>
                    </c:forEach>
                </select>--%>
                <input hidden="hidden" name="Q_EQ_id" value="${param['Q_EQ_id']}"/>
                <input type="text" placeholder="key" name="Q_LIKE_parameterKey" value="${param['Q_LIKE_parameterKey']}"/>
                <span class="input-group-btn">
                    <button class="btn btn-info btn-minier" type="submit">
                        <i class="ace-icon glyphicon glyphicon-search"></i>
                    </button>
                </span>
            </form>
        </div>
    </div>
    <div class="table-responsive td_o table-responsive1" id="parentId">
        <table class="table table-striped table-bordered table-hover dataTable">
            <thead>
            <tr>
                <th class="center" width="10%">key</th>
                <th class="center" width="15%">val</th>
                <th class="center" width="11%">集群类型</th>
                <th class="center" width="20%">描述</th>
                <th class="center" width="10%">操作</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${parameterNewPage.list}" var="item">
                <%--<tr class="myClick" onclick="loadUrl('childrenId', '${ctx}/parameter_child_pop.do?parentId=${item.id}')">--%>
                <tr class="myClick <c:if test="${parentId eq item.id}">actives</c:if>">
                    <td class="center" style="cursor:pointer"
                        onclick="loadUrls('childrenId', '${ctx}/parameter_child_pop.do?parentId=${item.id}', {}, this)">${item.parameterKey}</td>
                    <td class="center" style="cursor:pointer"
                        onclick="loadUrls('childrenId', '${ctx}/parameter_child_pop.do?parentId=${item.id}', {}, this)">${item.parameterVal}</td>
                    <td class="center" style="cursor:pointer"
                        onclick="loadUrls('childrenId', '${ctx}/parameter_child_pop.do?parentId=${item.id}', {}, this)">${item.clusterType.clusterTypeName}</td>
                    <td class="center" style="cursor:pointer"
                        onclick="loadUrls('childrenId', '${ctx}/parameter_child_pop.do?parentId=${item.id}', {}, this)">${item.note}</td>
                    <td class="center">
                        <div class="action-buttons">
                            <shiro:hasPermission name="parameter_parent_modify">
                                <a title="编辑"
                                   onClick="win.init({title:'修改参数',rel:''}).show('${ctx}/add_parameterNew_pop.do?rel=parentFormId&id=${item.id}',{})">
                                    <i class="ace-icon fa fa-pencil bigger-130"></i>
                                </a>
                            </shiro:hasPermission>
                            <shiro:hasPermission name="parameter_parent_delete">
                                <a title="删除" href="${ctx}/delete_parent.do?id=${item.id}&&pageNum=${parameterNewPage.pageNum}"
                                   onclick="if(confirm('确定要删除吗?')==false)return false;">
                                        <%--onClick="win.init({title:'确定要删除吗？',rel:'parentId'}).confirm('${ctx}/delete_parameterNew.do?id=${item.id}')">--%>
                                    <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                </a>
                            </shiro:hasPermission>
                        </div>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <!-- /.table-responsive -->
    <div class="row row_height_x message-footer">
        <tags:page page="${parameterNewPage}"/>
    </div>
</div>