<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<style>
    input {
        width: 300px
    }

    select {
        width: 300px
    }

    textarea {
        width: 300px
    }
</style>
<script type="text/javascript">
    var flag;
    function checkFormat() {
        var key = $("input[name='parameterKey']").val();
        var val = $("input[name='parameterVal']").val();
//        alert(val);
        //判断key是否为空
        if (key == "") {
            layer.tips("key不能为空","input[name='parameterKey']",{tips: [2, '#259de0']});
            return false;
        }
        //判断val是否为空
        if (val == "") {
            layer.tips("val不能为空","input[name='parameterVal']",{tips: [2, '#259de0']});
            return false;
        }
        checkKey();
        if (flag) {
            $("form").submit();
            win.close();
        } else {
            return false;
        }
    }
    function checkKey() {
        var key = $("input[name='parameterKey']").val();
        $.ajax({
            url: "${ctx}/check_key.do?id=${id}",
            data: "parameterKey=" + key,
            dataType: "text",
            async: false,
            type: "post",
            success: function (data) {
                if (data == "success") {
                    $("#error").show()
                    flag = false;
                }
                if (data == "error") {
                    $("#error").hide()
                    flag = true;
                }
            },
            error: function () {
                alert("error")
            }
        })
    }
</script>


<form rel="${rel}" action="${ctx}/save_parameterNew.do" method="post" onsubmit="return validate(this)">
    <div class="clearfix">
        <table class="table table-striped table-bordered table-hover dataTable" style="font-size: 13px">
            <tr>
                <td class="center">key</td>
                <td class="center">
                    <input name="parameterKey" title="key不能为空" value="${parameterNew.parameterKey}">
                </td>
            </tr>
            <tr>
                <td class="center">val</td>
                <td class="center">
                    <input id="val" name="parameterVal" title="val不能为空" value="${parameterNew.parameterVal}"/>
                </td>
            </tr>
            <tr>
                <td class="center">集群类型</td>
                <td class="center">
                    <select name="clusterType.clusterTypeId"   onpropertychange="checkNameIp()">
                        <option  value="0">选择集群类型</option>
                        <c:forEach items="${clusterTypes}" var="clusterType">
                            <option value="${clusterType.clusterTypeId}" <c:if test="${clusterType.clusterTypeId eq parameterNew.clusterTypeId}">selected</c:if>>${clusterType.clusterTypeName}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="center">描述</td>
                <td class="center">
                    <input name="note" value="${parameterNew.note}"/>
                </td>
            </tr>
        </table>
        <label id="error" style="display: none ; color: #ff0000">key已存在，请换一个key</label>
    </div>
    <c:if test="${empty parameterNew }">
        <input type="hidden" name="parentId" value="${parentId}"/>
    </c:if>
    <c:if test="${not empty parameterNew }">
        <input type="hidden" name="parentId" value="${parameterNew.parentId}"/>
        <input type="hidden" name="id" value="${parameterNew.id}">
    </c:if>
    <div class="ui_t_foot">
        <button class="btn" type="button" onclick="win.close();">取消</button>
        <button class="btn btn-primary" type="button" onclick="checkFormat()">确定</button>
    </div>
</form>