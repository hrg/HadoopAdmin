<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="/tags" prefix="date" %>
<html>
<head>
    <title></title>
    <style type="text/css">
        select {
            width: 170px;
        }

        #div {
            padding: 0 11px;
        }
        table{
            table-layout: fixed;
        }
        th, td {
            font-weight: normal;
        }
    </style>
    <script type="text/javascript">
        function getResult() {
            var clusterTypeId = $("#cluster").val();
            //清空hadoopUser和hdfs数据
            $("#sourceHadoopUser").empty();
            $("#sourceHadoopUser").append("<option><option>");
            $("#sourceHadoopUser").trigger("chosen:updated");
            $("#targetCluster").empty();
            $("#targetCluster").append("<option><option>");
            $("#targetCluster").trigger("chosen:updated");
            $("#sourceHdfs").empty();
            $("#sourceHdfs").append("<option><option>");
            $("#sourceHdfs").trigger("chosen:updated");
            $.ajax({
                url: "${ctx}/findClusterUser.do",
                type: "post",
                data: {"clusterTypeId": clusterTypeId},
                dataType: "json",
                success: function (result) {
                    var clusterUsers = result.clusterUsers;
                    var clusterTypes = result.clusterTypes;
                    for (var i = 0; i < clusterUsers.length; i++) {
                        var hpId = clusterUsers[i].id;
                        var hpUserName = clusterUsers[i].userName;
                        var sourceHadoopUserVal = $("#sourceHadoopUser").val()
                        if (sourceHadoopUserVal == hpId) {
                            $("#sourceHadoopUser").append("<option value= '" + hpId + "' selected>" + hpUserName + "</option>");
                            $("#sourceHadoopUser").trigger("chosen:updated");
                        } else {
                            $("#sourceHadoopUser").append("<option value= '" + hpId + "'>" + hpUserName + "</option>");
                            $("#sourceHadoopUser").trigger("chosen:updated");
                        }
                    }
                    for (var i = 0; i < clusterTypes.length; i++) {
                        var targetClusterTypeId = clusterTypes[i].clusterTypeId;
                        var targetClusterName = clusterTypes[i].clusterTypeName;
                        var targetClusterVal = $("#targetCluster").val();
                        if (targetClusterTypeId == targetClusterVal) {
                            $("#targetCluster").append("<option value= '" + targetClusterTypeId + "' selected>" + targetClusterName + "</option>");
                            $("#targetCluster").trigger("chosen:updated");
                        } else {
                            $("#targetCluster").append("<option value= '" + targetClusterTypeId + "'>" + targetClusterName + "</option>");
                            $("#targetCluster").trigger("chosen:updated");
                        }
                    }
                }
            });
        }
        function getHdfs() {
            var clusterUserId = $("#sourceHadoopUser").val();
            $("#sourceHdfs").empty();
            $("#sourceHdfs").append("<option></option>");
            $("#sourceHdfs").trigger("chosen:updated");
            $.ajax({
                url: "${ctx}/findHdfsPath.do",
                type: "post",
                data: {"clusterUserId": clusterUserId},
                dataType: "json",
                success: function (result) {
//          var data = result.data;
                    var hdfsInfos = result;
                    for (var i = 0; i < hdfsInfos.length; i++) {
                        var sourceHdfs = $("#sourceHdfs").val();
                        var hdfsId = hdfsInfos[i].id;
                        var hpFolderName = hdfsInfos[i].hdfsPath;
                        if (sourceHdfs == hdfsId) {
                            $("#sourceHdfs").append("<option value= '" + hdfsId + "' selected>" + hpFolderName + "</option>");
                            $("#sourceHdfs").trigger("chosen:updated");
                        } else {
                            $("#sourceHdfs").append("<option value= '" + hdfsId + "'>" + hpFolderName + "</option>");
                            $("#sourceHdfs").trigger("chosen:updated");
                        }
                    }
                }
            });
        }
        function getFolderName() {
            $("#targetHdfs").val("");
            var folderName = $("#sourceHdfs").find("option:selected").text();
            var sourceHdfs = $("#sourceHdfs").val();
            if ("" == sourceHdfs) {
                $("#targetHdfs").val("");
            } else {
                $("#targetHdfs").val(folderName);
                $("#targetHdfs").attr("title", folderName);
            }
        }
        function transferDataSource() {
            //进行验证
            var sourceClusterType = $("#cluster").val();
            var targetClusterType = $("#targetCluster").val();
            var hadoopuser = $("#sourceHadoopUser").val();
//            var runCycle = $("#runCycle").val();
            if (sourceClusterType == "") {
                alert("请选择源集群");
                return false;
            }
            if (hadoopuser == "") {
                alert("请选择源用户");
                return false;
            }
            if (targetClusterType == "") {
                alert("请选择目标集群");
                return false;
            }
            $("#form").submit();
            $("button").attr("disabled",true);
            return false;
        }
    </script>
</head>
<body>
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="${ctxRoot}">首页</a>
        </li>
        <li>
            权限管理
        </li>
        <li class="active">
            HDFS集群数据迁移
        </li>
    </ul>
</div>
<div class="page-content">
            <form id="form" action="${ctx}/transferDataSource.do" method="post">
                <div id="div" class="row">
                    <div class="col-md-7">
                        <div class="row">
                            <div class="widget-box">
                                <div class="widget-header">
                                    <h4 class="widget-title">源集群数据路径</h4>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <div class="row">
                                            <div class="center col-md-6">
                                                <div class="form-group">
                                                    <select id="cluster" name="sourceClusterTypeId" class="chosen-select form-control" data-placeholder="源数据集群" onchange="getResult()">
                                                        <option></option>
                                                        <c:forEach var="item" items="${clusterTypes}">
                                                            <option value="${item.clusterTypeId}"
                                                                    <c:if test="${item.clusterTypeId==param['sourceClusterType.clusterTypeId']}"> selected </c:if> >${item.clusterTypeName}</option>
                                                        </c:forEach>
                                                    </select></div>
                                                <div class="form-group">
                                                    <select id="sourceHadoopUser" name="clusterUserId" class="chosen-select form-control" data-placeholder="源数据用户"
                                                            onchange="getHdfs()">
                                                        <option></option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select id="sourceHdfs" name="hdfsInfoId" class="chosen-select form-control" data-placeholder="源hdfs地址"
                                                            onchange="getFolderName()">
                                                        <option></option>
                                                    </select>
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input style="width:46%;height: 30px" placeholder="数据开始时间"
                                                           type="text" class="Wdate" name="start"
                                                           onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})">
                                                    - <input style="width:46%;height: 30px" placeholder="数据结束时间"
                                                            type="text"
                                                            class="Wdate" name="end"
                                                            onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})">
                                                </div>
                                                <div class="form-group">运行时间(月):<input type="text" style="width: 73%;height: 30px;"
                                                                                    class="Wdate" name="run"
                                                                                    onfocus="WdatePicker({dateFmt:'yyyy-MM'})">
                                                </div>
                                                <div class="form-group">运行周期(月):<input id="runCycle" style="width: 73%;height: 30px;" type="text"
                                                                                    name="runCycle">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5" style="padding-left: 30px;">
                        <div class="row">
                            <div class="widget-box">
                                <div class="widget-header">
                                    <h4 class="widget-title">目标集群数据路径</h4>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <div class="row">
                                            <div class="center">
                                                <div class="form-group">
                                                    <select  style="width: 70%"  id="targetCluster" name="targetClusterTypeId"  class="chosen-select form-control" data-placeholder="目标数据集群">
                                                        <option></option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <input id="targetHdfs" name="hdfsPath"
                                                           style="width: 70%;height: 30px;"
                                                           placeholder="目标hdfs路径" title="" readonly><br>
                                                </div>
                                                <div class="form-group">
                                                    <button class="btn btn-primary" onclick="return transferDataSource()" style="height: 30px;" >
                                                        集群数据迁移
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable" >
                    <thead>
                    <tr>
                        <th width="10%" >源集群</th>
                        <th width="10%" >目标集群</th>
                        <th width="15%" >用户名称</th>
                        <th width="20%" >HDFS路径</th>
                        <th width="12%" >数据开始时间</th>
                        <th width="12%" >数据结束时间</th>
                        <th width="12%" >数据运行时间</th>
                        <th width="8%" >运行周期</th>
                        <th width="8%" >状态</th>
                        <th width="10%" >操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="item" items="${page.list}">
                        <tr>
                            <th >${item.sourceClusterType.clusterTypeName}</th>
                            <th >${item.targetClusterType.clusterTypeName}</th>
                            <th
                                style="text-overflow:ellipsis; overflow: hidden;white-space: nowrap">${item.clusterUser.userName}</th>
                            <th  title="${item.hdfsPath}"
                                style="text-overflow:ellipsis; overflow: hidden;white-space: nowrap">${item.hdfsPath}</th>
                            <th  title="<date:date value="${item.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
                                style="text-overflow:ellipsis; overflow: hidden;white-space: nowrap"><date:date value="${item.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/></th>
                            <th  title="<date:date value="${item.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
                                style="text-overflow:ellipsis; overflow: hidden;white-space: nowrap"><date:date value="${item.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/></th>
                            <th  title="<date:date value="${item.runTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
                                style="text-overflow:ellipsis; overflow: hidden;white-space: nowrap"><date:date value="${item.runTime}" pattern="yyyy-MM-dd HH:mm:ss"/></th>
                            <th >${item.runCycle}</th>
                            <th >
                                <c:if test="${item.status==0}">开启</c:if>
                                <c:if test="${item.status==1}">暂停</c:if>
                            </th>
                            <th  >
                                <div class="action-buttons">
                                    <shiro:hasPermission name="tranferOpen">
                                        <a title="开启" href="${ctx}/openStatus.do?id=${item.id}">
                                            <i class="fa fa-play"></i>
                                        </a>
                                    </shiro:hasPermission>
                                    <shiro:hasPermission name="tranferPause">
                                        <a title="暂停" href="${ctx}/stopStatus.do?id=${item.id}">
                                            <i class="ace-icon glyphicon glyphicon-off"></i>
                                        </a>
                                    </shiro:hasPermission>
                                    <shiro:hasPermission name="tranferDelete">
                                        <a title="删除" href="${ctx}/deleteTransferData.do?id=${item.id}">
                                            <i class="ace-icon glyphicon glyphicon-trash"></i>
                                        </a>
                                    </shiro:hasPermission>

                                </div>
                            </th>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="row row_height_x message-footer">
                <tags:page page="${page}"/>
            </div>
</div>
</body>
</html>
