<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <title>用户管理</title>
</head>

<body>
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="${ctxRoot}">首页</a>
        </li>
        <li>
            集群管理
        </li>
        <li class="active">
            用户管理
        </li>
    </ul>
</div>
<div class="page-content">
    <form action="${ctx}/query_clusterUser_list.do" method="post" onsubmit="return search(this);">
        <div class="row_height_s">
            <div class="form-group_li">
                <input type="text" placeholder="用户名" name="Q_LIKE_userName" class="col_d"
                       value="${param.Q_LIKE_userName}"/>
            </div>
            <div class="form-group_li">
                <div class="col_right_li">
                    <select class="chosen-select form-control" name="Q_EQ_systemCompany.id" style="width:200px"
                            data-placeholder="租户名称">
                        <option></option>
                        <c:forEach items="${systemCompanies}" var="row">
                            <option value="${row.id}"
                                    <c:if test="${row.id==param['Q_EQ_systemCompany.id']}">selected</c:if>>${row.companyName}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group_li">
                <select class="chosen-select form-control" name="Q_EQ_userType.userTypeId" style="width:200px"
                        data-placeholder="用户类型">
                    <option></option>
                    <c:forEach items="${userTypes}" var="row">
                        <option value="${row.userTypeId}"
                                <c:if test="${row.userTypeId==param['Q_EQ_userType.userTypeId']}">selected</c:if>>${row.userTypeName}</option>
                    </c:forEach>
                </select>
            </div>
            <button class="btn btn-primary btn-sm" type="submit">搜索
                <i class="ace-icon glyphicon glyphicon-search"></i>
            </button>
        </div>
    </form>
    <div class="row row_height_y">
        <shiro:hasPermission name="addClusterUser">
            <div class="ui-pg-div icon_left">
                <a href="#" onclick="addClusterUser('add')" data-toggle="modal"
                   class="btn btn-minier btn-primary">新增</a>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="editClusterUser">
            <div class="ui-pg-div icon_left">
                <a href="#" onclick="updateClusterUser('update')" dsynchronyClusterMachineata-toggle="modal"
                   class="btn btn-minier btn-primary">修改</a>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="seeClusterUser">
            <div class="ui-pg-div icon_left">
                <a href="#" onclick="updateClusterUser('see')" data-toggle="modal"
                   class="btn btn-minier btn-primary">查看</a>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="deleteClusterUser">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-danger"
                        onClick="mywin.init({title:'确定要删除吗？'}).confirm('${ctx}/delete_clusterUser.do?id={id}','确定要删除吗？')">
                    删除
                </button>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="resetClusterUserPassword">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-info"
                        onclick="win.init({title:'重置密码'}).show('${ctx}/reset_password_pop.do?id={id}')">重置密码
                </button>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="openClusterUser">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-info"
                        onclick="mywin.init({title:'确定要开始吗？'}).confirm('${ctx}/open_user.do?id={id}','你确定要开启吗？')"> 用户启用
                </button>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="closeClusterUser">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-info"
                        onclick="mywin.init({title:'确定要暂停吗？'}).confirm('${ctx}/pause_user.do?id={id}','你确定要暂停吗？')">用户暂停
                </button>
            </div>
        </shiro:hasPermission>
        <%--暂时注释!!!!!!!!!!-%>
        <%--<shiro:hasPermission name="batchProcess">--%>
        <%--<div class="col-md-offset-4" style="width: 20%">--%>
        <%--<input type="file" id="id-input-file-2"/>--%>
        <%--</div>--%>
        <%--</shiro:hasPermission>--%>
        <shiro:hasPermission name="batchUpdateClusterUser">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-info"
                        onclick="batchUpdateClusterUser()">批量修改
                </button>
            </div>
        </shiro:hasPermission>

    </div>
    <div class="table-responsive">
        <table id="cluster_user_list" class="table table-striped table-bordered table-hover dataTable" id="parentId">
            <thead>
            <tr>
                <th class="center">
                    <label>
                        <input type="checkbox" class="ace" onclick="checkAll(this);">
                        <span class="lbl"></span>
                    </label>
                </th>
                <th>用户名</th>
                <th>租户名称</th>
                <th>联系人</th>
                <th>联系方式</th>
                <th>邮箱地址</th>
                <th>用户类型</th>
                <th>用户状态</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${page.list}" var="item">
                <tr>
                    <td class="center">
                        <label>
                            <input type="checkbox" class="ace" name="id" value="${item.id}"/>
                            <span class="lbl"></span>
                        </label>
                    </td>
                    <td>${item.userName}</td>
                    <td>${item.systemCompany.companyName}</td>
                    <td>${item.contactPerson}</td>
                    <td>${item.phoneNumber}</td>
                    <td>${item.email}</td>
                    <td>${item.userType.userTypeName}</td>
                    <td>
                        <c:if test="${item.status==0}">正在使用</c:if>
                        <c:if test="${item.status==1}">暂停使用</c:if>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="row row_height_x message-footer">
            <tags:page page="${page}"/>
        </div>
    </div>
    <!-- /.table-responsive -->
</div>

<!------------用户配置-------------->
<div id="modal-wizard" class="modal" data-backdrop="static">

</div>

<script src="${ctxRoot}/static/ace/assets/js/fuelux.wizard.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/jquery.validate.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/additional-methods.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/bootbox.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/jquery.maskedinput.min.js"></script>
<script src="${ctxRoot}/static/ace/assets/js/select2.min.js"></script>
<!-- page specific plugin styles -->
<link rel="stylesheet" href="${ctxRoot}/static/ace/assets/css/select2.min.css"/>
<%--模板引擎--%>
<%--<script src="${ctxRoot}/static/js/templete.js"></script>--%>
<style>
    form {
        font-size: 13px;
    }

    .clearfix {
        font-size: 13px;
    }

    .save {
        color: red;
    }

    .close {
        margin-left: 5px;
        margin-right: 0px;
        color: #111;
        font-size: 15px;
        cursor: pointer;
    }
</style>

<script src="${ctxRoot}/static/js_project/clusterUser/hadoop_user.js"></script>
<script src="${ctxRoot}/static/js_project/select.jquery.min.js"></script>
</body>
</html>
