<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib uri="/tags" prefix="date" %>
<html>
<head>
    <title></title>
    <style type="text/css">
        table,td,th {  border: 1px solid #8DB9DB; padding:5px; border-collapse: collapse; font-size:13px;font-family: Microsoft Yahei; }
    </style>
    <%@include file="/WEB-INF/layouts/meta.jsp" %>

</head>
<body>
<div class="page-content">
    <input type="hidden" value="${userIds}" id="userIds">
    <div class="table-responsive">
        <table id="treeTable1" class="table table-striped table-bordered table-hover dataTable" >
            <tr>
                <td class="center" width="2%">用户名</td>
                <td class="center" width="3%">机器IP</td>
                <td class="center" width="2%">集群</td>
                <td class="center" width="3%">描述</td>
                <td class="center" width="4%">用户目录</td>
                <td class="center" width="3%">用户目录配额</td>
                <td class="center" width="2%">配额单位</td>
                <td class="center" width="3%">有效期</td>
                <td class="center" width="3%">目录配额空间</td>
                <td class="center" width="3%">文件数配额</td>
            </tr>
            <c:forEach items="${page.list}" var="item">
                <tr hasChild="true" id="${item.userId}" userNameFlag="${item.userName}" parentTr="true">
                    <td colspan="7"><span controller="true">${item.userName}</span></td>
                    <td align="center"><input class="inp" name="validDay" id="validDay"
                              style="width:60%" value="${item.validDay}" onkeyup="this.value=this.value.replace(/\D/g,'')"/></td>
                    <td align="center">
                            <input type="hidden" id="hdfsquotaid" value="${item.hdfsId}">
                            <input name="hdfsSpace" id="hdfsSpace" value="${item.hdfsSpace}"
                                   style="width: 60%" title="请填写正确的数字" onkeyup="checkHdfsSpace(this)"
                                   class="inp">
                            <select name="hdfsSpaceUnit" id="hdfsSpaceUnit" value="${item.hdfsSpaceUnit}" >
                                <option value="T" <c:if test="${item.hdfsSpaceUnit=='T'}">selected</c:if>>T</option>
                                <option value="G" <c:if test="${item.hdfsSpaceUnit=='G'}">selected</c:if>>G</option>
                                <option value="M" <c:if test="${item.hdfsSpaceUnit=='M'}">selected</c:if>>M</option>
                            </select>
                       </td>
                    <td align="center"><input name="hdfsFileCount" id="hdfsFileCount" class="inp" value="${item.hdfsFileCount}"
                               style="width:70%" title="请填写正确的正整数" onkeyup="this.value=this.value.replace(/\D/g,'')"
                            /></td>
                </tr>
            </c:forEach>
        </table>
        <div class="row row_height_x message-footer">
            <tags:page page="${page}"></tags:page>
        </div>
    </div>
</div>
<br>
<div style="text-align: center;">
    <a href="#" onclick="saveBatch()" class="btn btn-minier btn-primary">保存</a>
    <a href="#" onclick="parent.thirdWin.close();" class="btn btn-minier">取消</a>
</div>
<script>

</script>
<script src="${ctxRoot}/static/js_project/clusterUser/batch_update_clusterUser_pop.js"></script>
</body>
</html>
