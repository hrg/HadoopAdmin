<%--
  Created by IntelliJ IDEA.
  User: 袁颖
  Date: 2016/3/17
  Time: 10:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>

<body>
<div id="modal-wizard" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div id="modal-wizard-container">
        <div class="modal-header">
          <ul class="steps">
            <li data-step="1" class="active">
              <span class="step">1</span>
              <span class="title">Validation states</span>
            </li>

            <li data-step="2">
              <span class="step">2</span>
              <span class="title">Alerts</span>
            </li>

            <li data-step="3">
              <span class="step">3</span>
              <span class="title">Payment Info</span>
            </li>

            <li data-step="4">
              <span class="step">4</span>
              <span class="title">Other Info</span>
            </li>
          </ul>
        </div>

        <div class="modal-body step-content">
          <div class="step-pane active" data-step="1">
            <div class="center">
              <h4 class="blue">Step 1</h4>
            </div>
          </div>

          <div class="step-pane" data-step="2">
            <div class="center">
              <h4 class="blue">Step 2</h4>
            </div>
          </div>

          <div class="step-pane" data-step="3">
            <div class="center">
              <h4 class="blue">Step 3</h4>
            </div>
          </div>

          <div class="step-pane" data-step="4">
            <div class="center">
              <h4 class="blue">Step 4</h4>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer wizard-actions">
        <button class="btn btn-sm btn-prev">
          <i class="ace-icon fa fa-arrow-left"></i>
          Prev
        </button>

        <button class="btn btn-success btn-sm btn-next" data-last="Finish">
          Next
          <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
        </button>

        <button class="btn btn-danger btn-sm pull-left" data-dismiss="modal">
          <i class="ace-icon fa fa-times"></i>
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>
<!-- PAGE CONTENT ENDS -->
<!-- basic scripts -->
<script type="text/javascript">
  jQuery(function($) {
    $('#modal-wizard-container').ace_wizard();
    $('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');
  })
</script>
</body>
</html>
