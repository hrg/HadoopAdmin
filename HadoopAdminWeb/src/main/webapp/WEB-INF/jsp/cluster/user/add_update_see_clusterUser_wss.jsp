<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib uri="/tags" prefix="date" %>
<meta name="decorators" content="defalut_metra"/>

<script src="${ctxRoot}/static/js_project/clusterUser/edit_clusterUser_pop.js"></script>
<script>
    $(document).ready(function () {
        var status = $("#status_val").val();
        if (status == 1) {
            $('#status').removeAttr("checked");
        }
        if($("#cluster_user_list input:checked").length>1){
            $("#days").val("0");//批量处理的时候,kerbs也应该加上默认值
        }
        //当有效时间为0时,剩余时间和结束时间都为永久
        if("0"== $("#days").val()){
            $("#loseTime").val("永久");
            $("#end").val("永久");
        }

    });
    function tip() {
        var val = $("#status:checked").val();
        if (val == "on") {
            layer.tips('开启认证', '#status', {
                tips: [1, '#0FA6D8'] //还可配置颜色
            });
        } else {
            layer.tips('关闭认证', '#status', {
                tips: [1, '#0FA6D8'] //还可配置颜色
            });
        }
    }
</script>
<div class="modal-dialog">
    <div class="modal-content" style="width: 150%;margin-left: -100px;">
        <div id="modal-wizard-container">
            <div class="modal-header">
                <c:if test="${operate=='add'}">
                    <h3 class="smaller lighter blue no-margin" style="font-size: 17px;">新建用户配置</h3>
                </c:if>
                <c:if test="${operate=='update'}">
                    <h3 class="smaller lighter blue no-margin" style="font-size: 17px;">修改用户配置</h3>
                </c:if>
                <c:if test="${operate=='see'}">
                    <h3 class="smaller lighter blue no-margin" style="font-size: 17px;">查看用户配置</h3>
                </c:if>
            </div>
            <div class="modal-header">
                <ul class="steps">
                    <c:if test="${ids.size()<=1}">
                        <li data-step="1" class="active">
                            <span class="step">1</span>
                            <span class="title">用户配置</span>
                        </li>
                    </c:if>
                    <li data-step="2"
                            <c:if test="${ids.size()>1}"> class="active" </c:if>
                        <c:if test="${operate!='add'}">onclick="step(2);"</c:if> id="step2" style="cursor: pointer;">
                        <span class="step">
                            <%--2--%>
                            <c:choose>
                                <c:when test="${ids.size()>1}">1</c:when>
                                <c:otherwise>2</c:otherwise>
                            </c:choose>
                        </span>
                        <span class="title">kebers配置</span>
                    </li>
                    <li data-step="3" id="step3"
                        <c:if test="${operate!='add'}">onclick="step(3);" </c:if> style="cursor: pointer;">
                        <span class="step">
                            <%--3--%>
                            <c:choose>
                                <c:when test="${ids.size()>1}">2</c:when>
                                <c:otherwise>3</c:otherwise>
                            </c:choose>

                        </span>
                        <span class="title">HDFS配置</span>
                    </li>

                    <li data-step="4" id="step4"
                        <c:if test="${operate!='add'}">onclick="step(4);" </c:if> style="cursor: pointer;">
                        <span class="step">
                            <%--4--%>
                            <c:choose>
                                <c:when test="${ids.size()>1}">3</c:when>
                                <c:otherwise>4</c:otherwise>
                            </c:choose>

                        </span>
                        <span class="title">队列配置</span>
                    </li>
                    <%--<c:if test="${ids.size()<=1}">
                        <li data-step="5" id="step5"
                            <c:if test="${operate!='add'}">onclick="step(5);$('#finally_btn').hide();"</c:if> style="cursor: pointer;">
                            <span class="step">5</span>
                            <span class="title">client配置</span>
                        </li>
                    </c:if>--%>
                </ul>
            </div>
            <div class="modal-body step-content">
                <input id="ids" value="${ids}" hidden="hidden">
                <c:if test="${ids.size()<=1}">
                    <div class="step-pane active" data-step="1" style="height: 330px;">
                        <form id="clusterUser">
                            <input name="id" id="userid" type="hidden" value="${clusterUser.id}">

                            <div class="form_g_li">
                                <label class="col_left_li"> <span>*</span>用户名 </label>

                                <div class="col_right_li">
                                    <div>
                                        <input type="text" class="inp required" maxlength="20" value="${clusterUser.userName}"
                                               name="userName" id="userName"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form_g_li">
                                <label class="col_left_li"> <span>*</span>用户机密码 </label>
                                <input type="hidden" name="hpUserSyspw" id="hpUserSyspw_update"/>
                                <div class="col_right_li">
                                    <input type="password" class="inp required password" value="${clusterUser.clientPW}"
                                           minlength="6"
                                           name="clientPW" id="clientPW"/>
                                </div>
                            </div>
                            <div class="form_g_li">
                                <label class="col_left_li"> <span>*</span>联系人 </label>

                                <div class="col_right_li">
                                    <input type="text" class="inp required" value="${clusterUser.contactPerson}"
                                           name="contactPerson"
                                           id="contactPerson"/>
                                </div>

                            </div>
                            <div class="form_g_li">
                                <label class="col_left_li"> <span>*</span>联系方式 </label>

                                <div class="col_right_li">
                                    <input type="text" class="inp required mobile" value="${clusterUser.phoneNumber}"
                                           name="phoneNumber" id="phoneNumber"/>
                                </div>

                            </div>
                            <div class="form_g_li">
                                <label class="col_left_li"> <span>*</span>联系邮箱 </label>

                                <div class="col_right_li">
                                    <input type="text" class="inp required mail" value="${clusterUser.email}"
                                           name="email"
                                           id="email"/>
                                </div>
                            </div>
                            <div class="form_g_li">
                                <label class="col_left_li"> itsm工单号 </label>

                                <div class="col_right_li">
                                    <input type="text" class="inp" value="${clusterUser.itsmNumber}"
                                           name="itsmNumber"
                                           id="itsmNumber"/>
                                </div>
                            </div>
                            <div class="form_g_li">
                                <label class="col_left_li"> 用户类型 </label>
                                <input type="hidden" value="${clusterUser.userType.userTypeId}" id="userTypeId_set"/>

                                <div class="col_right_li">
                                    <select name="userTypeId" id="userTypeId">
                                        <c:forEach items="${userTypes}" var="item">
                                            <option value="${item.userTypeId}"
                                                    <c:if test="${item.userTypeId==clusterUser.userTypeId}">selected</c:if>>
                                                    ${item.userTypeName}
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form_g_li">
                                <label class="col_left_li"> 集群类型 </label>
                                <input type="hidden" id="clusterTypeId_set" value="${clusterUser.clusterTypeId}">

                                <div class="col_right_li">
                                    <select name="clusterTypeId" id="clusterTypeId">
                                        <c:forEach items="${clusterTypes}" var="item">
                                            <option value="${item.clusterTypeId}"
                                                    <c:if test="${item.clusterTypeId==clusterUser.clusterTypeId}">selected</c:if>>
                                                    ${item.clusterTypeName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form_g_li form_g_li1">
                                <label class="col_left_li"> <span>*</span> 租户类型 </label>
                                <input type="hidden" id="companyId_set" value="${clusterUser.systemCompany.id}"/>

                                <div class="col_right_li">
                                    <select name="companyId" id="companyId">
                                        <c:forEach items="${systemCompanies}" var="item">
                                            <option value='${item.id}'>${item.companyName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form_g_li form_g_li" style="width: 800px">
                                <input type="hidden" id="remark_set" value="${clusterUser.remark}">
                                <label class="col_left_li"> 简介 </label>

                                <div class="col_right_li">
                                    <textarea style="width:500px" name="remark" id="remark" class="inp_l"> </textarea>
                                </div>
                            </div>
                            <c:choose>
                                <c:when test="${operate=='update'}">
                                    <div class="form_g_li form_g_li1" id="clusterUser_operation">
                                        <button class="btn btn-minier btn-primary" type="button"
                                                onclick="editClusterUser()"
                                                style="margin-left: 45%">编辑
                                        </button>
                                        <button class="btn btn-minier btn-primary" id="hp_update" disabled type="button"
                                                onclick="saveClusterUser()">保存
                                        </button>
                                    </div>
                                </c:when>
                            </c:choose>

                        </form>
                    </div>
                </c:if>
                <div class="step-pane <c:if test="${ids.size()>1}">active</c:if>" data-step="2">
                    <form id="kerbers_form">
                        <input type="hidden" id="kerber_id_update" value="${kbrconfig.id}">
                        <input type="hidden" id="client_home" value="${clientHome}">
                        <div class="clearfix">
                            <div class="form_g_li">
                                <label class="col_left_li"> <span>*</span> 有效期 </label>

                                <div class="col_right_li">
                                    <input type="text" class="inp required" name="days" id="days"
                                           <c:if test="${operate!='add'}">value="${kbrconfig.validDay}"</c:if> value="0"
                                           onkeyup="kerbersValidDay_update(this)" onpaste="javascript: return false;"/>
                                </div>
                            </div>
                            <div class="form_g_li">
                                <label class="col_left_li"> 剩余时间 </label>

                                <div class="col_right_li">
                                    <input type="text" class="inp " disabled="disabled" name="loseTime" id="loseTime"
                                           <c:if test="${operate!='add'}">value="${laveDay}"</c:if> value="0"/>
                                </div>
                            </div>
                            <div style="padding-top: 10px;padding-left: 45px;display:none;" >
                                <input type="hidden" id="status_val" value="${kbrconfig.status}">
                                <input id="status" onclick="tip()" class="ace ace-switch ace-switch-5"
                                       name="switch-field-1" type="checkbox"
                                       checked="checked">
                                <span class="lbl col_right_li"></span>
                            </div>
                            <div class="form_g_li">
                                <label class="col_left_li"> 开始时间 </label>

                                <div class="col_right_li">
                                    <input type="text" class="inp " disabled="disabled"
                                           value="<date:date value="${kbrconfig.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
                                           name="start" id="start"/>
                                </div>
                            </div>
                            <div class="form_g_li">
                                <label class="col_left_li"> 结束时间 </label>

                                <div class="col_right_li">
                                    <input type="text" class="inp "
                                           value="<date:date value="${kbrconfig.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
                                           disabled="disabled" name="end" id="end"/>
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover " style="font-size: 1.5ex;" id="kerbers_table">
                                <thead>
                                <tr>
                                    <th width="1%" class="center">
                                        <label>
                                            <input type="checkbox" class="ace" id="checkAll">
                                            <span class="lbl"></span>
                                        </label>
                                    </th>
                                    <th class="center" width="11%">机器IP</th>
                                    <th class="center" width="11%">集群</th>
                                    <th class="center" width="11%">描述</th>
                                    <th class="center" width="11%">用户目录</th>
                                    <th class="center" width="11%">用户目录配额</th>
                                    <th class="center" width="11%">配额单位</th>
                                </tr>
                                </thead>
                                <tbody id="kerbers_update">
                                <c:forEach items="${clusterMachines}" var="clusterMachine">
                                    <input type="hidden" name="kid" value="${clusterMachine.id}"/>
                                    <tr id="kerbers_tr_${clusterMachine.id}">
                                        <input type="hidden" name="clientquotaId" id="clientquotaId" value="${clusterMachine.clientquotaId}"/>
                                        <input type="hidden" name="clientCatalogHidden" id="clientCatalogHidden" value="${clusterMachine.clientCatalog}"/>
                                        <td class="center">
                                            <label>
                                                <input type="checkbox" class="ace" onclick="kerberidCheck(this,'${clusterMachine.id}')"
                                                       name="machineId" id="machineId" value="${clusterMachine.id}"
                                                        <c:forEach items="${machineIds}" var="machineId">
                                                            <c:if test="${clusterMachine.id==machineId}">checked="true"</c:if>
                                                        </c:forEach>/>
                                                <span class="lbl"></span>
                                            </label>
                                        </td>
                                        <td class="center">${clusterMachine.machineIp}</td>
                                        <td class="center">${clusterMachine.clusterTypeName}</td>
                                        <td class="center">${clusterMachine.note}</td>
                                        <td class="center" id="catalog_td" >
                                            <%--<input name="clientCatalog" id="clientCatalog" value="${clusterMachine.clientCatalog}"
                                                   title="用户目录"  style="width: 100%" disabled/>--%>
                                            <select name="clientCatalog" id="clientCatalog" value="${clusterMachine.clientCatalog}" disabled>
                                            </select>
                                        </td>
                                        <td class="center">
                                            <input name="clientCatalogSize" id="clientCatalogSize" value="${clusterMachine.clientCatalogSize}"
                                                   title="请填写正确的正整数" onkeyup="this.value=this.value.replace(/\D/g,'')" style="width: 100%" disabled>
                                        </td>
                                        <td class="center">
                                            <select name="clientCatalogUnit" id="clientCatalogUnit" value="${clusterMachine.clientCatalogUnit}" disabled>
                                                <option value="T" <c:if test="${clusterMachine.clientCatalogUnit=='T'}">selected</c:if>>T</option>
                                                <option value="G" <c:if test="${clusterMachine.clientCatalogUnit=='G'}">selected</c:if>>G</option>
                                                <option value="M" <c:if test="${clusterMachine.clientCatalogUnit=='M'}">selected</c:if>>M</option>
                                            </select>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <div class="col_right_li">
                            <c:choose>
                                <c:when test="${operate=='add'}">
                                    <input type="hidden" class="inp required" name="kerberid" id="kerberid"/>
                                </c:when>
                                <c:otherwise>
                                    <input type="hidden" value="0" class="inp required" name="kerberid" id="kerberid"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <c:if test="${operate=='update'}">
                            <div class="form_g_li form_g_li1" id="kerbers_operation">
                                <button class="btn btn-minier btn-primary" type="button" onclick="editKerbers()"
                                        style="margin-left: 45%">编辑
                                </button>
                                <button class="btn btn-minier btn-primary" id="ker_up" disabled type="button"
                                        onclick="updateKerbers()">
                                    保存
                                </button>
                            </div>
                        </c:if>
                    </form>
                </div>
                <div class="step-pane" data-step="3">
                    <form id="hdfsquota_form">
                        <table style="font-size: 13px; width: 80%;margin-left: auto;margin-right: auto;">
                            <tr>
                                <input type="hidden" name="hdfsquota_id " id="hdfsquota_id" value="${hdfsquota.id}"/>
                                <td class="center" style="width: 20%">目录配额空间</td>
                                <td class="center" style="width: 70%">
                                    <%--<input name="hdfsSpace" id="hdfsSpace" value="${hdfsquota.hdfsSpace}"--%>
                                           <%--placeholder="默认无限制."--%>
                                           <%--title="请填写正确的正整数" onkeyup="this.value=this.value.replace(/\D/g,'')"--%>
                                           <%--style="width: 100%">--%>
                                    <input name="hdfsSpace" id="hdfsSpace" value="${hdfsquota.hdfsSpace}"
                                           placeholder="默认无限制."
                                           title="请填写正确的数字" onblur="checkHdfsSpace(this)"
                                           style="width: 100%">
                                </td>
                                <td class="center" style="width: 10%">
                                    <select name="hdfsSpaceUnit" id="hdfsSpaceUnit" value="${hdfsquota.hdfsSpaceUnit}">
                                        <option value="T" <c:if test="${hdfsquota.hdfsSpaceUnit=='T'}">selected</c:if>>T</option>
                                        <option value="G" <c:if test="${hdfsquota.hdfsSpaceUnit=='G'}">selected</c:if>>G</option>
                                        <option value="M" <c:if test="${hdfsquota.hdfsSpaceUnit=='M'}">selected</c:if>>M</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="center" style="width: 20%">文件数配额</td>
                                <td class="center" style="width: 70%">
                                    <input name="hdfsFileCount" id="hdfsFileCount" value="${hdfsquota.hdfsFileCount}"
                                           placeholder="默认无限制."
                                           title="请填写正确的正整数" onkeyup="this.value=this.value.replace(/\D/g,'')"
                                           style="width: 100%"/>
                                </td>
                            </tr>
                        </table>
                        <c:if test="${operate=='update'}">
                            <div class="form_g_li form_g_li1" id="hdfsquota_opertaion">
                                <button class="btn btn-minier btn-primary" type="button" onclick="editHdfs()"
                                        style="margin-left: 45%">
                                    编辑
                                </button>
                                <button class="btn btn-minier btn-primary" id="up_hdfsquota" disabled type="button"
                                        onclick="updateHdfs()">保存
                                </button>
                            </div>
                        </c:if>
                    </form>
                </div>
                <input type="hidden" id="operate" value="${operate}">
                <c:if test="${ids.size()<=1}">
                    <div class="step-pane" data-step="4">
                        <c:if test="${empty queues}">
                            <div class="clearfix">
                                <button class="btn btn-minier btn-primary" type="button" onclick="addLi()"
                                        style="margin-left: 12px">添加
                                </button>
                                <div class="col-sm-6" style="width: 100%">
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
                                            <li class="active">
                                                <a data-toggle="tab" href="#li_1"><span class="save">*</span><span
                                                        class="title">队列配置MR_1</span></a>
                                                <input type="hidden" value="1">
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="tab-content">
                                            <div id="li_1" class="tab-pane in active">
                                                <div class="table-responsive">
                                                    <form id="queue1">
                                                        <input type="hidden" value="" name="id">
                                                        <table style="font-size: 13px; width: 100%">
                                                            <tr>
                                                                <td class="center" style="width: 20%">队列名称</td>
                                                                <td class="center" style="width: 50%">
                                                                    <input name="queueName" readonly title="队列名称不能为空"
                                                                          value="${param['userName']}" style="width: 100%"/>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="center" style="width: 20%">最小资源量</td>
                                                                <td class="center" style="width: 50%">
                                                                    <input name="minResource" value="102400"
                                                                           title="请填写正确的正整数"
                                                                           onkeyup="this.value=this.value.replace(/\D/g,'')"
                                                                           style="width: 100%"/>
                                                                </td>
                                                                <td class="center" style="width: 10%">
                                                                    <select>
                                                                        <option value="M">M</option>
                                                                    </select>
                                                                </td>
                                                                <td class="center" style="width: 10%">
                                                                    <input name="minCpu" value="25" title="请填写正确的正整数"
                                                                           onkeyup="this.value=this.value.replace(/\D/g,'')"/>
                                                                </td>
                                                                <td class="center" style="width: 10%">
                                                                    <select>
                                                                        <option value="H">核</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="center" style="width: 20%">最大资源量</td>
                                                                <td class="center" style="width: 50%">
                                                                    <input name="maxResource" value="409600"
                                                                           title="请填写正确的正整数"
                                                                           style="width: 100%"
                                                                           onkeyup="this.value=this.value.replace(/\D/g,'')"/>
                                                                </td>
                                                                <td class="center" style="width: 10%">
                                                                    <select>
                                                                        <option value="M">M</option>
                                                                    </select>
                                                                </td>
                                                                <td class="center" style="width: 10%">
                                                                    <input name="maxCpu" value="100" title="请填写正确的正整数"
                                                                           onkeyup="this.value=this.value.replace(/\D/g,'')"/>
                                                                </td>
                                                                <td class="center" style="width: 10%">
                                                                    <select>
                                                                        <option value="H">核</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="center" style="width: 20%">最大app数量</td>
                                                                <td class="center" style="width: 50%">
                                                                    <input name="maxApp" value="50" title="请填写正确的正整数"
                                                                           style="width: 100%"
                                                                           onkeyup="this.value=this.value.replace(/\D/g,'')"/>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="center" style="width: 20%">权重值</td>
                                                                <td class="center" style="width: 50%">
                                                                    <input name="weight" value="1"
                                                                           title="请填写正确的权值格式，最多保留4位小数"
                                                                           style="width: 100%"
                                                                           onkeyup="this.value=this.value.replace(/\D/g,'')"/>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="center" style="width: 20%">权限值</td>
                                                                <td class="center" style="width: 50%">
                                                                    <input name="acl" id="acl" style="width: 100%" value=""/>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                    <%----------------------------------%>
                            </div>
                        </c:if>
                        <c:if test="${not empty queues}">
                            <div class="clearfix">
                                <c:if test="${operate=='update'}">
                                    <button class="btn btn-minier btn-primary" type="button" onclick="addLi()"
                                            style="margin-left: 12px">添加
                                    </button>
                                </c:if>
                                <div class="col-sm-6" style="width: 100%">
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
                                            <c:forEach items="${queues}" var="queue" varStatus="status">
                                                <li
                                                        <c:if test="${status.index eq 0}">class="active"</c:if> >
                                                    <a data-toggle="tab" href="#li_${status.index+1}"><span class="save"
                                                                                                            style="display: none">*</span><span
                                                            class="title">配置MR_${status.index+1}</span>
                                                        <c:if test="${status.index != 0&&operate=='update'}"><span
                                                                class='close'
                                                                onclick='closeLi(this, event)'>&times;</span></c:if>
                                                    </a>

                                                    <input type="hidden" value="${status.index+1}">
                                                </li>
                                            </c:forEach>
                                        </ul>

                                        <div class="tab-content" id="tab-content">
                                            <c:forEach items="${queues}" var="queue" varStatus="status">
                                                <div id="li_${status.index+1}"
                                                     <c:if test="${status.index eq 0}">class="tab-pane in active"</c:if>
                                                     <c:if
                                                             test="${status.index ne 0}">class="tab-pane"</c:if>>
                                                    <div class="table-responsive">
                                                        <form>
                                                            <table style="font-size: 13px; width: 100%">
                                                                <input value="${queue.id}" name="id" type="hidden"/>
                                                                <input type="hidden" name="username"
                                                                       value="${clusterUser.id}"/>

                                                                <tr>
                                                                    <td class="center" style="width: 20%">队列名称</td>
                                                                    <td class="center" style="width: 50%">
                                                                        <input name="queueName"
                                                                               value="${queue.queueName}" readonly
                                                                               title="队列名称不能为空"
                                                                               style="width: 100%"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="center" style="width: 20%">最小资源量</td>
                                                                    <td class="center" style="width: 50%">
                                                                        <input name="minResource"
                                                                               value="${queue.minResource}" readonly
                                                                               title="请填写正确的正整数"
                                                                               style="width: 100%"/>
                                                                    </td>
                                                                    <td class="center" style="width: 10%">
                                                                        <select>
                                                                            <option value="M">M</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="center" style="width: 10%">
                                                                        <input name="minCpu" readonly title="请填写正确的正整数"
                                                                               value="${queue.minCpu}"/>
                                                                    </td>
                                                                    <td class="center" style="width: 10%">
                                                                        <select>
                                                                            <option value="H">核</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="center" style="width: 20%">最大资源量</td>
                                                                    <td class="center" style="width: 50%">
                                                                        <input name="maxResource"
                                                                               value="${queue.maxResource}" readonly
                                                                               title="请填写正确的正整数"
                                                                               style="width: 100%"/>
                                                                    </td>
                                                                    <td class="center" style="width: 10%">
                                                                        <select>
                                                                            <option value="M">M</option>
                                                                        </select>
                                                                    </td>
                                                                    <td class="center" style="width: 10%">
                                                                        <input name="maxCpu" readonly title="请填写正确的正整数"
                                                                               value="${queue.maxCpu}"/>
                                                                    </td>
                                                                    <td class="center" style="width: 10%">
                                                                        <select>
                                                                            <option value="H">核</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="center" style="width: 20%">最大app数量</td>
                                                                    <td class="center" style="width: 50%">
                                                                        <input name="maxApp" value="${queue.maxApp}"
                                                                               readonly title="请填写正确的正整数"
                                                                               style="width: 100%"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="center" style="width: 20%">权重值</td>
                                                                    <td class="center" style="width: 50%">
                                                                        <input name="weight" value="${queue.weight}"
                                                                               readonly
                                                                               title="请填写正确的权值格式，最多保留4位小数"
                                                                               style="width: 100%"/>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td class="center" style="width: 20%">权限值</td>
                                                                    <td class="center" style="width: 50%">
                                                                        <input name="acl" value="${queue.acl}" readonly
                                                                               style="width: 100%">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <c:if test="${operate=='update'}">
                                                                <button class="btn btn-minier btn-primary" disabled
                                                                        type="button"
                                                                        onclick="save(this)"
                                                                        style="margin-left: 45%">保存
                                                                </button>
                                                                <button class="btn btn-minier btn-primary" type="button"
                                                                        onclick="edit(this)">编辑
                                                                </button>
                                                            </c:if>
                                                        </form>
                                                    </div>
                                                </div>
                                            </c:forEach>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </c:if>
                    </div>
                </c:if>

            </div>
        </div>

        <div class="modal-footer wizard-actions">
            <button class="btn btn-sm btn-prev">
                <i class="ace-icon fa fa-arrow-left"></i>
                上一步
            </button>
            <c:if test="${operate=='add'}">
                <button class="btn btn-success btn-sm btn-next" data-last="完成">
                    下一步
                    <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                </button>
            </c:if>
            <c:if test="${operate!='add'}">
                <button class="btn btn-success btn-sm btn-next" data-last="完成" id="finally_btn">
                    下一步
                    <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                </button>
            </c:if>
            <button class="btn btn-danger btn-sm pull-left" onclick="cancelUpdate()" >
                <i class="ace-icon fa fa-times"></i>
                关闭
            </button>
        </div>
    </div>

</div>
