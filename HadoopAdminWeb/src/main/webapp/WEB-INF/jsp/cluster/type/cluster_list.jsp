<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<html>
<head>
    <title>集群管理</title>
    <style>
        th{
            font-weight: normal;

        }
    </style>
</head>
<body>
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="${ctxRoot}">首页</a>
        </li>
        <li>
            集群管理
        </li>
        <li class="active">
            集群管理
        </li>
    </ul>
</div>
<div class="page-content">
    <div class="row row_height_y">
        <shiro:hasPermission name="addCluster">
            <div class="icon_left">
                <button class="btn btn-primary btn-minier"  onClick="win.init({title:'添加集群',rel:''}).show('${ctx}/edit_cluster_pop.do',{})">新增</button>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="editCluster">
            <div class="icon_left">
                <button class="btn btn-primary btn-minier" onClick="win.init({title:'修改集群',rel:''}).show('${ctx}/edit_cluster_pop.do?id={id}',{})">修改</button>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="deleteCluster">
            <div class="icon_left">
                <button class="btn btn-primary btn-minier"  onClick="mywin.init({title:'删除集群'}).confirm('${ctx}/delete_cluster.do?id={id}','确定要删除吗?')">删除</button>
            </div>
        </shiro:hasPermission>

    </div>
    <div class="table-responsive">
        <table class="table table-bordered dataTable">
            <thead>
                <tr>
                    <th class="center" style="width: 2%">
                        <label>
                            <input type="checkbox" class="ace" onclick="checkAll(this);">
                            <span class="lbl"></span>
                        </label>
                    </th>
                    <th>集群类型</th>
                    <th>集群名字</th>
                    <th>集群描述</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="item" items="${page.list}">
                    <tr >
                        <th class="center">
                            <label>
                                <input type="checkbox" class="ace" name="id" value="${item.id}"/>
                                <span class="lbl"></span>
                            </label>
                        </th>
                        <th>${item.clusterTypeId}</th>
                        <th >${item.clusterTypeName}</th>
                        <th >${item.note}</th>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <div class="row row_height_x message-footer">
            <tags:page page="${page}"/>
        </div>
    </div>
</div>
</body>
</html>
