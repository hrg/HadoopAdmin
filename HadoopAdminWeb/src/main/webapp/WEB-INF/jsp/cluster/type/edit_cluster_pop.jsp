<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<form action="${ctx}/save_cluster.do" method="post" onsubmit="return validate(this)">
    <input hidden="hidden" name="id" value="${clusterType.id}">

    <div class="clearfix">
        <%--<div class="form_g_li">--%>
            <%--<label class="col_left_li"><span>*</span>集群类型</label>--%>
            <%--<div class="col_right_li">--%>
                <%--<input class="inp required " name="clusterTypeId" value="${clusterType.clusterTypeId}">--%>
            <%--</div>--%>
        <%--</div>--%>
        <div class="form_g_li">
            <label class="col_left_li"><span>*</span>集群名称</label>
            <div class="col_right_li">
                <input class="inp required" name="clusterTypeName" value="${clusterType.clusterTypeName}">
            </div>
        </div>
        <div class="form_g_li">
            <label class="col_left_li">集群描述</label>
            <div class="col_right_li">
                <input name="note" value="${clusterType.note}">
            </div>
        </div>
    </div>
    <div class="ui_t_foot">
        <button class="btn btn-primary" type="button" onclick="win.close()">取消</button>
        <button class="btn btn-primary" type="submit">确定</button>
    </div>
</form>

<%--<script type="text/javascript">--%>
    <%--function save(obj) {--%>
        <%--//验证clusterTypeId是否重复--%>

        <%--//提交表单--%>
        <%--$("form").submit();--%>
    <%--}--%>
<%--</script>--%>
