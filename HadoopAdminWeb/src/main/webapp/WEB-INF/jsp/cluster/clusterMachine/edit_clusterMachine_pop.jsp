<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<style>
    input{
        width: 300px
    }
    select{
        width: 300px
    }
    textarea{
        width: 300px
    }

</style>
<script type="text/javascript">
    var flag;
    var username = /^[a-zA-Z\d]\w{0,16}[a-zA-Z\d]$/;
    var IP=/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
function check(){
    var ipStr = $("input[name='machineIp']").val();
    var userStr = $("input[name='loginUserName']").val();
    //loginUserName验证
    if($("input[name='loginUserName']").val() == "" || !(username.test(userStr))){
        layer.tips("用户名必须为长度为2~16位的英文字母或数字或下划线","input[name='loginUserName']",{tips: [2, '#259de0']});
        return false;
    }
    //ip地址验证
    if (ipStr == "" || !(IP.test(ipStr))) {
        layer.tips("请输入正确的IP格式","input[name='machineIp']",{tips: [2, '#259de0']});
        return false;
    }
    //机器类型验证
    if($("select[name='machineType.machineTypeId']").val() == "0"){
        layer.tips("请选择正确的机器类别","select[name='machineType.machineTypeId']",{tips: [2, '#259de0']});
        return false;
    }
    //密码验证
    if($("input[name='loginPassWord']").val() == ""){
        layer.tips("密码不能为空","input[name='loginPassWord']",{tips: [2, '#259de0']});
        return false;
    }
    if($("input[name='loginPassWord']").val() != $("input[name='loginPassWord2']").val()){
        layer.tips("两次密码不同","input[name='loginPassWord']",{tips: [2, '#259de0']});
        return false;
    }
    //集群类型验证
    if($("select[name='clusterType.clusterTypeId']").val() == "0"){
        layer.tips("请选择正确的集群类型","select[name='']",{tips: [2, '#259de0']});
        return false;
    }
    //机器状态验证
    if($("select[name='status']").val() == "-1") {
        layer.tips("请选择正确的机器状态","select[name='status']",{tips: [2, '#259de0']});
        return false;
    }
    checkNameIp();
    if(flag){
        $("input[name='machineIp']").val(ipStr);
        $("form").submit();
        win.close()
    }

}
function checkNameIp(){
//        alert($("input[name='loginUserName']").val() +":"+$("input[name='machineIp']").val()+":"+$("select[name='machineType']").val()+":"+$("select[name='clusterType']").val())
        if($("input[name='loginUserName']").val() != "" && $("input[name='machineIp']").val() != "" && username.test($("input[name='loginUserName']").val()) && IP.test($("input[name='machineIp']").val())
                && $("select[name='machineType.machineTypeId']").val() != "0" && $("select[name='clusterType.clusterTypeId']").val() != "0"){
            $.ajax({
                url:"${ctx}/check_name_ip.do",
                data:"loginUserName="+$("input[name='loginUserName']").val()+"&machineIp="+$("input[name='machineIp']").val()+"&machineType.machineTypeId="+$("select[name='machineType.machineTypeId']").val()+
                "&clusterType.clusterTypeId="+$("select[name='clusterType.clusterTypeId']").val()+"&id="+${clusterMachine.id},
                type:"post",
                async:false,
                dataType:"text",
                success:function(data){
                    if("success" == data){
//                        alert("用户名和机器IP对应的机器类型及集群状态已存在，请更换用户名或机器IP或机器类型或集群状态")
                        $("#userIp").show();
                        flag = false;
                    }
                    if("error" == data){
                        flag = true;
                        $("#userIp").hide();
                    }
                },
                error:function(){alert("未知错误")}
            });
        }

    }
    function labelHide(){
        $("#userIp").hide();
    }
</script>
<form action="${ctx}/save_clusterMachine.do?page=${savePage}" method="post">
    <input type="hidden" name="id" value="${clusterMachine.id}">
    <div class="clearfix">

    <table class="table table-striped table-bordered table-hover dataTable" style="font-size: 13px">
        <tr>
            <td class="center">用户名称</td>
            <td class="center">
                <input type="text" name="loginUserName" value="${clusterMachine.loginUserName}"  />
            </td>
        </tr>
        <tr>
            <td class="center">机器IP</td>
            <td class="center">
                <input type="text" name="machineIp" value="${clusterMachine.machineIp}" />
            </td>
        </tr>
        <tr>
            <td class="center">机器类别</td>
            <td class="center">
                <select name="machineType.machineTypeId"  >
                    <option  value="0">选择机器类别</option>
                    <c:forEach items="${machineTypes}" var="machineType">
                        <option value="${machineType.machineTypeId}" <c:if test="${machineType.machineTypeId eq clusterMachine.machineType.machineTypeId}">selected </c:if> >${machineType.machineTypeName}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <td class="center">密码</td>
            <td class="center">
                <input name="loginPassWord" type="password"  value="${clusterMachine.loginPassWord}"/>
            </td>
        </tr>
        <tr>
            <td class="center">再次输入密码</td>
            <td class="center">
                <input name="loginPassWord2" type="password"  value="${clusterMachine.loginPassWord}"/>
            </td>
        </tr>
        <tr>
            <td class="center">集群类型</td>
            <td class="center">
                <select name="clusterType.clusterTypeId"   >
                    <option  value="0">选择集群类型</option>
                    <c:forEach items="${clusterTypes}" var="clusterType">
                        <option value="${clusterType.clusterTypeId}" <c:if test="${clusterType.clusterTypeId eq clusterMachine.clusterType.clusterTypeId}">selected</c:if> >${clusterType.clusterTypeName}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <td class="center">机器状态</td>
            <td class="center">
                <select name="status"  >
                    <option  value="-1">选择机器状态</option>
                    <option value="0" <c:if test="${clusterMachine.status eq 0}">selected </c:if> >关闭</option>
                    <option value="1" <c:if test="${clusterMachine.status eq 1}">selected </c:if> >开启</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="center">描述</td>
            <td class="center">
                <textarea name="note" style=" height: 30px">${clusterMachine.note}</textarea>
            </td>
        </tr>
    </table>
        <label id="userIp" style="color: red; display: none">用户名和机器IP对应的机器类型及集群状态已存在，请更换用户名或机器IP或机器类型或集群状态</label>
    </div>
    <div class="ui_t_foot">
        <button class="btn" type="button" onclick="win.close();">取消</button>
        <button class="btn btn-primary" type="button" onclick="check()">确定</button>
    </div>
</form>