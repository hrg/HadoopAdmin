<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <title>机器管理</title>
    <%--<link rel="stylesheet" href="${ctx}/static/framework/Validator/css/bootstrapValidator.min.css"/>--%>
    <%--<script type="text/javascript" src="${ctx}/static/framework/Validator/js/language/zh_CN.js"></script>--%>
    <%--<script type="text/javascript" src="${ctx}/static/js/jquery.validate.custom.js"></script>--%>
    <script>
        <%--var url = "${ctx}/getAll_mcUser.do";--%>
        var mcUserArr = new Array();
        var mcIpArr = new Array();
        <c:forEach var="machineUser" items="${clusterMachineUsers}">
        mcUserArr.push("${clusterMachineUsers}");
        </c:forEach>
        <c:forEach var="machineIp" items="${clusterMachineIps}">
        mcIpArr.push("${machineIp}");
        </c:forEach>
    </script>
</head>
<body>
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="${ctxRoot}">首页</a>
        </li>
        <li>
            集群管理
        </li>
        <li class="active">
            机器管理
        </li>
    </ul>
</div>
<div class="page-content">
    <%--查询条件--%>
    <form action="${ctx}/clusterMachine_list.do" method="post" onsubmit="return search(this);">
        <div class="row_height_s">
            <div class="form-group_li">
                <input type="text"  placeholder="用户名称" class="col_d" value="${param.Q_LIKE_loginUserName}" name="Q_LIKE_loginUserName"/>
            </div>
            <div class="form-group_li">
                <input type="text"  placeholder="机器IP" class="col_d" value="${param.Q_LIKE_machineIp}" name="Q_LIKE_machineIp" />
            </div>
            <div class="form-group_li">
                <div class="col_right_li">
                    <select  class="chosen-select form-control" name="Q_EQ_clusterType.clusterTypeId" style="width:200px" data-placeholder="集群类型">
                        <option ></option>
                        <c:forEach items="${clusterTypes}" var="clusterType">
                            <option value="${clusterType.clusterTypeId}" <c:if test="${clusterType.clusterTypeId==param['Q_EQ_clusterType.clusterTypeId']}">selected</c:if>>${clusterType.clusterTypeName}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group_li">
                <div class="col_right_li">
                    <select  class="chosen-select form-control" name="Q_EQ_machineType.machineTypeId" style="width:200px" data-placeholder="机器类别">
                        <option ></option>
                        <c:forEach items="${machineTypes}" var="machineType">
                            <option value="${machineType.machineTypeId}" <c:if test="${machineType.machineTypeId == param['Q_EQ_machineType.machineTypeId']}">selected</c:if>>${machineType.machineTypeName}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <button class="btn btn-primary btn-sm" type="submit">搜索
                <i class="ace-icon glyphicon glyphicon-search"></i>
            </button>
        </div>
    </form>
    <div class="row row_height_y">
        <shiro:hasPermission name="addClusterMachine">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-primary"
                        onClick="win.init({title:'添加机器',rel:''}).show('${ctx}/add_clusterMachine_pop.do',{})">添加机器
                </button>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="editeClusterMachine">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-primary"
                        onClick="win.init({title:'修改',rel:''}).show('${ctx}/edit_clusterMachine_pop.do?id={id}',{})">修改机器
                </button>
            </div>
        </shiro:hasPermission>
        <shiro:hasPermission name="deleteClusterMachine">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-primary"
                        onClick="mywin.init({title:'确定要删除吗？'}).confirm('${ctx}/delete_clusterMachine.do?id={id}','确定要删除吗')">删除机器
                </button>
            </div>
        </shiro:hasPermission>
        <%--<shiro:hasPermission name="synchronyClusterMachine">--%>
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-primary"
                        <%--onClick="return synchronyClusterMachine()">用户同步--%>
                        onClick="win.init({title:'用户同步',rel:''}).synchrony('${ctx}/synchrony_clusterMachine_pop.do?id={id}',{})">用户同步
                </button>
            </div>
        <%--</shiro:hasPermission>--%>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTable" >
            <thead>
            <tr>
                <th width="5%" class="center">
                    <lable>
                        <input type="checkbox" class="ace"  onclick="checkAll(this)"/>
                        <span class="lbl"></span>
                    </lable>
                </th>
                <th class="center" width="12%">用户名称</th>
                <th class="center" width="11%">机器IP</th>
                <th class="center" width="11%">机器状态</th>
                <th class="center" width="11%">机器类别</th>
                <th class="center" width="11%">集群类型</th>
                <th class="center" width="11%">描述</th>
                <%--<th class="center" width="11%">操作</th>--%>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${page.list}" var="machine">
                <tr>
                    <td class="center">
                        <label>
                            <input type="checkbox" class="ace" name="id" value="${machine.id}"/>
                            <span class="lbl"></span>
                        </label>
                    </td>
                    <td class="center">${machine.loginUserName}</td>
                    <td class="center">${machine.machineIp}</td>
                    <td class="center">
                            <c:if test="${machine.status eq 0}">关闭</c:if>
                            <c:if test="${machine.status eq 1}">运行</c:if>
                    </td>

                    <td class="center">
                            ${machine.machineType.machineTypeName}
                    </td>

                    <td class="center">
                            ${machine.clusterType.clusterTypeName}
                    </td>

                    <td class="center">${machine.note}</td>
                    <%--<td class="center">--%>
                        <%--<div class="action-buttons">--%>
                            <%--<shiro:hasPermission name="editeClusterMachine">--%>
                                <%--<a style="cursor: pointer;"  onClick="win.init({title:'修改机器',rel:''}).show('${ctx}/edit_clusterMachine_pop.do?id=${machine.id}&page=${page.getPageNum()}',{})">--%>
                                    <%--<i class="ace-icon fa fa-pencil bigger-130"></i>--%>
                                <%--</a>--%>
                            <%--</shiro:hasPermission>--%>
                            <%--<shiro:hasPermission name="deleteClusterMachine">--%>
                                <%--<a style="cursor: pointer;" onClick="mywin.init({title:'确定要删除吗？'}).confirm('${ctx}/delete_clusterMachine.do?id=${machine.id}','确定要删除吗')">--%>
                                    <%--<i class="ace-icon glyphicon glyphicon-trash" ></i>--%>
                                <%--</a>--%>
                            <%--</shiro:hasPermission>--%>
                        <%--</div>--%>
                    <%--</td>--%>
                </tr>
            </c:forEach>
            </tbody>
            </table>
        <div class="row row_height_x message-footer">
            <tags:page page="${page}"/>
        </div>
</div>
</div>
<script src="${ctxRoot}/static/js_project/select.jquery.min.js"></script>
<script  src="${ctxRoot}/static/js_project/system/machine_list.js"></script>
<script src="${ctxRoot}/static/js_project/system/add_machine_pop.js"></script>

</body>
</html>
