<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<style>
    input{
        width: 300px
    }
    select{
        width: 300px
    }
    textarea{
        width: 300px
    }
</style>
<%--onsubmit="return validate(this)"--%>
<form action="${ctx}/save_clusterMachine.do" method="post">
    <div class="clearfix">

    <table class="table table-striped table-bordered table-hover dataTable" style="font-size: 13px">
        <tr>
            <td class="center">用户名</td>
            <td class="center">
                <input type="text" name="loginUserName" autocomplete="off" value=""  onpropertychange="checkNameIp()">
            </td>
        </tr>
        <tr>
            <td class="center">机器IP</td>
            <td class="center">
                <input type="text" name="machineIp" value="" autocomplete="off"  onpropertychange="checkNameIp()"/>
            </td>
        </tr>
        <tr>
            <td class="center">机器类别</td>
            <td class="center">
                <select name="machineType.machineTypeId"   onpropertychange="checkNameIp()">
                    <option  value="0">选择机器类别</option>
                    <c:forEach items="${machineTypes}" var="machineType">
                        <option value="${machineType.machineTypeId}">${machineType.machineTypeName}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <td class="center">密码</td>
            <td class="center">
                <input name="loginPassWord" type="password" value="" autocomplete="off"  onpropertychange="test(this)"/>
                <%--<input type="hidden" name="loginPassWord" value=""  />--%>
            </td>
        </tr>
        <tr>
            <td class="center">再次输入密码</td>
            <td class="center">
                <input name="loginPassWord2" type="password" value="" autocomplete="off"  onpropertychange="test(this)"/>
                <%--<input type="hidden" name="loginPassWord3" value=""  />--%>
            </td>
        </tr>
        <tr>
            <td class="center">集群类型</td>
            <td class="center">
                <select name="clusterType.clusterTypeId"   onpropertychange="checkNameIp()">
                    <option  value="0">选择集群类型</option>
                    <c:forEach items="${clusterTypes}" var="clusterType">
                        <option value="${clusterType.clusterTypeId}">${clusterType.clusterTypeName}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <td class="center">机器状态</td>
            <td class="center">
                <select name="status"  >
                    <option  value="-1">选择机器状态</option>
                    <option value="0" <c:if test="${machine.status eq 0}">selected </c:if> >关闭</option>
                    <option value="1" <c:if test="${machine.status eq 1}">selected </c:if> >开启</option>
                </select>
            </td>
        </tr>
        <tr>
            <td class="center">描述</td>
            <td class="center">
                <textarea name="note" style=" height: 30px"></textarea>
            </td>
        </tr>
    </table>
        <label id="userIp" style="color: red; display: none">用户名和机器IP对应的机器类型及集群状态已存在，请更换用户名或机器IP或机器类型或集群状态</label>
    </div>
    <div class="ui_t_foot">
        <button class="btn" type="button" onclick="win.close();">取消</button>
        <button class="btn btn-primary" type="button" onclick="check()" >确定</button>
        <%--<button class="btn btn-primary" type="button" onclick="checkTest()" >确定</button>--%>
    </div>
</form>