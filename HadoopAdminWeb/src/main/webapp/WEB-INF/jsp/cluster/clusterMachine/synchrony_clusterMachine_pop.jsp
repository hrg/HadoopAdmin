<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<html>
<head>
    <script>
        $(document).ready(function () {
            $('#multiple-dbNames').multipleSelect({
                width: 660,
                placeholder: '选择用户',
                selectAllText: '选择全部',
                multipleWidth: 300,
                multiple: true,
                filter: true,
//                single: false,
                noMatchesFound: '找不到匹配的用户',
                isOpen: true,
                keepOpen: true,
                maxHeight: 170
            });
        });
//        function save() {
//            var ids = $("#ids").val();
//            console.log(ids);
//            var userIds = $("#multiple-dbNames").val();
//            console.log(userIds);
//            $("form").submit();
////            return false;
//        }
    </script>
</head>
<body>
<form name="demoForm" action="${ctx}/save_synchrony_clusterUser.do" method="post" onsubmit="return validate(this)">
    <div class="clearfix">
        <div class="row " style="height: 250px;padding-left: 80px;">
            <input id="ids" type="hidden" name="ids" value="${ids}">
            <%--<label class="col_left_li"><span>*</span>用户名称</label>--%>
            <select id="multiple-dbNames" class="required" multiple="multiple" name="userId">
                <c:forEach var="item" items="${clusterUsers}">
                    <option value="${item.id}">${item.userName}</option>
                </c:forEach>
            </select>
        </div>
    </div>
    <div class="ui_t_foot">
        <button class="btn btn-primary" type="button" onclick="win.close()">取消</button>
        <%--<button class="btn btn-primary" onclick="return save()">确定</button>--%>
        <button class="btn btn-primary" type="submit">确定</button>
    </div>
</form>
</body>

</html>

