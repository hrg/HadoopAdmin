<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path ;
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>homepage</title>
</head>
<body>
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="${ctxRoot}">首页</a>
        </li>
    </ul>
</div>
<div class="desktop-bg">
    <div class="desktop-menu-wrap">
        <c:forEach items="${menuList}" var="menu">
            <span class="item">
                    <a href="#">
                        <c:choose>
                            <c:when test="${menu.title=='系统管理'}">
                                <img src="${ctxRoot}/static/images/system.png"/>
                            </c:when>
                            <c:when test="${menu.title=='集群管理'}">
                                <img src="${ctxRoot}/static/images/cluster.png"/>
                            </c:when>
                            <c:when test="${menu.title=='元数据管理'}">
                                <img src="${ctxRoot}/static/images/metadata.png"/>
                            </c:when>
                            <c:when test="${menu.title=='报表管理'}">
                                <img src="${ctxRoot}/static/images/report.png"/>
                            </c:when>
                            <c:otherwise>
                                <%--<img src="${ctxRoot}/static/images/management_bac.png"/>--%>
                            </c:otherwise>
                        </c:choose>
                        <span class="title">${menu.title}</span>
                        <c:if test="${not empty menu.subMenu}">
                            <div class="sub">
                                <ul>
                                    <c:forEach items="${menu.subMenu}" var="sub" varStatus="status">
                                        <li><a href="<%=basePath%>${sub.url}">
                                                ${sub.title}
                                        </a></li>
                                    </c:forEach>
                                </ul>
                            </div>
                        </c:if>
                    </a>
            </span>
        </c:forEach>
    </div>
</div>


</body>
</html>