<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<html>
<head>
    <title>hdfs元数据管理</title>
</head>
<body>
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="${ctxRoot}">首页</a>
        </li>
        <li>
            元数据管理
        </li>
        <li class="active">
            hdfs元数据管理
        </li>
    </ul>
</div>
<div class="page-content">
    <form action="${ctx}/hdfs_metadata_management.do" method="post" onsubmit="return search(this);">
        <div class="row_height_s">

            <%--集群类型查询--%>
            <div class="form-group_li">
                <select name="Q_EQ_clusterType.clusterTypeId" id="clusterTypeId" onchange="select()">
                    <option value="">请选择集群类型</option>
                    <c:forEach items="${clusterTypePage}" var="clusterType">
                        <option value="${clusterType.id}"
                                <c:if test="${clusterType.id==param['Q_EQ_clusterType.clusterTypeId']}">selected</c:if>>${clusterType.clusterTypeName}</option>
                    </c:forEach>

                </select>
            </div>
            <%--租户查询--%>
            <div class="form-group_li">
                <%--<input type="text" placeholder="集群用户ID" class="col_d" value="${param.Q_LIKE_id}" name="Q_LIKE_id"/>--%>
                <select name="Q_EQ_company.id" id="gsName" onchange="select()">
                    <option value="">请选择租户</option>
                    <c:forEach items="${cusPage}" var="cus">
                        <option value="${cus.id}"
                                <c:if test="${cus.id==param['Q_EQ_company.id']}">selected</c:if>>${cus.companyName}</option>
                    </c:forEach>

                </select>
            </div>

            <%--用户查询--%>
            <div class="form-group_li">
                <%--<input type="text" placeholder="目录名称" class="col_d" value="${param.Q_LIKE_hpFolderName}" name="Q_LIKE_hpFolderName"/>--%>
                <select name="Q_EQ_cu.id" id="userName">
                    <option value="">请选择用户</option>
                    <c:forEach items="${hpPage}" var="hp">
                        <option value="${hp.id}" <c:if test="${hp.id==param['Q_EQ_cu.id']}">selected</c:if>>${hp.userName}</option>
                    </c:forEach>
                </select>
            </div>
            <%--目录查询--%>
            <div class="form-group_li">
                <input type="text" placeholder="hdfs地址" class="col_d" value="${param['Q_LIKE_mhi.hdfsPath']}"
                       name="Q_LIKE_mhi.hdfsPath"/>
            </div>
            <button class="btn btn-primary btn-sm" type="submit">搜索
                <i class="icon-search icon-on-right bigger-110"></i>
            </button>
        </div>
    </form>
    <div class="row row_height_y">
        <shiro:hasPermission name="addHdfs">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-primary"
                        onClick="myWin.init({title:'添加目录',rel:'tables'}).show('${ctx}/add_dir_pop.do?rel=dirForm')">
                    新增目录
                </button>
            </div>
        </shiro:hasPermission>
        <%--<shiro:hasPermission name="pushHdfs">--%>
        <%--<div class="ui-pg-div icon_left">--%>
            <%--<button id="flushButton" class="btn btn-minier btn-info"--%>
                    <%--onClick="flush()">--%>
                <%--刷新列表<span id="time"></span>--%>
            <%--</button>--%>
                <%--&lt;%&ndash;<input type="hidden" id="requestTime" value="${timeMetadata}"/>&ndash;%&gt;--%>
        <%--</div>--%>
    <%--</shiro:hasPermission>--%>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTable">
            <thead>
            <tr>
                <th width="14%" class="center">租户名称</th>
                <th width="6%" class="center">集群用户</th>
                <th width="6%" class="center">集群类型</th>
                <th width="6%" class="center">权限所属人</th>
                <th width="18%" class="center">hdfs地址</th>
                <th width="6%" class="center">地址所属组</th>
                <th width="6%" class="center">默认权限</th>
                <th width="12%" class="center">操作</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${page.list}" var="item">
                <tr>
                    <td class="center">${item.clusterUserId.systemCompany.companyName}</td>
                    <td class="center">${item.clusterUserId.userName}</td>
                    <td class="center">${item.clusterUserId.clusterType.clusterTypeName}</td>
                    <td class="center">${item.hdfsOwner}</td>
                    <td class="center">${item.hdfsPath}</td>
                    <td class="center">${item.hdfsGroup}</td>
                    <td class="center">${item.hdfsPerm}</td>
                        <%--修改查看按钮--%>
                    <td class="center">
                        <div class="action-buttons">
                            <shiro:hasPermission name="editHdfs">
                                <a title="修改hdfs元数据"
                                   onClick="win.init({title:'修改hdfs元数据',rel:''}).show('${ctx}/edit_hdfs_pop.do?id=${item.id}',{})"
                                   style="cursor: pointer;">
                                    <i class="ace-icon fa fa-pencil-square-o"></i>
                                </a>
                            </shiro:hasPermission>
                            <shiro:hasPermission name="deleteHdfs">
                                <a title="删除hdfs元数据"
                                   onClick="win.init({title:'删除HDFS',rel:''}).show('${ctx}/delete_hdfs_pop.do?hdfsId=${item.id}&&name=${item.hdfsPath}',{})"
                                   style="cursor: pointer;">
                                    <i class="ace-icon glyphicon glyphicon-trash"></i>
                                </a>
                            </shiro:hasPermission>
                            <shiro:hasPermission name="hdfsConfigureUser">
                                <a title="配置用户"
                                   onClick="thirdWin.init({title:'配置用户',rel:''}).show('${ctxRoot}/meta/hdfs/config_hadoopUser_wss.do?resourceId=${item.id}',{})"
                                   style="cursor: pointer;">
                                    <i class="ace-icon glyphicon glyphicon-list"></i>
                                </a>
                            </shiro:hasPermission>
                        </div>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

        <div class="row row_height_x message-footer">
            <tags:page page="${page}"/>
        </div>
    </div>
</div>
<script src="${ctxRoot}/static/js_project/select.jquery.min.js"></script>
<script type="text/javascript" src="${ctxRoot}/static/js/xbb.js"></script>
<script src="${ctxRoot}/static/js_project/metadata/hdfs_list.js"></script>
</body>
</html>
