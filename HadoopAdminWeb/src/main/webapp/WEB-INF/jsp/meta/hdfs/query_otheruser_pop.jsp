<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE HTML>
<html>
<head>
    <%@include file="/WEB-INF/layouts/meta.jsp" %>

    <style type="text/css" >
        body{overflow: hidden}
        .table-list{height: 256px;}
    </style>
</head>
<body>
<div class="unit" id="otherUsers">
    <%--<div class="widget-header header-color-blue2">--%>
        <%--<h5 class="lighter smaller">组外用户列表</h5>--%>
    <%--</div>--%>
        <div class="width-110 label label-info label-xlg arrowed-in arrowed-in-right" style="width: 600px">
            <%--<h5 class="lighter smaller">组外用户列表</h5>--%>
                组外用户列表
        </div>
    <div class="row row_height_y">
        <div class="pull-left">
            <div class="ui-pg-div icon_left">
                <button class="btn btn-minier btn-primary"
                        <%--onClick="win.init({title:'',rel:'otherUsers'}).flushDoubleDiv('${ctx}/add_pri_pop.do?hadoopUserId={id}&resourceId={resourceId}','确定要添加所选用户的权限吗?')">添加权限--%>
                        onClick="addJurisdiction()">添加权限
                </button>
                <div id="ahref" style="display: none;">
                    <a href="${ctxRoot}/meta/hdfs/query_groupuser_pop.do?resourceId=${resourceId}&divId=groupUsers"
                       target="li_1">li_1</a>
                    <a href="${ctxRoot}/meta/hdfs/query_otheruser_pop.do?resourceId=${resourceId}&divId=otherUsers"
                       target="li_2">li_2</a>
                </div>
            </div>
        </div>
        <div class="pull-right2">
            <form rel="otherUsers" action="${ctx}/query_otheruser_pop.do" onsubmit="return search(this)">
                <c:choose>
                <c:when test="${param.resourceId==null || param.resourceId==''}">
                    <input type="hidden" name="resourceId" value="${param.resourceId}"/>
                </c:when>
                <c:otherwise>
                    <input type="hidden" name="resourceId" value="${resourceId}"/>
                </c:otherwise>
                </c:choose>
                <input type="hidden" name="divId_otheruser" value="${divId}"/>
                <input type="text" placeholder="用户名称" name="Q_LIKE_clusterUser.userName" value="${param['Q_LIKE_clusterUser.userName']}"/>
                <input type="text" placeholder="租户名称" name="Q_LIKE_systemCompany.companyName" value="${param['Q_LIKE_systemCompany.companyName']}"/>
                <span class="input-group-btn">
                    <button class="btn btn-info btn-minier" type="submit">
                        <i class="ace-icon glyphicon glyphicon-search"></i>
                    </button>
                </span>
            </form>
        </div>
    </div>
    <div class="table-responsive td_o table-responsive_f table-list" >
        <table id="mainId" class="table table-striped table-bordered table-hover dataTable">
            <thead>
            <tr>
                <th width="1%" class="center">
                    <label>
                        <input type="checkbox" class="ace" onclick="checkAll(this);">
                        <span class="lbl"></span>
                    </label>
                </th>
                <th width="15%">用户名</th>
                <th width="25%">租户名称</th>
                <%--<th width="15%">联系人</th>--%>
                <%--<th width="15%">联系电话</th>--%>
                <th width="15%">用户类型</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${page_other.list}" var="item">
                <tr>
                    <td class="center">
                        <label>
                                <input type="checkbox" class="ace" name="id" value="${item.id}"/>
                                <span class="lbl"></span>
                        </label>
                    </td>
                    <td>${item.userName}</td>
                    <td>${item.systemCompany.companyName}</td>
                    <%--<td>${item.hpContacts}</td>--%>
                    <%--<td>${item.hpPhone}</td>--%>
                    <td>
                        <c:if test="${item.userTypeId==1}">厂商</c:if>
                        <c:if test="${item.userTypeId==2}">内部用户</c:if>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <!-- /.table-responsive -->
    <div class="row row_height_x message-footer">
        <c:if test="${page_other != null}">
            <tags:page page="${page_other}"/>
        </c:if>
    </div>
</div>
<script src="${ctxRoot}/static/js_project/meta/hdfs/query_otheruser_pop.js"></script>

</body>
</html>