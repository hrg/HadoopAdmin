<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%--<link rel="stylesheet" href="${ctxRoot}/static/css/bootstrap.min.css"/>--%>
<link rel="stylesheet" href="${ctxRoot}/static/framework/FontAwesome/css/font-awesome.css"/>


<form method="post">
    <div class="clearfix" style="padding-left: 10px">

        <table style="font-size: 13px; width: 80%">
            <tr>
                <td class="center" style="width: 20%">用户名</td>
                <td class="center" style="width: 70%">
                    <input name="name" readonly style="width: 100%; height: 36px" value='${name}'/>
                    <input name="id" type="hidden" value="${hdfsId}"/>
                </td>
            </tr>
            <tr>
                <td class="center" style="width: 20%">密码</td>
                <td class="center" style="width: 70%">
                    <input type="password" name="password" style="width: 100%; height: 36px"/>
                    <input name="flag" type="hidden" value="${flag}"/>
                </td>
            </tr>
        </table>
        <label id="empty" style="color: red; display: none">密码不能为空</label>
        <label id="error" style="color: red; display: none">请输入正确的密码</label>
    </div>

    <div class="ui_t_foot">
        <button class="btn" type="button" onclick="win.close();">取消</button>
        <button class="btn btn-primary" type="button" onclick="delHdfs()">删除</button>
    </div>
</form>

