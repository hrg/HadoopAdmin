<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>

<form id="hdfs_info" method="post" action="${ctx}/save_hdfs_Info.do" onsubmit="return validate(this)">
    <div class="clearfix">
    <div class="form_g_li">
        <label class="col_left_li"> <span>*</span>目录名称 </label>
        <div class="col_right_li">
            <input type="text" maxlength="25" class="inp required directory" name="hdfsPath"/>
        </div>
    </div>

    <div class="form_g_li">
        <label class="col_left_li"> <span>*</span>所属用户</label>
        <div class="col_right_li">
            <select class="inp" name="clusterUserId" >
                <c:forEach items="${users}" var="item">
                    <option value='${item.id}'>${item.userName}</option>
                </c:forEach>
            </select>
        </div>
    </div>
    <div class="form_g_li form_g_li1">
        <label class="col_left_li"> 描述 </label>
        <div class="col_right_li">
            <input type="text" class="inp" name="note"/>
        </div>
    </div>
</div>
    <div class="ui_t_foot">
        <button class="btn" type="button" onclick="close1()">取消</button>
        <button class="btn btn-primary" type="submit">确定</button>
        <%--<button class="btn btn-primary" type="button" onclick="saveHdfsInfo()">确定</button>--%>
    </div>
</form>


</script>

