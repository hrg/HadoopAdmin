<%--
  User: fwj
  Date: 2016/2/24
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<script type="text/javascript">

</script>
<form action="${ctx}/save_hdfs_Info.do" id="metaHdfsInfo" method="post" onsubmit="return validate(this)">
    <input type="hidden" name="id" value="${metaHdfsInfo.id}">

    <div class="clearfix">
        <div class="form_g_li">
            <label class="col_left_li"> 集群用户: </label>
            <div class="col_right_li">
                <input type="text" class="inp " disabled="disabled" name="clusterUserId" value="${metaHdfsInfo.clusterUserId.userName}" />
            </div>
        </div>
        <div class="form_g_li">
            <label class="col_left_li"> 权限所属人: </label>
            <div class="col_right_li">
                <input type="text" class="inp " disabled="disabled" name="hdfsOwner" value="${metaHdfsInfo.hdfsOwner}" />
            </div>
        </div>
        <div class="form_g_li">
            <label class="col_left_li"> hdfs地址: </label>
            <div class="col_right_li">
                <input type="text" class="inp " disabled="disabled" name="hdfsPath" value="${metaHdfsInfo.hdfsPath}" />
            </div>
        </div>
        <div class="form_g_li">
            <label class="col_left_li"> 地址所属组: </label>
            <div class="col_right_li">
                <input type="text" class="inp " disabled="disabled" name="hdfsGroup" value="${metaHdfsInfo.hdfsGroup}" />
            </div>
        </div>
        <div class="form_g_li">
            <label class="col_left_li"> 默认权限: </label>
            <div class="col_right_li">
                <input type="text" class="inp " disabled="disabled" name="hdfsPerm" value="${metaHdfsInfo.hdfsPerm}" />
            </div>
        </div>
        <%--<div class="form_g_li">--%>
            <%--<label class="col_left_li"> 添加人: </label>--%>
            <%--<div class="col_right_li">--%>
                <%--<input type="text" class="inp " disabled="disabled" name="userId" value="${metaHdfsInfo.userId.userName}" />--%>
            <%--</div>--%>
        <%--</div>--%>

        <div class="form_g_li">
            <label class="col_left_li"> 描述: </label>
            <div class="col_right_li">
                <input type="text" class="inp " readonly name="note" value="${metaHdfsInfo.note}" />
            </div>
        </div>
        <div class="form_g_li">
            <label class="col_left_li" style="display: none;"> 添加时间: </label>
            <div class="col_right_li">
                <input type="hidden" class="inp " disabled="disabled" name="createTime" value="${metaHdfsInfo.createTime}" />
            </div>
        </div>

    </div>
    <div class="ui_t_foot">
        <button class="btn" type="button" onclick="win.close();">取消</button>
        <button class="btn btn-warning" type="button" onclick="editHdfsPop();">编辑</button>
        <button class="btn btn-primary" disabled type="submit" onclick="">确定</button>
    </div>
</form>