<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<html>
<head>
    <title>hdfs权限管理</title>
</head>
<body>
<script>
    $(document).ready(function () {
        //发送俩个请求
        $("#dbList").load("${ctx}/query_dbList_pop.do");
        $("#hdfsList").load("${ctx}/query_hdfsList_pop.do");
    })
</script>
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="${ctxRoot}">首页</a>
        </li>
        <li>
            元数据管理
        </li>
        <li class="active">
            hdfs对比
        </li>
    </ul>
</div>
<div class="page-content">
    <div class="lieb_lis">
        <div class="lieb2_li" id="dbList">
            <%--<iframe width="100%" src="query_dbList_pop.jsp"></iframe>--%>
            <jsp:include page="query_dbList_pop.jsp">
                <jsp:param name="name" value="1"/>
            </jsp:include>
        </div>
        <div class="lieb2_li" id="hdfsList">
            <%--<iframe width="100%" src="query_hdfsList_pop.jsp"></iframe>--%>
            <jsp:include page="query_hdfsList_pop.jsp">
                <jsp:param name="name" value="2"/>
            </jsp:include>
        </div>
        <%--<div class="lieb2_li" id="test" >--%>
        <%--<jsp:include page="query_dbList_pop.jsp">--%>
        <%--<jsp:param name="name" value="2" />--%>
        <%--</jsp:include>--%>
        <%--</div>--%>
    </div>
</div>
</body>
</html>
