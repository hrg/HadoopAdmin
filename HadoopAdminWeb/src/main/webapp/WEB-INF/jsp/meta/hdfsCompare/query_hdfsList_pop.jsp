<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib uri="/tags" prefix="date" %>
<script>
    function hdfsOperation(obj){
        var url = $(obj).attr("rel").toString();
        $.ajax({
            url:url,
            success:function(data){
                layer.alert(data.data,function(){
                    window.location.reload();
                    return;
                })
            },
            error:function(){
                alert("hdfsOperation error");
            }
        });
    }

</script>
<div class="unit">
    <div class="widget-header header-color-blue2">
        <h5 class="lighter smaller">hdfsInfo目录列表</h5>
    </div>
    <form rel="hdfsList" action="${ctx}/query_hdfsList_pop.do" onsubmit="return search(this)">
        <div class="row_height_s">
            <div class="form-group_li">
                <input type="text" placeholder="目录名称" name="Q_LIKE_hdfsPath"
                       value="${param['Q_LIKE_hdfsPath']}"/>
            </div>
            <button class="btn btn-primary btn-sm" type="submit">搜索
                <i class="ace-icon glyphicon glyphicon-search"></i>
            </button>
        </div>
    </form>
    <div class="table-responsive td_o table-responsive3" id="parentId">
        <table class="table table-striped table-bordered table-hover dataTable">
            <thead>
            <tr>
                <th width="30%">目录</th>
                <th width="30%">时间</th>
                <th width="20%">权限</th>
                <th width="10%">操作</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${hdfsPageInfo.list}" var="item">
                <tr class="myClick">
                    <td>${item.hdfsPath}</td>
                    <td><date:date value="${item.createTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
                    <td>${item.hdfsPerm}</td>
                    <td>
                        <div class="action-buttons">
                            <a title="添加到hdfsInfoBak去" rel="${ctx}/addHdfsInfoBak.do?id=${item.id}" onclick="hdfsOperation(this)">
                                <i class="ace-icon glyphicon glyphicon-plus"></i>
                            </a>
                            <a title="从HDFS中删除" rel="${ctx}/delHdfs.do?id=${item.id}" onclick="hdfsOperation(this)">
                                <i class="ace-icon glyphicon glyphicon-trash"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <!-- /.table-responsive -->
    <div class="row row_height_x message-footer">
        <tags:page page="${hdfsPageInfo}"/>
    </div>
</div>