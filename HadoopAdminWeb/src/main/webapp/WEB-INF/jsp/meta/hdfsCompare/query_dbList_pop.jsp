<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib uri="/tags" prefix="date" %>
<script type="text/javascript" src="${ctxRoot}/static/js/xbb.js"></script>

<script>
    function addHdfs(obj){
        var url = $(obj).attr("rel").toString();
        $.ajax({
            url:url,
            success:function(data){
                console.log(data);
                layer.alert(data.data,function(){
                    window.location.reload();
                    return;
                })
            },
            error:function(){
                alert("addHdfs error");
            }
        });
    }
    function delHdfsBak(obj){
        var url = $(obj).attr("rel").toString();
        $.ajax({
            url:url,
            success:function(data){
                layer.alert(data.data,function(){
                    window.location.reload();
                    return;
                })
            },
            error:function(){
                alert("delHdfsBak error");
            }
        });
    }
</script>
<div class="unit">
    <div class="widget-header header-color-blue2">
        <h5 class="lighter smaller">hdfsInfoBak目录列表</h5>
    </div>
    <form rel="dbList" action="${ctx}/query_dbList_pop.do" onsubmit="return search(this);">
        <div class="row_height_s">
            <div class="form-group_li">
                <input type="text" placeholder="目录名称" name="Q_LIKE_hdfsPath"
                       value="${param['Q_LIKE_hdfsPath']}"/>
            </div>
            <button class="btn btn-primary btn-sm" type="submit">搜索
                <i class="ace-icon glyphicon glyphicon-search"></i>
            </button>
        </div>
    </form>
    <div class="table-responsive td_o table-responsive3" id="parentId">
        <table class="table table-striped table-bordered table-hover dataTable">
            <thead>
            <tr>
                <th width="30%">目录</th>
                <th width="30%">时间</th>
                <th width="20%">权限</th>
                <th width="10%">操作</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${hdfsPageInfoBak.list}" var="item">
                <tr class="myClick">
                    <td>${item.hdfsPath}</td>
                    <td><date:date value="${item.createTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
                    <td>${item.hdfsPerm}</td>
                    <td>
                        <div class="action-buttons">
                            <a title="添加到HDFS" rel="${ctx}/addHdfs.do?id=${item.id}" onclick="addHdfs(this)">
                                <i class="ace-icon glyphicon glyphicon-plus"></i>
                            </a>
                            <a title="从HdfsBak中删除" rel="${ctx}/delHdfsBak.do?id=${item.id}" onClick="delHdfsBak(this)">
                                <i class="ace-icon glyphicon glyphicon-trash"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <!-- /.table-responsive -->
    <div class="row row_height_x message-footer">
        <tags:page page="${hdfsPageInfoBak}"/>
    </div>
</div>