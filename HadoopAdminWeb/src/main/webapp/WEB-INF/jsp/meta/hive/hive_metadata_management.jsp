<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib uri="/tags" prefix="date"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<html>
<head>
    <title>hive元数据管理 </title>
    <script type="text/javascript">
        function flush() {
            $.ajax({
                url: "${ctx}/flush_hive.do",
                dataType: "json",
                success: function () {
                    alert("刷新成功");
                    window.location.reload();
                },
                error: function () {
//                alert("error")
                }
            })
        }
    </script>
</head>
<body>
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="${ctxRoot}">首页</a>
        </li>
        <li>
            元数据管理
        </li>
        <li class="active">
            hive元数据管理
        </li>
    </ul>
</div>
<div class="page-content">
    <div class="row">
        <div class="col-sm-12">
            <form action="${ctx}/hive_metadata_management.do" method="post" onsubmit="return search(this);" class="form-horizontal"
                  role="form">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="widget-box">
                            <div class="widget-header">
                                <h4 class="widget-title">查询条件</h4>

                                <div class="widget-toolbar">
                                    <a data-action="collapse" href="#">
                                        <i class="ace-icon fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row">
                                        <fieldset class="col-xs-6">
                                            <legend>根据库名查询</legend>
                                            <div>
                                                <select id="multiple-dbNames" name="Q_LIKE_dbName"
                                                        multiple="multiple">
                                                    <option></option>
                                                    <c:forEach items="${dbNames}" var="dbName" varStatus="status">
                                                        <option value="${dbName}" <c:if test="${dbName==param['Q_LIKE_dbName']}">selected</c:if>>${dbName}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </fieldset>
                                        <fieldset class="col-xs-4">
                                            <legend>根据其他查询</legend>
                                            <div style="margin: 25px 0" class="col-xs-4">
                                                <select name="Q_EQ_clusterType.clusterTypeId" id="clusterTypeId" onchange="select()"
                                                        class="chosen-select form-control"
                                                        data-placeholder="集群类型">
                                                    <option></option>
                                                    <c:forEach items="${clusterTypePage}" var="clusterType">
                                                        <option value="${clusterType.id}"
                                                                <c:if test="${clusterType.id==param['Q_EQ_clusterType.clusterTypeId']}">selected</c:if>>${clusterType.clusterTypeName}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div style="margin: 25px 0" class="col-xs-4">
                                                <select name="Q_EQ_systemCompany.id" id="gsName" onchange="select()"
                                                        class="chosen-select form-control"
                                                        data-placeholder="租户名称">
                                                    <option></option>
                                                    <c:forEach items="${cusPage}" var="cus">
                                                        <option value="${cus.id}"
                                                                <c:if test="${cus.id==param['Q_EQ_systemCompany.id']}">selected</c:if>>${cus.companyName}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <%--集群用户查询--%>
                                            <div style="margin: 25px 0" class="col-xs-4">
                                                <select name="Q_EQ_clusterUser.id" id="hpUserName" multiple="multiple">
                                                    <option value></option>
                                                    <c:forEach items="${hpPage}" var="hp">
                                                        <option value="${hp.id}"
                                                                <c:if test="${hp.id==param['Q_EQ_clusterUser.id']}">selected</c:if>>${hp.userName}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div style="margin: 25px 0" class="col-xs-6">
                                                <input type="text" placeholder="表名" class="form-control"
                                                       value="${param['Q_LIKE_tableName']}"
                                                       name="Q_LIKE_tableName"/>
                                            </div>

                                            <%--选择公有或私有--%>
                                            <div style="margin: 25px 0" class="col-xs-6">
                                                <select name="Q_EQ_hdfsInfoBak.properties" class="chosen-select form-control" >
                                                    <option value="0"
                                                            <c:if test="${param['Q_EQ_hdfsInfoBak.properties']==0||param['Q_EQ_hdfsInfoBak.properties']==null}">selected</c:if>>
                                                        公有
                                                    </option>
                                                    <option value="1"
                                                            <c:if test="${param['Q_EQ_hdfsInfoBak.properties']==1}">selected</c:if>>
                                                        私有
                                                    </option>
                                                    <option value=""
                                                            <c:if test="${param['Q_EQ_hdfsInfoBak.properties']==''}">selected</c:if>>
                                                        公私有
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="pull-right inline">
                                                <button class="btn btn-primary btn-sm" type="submit">搜索
                                                    <i class="ace-icon glyphicon glyphicon-search"></i>
                                                </button>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row row_height_y">
                <div class="pull-left">
                    <shiro:hasPermission name="pushHive">
                        <div class="ui-pg-div icon_left">
                            <button class="btn btn-minier btn-info"
                                    onClick="flush()">
                                刷新列表
                            </button>
                        </div>
                    </shiro:hasPermission>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTable">
                    <thead>
                    <tr>
                        <th width="10%" class="center">租户名称</th>
                        <th width="10%" class="center">集群用户</th>
                        <th width="10%" class="center">集群类型</th>
                        <th width="5%" class="center">库名</th>
                        <th width="5%" class="center">表名</th>
                        <th width="10%" class="center">公有私有</th>
                        <th width="10%" class="center">创建时间</th>
                        <th width="12%" class="center">操作</th>
                    </tr>
                    </thead>

                    <tbody>
                    <c:forEach items="${page.list}" var="item">
                        <tr>
                            <td class="center">${item.clusterUser.systemCompany.companyName}</td>
                            <td class="center">${item.clusterUser.userName}</td>
                            <td class="center">${item.clusterUser.clusterType.clusterTypeName}</td>
                            <td class="center">${item.dbName}</td>
                            <td class="center">${item.tableName }</td>
                            <td class="center">
                                <c:if test="${item.hdfsInfoBak.properties ==0}">公有</c:if>
                                <c:if test="${item.hdfsInfoBak.properties ==1}">私有</c:if>
                            </td>
                                <td class="center">
                                    <date:date value="${item.createTime}" pattern="yyyy-MM-dd HH:mm:ss"/>
                                </td>
                            <td class="center">
                                <div class="action-buttons">
                                    <shiro:hasPermission name="seeHive">
                                        <a title="查看hive元数据"
                                           onClick="win.init({title:'查看hive元数据',rel:''}).show('${ctx}/edit_hive_pop.do?id=${item.id}',{})"
                                           style="cursor: pointer;">
                                            <i class="ace-icon fa fa-eye"></i>
                                        </a>
                                    </shiro:hasPermission>
                                    <shiro:hasPermission name="hiveConfigureUser">
                                        <c:if test="${item.hdfsInfoBak.properties ==0}">
                                            <a title="配置用户"
                                               <%--onClick="thirdWin.init({title:'配置用户',rel:''}).show('${ctxRoot}/meta/hive/config_hive_pop.do?resourceId=${item.id}',{})"--%>
                                               onClick="thirdWin.init({title:'配置用户权限',rel:''}).show('${ctxRoot}/meta/hive/config_hive_privilege_pop.do?resourceId=${item.id}&userId=${item.clusterUser.id}',{})"
                                               style="cursor: pointer;">
                                                <i class="ace-icon glyphicon glyphicon-list"></i>
                                            </a>
                                        </c:if>
                                    </shiro:hasPermission>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <div class="row row_height_x message-footer">
                    <tags:page page="${page}"/>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${ctxRoot}/static/js_project/select.jquery.min.js"></script>
<script type="text/javascript" src="${ctxRoot}/static/js/xbb.js"></script>
<script src="${ctxRoot}/static/js_project/metadata/hive_list.js"></script>

</body>
</html>
