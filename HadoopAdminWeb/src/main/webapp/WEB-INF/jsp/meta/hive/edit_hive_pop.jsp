<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib uri="/tags" prefix="date"%>

<style>
  textarea{
    resize: none;
    height: 80px;
    width: 570px;
  }
  .form_g_lit{
    float: left;
    width: 750px;
    color: #333;
    font-size: 12px;
    margin-top: 8px;
  }
</style>
<script type="text/javascript">
</script>
<form id="hiveInfo" action="#" >
  <input id="id" type="hidden" name="id" value="${hiveInfo.id}">
  <div class="clearfix">
    <div class="form_g_li">
      <label class="col_left_li"> 集群用户 </label>
      <div class="col_right_li">
            <input type="text" class="inp " disabled="disabled" name="hadoopUserId" value="${hiveInfo.clusterUser.userName}" />
      </div>
    </div>
    <div class="form_g_li">
      <label class="col_left_li"> 库名 </label>
      <div class="col_right_li">
        <input type="text" class="inp " disabled="disabled"  name="hpDbName" value="${hiveInfo.dbName}"/>
      </div>
    </div>
    <div class="form_g_li">
      <label class="col_left_li"> 表名 </label>
      <div class="col_right_li">
        <input type="text" class="inp " disabled="disabled"  name="hpTbName" value="${hiveInfo.tableName}"/>
      </div>
    </div>
    <div class="form_g_li">
      <label class="col_left_li"> hdfs路径 </label>
      <div class="col_right_li">
        <input type="text" class="inp "  disabled="disabled"  name="hpHdfsLoc" value="${hiveInfo.hdfsInfoBak.hdfsPath}"/>
      </div>
    </div>
    <div class="form_g_li">
      <label class="col_left_li"> 公有私有 </label>
      <div class="col_right_li">
        <select name="hpPubPri" id="hpPubPri" disabled="disabled" style="width: 220px;background-color: #EEEEEE">
          <option value="0" <c:if test="${1==hiveInfo.hdfsInfoBak.properties}">selected</c:if>>公有</option>
          <option value="1" <c:if test="${2==hiveInfo.hdfsInfoBak.properties}">selected</c:if>>私有</option>
        </select>
        <%--<input type="text" class="inp " disabled="disabled"  name="hpPubPri" value="${hiveInfo.hpPubPri}"/>   #d9edf7   --%>
      </div>
    </div>

    <div class="form_g_li">
      <label class="col_left_li"> 创建时间 </label>
      <div class="col_right_li">
        <input type="text" class="inp " disabled="disabled"  name="hpCreatetime" value=" <date:date value="${hiveInfo.createTime}" pattern="yyyy-MM-dd HH:mm:ss"/>" />
      </div>
    </div>
    <div class="form_g_lit">
      <label class="col_left_li"> 建表语句 </label>
      <div >
        <button class="btn btn-primary" type="button" onclick="return seeHiveSql()" id="sql_see_button">点击查看</button>
          <button class="btn btn-primary" type="button" onclick="return cancleHiveSql()" style="display: none;" id="sql_cancle_button">取消查看</button>
        <%--<textarea  name="hpTbSql" disabled="disabled" rows="50" cols="15" style="height: 80px;background-color: #EEEEEE">${hiveInfo.hiveSQL}</textarea>--%>
      </div>
    </div>
      <div class="form_g_li"id="sql_div" style="display:none">
          <label class="col_left_li"></label>
          <div class="col_right_li" style="margin-left: 35%;">
              <textarea id="sql_text" name="sql_text"  disabled="disabled" style="overflow-y:visible;background-color: #EEEEEE;height:100px"> </textarea>
          </div>
      </div>
  </div>
  <div class="ui_t_foot">
      <button class="btn" type="button" onclick="win.close();">取消</button>
    <%--<button class="btn btn-warning" type="button" onclick="editHivePop()">编辑</button>--%>
      <%--<button class="btn btn-primary" disabled type="submit" onclick="">确定</button>--%>
</div>
</form>