<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE HTML>
<html>
<head>
    <%@include file="/WEB-INF/layouts/meta.jsp" %>
    <style type="text/css">
        body {
            overflow: hidden
        }

        .table-list {
            height: 256px;
        }
    </style>
</head>
<body id="mainId">
<div class="unit" id="groupUsers" style="height:580px" >
    <form id="form_page_param">
        <input type="hidden" id="resourceId" name="resourceId" value="${resourceId}"/>
        <input type="hidden" id="userId" name="userId" value="${userId}"/>
    </form>
    <input type="hidden" id="ctx" value="${ctxRoot}"/>
    <div class="row row_height_y">
        <div class="pull-right2">
            <form rel="groupUsers" action="${ctx}/query_hive_user_privilege_pop.do?resourceId=${resourceId}&userId=${userId}"
                  onsubmit="return search(this)" id="hiveaccessformid">
                <input type="hidden" name="divId_groupuser" value="${divId}"/>
                <input type="text" placeholder="用户名称" name="Q_LIKE_clusterUser.userName"
                       value="${param['Q_LIKE_clusterUser.userName']}"/>
                <input type="text" placeholder="租户名称" name="Q_LIKE_systemCompany.companyName"
                       value="${param['Q_LIKE_systemCompany.companyName']}"/>
                <span class="input-group-btn">
                    <button class="btn btn-info btn-minier" type="submit">
                        <i class="ace-icon glyphicon glyphicon-search"></i>
                    </button>
                </span>
            </form>
        </div>
    </div>
    <div class="table-responsive td_o table-responsive_f  table-list" style="height:85%">
        <table class="table table-striped table-bordered table-hover dataTable" id="table_privilege" >
            <thead>
            <tr>
                <%-- <th width="1%" class="center">
                     <label>
                         <input type="checkbox" class="ace" onclick="checkAll(this);"/>
                         <span class="lbl"></span>
                     </label>
                 </th>--%>
                <th class="center" width="5%">用户名</th>
                <th class="center" width="10%">租户名称</th>
                <th class="center" width="5%">用户类型</th>
                    <c:forEach items="${metaAccessPrivilege}" var="pri">
                        <th class="center" width="4%" title="${pri.note}" style="cursor: pointer;">${pri.privilegeType}</th>
                    </c:forEach>
                <th class="center" width="7%">操作</th>
            </tr>
            </thead>
            <tbody>

            <c:forEach items="${page.list}" var="item">
                <tr id="${item.userId}">
                    <td>${item.userName}</td>
                    <td class="center">${item.companyName}</td>
                    <td class="center">${item.userTypeName}</td>
                    <c:forEach items="${metaAccessPrivilege}" var="pri">
                        <td class="center"><input type="checkbox" value="${pri.id}" <c:if test="${fn:contains(item.privilegeStr,pri.id)}">checked="true"</c:if>></td>
                    </c:forEach>
                    <td class="center">
                        <button class="btn btn-minier btn-primary" type="button" onclick="editPrivilege('${item.userId}')" style="margin-left: 5%">编辑
                        </button>
                        <button class="btn btn-minier btn-primary" id="button_save" disabled="" type="button" onclick="savePrivilege('${item.userId}','${resourceId}','${item.hiveAccessId}')">保存
                        </button>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <!-- /.table-responsive -->
    <div class="row row_height_x message-footer">
        <c:if test="${page != null}">
            <tags:page page="${page}"/>
        </c:if>
    </div>
</div>
<script src="${ctxRoot}/static/js_project/meta/hive/query_hive_user_privilege_pop.js"></script>

</body>
</html>