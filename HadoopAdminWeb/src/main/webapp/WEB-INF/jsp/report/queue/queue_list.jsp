<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>队列资源报表</title>
    <style>
        .table th, .table td {
            text-align: center;
            vertical-align: middle;
        }
    </style>
</head>
<body>
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="${ctxRoot}">首页</a>
        </li>
        <li>
            报表管理
        </li>
        <li class="active">
            队列资源报表
        </li>
    </ul>
</div>
<div class="page-content">
    <%--查询条件--%>
    <form action="${ctx}/queue_list.do" method="post"
          onsubmit="return search(this);">
        <input type="hidden" name="isQuery" value="query"/>

        <div class="row_height_s">
            <div class="form-group_li">
                <div class="col_right_li">
                    <select name="Q_EQ_cu.id" id="cuId" onchange="findQueue(false)" class="chosen-select form-control"
                            data-placeholder="用户名称">
                        <option></option>
                        <c:forEach var="username" items="${clusterUserList}">
                            <option value="${username.id}"
                                    <c:if test="${username.id eq param['Q_EQ_cu.id']}">selected</c:if>>${username.userName}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <input type="hidden" id="queue_type" value="${param['Q_EQ_fca.queueName']}">
            <div class="form-group_li">
                <div class="col_right_li">
                    <select id="queueType" onchange="showDel()" name="Q_EQ_fca.queueName" class="chosen-select form-control"
                            data-placeholder="队列名称">
                    </select>
                </div>
            </div>
            <button class="btn btn-primary btn-sm" type="submit">搜索
                <i class="ace-icon glyphicon glyphicon-search"></i>
            </button>
        </div>
    </form>

    <div class="row row_height_y">
        <shiro:hasPermission name="downloadQueueResource">
            <div class="ui-pg-div icon_left">
                <a class="btn btn-minier btn-primary" href="${ctx}/down_all.do">
                    <i class="fa fa-download"></i>
                    下载全部
                </a>
            </div>
            <div class="ui-pg-div icon_left">
                <a class="btn btn-minier btn-primary" onclick="checkDown()">
                    <i class="fa fa-download"></i>
                    下载搜索结果
                </a>
            </div>
        </shiro:hasPermission>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTable">
            <thead>
            <tr>
                <th class="center" width="12%">用户名称</th>
                <th class="center" width="11%">队列名称</th>
                <th class="center" width="11%">最小资源</th>
                <th class="center" width="11%">最大资源</th>
                <th class="center" width="11%">最大APP</th>
                <th class="center" width="11%">权值</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${fairSchedulerAllocationPage.list}" var="fair">
                <tr>
                    <td class="center" style="vertical-align: middle">${fair.clusterUser.userName}</td>
                    <td class="center">${fair.queueName}</td>
                    <td class="center">${fair.minResource}M &nbsp;${fair.minCpu}核</td>
                    <td class="center">${fair.maxResource}M &nbsp;${fair.maxCpu}核</td>
                    <td class="center">${fair.maxApp}</td>
                    <td class="center">${fair.weight}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="row row_height_x message-footer">
            <tags:page page="${fairSchedulerAllocationPage}"/>
        </div>
    </div>
</div>
<script src="${ctxRoot}/static/js_project/select.jquery.min.js"></script>
<script src="${ctxRoot}/static/js_project/report/queue_list.js"></script>
</body>
</html>
