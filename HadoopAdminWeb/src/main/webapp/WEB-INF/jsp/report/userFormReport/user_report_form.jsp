<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>用户查询报表</title>


</head>
<body>
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="${ctxRoot}">首页</a>
        </li>
        <li>
            报表管理
        </li>
        <li class="active">
            用户查询报表
        </li>
    </ul>
</div>
<div class="page-content">
    <%--查询条件--%>
        <form action="${ctx}/user_report_form.do" method="post" onsubmit="return search(this);">
        <div class="row_height_s">
            <div class="form-group_li">
                <div class="col_right_li">
                    <input class="form-control date-picker"  id="datetime" name="datetime" type="text" style="float: left;" data-date-format="yyyy-mm-dd" />
                    <span class="input-group-addon" style="float: left;height: 33px;">
					<i class="ace-icon fa fa-calendar"></i>
					</span>
                </div>
            </div>
            <button class="btn btn-primary btn-sm" style="margin-left: 40px;" type="submit">搜索
                <i class="ace-icon glyphicon glyphicon-search"></i>
            </button>
        </div>
    </form>
<div class="table-responsive">
<div id="container" style="min-width: 400px; max-width: 600px; height: 400px; margin: 0 auto"></div>
</div>
</div>
<div style="display: none;" id="user">
    ${user}
</div>
<div style="display: none;" id="series">
    ${series}
</div>
<%--<input type="hidden" value="${user}" id="user">--%>
<input type="hidden" value="${date}" id="date">

<%--Highcharts--%>
<script type="text/javascript" src="${ctxRoot}/static/framework/Highcharts-4.1.8/highcharts.js"></script>
<script type="text/javascript" src="${ctxRoot}/static/framework/Highcharts-4.1.8/exporting.js"></script>
<script type="text/javascript" src="${ctxRoot}/static/framework/Highcharts-4.1.8/highcharts-more.js"></script>
<script src="${ctxRoot}/static/js/user_report_form/user_repor_form.js"></script>
<style>
    .table th, .table td {
        text-align: center;
        vertical-align: middle;
    }
</style>

</body>
</html>
