<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<html>
<head>
    <title>用户认证信息列表</title>
</head>
<body>
<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home home-icon"></i>
            <a href="${ctxRoot}">首页</a>
        </li>
        <li>
            报表管理
        </li>
        <li class="active">
            用户认证信息报表
        </li>
    </ul>
</div>
<div class="page-content">
    <form action="${ctx}/userApprove_list.do" method="post" onsubmit="return search(this);">
        <input type="hidden" name="isQuery" value="query"/>
        <div class="row_height_s">
            <div class="form-group_li">
                <select name="Q_EQ_cu.userName" class="chosen-select form-control" data-placeholder="用户名称">
                    <option></option>
                    <c:forEach var="username" items="${hadoopUserList}">
                        <option value="${username.userName}"
                                <c:if test="${username.userName eq param['Q_EQ_cu.userName']}">selected</c:if>>${username.userName}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group_li">
                <select name="Q_EQ_cm.machineIp" class="chosen-select form-control" data-placeholder="机器IP">
                    <option></option>
                    <c:forEach var="item" items="${clusterMachines}">
                        <option value="${item.machineIp}"
                                <c:if test="${item.machineIp eq param['Q_EQ_cm.machineIp']}">selected</c:if>>${item.machineIp}</option>
                    </c:forEach>
                </select>
            </div>
            <button class="btn btn-primary btn-sm" type="submit">搜索
                <i class="ace-icon glyphicon glyphicon-search"></i>
            </button>
        </div>
    </form>
    <div class="row row_height_y">
        <shiro:hasPermission name="downloadUserAuthentication">
            <div class="ui-pg-div icon_left">
                <a class="btn btn-minier btn-primary" href="${ctx}/down_app.do">
                    <i class="fa fa-download"></i>
                    下载全部
                </a>
            </div>
            <div class="ui-pg-div icon_left">
                <a class="btn btn-minier btn-primary" onclick="checkDown()">
                    <i class="fa fa-download"></i>
                    下载搜索结果
                </a>
            </div>
        </shiro:hasPermission>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover dataTable">
            <thead>
            <tr>
                <th width="8%" class="center">用户名</th>
                <th width="12%" class="center">认证机器</th>
                <th width="10%" class="center">princple</th>
                <th width="10%" class="center">开始时间</th>
                <th width="13%" class="center">结束时间</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="item" items="${userApproveFormList}">
                <tr <c:if test="${item.red==true}">style="color: #f47983" </c:if>>
                    <td class="center" style="vertical-align: middle">${item.name}</td>
                    <td class="center">${item.ip}</td>
                    <td class="center">${item.princ}</td>
                    <td class="center">${item.startTime}</td>
                    <td class="center">${item.endTime}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="row row_height_x message-footer">
            <tags:page page="${page}"/>
        </div>
    </div>
</div>
<script src="${ctxRoot}/static/js_project/select.jquery.min.js"></script>
<script src="${ctxRoot}/static/js_project/report/userApprove_list.js"></script>
</body>
</html>
