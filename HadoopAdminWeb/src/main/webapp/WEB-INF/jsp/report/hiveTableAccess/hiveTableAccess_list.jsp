<%--
  Created by IntelliJ IDEA.
  User: 袁颖
  Date: 2016/5/9
  Time: 14:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<html>
<head>
    <title>HIVE权限管理(hive表名主导)</title>
    <style type="text/css">
        table,td,th {  border: 1px solid #8DB9DB; padding:5px; border-collapse: collapse; font-size:16px; }
    </style>
</head>
<body>
<div class="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <a href="${ctxRoot}">首页</a>
        </li>
        <li>
            报表管理
        </li>
        <li>
            HIVE权限管理(hive表名主导)
        </li>
    </ul>
</div>
<div class="page-content">
    <form action="hiveTableAccess_list.do" method="post">
        <input type="hidden" name="isQuery" value="query"/>
        <div class="row_height_s">
            <div class="form-group_li">
                <div class="col_right_li">
                    <select class="chosen-select form-control" name="Q_EQ_tableName" data-placeholder="hive表名" id="selectHiveTable">
                        <option></option>
                        <c:forEach items="${metaHiveInfos}" var="item">
                            <option value="${item.tableName}"<c:if
                                    test="${param['Q_EQ_tableName']==item.tableName}">selected </c:if>>${item.tableName}</option>
                        </c:forEach>
                    </select>
                    <button class="btn btn-primary btn-sm" type="submit">搜索</button>
                </div>
            </div>
        </div>
    </form>
    <div class="row row_height_y">
        <shiro:hasPermission name="downloadHive">
            <div class="ui-pg-div icon_left">
                <a class="btn btn-minier btn-primary" href="${ctx}/down_all.do">
                    <i class="fa fa-download"></i>
                    下载全部
                </a>
            </div>
            <div class="ui-pg-div icon_left">
                <a class="btn btn-minier btn-primary" onclick="checkDown()">
                    <i class="fa fa-download"></i>
                    下载搜索结果
                </a>
            </div>
        </shiro:hasPermission>
    </div>
    <div class="table-responsive">
        <table id="treeTable1" style="width:100%"class="table table-striped table-bordered table-hover dataTable" >
            <tr>
                <td style="width:20%">所属用户</td>
                <td style="width:60%">hive表名</td>
                <td>可访问用户</td>
            </tr>
            <c:forEach items="${page.list}" var="item">
                <tr hasChild="true" id="${item.clusterUser.id}">
                    <td ><span controller="true">${item.clusterUser.userName}</span></td>
                    <td>共<font color="red">${item.countByCluster}</font>个表</td>
                    <td></td>
                </tr>
            </c:forEach>

        </table>
        <div class="row row_height_x message-footer">
            <tags:page page="${page}"></tags:page>
        </div>
    </div>
</div>
<script src="${ctxRoot}/static/js_project/report/hiveTableAccess_list.js"></script>
</body>
</html>
