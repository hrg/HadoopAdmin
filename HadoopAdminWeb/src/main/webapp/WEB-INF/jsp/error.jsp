<%--
  Created by IntelliJ IDEA.
  User: 袁颖
  Date: 2016/5/3
  Time: 9:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/WEB-INF/jsp/common/taglib.jsp" %>
<html>
<head>
    <title></title>
</head>
<body>
<div class="breadcrumbs">
    <ul class="breadcrumb">
        <i class="icon-home home-icon"></i>
        <a href="${ctxRoot}">首页</a>
    </ul>
</div>
<div class="page-content">
    <h1 class="center">出错啦!</h1>
    <div>
        <dl>
            <dt>错误信息:</dt>
            <dd>${ex}</dd>
        </dl>
    </div>
</div>
</body>
</html>
