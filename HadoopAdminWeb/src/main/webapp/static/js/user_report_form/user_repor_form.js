/**
 * Created by fwj on 15-12-18.
 */

var basePath=PATH.BASEPATH;

$(function () {
    var user = $("#user").text();
    user = $.trim(user);
    var users = user.substring(1,user.length-1);
    users = users.split(",");
    var series=$("#series").html();
    var series_data=eval(series);
    $('#container').highcharts({
        chart: {
            polar: true,
            type: 'line'
        },
        title: {
            text: '用户访问表的次数'//,
//            x: -30
        },
        pane: {
            size: '80%'
        },
        xAxis: {
            categories:users,
            tickmarkPlacement: 'on',
            lineWidth: 0
        },

        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y}</b><br/>'
        },

        legend: {
            align: 'right',
            verticalAlign: 'top',
            y: 70,
            layout: 'vertical'
        },
        credits:{
            enabled:false
        },
        exporting: {
            enabled: false
        },
        series: series_data
    });

    $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
        $(this).prev().focus();
    });
    var setDate=$("#date").val();
    $("#datetime").datepicker('setDate',eval(setDate) ).datepicker('hide').blur();
});





