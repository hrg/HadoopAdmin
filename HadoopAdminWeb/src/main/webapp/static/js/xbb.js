function delConfirm(obj) {
    if (confirm("确定要删除吗？")) {
        $.ajax({
            type: 'GET',
            url: $(obj).attr("url").toString(),
            dataType: "json",
            cache: false,
            success: function () {
                var rel = $(obj).attr("rel");
                if (rel) {
                    $("#" + rel).submit();
                }
            },
            error: function () {
                alert("error")
            }
        });
    }
}

function delParent(obj) {
    $.ajax({
        type: 'GET',
        url: $(obj).attr("url").toString(),
        dataType: "json",
        cache: false,
        success: function () {
            var rel = $(obj).attr("rel");
            if (rel) {
                window.location.href = "${ctx}/parameter.do"
            }
        },
        error: function () {
            alert("error")
        }
    });
}

function loadUrls(id, url, pars, obj) {
    $panel = $("#" + id);
    if (url) {
        url += ((url.indexOf("?") == -1) ? "?" : "&") + new Date().getTime();
    }
    $panel.load(url, pars, function () {
    });
    $(".myClick").removeClass("actives");
    $(obj).parent('tr').addClass("actives");
}

function findGroupAndOtherUser1(groupUrl, otherUrl, divId, obj) {
    $groupPanel = $("#groupUsers");

    var grouppars = "resourceId=" + divId + "&divId=groupUsers";
    if (groupUrl) {
        groupUrl += ((groupUrl.indexOf("?") == -1) ? "?" : "&") + new Date().getTime();
    }
    $groupPanel.load(groupUrl, grouppars, function () {
    });

    $otherPanel = $("#otherUsers");
    var otherpars = "resourceId=" + divId + "&divId=otherUsers";
    if (otherUrl) {
        otherUrl += ((otherUrl.indexOf("?") == -1) ? "?" : "&") + new Date().getTime();
    }
    $otherPanel.load(otherUrl, otherpars, function () {
    });
    /**
     * xbb   页面跳转到指定的锚点
     * @type {string}
     */
    window.location.href = "#groupUsers"
    $groupPanel.show();
    $otherPanel.show();
    $(".myClick").removeClass("actives");
    $(obj).parents("tr").addClass("actives")
}

//hdfs db 对比
function findGroupAndOtherUser2(groupUrl, otherUrl, divId, obj) {
    $groupPanel = $("#dbList");

    var grouppars = "resourceId=" + divId + "&divId=hdfsList";
    if (groupUrl) {
        groupUrl += ((groupUrl.indexOf("?") == -1) ? "?" : "&") + new Date().getTime();
    }
    $groupPanel.load(groupUrl, grouppars, function () {
    });

    $otherPanel = $("#hdfsList");
    var otherpars = "resourceId=" + divId + "&divId=dbList";
    if (otherUrl) {
        otherUrl += ((otherUrl.indexOf("?") == -1) ? "?" : "&") + new Date().getTime();
    }
    $otherPanel.load(otherUrl, otherpars, function () {
    });
}

var myWin = {
    init: function (param) {
        if (param) {
            this.rel = param.rel || "mainId";
            $("#_winTitle").html(param.title || '操作');
        }
        $("#groupUsers").hide();
        $("#otherUsers").hide();
        return win;
    },
    show: function (url, pars) {
        var $panel = $("#_winId");
        var $content = $("#_winContentId");
        var reg = /\{(\w+)\}/g;
        var map = url.match(reg);
        for (key in map) {
            var mat = map[key];
            var val = mat.replace("{", "").replace("}", "");
            var lst = $("input:checked[name=" + val + "]", "#" + this.rel);
            if (lst.size() == 1) {
                url = url.replace(map[key], lst.val());
            }
            else {
                alert("请选择一条记录！")
                return;
            }
        }
        if (url) {
            url += ((url.indexOf("?") == -1) ? "?" : "&") + new Date().getTime();
        }
        $content.load(url, pars, function () {
            $panel.show();
        });
    }
}