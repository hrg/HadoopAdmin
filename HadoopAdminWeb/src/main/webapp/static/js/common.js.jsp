<%@ page language="java" contentType="text/javascript; charset=utf-8"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
%>
var PATH = {
	VERSION : "1.0",
	BASEPATH : "<%=basePath%>",
	SESSIONID: "<%=request.getSession().getId()%>"
};
(function(){
    PATH.Callback = {};
    window.PATH = PATH;
})();