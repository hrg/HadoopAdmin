/**
* @author wanghongbo@163.com
*/

function checkAll(obj, divId) {
    divId = divId || "mainId";
    if ($(obj).prop("checked")) {
        $(":checkbox", "#" + divId).prop("checked", true);
    } else {
        $(":checkbox", "#" + divId).prop("checked", false);
    }
}

function selectOne(obj,divId){
    divId = divId || "mainId";
    if($(obj).prop("checked")){
        $(":checkbox", "#" + divId).each(function(){
            if($(obj).val()==$(this).val()){

            }else{
                $(this).prop("checked",false);
            }
        });
    }else{

    }
}

/*$(document).ready(function(){
    $('.date-picker', "#mainId").datepicker({autoclose:true}).next().on(ace.click_event, function(){
        $(this).prev().focus();
    });
});*/

function showCity(obj){
    var address = $(obj).val();
    if(address!=null){
        var areas = address.split(",");
        for(var i = 0 ; i < areas.length ; i++){
            $("input[name='area']").each(function(){
                if(areas[i]==$(this).val()){
                    $(this).attr("checked",true);
                }
            });
        }
    }
    $("#address").show();
}

function addressClose(){
    $("#address").hide();
    $("input[name='area']").attr("checked",false);
}

function addressOk(){
    var areas = "";
    $("input[name='area']:checked").each(function(){
        areas+=$(this).val()+",";
    });
    if(areas!=null){
        $("#planAddress").val(areas.substring(0,areas.length-1));
    }
    $("#address").hide();
}

function changePlanType(obj){
    if($(obj).find("option:selected").val()==1){
        $("#filePath").attr("disabled",true);
        $("#planAddress").attr("disabled",false);
    }else{
        $("#filePath").attr("disabled",false);
        $("#planAddress").attr("disabled",true);
    }
}

function selectRoundMode(obj,divId){
    divId = divId || "mainId";
    if($(obj).attr("name")=="round_mode"){
        $("input[name='round_mode']","#"+divId).attr("disabled",true);
        var flag = true;
        var roundMode = "";
        $("#channel_info input[name='round_mode']").each(function(){
            if($(this).attr("checked")){
                flag = false;
                roundMode += "1";
            }else{
                roundMode += "0";
            }
        });
        if(flag){
            roundMode = "1111111";
        }
        $("#roundMode").val(roundMode);
    }
}

function initEnv() {
    var ajaxbg = $("#background,#progressBar");
    ajaxbg.hide();
    /*var _timeout=null;*/
    $(document).ajaxStart(function(){
        ajaxbg.show();
        /*_timeout=setTimeout(function(){ajaxbg.show()}, 500*1);*/
    }).ajaxStop(function(){
        /*if(_timeout) {
            clearTimeout(_timeout);
            _timeout=null;
        }*/
        ajaxbg.hide();
    });
}
$(document).ready(function(){
    initEnv();
});

function page(page,searchParams,divId){
    var isQuery = "&isQuery=query";
    if(divId){
        var url = $("form","#"+divId).attr("action");
        if(url){
            url+=((url.indexOf("?")==-1)?"?":"&")+ new Date().getTime();
        }
        var pars = "page="+page+"&divId="+divId+"&"+$("form","#"+divId).serialize()+isQuery;
        $("#"+divId).load(url, pars, function () {
        });
    }else{
        url = "?page="+page+"&"+searchParams+isQuery;
        if($("#form_page_param") && $("#form_page_param").serialize()!=""){
                url+="&"+$("#form_page_param").serialize();
        }
        window.location.href=url;

    }
}