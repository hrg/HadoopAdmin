/**
 * Created by wanghongbo on 2015/1/19.
 */

var win = {

    init : function(param){
        if(param){
            this.rel = param.rel || "mainId";
            $("#_winTitle").html(param.title || '操作');
        }
        return win;
    },
    show : function(url, pars) {
        var $panel = $("#_winId");
        var $content = $("#_winContentId");
        var reg = /\{(\w+)\}/g;
        var map = url.match(reg);
        for(key in map){
            var mat = map[key];
            var val = mat.replace("{","").replace("}","");
            var lst = $("input:checked[name="+val+"]", "#" + this.rel);
            if(lst.size()==1){
                url = url.replace(map[key],lst.val());
            }
            else{
                layer.alert("请选择一条记录！")
                return;
            }
        }
        if(url) {
            url += ((url.indexOf("?")== -1)?"?":"&") + new Date().getTime();
        }
        $content.load(url, pars, function(){
            $panel.show();
        });
    },
    synchrony :function(url, pars) {
        var reg = /(\w+)=\{(\w+)\}/g;
        var map = url.match(reg);
        var $panel = $("#_winId");
        var $content = $("#_winContentId");
        for(key in map){
            var tKeyValue = map[key];
            url = url.replace(tKeyValue,"");
            var tValue = tKeyValue.match(/\{(\w+)\}/g);
            var iValue = tValue[0].replace("{","").replace("}","");
            var lst = $("input:checked[name="+iValue+"]","#" + this.rel);
            if(lst.size()>=1){
                $(lst).each(function(index){
                    if(/\w+$/g.test(url)){
                        url += "&";
                    }
                    url += tKeyValue.replace(tValue[0],$(this).val());
                });
            }
            else{
                layer.alert("请选择一条记录！")
                return;
            }
        }
        if(url) {
            url += ((url.indexOf("?")== -1)?"?":"&") + new Date().getTime();
        }
        $content.load(url, pars, function(){
            $panel.show();
        });
    },
    multi : function(url, pars) {
        var $panel = $("#_winId");
        var $content = $("#_winContentId");
        var reg = /(\w+)=\{(\w+)\}/g;
        var map = url.match(reg);
        for(key in map){
            var tKeyValue = map[key];
            url = url.replace(tKeyValue,"");
            var tValue = tKeyValue.match(/\{(\w+)\}/g);
            var iValue = tValue[0].replace("{","").replace("}","");
            var lst = $("input:checked[name="+iValue+"]","#" + this.rel);
            if(lst.size()>=1){
                $(lst).each(function(index){
                    if(/\w+$/g.test(url)){
                        url += "&";
                    }
                    url += tKeyValue.replace(tValue[0],$(this).val());
                });
            }
            else{
                layer.alert("请选择一条记录！")
                return;
            }
        }
        if(url) {
            url += ((url.indexOf("?")== -1)?"?":"&") + new Date().getTime();
        }
        $content.load(url, pars, function(){
            $panel.show();
        });
    },
    href : function(url, pars) {
        var reg = /\{(\w+)\}/g;
        var map = url.match(reg);
        for(key in map){
            var mat = map[key];
            var val = mat.replace("{","").replace("}","");
            var lst = $("input:checked[name="+val+"]", "#" + this.rel);
            if(lst.size()==1){
                url = url.replace(map[key],lst.val());
            }
            else{
                layer.alert("请选择一条记录！")
                return;
            }
        }
        if(url) {
            url += ((url.indexOf("?")== -1)?"?":"&") + new Date().getTime();
        }
        location.href = url;
    },
    export:function(url, pars) {
        var $panel = $("#_winId");
        var $content = $("#_winContentId")
        var reg = /\{(\w+)\}/g;
        var map = url.match(reg);
        for(key in map){
            var mat = map[key];
            var val = mat.replace("{","").replace("}","");
            var lst = $("input:checked[name="+val+"]", "#" + this.rel);
            if(lst.size()==1){
                url = url.replace(map[key],lst.val());
            }
            else{
                layer.alert("请选择一条记录！")
                return;
            }
        }
        $("#callbackframe").attr('src',url);
    },
    confirm : function(url, pars, callback) {
        var reg = /(\w+)=\{(\w+)\}/g;
        var map = url.match(reg);
        for(key in map){
            var tKeyValue = map[key];
            url = url.replace(tKeyValue,"");
            var tValue = tKeyValue.match(/\{(\w+)\}/g);
            var iValue = tValue[0].replace("{","").replace("}","");
            var lst = $("input:checked[name="+iValue+"]","#" + this.rel);
            if(lst.size()>=1){
                $(lst).each(function(index){
                    if(/\w+$/g.test(url)){
                        url += "&";
                    }
                    url += tKeyValue.replace(tValue[0],$(this).val());
                });
            }
            else{
                layer.alert("请选择一条记录！")
                return;
            }
        }
        if(confirm(pars||"确定要删除吗？")){
            var rel = this.rel;
            if(rel){
                $.ajax({
                    type: 'GET',
                    url:url,
                    dataType:"json",
                    cache: false,
                    success: callback||function(json){
                        var $searchForm;
                        $searchForm = $("form","#"+rel);
                        if($searchForm.size()==0){
                            window.location.reload(true);
                        }
                        if($searchForm.size()>=1){
                            $searchForm.submit();
                        }
                    }
                });
            }else{
                $.ajax({
                    type: 'GET',
                    url:url,
                    dataType:"json",
                    cache: false,
                    success: callback||function(json){
                        var $searchForm;
                        if(json["type"]=="alert"){
                            layer.alert(json["data"]["message"], {maxWidth: 630});
                        }
                        $searchForm=$("form", "#mainId");
                        if($searchForm.size()==0){
                            window.location.reload(true);
                        }
                        if($searchForm.size()>=1){
                            $searchForm.submit();
                        }
                    }
                });
            }

        }
    },
    flushDoubleDiv:function(url, pars, callback){
        var reg = /(\w+)=\{(\w+)\}/g;
        var map = url.match(reg);
        for(key in map){
            var tKeyValue = map[key];
            url = url.replace(tKeyValue,"");
            var tValue = tKeyValue.match(/\{(\w+)\}/g);
            var iValue = tValue[0].replace("{","").replace("}","");
            if(iValue=='id'){
                var lst = $("input:checked[name="+iValue+"]","#" + this.rel);
                if(lst.size()>=1){
                    $(lst).each(function(index){
                        if(/\w+$/g.test(url)){
                            url += "&";
                        }
                        url += tKeyValue.replace(tValue[0],$(this).val());
                    });
                }else{
                    layer.alert("请选择一条记录！");
                    return;
                }
            }else{
                var lst = $("input[name="+iValue+"]","#" + this.rel);
                if(lst.size()>=1){
                    $(lst).each(function(index){
                        if(/\w+$/g.test(url)){
                            url += "&";
                        }
                        url += tKeyValue.replace(tValue[0],$(this).val());
                    });
                }else{
                    layer.alert("缺少关键数据！");
                    return;
                }
            }

        }
        if(confirm(pars||"确定要删除吗？")){
            $.ajax({
                type: 'GET',
                url:url,
                dataType:"json",
                cache: false,
                success: callback||function(json){
                    var $searchForm1 = $("#groupUsers form");
                    var $searchForm2 = $("#otherUsers form");
                    if(json["type"]=="alert"){
                        if(json["data"]["type"]=="success"){
                            layer.alert(json["data"]["message"], {maxWidth: 630});
                        }else{
                            layer.alert(json["data"]["message"], {maxWidth: 630});
                            return;
                        }
                    }
                    if($searchForm1.size()>=1){
                        $searchForm1.submit();
                    }
                    if($searchForm2.size()>=1){
                        $searchForm2.submit();
                    }
                }
            });
        }
    },
    close : function(){
        $panel = $("#_winId");
        $panel.hide();
    }
}

function alertResult(data){
    if(data['data'].type=="success"){
        layer.alert(data['data'].message);
        var $searchForm=$("form", "#mainId");
        if($searchForm.size()==0){
            window.location.reload(true)
        }
        if($searchForm.size()>=1){
            $searchForm.submit();
        }
    }else{
        layer.alert(data['data'].message);
    }
}

var thirdWin = {

    init : function(param){
        if(param){
            this.rel = param.rel || "mainId";
            $("#_thirdTitle").html(param.title || '操作');
        }
        return thirdWin;
    },
    show : function(url, pars) {
        var $panel = $("#_thirdId");
        var $content = $("#_thirdContentId")
        var reg = /\((\w+)\)/g;
        var map = url.match(reg);
        for(key in map){
            var mat = map[key];
            var val = mat.replace("(","").replace(")","");
            var select = $("#"+val);
            if(select.size()==1){
                url = url.replace(map[key],select.find("option:selected").val());
            }
            else{
                layer.alert("请选择一条记录！")
                return;
            }
        }
        $content.load(url, pars, function(){
            $panel.show();
        });
    },
    confirm : function(url, pars) {
        var reg = /(\w+)=\{(\w+)\}/g;
        var map = url.match(reg);
        for(key in map){
            var tKeyValue = map[key];
            url = url.replace(tKeyValue,"");
            var tValue = tKeyValue.match(/\{(\w+)\}/g);
            var iValue = tValue[0].replace("{","").replace("}","");
            var lst = $("input:checked[name="+iValue+"]","#" + this.rel);
            if(lst.size()>=1){
                $(lst).each(function(index){
                    if(/\w+$/g.test(url)){
                        url += "&";
                    }
                    url += tKeyValue.replace(tValue[0],$(this).val());
                });
            }
            else{
                layer.alert("请选择一条记录！")
                return;
            }
        }
        if(confirm(pars)){
            $.ajax({
                type: 'GET',
                url:url,
                dataType:"json",
                cache: false,
                success: function(){
                    var $searchForm=$("form", "#mainId");
                    if($searchForm.size()==0){
                        window.location.reload(true)
                    }
                    if($searchForm.size()>=1){
                        $searchForm.submit();
                    }
                },
                error: function(){}
            });
        }
    },
    close : function(){
        $panel = $("#_thirdId");
        $panel.hide();
    }
}

var mywin = {
    init : function(param){
        if(param){
            this.rel = param.rel || "mainId";
            $("#_winTitle").html(param.title || '操作');
        }
        return mywin;
    },
    confirm : function(url, pars) {
        var reg = /(\w+)=\{(\w+)\}/g;
        var map = url.match(reg);
        for(key in map){
            var tKeyValue = map[key];
            url = url.replace(tKeyValue,"");
            var tValue = tKeyValue.match(/\{(\w+)\}/g);
            var iValue = tValue[0].replace("{","").replace("}","");
            var lst = $("input:checked[name="+iValue+"]","#" + this.rel);
            if(lst.size()>=1){
                $(lst).each(function(index){
                    if(/\w+$/g.test(url)){
                        url += "&";
                    }
                    url += tKeyValue.replace(tValue[0],$(this).val());
                });
            }
            else{
                layer.alert("请选择一条记录！")
                return;
            }
        }
        if(confirm(pars)){
            $.ajax({
                type: 'GET',
                url:url,
                dataType:"json",
                cache: false,
                success: function(json){
                    layer.alert(json['data'].message, {maxWidth: 630},function(){
                        window.location.reload();
                        return;
                    });
                },
                error: function(){
                    layer.alert(json['data'].message, {maxWidth: 630});
                    return;
                }
            });
        }
    }
}
