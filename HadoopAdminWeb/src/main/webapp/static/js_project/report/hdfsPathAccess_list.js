$(function(){
    var option = {
        theme:'vsStyle',
        expandLevel : 1,
        beforeExpand : function($treeTable, id) {
            //判断id是否已经有了孩子节点，如果有了就不再加载，这样就可以起到缓存的作用
            if ($('.' + id, $treeTable).length) { return; }
            var hdfsPath = $("#selectHdfsPath option:selected").val();
            $.ajax({
                url: "hdfsPathAccessChild_list.do",
                data: {"clusterUserId":id,"hdfsPath":hdfsPath},
                dataType: "json",
                async:false,
                success: function (data) {
                    var html="";
                    if(data!=null && data.length>0){
                        for (var j = 0; j < data.length; j++) {
                            var s=data[j];
                            html +="<tr id="+s.id+ s.accessUserName+" pId="+id+" flag='rowspan'>"
                            +"<td>"+ s.ownerUserName+"</td>"
                            +"<td>"+ s.hdfsPath+"</td>"
                            +"<td>"+ s.accessUserName+"</td>"
                            +"</tr>"
                        }
                    }
                    $treeTable.addChilds(html);
                    $("table").rowspan(0);
                    $("table").rowspan(1);
                }
            })

        },
        onSelect : function($treeTable, id) {
            window.console && console.log('onSelect:' + id);
        }
    };
    //加载treeTable
    $('#treeTable1').treeTable(option);
});

/*合并单元格*/
jQuery.fn.rowspan = function (colIdx) {
    return this.each(function () {
        var that;
        $('tr[flag="rowspan"]', this).each(function (row) {
            $('td:eq(' + colIdx + ')', this).filter(':visible').each(function (col) {
                if (that != null && $(this).text() == $(that).text()) {
                    rowspan = $(that).attr("rowSpan");
                    if (rowspan == undefined) {
                        $(that).attr("rowSpan", 1);
                        rowspan = $(that).attr("rowSpan");
                    }
                    rowspan = Number(rowspan) + 1;
                    $(that).attr("rowSpan", rowspan);
                    $(this).hide();
                } else {
                    that = this;
                }
            });
        });
    });
}

/*下载搜索结果add20160721qinfengxia*/
function checkDown(){
    var hdfsPath = $("#selectHdfsPath option:selected").val();
    console.log(hdfsPath);
    if (hdfsPath =="" ||  hdfsPath == null ) {
        alert("请先选择过滤条件");
        return false;
    }
    window.location.href = ("down_all.do?hdfsPath=" + hdfsPath);
}