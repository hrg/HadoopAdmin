/**
 * Created by fwj on 16/1/14.
 */
function findQueue(f) {
    $.ajax({
        url: "queue/find_queue.do",
        data: "clusterUserId=" + $('#cuId').val(),
        dataType: "json",
        async: "false",
        success: function (json) {
            var flag = false;
            var $select = $("select[name='Q_EQ_fca.queueName']")
            $select.empty();
            $("#queueType").trigger("chosen:updated");
            $select.append("<option></option>");
            var select = $("#queue_type").val();
            for (var i = 0; i < json.length; i++) {
                if (select == json[i] && f) {
                    $select.append("<option selected value='" + json[i] + "'>" + json[i] + "</option>")
                    $("#queueType").trigger("chosen:updated");
                    flag = true
                } else {
                    $select.append("<option  value='" + json[i] + "'>" + json[i] + "</option>")
                    $("#queueType").trigger("chosen:updated");
                }

            }
            if (flag) {
                $select.next().find('a').append("<abbr class='search-choice-close'></abbr>")
            }
        },
        error: function () {
        }
    })
}

function checkDown() {
    if ($("select[name='Q_EQ_cu.id']").val() == "" && $("select[name='Q_EQ_fca.queueName']").val() == "") {
        alert("请先选择过滤条件");
        return false;
    }
    window.location.href = ("down_all.do?clusterId=" + $("select[name='Q_EQ_cu.id']").val() + "&&queue=" + $("select[name='Q_EQ_fca.queueName']").val());
}

function showDel() {
    var $select = $("select[name='Q_EQ_fca.queueName']")
    if ($select.val() == null || $select.val() == "") {

    } else {
        $select.next().find('a').append("<abbr class='search-choice-close'></abbr>")
    }

}


/****
 * 动态合并单元格
 * **/
jQuery.fn.rowspan = function (colIdx) { //封装的一个JQuery小插件
    return this.each(function () {
        var that;
        $('tr', this).each(function (row) {
            $('td:eq(' + colIdx + ')', this).filter(':visible').each(function (col) {
                if (that != null && $(this).html() == $(that).html()) {
                    rowspan = $(that).attr("rowSpan");
                    if (rowspan == undefined) {
                        $(that).attr("rowSpan", 1);
                        rowspan = $(that).attr("rowSpan");
                    }
                    rowspan = Number(rowspan) + 1;
                    $(that).attr("rowSpan", rowspan);
                    $(this).hide();
                } else {
                    that = this;
                }
            });
        });
    });
}

$(function () {
    findQueue(true);
    $("table.table").rowspan(0);//传入的参数是对应的列数从0开始，哪一列有相同的内容就输入对应的列数值
});