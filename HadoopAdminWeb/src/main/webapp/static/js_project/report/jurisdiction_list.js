/**
 * Created by fwj on 16/1/14.
 */
function check() {
    var username = $("select[name='Q_EQ_username']").val();
    if (username == null || username == "") {
        alert("请选择搜索条件")
        return false;
    }
    window.location.href = "down_hdfs_hive.do?username=" + username;
}

function hdfsVictors(list,obj){
    var hvList=list.replace('[','').replace(']','');
    layer.tips('<div class="word-break">'+hvList+'</div>', obj, {
        maxWidth:358,
        tips: [1, '#259de0'],
        time: 0
    });
}
function hiveVictors(list,obj){
    var hvList=list.replace('[','').replace(']','');
    layer.tips('<div class="word-break">'+hvList+'</div>', obj, {
        maxWidth:358,
        tips: [1, '#259de0'],
        time: 0
    });
}
function closeHdfsVictors(){
    layer.closeAll();
}

/***
 * 展开hdfs
 * */
function hdfsOpen(count_index){
    $("#hdfsDisplay"+count_index+"").show();
    $("#hdfsHide"+count_index+"").hide();
    $("#hiveDisply"+count_index+"").show();
    $("#hiveHide"+count_index+"").hide();
}
/**
 * 隐藏hdfs
 * */
function hdfsHide(count_index){
    $("#hdfsDisplay"+count_index+"").hide();
    $("#hdfsHide"+count_index+"").show();
    $("#hiveDisply"+count_index+"").hide();
    $("#hiveHide"+count_index+"").show();
}
 /**
 * 自动合并单元格
 * */
jQuery.fn.rowspan = function (colIdx) { //封装的一个JQuery小插件
    return this.each(function () {
        var that;
        $('tr', this).each(function (row) {
            $('td:eq(' + colIdx + ')', this).filter(':visible').each(function (col) {
                if (that != null && $(this).html() == $(that).html()) {
                    rowspan = $(that).attr("rowSpan");
                    if (rowspan == undefined) {
                        $(that).attr("rowSpan", 1);
                        rowspan = $(that).attr("rowSpan");
                    }
                    rowspan = Number(rowspan) + 1;
                    $(that).attr("rowSpan", rowspan);
                    $(this).hide();
                } else {
                    that = this;
                }
            });
        });
    });
}
$(function () {
    $("table.table").rowspan(0);//传入的参数是对应的列数从0开始，哪一列有相同的内容就输入对应的列数值
});