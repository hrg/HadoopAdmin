$(function(){
    $(":checkbox").each(function(){
        $(this).attr("disabled",true);
    });
    var close_btn = "<a data-action=\"close\" href=\"#\" onclick='$(\"#iframeId\")[0].contentWindow.closeHiveUserPrivilege();'><i class=\"ace-icon fa fa-times\"></i></a>";
    parent.$("#span_tool").html(close_btn);
});

function editPrivilege(userId){
    $("#"+userId).find(":checkbox").each(function(){
        $(this).removeAttr("disabled");
    });
    $("#"+userId).find("#button_save").removeAttr("disabled");
}

function savePrivilege(userId,resourceId,hiveAccessId){
    var ids=[];
/*    $("input:checkbox[name='id']:checked").each(function() {
        ids.push( $(this).val());  // 每一个被选中项的值
    });*/
    $("#"+userId).find(":checkbox:checked").each(function(){
        ids.push( $(this).val());  // 每一个被选中项的值
    });
    //当前页
    var page  = $("div[class='pull-right']").find("li[class='active']").find("a").html();
    var ctx = $("#ctx").val();
    if(confirm("确定要保存吗?")){
        layer.msg("数据加载中.....",{time:0});
        $.ajax({
            url: ctx+"/meta/privilege/add_privilege.do",
            data: {"userId":userId,"resourceId":resourceId,"ids":ids.toString(),"hiveAccessId":hiveAccessId},
            dataType: "json",
            success: function (data) {
                if(data!=null){
                    layer.alert(data.data.message,function(){
                        var src = $("#ctx").val()+ '/meta/hive/query_hive_user_privilege_pop.do?resourceId='+$("#resourceId").val()+'&userId='+$("#userId").val()+"&page="+page
                        parent.$('#iframeId').attr('src',src);
                    });

                }
            }
        })
    }
}

function closeHiveUserPrivilege() {
    var val = "";
    $("button[id='button_save']").each(function() {
        val = $(this).attr("disabled");
        if(val == undefined){
            if (confirm("用户权限配置没有保存,请进行保存")) {
                return false;
            }else{
                parent.thirdWin.close();
                return false;
            }
        }
    });
    if(val=="disabled"){
        parent.thirdWin.close();
    }
}