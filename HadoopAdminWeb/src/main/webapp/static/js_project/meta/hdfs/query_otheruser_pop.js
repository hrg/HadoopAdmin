function addJurisdiction(){
    var ids=[];
    $("input:checkbox[name='id']:checked").each(function() {
        ids.push( $(this).val());  // 每一个被选中项的值
    });
    var resourceId=$("input[name='resourceId']").val();
    if(ids.length==0){
        alert("请选择一条数据");
        return false;
    }
    if(confirm("确定要添加吗?")){
        layer.msg("数据加载中........",{time:0});
        $.ajax({
            url: "add_pri_pop.do",
            data: {"hadoopUserId":ids.toString(),"resourceId":resourceId},
            dataType: "json",
            success: function (data) {
                if(data!=null){
                    if(data.data.type=='error'){
                        layer.alert(data.data.message);
                    }else{
                        layer.alert(data.data.message,function(){
                            $('#ahref a').each(function(){
                                this.click();
                            });
                        });
                    }

                }
            }
        })
    }

}