/**
 * Created by fwj on 2016-01-07.
 */
var basePath = PATH.BASEPATH;
$(function () {
    $('#hpUserName_multiple').multipleSelect({
        width: 560,
        placeholder: '集群用户',
        selectAllText: '选择全部',
        multipleWidth: 200,
        multiple: true,
        filter: true,
        noMatchesFound: '找不到匹配的用户',
        single: true,
        isOpen: true,
        keepOpen: true,
        maxHeight: 200

    })
    if ($("#requestTime").val() == null || $("#requestTime").val() == "" || $("#requestTime").val() == undefined) {
        return false;
    }
    var requestTime1 = parseInt($("#requestTime").val()) + 600000;
    var nowTime1 = new Date().getTime();
    var time1 = requestTime1 - nowTime1;
    if (time1 <= 0) {
        $("#time").html("")
        $("#flushButton").prop("disabled", false);
        clearInterval(timer);
    } else {
        time1 = Math.round(time1 / 1000);
        var mina = parseInt(time1 / 60);
        var secondb = time1 % 60;
        var minc = parseInt(mina / 10);
        var mind = mina % 10;
        var secondc = parseInt(secondb / 10);
        var secondd = secondb % 10;
        $("#time").html(minc + mind + ":" + secondc + secondd)
        $("#flushButton").prop("disabled", true);
    }


    var timer = setInterval(function () {
        var requestTime = parseInt($("#requestTime").val()) + 600000;
        var nowTime = new Date().getTime();
        var time = requestTime - nowTime;
        if (time <= 0) {
            $("#time").html("")
            $("#flushButton").prop("disabled", false);
            clearInterval(timer);
        } else {
            time = Math.round(time / 1000);
            var min = parseInt(time / 60);
            var second = time % 60;
            var min1 = parseInt(min / 10);
            var min2 = min % 10;
            var second1 = parseInt(second / 10);
            var second2 = second % 10;
            $("#time").html(min1 + min2 + ":" + second1 + second2)
            $("#flushButton").prop("disabled", true);
        }
    }, 1000)




})


function editHdfsPop() {
    $("#metaHdfsInfo").find('input[name=note]').removeAttr('readonly');
    $(".ui_t_foot").find('button').removeAttr("disabled");
}

function close1() {
//    $("#groupUsers").show();
//    $("#otherUsers").show();
    win.close();
}

function close_third() {
//    $("#groupUsers").show();
//    $("#otherUsers").show();
    thirdWin.close();
}
/**
 * 新建目录
 * */
function saveHdfsInfo() {
    //var name = $("input[name='hdfsPath']").val();
    //if (name.indexOf("/") > 0) {
    //    layer.alert("只能填写目录名称，不能包含‘／’");
    //    return false;
    //}
    //$("#hdfs_info").submit();
}

/**
 * 删除
 * */
function delHdfs() {
    if ($("input[name='password']").val() == "" || $("input[name='password']").val() == null) {
        $("#empty").show();
        return false;
    }
    $.ajax({
        url: "delete_hdfs.do",
        data: "hdfsId=" + $("input[name='id']").val() + "&&password=" + $("input[name='password']").val() + "&&flag=" + $("input[name='flag']").val(),
        dataType: "json",
        success: function (json) {
            if (json["type"] == "alert") {
                if (json['data'].type == "success") {
                    layer.alert(json['data'].message, function (index) {
                        location.reload(true);
                        layer.close(index);
                    });

                }
                else {
                    layer.alert(json['data'].message, function (index) {
                        location.reload(true);
                        layer.close(index);
                    });
                }
            }
        }
    })
}


function select() {
    var id = $("#gsName option:selected").val();
    var clusterTypeId = $("#clusterTypeId option:selected").val();//页面上选中的集群类型add20160713qinfengxia
    $("#userName").html("");
    $.ajax({
        url: basePath + "/meta/hdfs/select_hpUv.do",
        data: "id=" + id + "&clusterTypeId=" + clusterTypeId,
        type: "post",
        async:false,
        dataType: "json",
        success: function (data) {
            if (data['data'].length > 0) {
                $("#userName").append("<option value=''></option>");
            }
            for (var i = 0; i < data['data'].length; i++) {
                $("#userName").append("<option value='" + data['data'][i]["id"] + "'>" + data['data'][i]["userName"] + "</option>");
            }
//            $("#userName").multipleSelect("refresh");
        },
        error: function () {
            alert("error");
            return false;
        }

    })
}

//刷新列表
function flush() {
    var ul = basePath + "/meta/hdfs/flush_hdfs.do";
    console.log(ul);
    $.ajax({
        url: basePath + "/meta/hdfs/flush_hdfs.do",
        type: "post",
        dataType: "json",
        success: function () {
            //if ($("#requestTime").val() == null || $("#requestTime").val() == "" || $("#requestTime").val() == undefined) {
            //    alert("刷新失败")
            //} else {
            alert("刷新成功");
            //}
            window.location.href = "${ctx}/hdfs_list.do";
        },
        error: function () {
            alert("error")
        }
    });
    window.location.href = basePath + "/meta/hdfs/hdfs_metadata_management.do";
}
