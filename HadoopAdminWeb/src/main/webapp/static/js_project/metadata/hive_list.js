$(function () {
    $('#hpUserName').multipleSelect({
        width: 209,
        placeholder: '集群用户',
        selectAllText: '选择全部',
        multipleWidth: 100,
        multiple: true,
        filter: true,
        single: true,
        noMatchesFound: '找不到匹配的用户'
    });
    $('#multiple-dbNames').multipleSelect({
        width: 560,
        placeholder: '库名',
        selectAllText: '选择全部',
        multipleWidth: 200,
        multiple: true,
        filter: true,
        single: true,
        noMatchesFound: '找不到匹配的用户',
        isOpen: true,
        keepOpen: true,
        maxHeight: 160
    });
});
function select() {
    var id = $("#gsName option:selected").val();
    var clusterTypeId = $("#clusterTypeId option:selected").val();//页面上选中的集群类型add20160713qinfengxia
    $("#hpUserName").empty().multipleSelect("refresh");
    $.ajax({
        url: "select_hpUn.do",
        data: "id=" + id + "&clusterTypeId=" + clusterTypeId,
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data['data'].length > 0) {
                $("#hpUserName").append("<option value=''></option>");
            }
            //$("#hpUserName").append("<option value=''></option>");
            for (var i = 0; i < data['data'].length; i++) {
                $("#hpUserName").append("<option value='" + data['data'][i]["id"] + "'>" + data['data'][i]["userName"] + "</option>");
            }
            $("#hpUserName").multipleSelect("refresh");
        },
        error: function () {
            alert("error");
            return false;
        }
    })
}
//点击查看hive的sql语句
function seeHiveSql(){
    var hiveInfoId = $("#id").val();
    $.ajax({
        url:"seeHiveSql.do",
        data:{"hiveInfoId":hiveInfoId},
        timeout:60*1000,
        type:"post",
        dataType:"json",
        success:function(result){
            //layer.alert(result.data);
            //return false;
            //不直接再alert弹出框，直接在本页面显示 update20160919qinfengxia
            $("#sql_text").html(result.data);
            $("#sql_div").show();
            $("#sql_see_button").css("display","none");
            $("#sql_cancle_button").css("display","");
        }
    });
}

/*取消查看sql add20160919qinfengxia*/
function cancleHiveSql(){
    $("#sql_text").html("");
    $("#sql_div").hide();
    $("#sql_see_button").css("display","");
    $("#sql_cancle_button").css("display","none");
}
function close1() {
//    $("#groupUsers").show();
//    $("#otherUsers").show();
    thirdWin.close();
}
function close_third() {
//    $("#groupUsers").show();
//    $("#otherUsers").show();
    thirdWin.close();
}