/**
 * Created by fwj on 16-3-7.
 * 菜单管理
 */
var setting = {
    check: {
        enable: true,
        chkStyle: "radio",
        radioType: "all"
    },
    view: {
        dblClickExpand: false
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    callback: {
        onClick: onClick,
        beforeClick: this.beforeClick,
        onCheck: onCheck
    }
};

function beforeClick(treeId, node) {
    if (node.isParent) {
        if (node.level === 0) {
            var pNode = curMenu;
            while (pNode && pNode.level !== 0) {
                pNode = pNode.getParentNode();
            }
            if (pNode !== node) {
                var a = $("#" + pNode.tId + "_a");
                a.removeClass("cur");
                zTree_Menu.expandNode(pNode, false);
            }
            a = $("#" + node.tId + "_a");
            a.addClass("cur");

            var isOpen = false;
            for (var i = 0, l = node.children.length; i < l; i++) {
                if (node.children[i].open) {
                    isOpen = true;
                    break;
                }
            }
            if (isOpen) {
                zTree_Menu.expandNode(node, true);
                curMenu = node;
            } else {
                zTree_Menu.expandNode(node.children[0].isParent ? node.children[0] : node, true);
                curMenu = node.children[0];
            }
        } else {
            zTree_Menu.expandNode(node);
        }
    }
    return !node.isParent;
}
function onClick(e, treeId, treeNode) {
    var zTree = $.fn.zTree.getZTreeObj("treeDemo");
    zTree.checkNode(treeNode, !treeNode.checked, null, true);
    return false;
}

function onCheck(e, treeId, treeNode) {
    var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
        nodes = zTree.getCheckedNodes(true),
        v = "";
    for (var i = 0, l = nodes.length; i < l; i++) {
        v += nodes[i].name + ",";
    }
    if (v.length > 0) v = v.substring(0, v.length - 1);
    var cityObj = $("#citySel");
    cityObj.attr("value", v);
}

/**
 * 新增菜单
 * */
function addMenu() {
    var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
        nodes = zTree.getCheckedNodes(true),
        v = "";
    for (var i = 0, l = nodes.length; i < l; i++) {
        v += nodes[i].id + ",";
    }
    if (v.length > 0) v = v.substring(0, v.length - 1);
    win.init({title: '添加菜单', rel: ''}).show('edit_menu_pop.do?pid=' + v, {});
}

/**
 * 修改菜单
 * */
function editMenu() {
    var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
        nodes = zTree.getCheckedNodes(true),
        v = "";
    for (var i = 0, l = nodes.length; i < l; i++) {
        v += nodes[i].id + ",";
    }
    if (v.length > 0) {
        v = v.substring(0, v.length - 1)
        win.init({title: '修改菜单', rel: ''}).show('edit_menu_pop.do?id=' + v, {});
    } else {
        layer.alert("请选择一个菜单!");
    }

}

/**
 * 删除菜单
 * */
function deleteMenu() {
    var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
        nodes = zTree.getCheckedNodes(true),
        v = "";
    for (var i = 0, l = nodes.length; i < l; i++) {
        v += nodes[i].id + ",";
    }
    if (v.length > 0) {
        v = v.substring(0, v.length - 1)
        layer.confirm('是否确定删除菜单?', {icon: 3, title: '提示'}, function (index) {
            //do something
            $.ajax({
                type: 'post',
                url: "delete_menu.do",
                dataType: "json",
                data: {"id": v},
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                },
                success: function (data) {
                    if (data.success) {
                        if (data.data.type == 'error') {
                            var message = data.data.message;
                            layer.alert(message);
                        } else {
                            var message = data.data;
                            layer.alert(message, function (index) {
                                window.location.reload();
                            });
                        }
                    } else {

                    }
                }
            });

            layer.close(index);
        });
    } else {
        layer.alert("请选择一个菜单!");
    }
}

$(document).ready(function () {
    $.fn.zTree.init($("#treeDemo"), setting, $.parseJSON($("#menuTreeData").text()));
    zTree_Menu = $.fn.zTree.getZTreeObj("treeDemo");
    curMenu = zTree_Menu.getNodes()[0].children[0].children[0];
    zTree_Menu.selectNode(curMenu);
    var a = $("#" + zTree_Menu.getNodes()[0].tId + "_a");
    a.addClass("cur");
});