$(function () {
    $( document ).tooltip();
    String.prototype.trim = function () {
        return this.replace(/(^\s*)|(\s*$)/g, '');
    };

});
var flag;
var username = /^[a-zA-Z\d]\w{0,16}[a-zA-Z\d]$/;
var IP=/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
function check(){
    var ipStr = $("input[name='machineIp']").val();
    var userStr = $("input[name='loginUserName']").val();
    //loginUserName验证
    if($("input[name='loginUserName']").val() == "" || !(username.test(userStr))){
        layer.tips("用户名必须为长度为2~16位的英文字母或数字或下划线","input[name='loginUserName']",{tips: [2, '#259de0']});
        return false;
    }
    //ip地址验证
    if (ipStr == "" || !(IP.test(ipStr))) {
        layer.tips("请输入正确的IP格式","input[name='machineIp']",{tips: [2, '#259de0']});
        return false;
    }
    //机器类型验证
    if($("select[name='machineType.machineTypeId']").val() == "0"){
        layer.tips("请选择正确的机器类别","select[name='machineType.machineTypeId']",{tips: [2, '#259de0']});
        return false;
    }
    //密码验证
    if($("input[name='loginPassWord']").val() == ""){
        layer.tips("密码不能为空","input[name='loginPassWord']",{tips: [2, '#259de0']});
        return false;
    }
    if($("input[name='loginPassWord']").val() != $("input[name='loginPassWord2']").val()){
        layer.tips("两次密码不同","input[name='loginPassWord']",{tips: [2, '#259de0']});
        return false;
    }
    //集群名称验证
    if($("select[name='clusterType.clusterTypeId']").val() == "0"){
        layer.tips("请选择正确的集群类型","select[name='clusterType.clusterTypeId']",{tips: [2, '#259de0']});
        return false;
    }
    //机器状态验证
    if($("select[name='mcStatus']").val() == "-1") {
        layer.tips("请选择正确的机器状态","select[name='mcStatus']",{tips: [2, '#259de0']});
        return false;
    }
    checkNameIp();
    if(flag){
        $("input[name='machineIp']").val(ipStr);
        $("form").submit();
        win.close()
    }

}

function checkNameIp(){
    var ipStr = $("input[name='machineIp']").val()
    if ($("input[name='loginUserName']").val() != "" && $("input[name='machineIp']").val() != "" && username.test($("input[name='loginUserName']").val()) && IP.test(ipStr)
        && $("select[name='machineType.machineTypeId']").val() != "0" && $("select[name='clusterType.clusterTypeId']").val() != "0"){
        $.ajax({
            url:"check_name_ip.do",
            data: "loginUserName=" + $("input[name='loginUserName']").val() + "&machineIp=" + ipStr + "&machineType.machineTypeId=" + $("select[name='machineType.machineTypeId']").val() +
            "&clusterType.clusterTypeId="+$("select[name='clusterType.clusterTypeId']").val(),
            type:"post",
            async: false,
            dataType:"text",
            success:function(data){
                if("success" == data){
//                        alert("用户名和机器IP对应的机器类型及集群状态已存在，请更换用户名或机器IP或机器类型或集群状态")
                    $("#userIp").show();
                    flag = false;
                }
                if("error" == data){
                    flag = true;
                    $("#userIp").hide();
                }

            },
            error:function(){
                alert("error");
                return false;
            }
        })
    }
}
function labelHide(){
    $("#userIp").hide();
}