package com.ideal.hadoopadmin.web.controller.meta.privilege;

import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.common.entity.Result;
import com.ideal.hadoopadmin.entity.cluster.ClusterType;
import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import com.ideal.hadoopadmin.entity.cluster.Parameter;
import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsInfo;
import com.ideal.hadoopadmin.entity.system.company.SystemCompany;
import com.ideal.hadoopadmin.framework.message.WebMessageLevel;
import com.ideal.hadoopadmin.framework.page.Page;
import com.ideal.hadoopadmin.framework.web.HttpRequests;
import com.ideal.hadoopadmin.framework.web.json.JsonObject;
import com.ideal.hadoopadmin.security.ShiroDbRealm;
import com.ideal.hadoopadmin.service.cluster.ClusterTypeService;
import com.ideal.hadoopadmin.service.cluster.ClusterUserService;
import com.ideal.hadoopadmin.service.cluster.ParameterService;
import com.ideal.hadoopadmin.service.meta.hdfs.MetaHdfsInfoBakService;
import com.ideal.hadoopadmin.service.meta.hdfs.MetaHdfsInfoService;
import com.ideal.hadoopadmin.service.meta.privilege.MetaAccessPrivilegeService;
import com.ideal.hadoopadmin.service.system.company.SystemCompanyService;
import com.ideal.hadoopadmin.web.controller.CommonDictionary;
import com.ideal.hadoopadmin.web.controller.UIController;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Controller
@RequestMapping(MetaAccessPrivilegeController.PORTAL_PREFIX)
public class MetaAccessPrivilegeController extends UIController {
    public final static String PORTAL_PREFIX = "/meta/privilege";
    private Logger logger = LoggerFactory.getLogger(MetaAccessPrivilegeController.class);
    @Resource
    private MetaAccessPrivilegeService metaAccessPrivilegeService;

    @RequestMapping("config_access_privilege")
    public void configAccessPrivilege(String type,String userIds, Long resourceId, HttpServletRequest request) {
        request.setAttribute("type",type);
        request.setAttribute("userIds", userIds);
        request.setAttribute("resourceId",resourceId);
    }

    /**
     * 查询权限
     */
    @RequestMapping("query_access_privilege")
    public void queryAccessPrivilege(String type,String userIds, Long resourceId,HttpServletRequest request) {
        request.setAttribute("type",type);
        request.setAttribute("userIds", userIds);
        request.setAttribute("resourceId",resourceId);
        PageInfo page = metaAccessPrivilegeService.pageAccessPrivilege(type, request);
        request.setAttribute("page", page);
    }


    /**
     * 添加权限
     */
    @RequestMapping("add_privilege")
    @ResponseBody
    public JsonObject add_privilege(Long resourceId,String userId,Long[] ids,String hiveAccessId) {
        Result result = metaAccessPrivilegeService.addPrivilege(resourceId, userId, ids, hiveAccessId);
        String message = StringUtils.strip(result.getMessageList().toString(),"[]");
        if(result.getFlag()){
            return JsonObject.alert(message, WebMessageLevel.SUCCESS);
        }
        return JsonObject.alert(message,WebMessageLevel.ERROR);
    }
    @Override
    public String getPortalPrefix() {
        return PORTAL_PREFIX;
    }
}
