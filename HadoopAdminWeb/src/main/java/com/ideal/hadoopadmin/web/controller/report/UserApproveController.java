package com.ideal.hadoopadmin.web.controller.report;

import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.common.entity.UserApproveForm;
import com.ideal.hadoopadmin.entity.cluster.ClusterMachine;
import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import com.ideal.hadoopadmin.entity.cluster.user.KbrAuth;
import com.ideal.hadoopadmin.service.cluster.ClusterMachineService;
import com.ideal.hadoopadmin.service.cluster.ClusterUserService;
import com.ideal.hadoopadmin.service.cluster.user.KbrAuthService;
import com.ideal.hadoopadmin.utils.ExUtils;
import com.ideal.hadoopadmin.web.controller.UIController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by fwj on 2016/3/4.
 * 用户认证信息报表
 */
@Controller
@RequestMapping(UserApproveController.PORTAL_PREFIX)
public class UserApproveController extends UIController {
    public final static String PORTAL_PREFIX = "/report/userApprove";
    private Logger logger = LoggerFactory.getLogger(UserApproveController.class);

    @Resource
    private KbrAuthService kbrAuthService;
    @Resource
    private ClusterUserService clusterUserService;
    @Resource
    private ClusterMachineService clusterMachineService;
    @RequestMapping("userApprove_list")
    public void index(Model model, HttpServletRequest request) {
        try {
            Map<String, Object> searchParams = WebUtils.getParametersStartingWith(request, "Q_");
            //默认不加载数据，只有当点击搜索的时候，才加载数据
            if(null != request.getParameter("isQuery") && "query".equals(request.getParameter("isQuery").toString())){
                PageInfo page = kbrAuthService.queryKbrAuthByPage(searchParams, request);
                model.addAttribute("page", page);
                List<KbrAuth> kbrAuthList=page.getList();
                List<UserApproveForm> userApproveFormList = new ArrayList<UserApproveForm>();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
                for (int i = 0; i < kbrAuthList.size(); i++) {
                    UserApproveForm userApproveForm = new UserApproveForm();
                    try {
                        Date ct = new Date(kbrAuthList.get(i).getCreateTime());
                        String createTime = sdf.format(ct);
                        userApproveForm.setCreateTime(createTime);
                        Date et = new Date(kbrAuthList.get(i).getEndTime());
                        String endTime = sdf.format(et);
                        userApproveForm.setEndTime(endTime);
                        userApproveForm.setHpid(kbrAuthList.get(i).getUserId().getId().toString());
                        userApproveForm.setName(kbrAuthList.get(i).getUserId().getUserName());
                        Date st = new Date(kbrAuthList.get(i).getCreateTime());
                        String startTime = sdf.format(st);
                        userApproveForm.setStartTime(startTime);
                        userApproveForm.setPrinc(kbrAuthList.get(i).getPrincipal());
                        userApproveForm.setPath(kbrAuthList.get(i).getTicketPath());
                        userApproveForm.setStatus(kbrAuthList.get(i).getStatus());
                        userApproveForm.setIp(kbrAuthList.get(i).getMachineId().getMachineIp());
                        if (kbrAuthList.get(i).getStatus() == 1) {
                            userApproveForm.setRed(true);
                        } else if (kbrAuthList.get(i).getEndTime() != null) {
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            Date end = new Date(kbrAuthList.get(i).getEndTime());
                            if (end.before(new Date())) {
                                userApproveForm.setRed(true);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    userApproveFormList.add(userApproveForm);
                }
                model.addAttribute("userApproveFormList",userApproveFormList);
            }

            List<ClusterUser> clusterUserList = clusterUserService.queryClusterUser();
            model.addAttribute("hadoopUserList", clusterUserList);
            //查找所有的机器
            List<ClusterMachine> clusterMachines = clusterMachineService.findAllMachine();
            model.addAttribute("clusterMachines",clusterMachines);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.getMessage());
        }
    }

    //    @RequestMapping("dataPur_list")
//    public void data(HttpServletRequest request,Model model){
//        String val = request.getParameter("Q_hadoopUser");
//        Page<DataPurForm> page = userApproveService.dataPur(val,buildPageRequest(request));
//        model.addAttribute("val",val);
//        model.addAttribute("page",page);
//    }
    public static final String[] FIELDS = new String[]{
            "name", "ip", "princ", "startTime", "endTime",
    };

    public static final String[] HEADER = new String[]{
            "名字", "机器ip", "princple", "开始时间", "结束时间"
    };

    //
//
    @RequestMapping("down_app")
    public void down(HttpServletResponse response, HttpServletRequest request, String username) {
        try {
            List<UserApproveForm> li = kbrAuthService.queryKbrAuthByDown(username);
            ExUtils<UserApproveForm> exUtils = new ExUtils<UserApproveForm>();
            exUtils.exportExcel("用户认证信息报表.xls", li, response, HEADER, FIELDS);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.getMessage());
        }
    }

    //
//    @RequestMapping("down_pur")
//    public void downPur(HttpServletRequest request,HttpServletResponse response,String username){
//        Page<DataPurForm> page = userApproveService.dataPur(username,buildPageRequest(request,1000));
//        ExUtils<DataPurForm> exUtils = new ExUtils<DataPurForm>();
//        exUtils.exportExcel("HDFSHIVE权限报表.xls",page.getContent(),response, DataPur.HEADER,DataPur.FILED);
//    }
    @Override
    public String getPortalPrefix() {
        return PORTAL_PREFIX;
    }


}
