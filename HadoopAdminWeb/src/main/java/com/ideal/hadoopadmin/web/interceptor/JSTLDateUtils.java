package com.ideal.hadoopadmin.web.interceptor;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by 袁颖 on 2016/3/2.
 * 将long类型的转换成对应的date型再根据对应的格式转成string类型输出
 */
public class JSTLDateUtils extends TagSupport {

    private static final long serialVersionUID = -3354015192721342312L;
    private String value;
    private String pattern;
    public void setValue(String value) {
        this.value = value;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public int doStartTag() throws JspException {
        if(value!=null&&!"".equals(value.trim())){
            String vv = String.valueOf(value);
            long time = Long.valueOf(vv);
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(time);
            SimpleDateFormat dateformat = new SimpleDateFormat(pattern);
            String s = dateformat.format(c.getTime());
            try {
            pageContext.getOut().write(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
        }else {
            pattern = value;
        }
        return super.doStartTag();
    }

}
