package com.ideal.hadoopadmin.web.controller.system.menu;

import com.ideal.hadoopadmin.entity.main.Menu;
import com.ideal.hadoopadmin.framework.message.WebMessageLevel;
import com.ideal.hadoopadmin.framework.web.json.JsonObject;
import com.ideal.hadoopadmin.service.main.SystemMenuService;
import com.ideal.hadoopadmin.web.controller.UIController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by fwj on 16-3-7.
 * 菜单管理
 */
@Controller
@RequestMapping(MenuManageController.PORTAL_PREFIX)
public class MenuManageController extends UIController {
    public final static String PORTAL_PREFIX = "/system/menu";
    private Logger logger = LoggerFactory.getLogger(MenuManageController.class);

    @Resource
    public SystemMenuService systemMenuService;

    @RequestMapping("menu_manage")
    public void index(HttpServletRequest request, Model model) {
        try {
            String menuList = systemMenuService.quereyMenuList();
            model.addAttribute("menuTreesData", menuList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 新增/编辑菜单
     */
    @RequestMapping("edit_menu_pop")
    public void edit(Model model, @RequestParam(value = "id", required = false) Long id, Long pid) {
        try {
            Menu menuInfo = null;
            if (id != null) {
                menuInfo = systemMenuService.getMenu(id);
            }
            List<Menu> parentMenus = systemMenuService.getMenuByParentId();
            List<Menu> selectMenus = systemMenuService.getMenuBySecond();
            model.addAttribute("parentMenus", parentMenus);
            model.addAttribute("selectMenus", selectMenus);
            model.addAttribute("menuInfo", menuInfo);
            model.addAttribute("pid", pid);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.getMessage());
        }

    }

    /**
     * 切换类型
     */
    @RequestMapping("select_menu")
    @ResponseBody
    public List<Menu> selectMenu(long type) {
        if (type == 1) {
            List<Menu> parentMenus = systemMenuService.getMenuByParentId();
            return parentMenus;
        } else {
            List<Menu> selectMenus = systemMenuService.getMenuBySecond();
            return selectMenus;
        }
    }

    @RequestMapping("save_menu")
    @ResponseBody
    public JsonObject save(Model model, @RequestParam(value = "id") Long id,
                           @RequestParam(required = false) String title,
                           @RequestParam(required = false) Long type,
                           @RequestParam(required = false) String url,
                           @RequestParam(required = false) String cssClass,
                           @RequestParam(required = false) String code,
                           @RequestParam(required = false) Long parentId,
                           @RequestParam(required = false) Long createTime,
                           @RequestParam(required = false) Long sort,
                           @RequestParam(required = false) int isHidden) {
        try {
            Menu menu = new Menu();
            if (id != null) {
                menu.setId(id);
                menu.setTitle(title);
                if (type==2){
                    menu.setIsParent(1);
                }
                menu.setType(Integer.parseInt(type.toString()));
                menu.setUrl(url);
                menu.setCode(code);
                menu.setParentId(parentId);
                menu.setCssClass(cssClass);
                if (sort == null) {
                    menu.setSort(0);
                } else {
                    menu.setSort(Integer.parseInt(sort.toString()));
                }
                if(parentId==null){
                    menu.setIsParent(0);
                }else{
                    menu.setIsParent(1);
                }
                menu.setIsHidden(isHidden);
                menu.setCreateTime(createTime);
                systemMenuService.updateMenu(menu);
                return JsonObject.success("修改菜单成功");
            } else {
                menu.setTitle(title);
                if (type==2){
                    menu.setIsParent(1);
                }
                menu.setType(Integer.parseInt(type.toString()));
                menu.setUrl(url);
                menu.setCode(code);
                menu.setParentId(parentId);
                menu.setCssClass(cssClass);
                if (sort == null) {
                    menu.setSort(0);
                } else {
                    menu.setSort(Integer.parseInt(sort.toString()));
                }
                if(parentId==null){
                    menu.setIsParent(0);
                }else{
                    menu.setIsParent(1);
                }
                menu.setIsHidden(isHidden);
                menu.setCreateTime(new Date().getTime());
                systemMenuService.saveMenu(menu);
                return JsonObject.success("添加菜单成功");
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.getMessage());
        }
        return JsonObject.error("添加/编辑菜单失败!");
    }

    /**
     * 删除菜单
     */
    @RequestMapping("delete_menu")
    @ResponseBody
    public JsonObject delete(@RequestParam(value = "id") Long id) {
        try {
            if (id != null) {
                if ("1".equals(systemMenuService.deleteMenu(id))) {
                    return JsonObject.success("删除成功!");
                } else {
                    return JsonObject.alert("父级菜单不能被删除！", WebMessageLevel.ERROR);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.getMessage());
        }
        return JsonObject.error("删除菜单失败!");
    }

    @Override
    public String getPortalPrefix() {
        return PORTAL_PREFIX;
    }
}
