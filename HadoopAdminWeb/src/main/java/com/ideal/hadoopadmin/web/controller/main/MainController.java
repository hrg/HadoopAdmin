package com.ideal.hadoopadmin.web.controller.main;

import com.ideal.hadoopadmin.service.main.SystemMenuService;
import com.ideal.hadoopadmin.service.meta.hdfs.MetaHdfsInfoBakService;
import com.ideal.hadoopadmin.service.meta.hdfs.MetaHdfsInfoService;
import com.ideal.hadoopadmin.web.controller.UIController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CC on 2016/2/16.
 */
@Controller
@RequestMapping(MainController.PORTAL_PREFIX)
public class MainController extends UIController {
    public final static String PORTAL_PREFIX = "/main";
    @Resource
    MetaHdfsInfoBakService metaHdfsInfoBakService;
    @Resource
    MetaHdfsInfoService metaHdfsInfoService;
    @Override
    public String getPortalPrefix() {
        return PORTAL_PREFIX;
    }

    @RequestMapping("home_page")
    public void index(HttpServletRequest request) {
//        Integer count = eventInfoService.getEventInfo();
//        HttpSession httpSession = request.getSession();
//        if (count != 0) {
//            httpSession.setAttribute("eventInfoCount", count);
//        }
    }

    @RequestMapping("countHDFS")
    @ResponseBody
    public Map<String,Object> countHDFS(HttpServletRequest request){
        List list1 = metaHdfsInfoBakService.compareWithHdfsInfo();
        List list2 = metaHdfsInfoService.compareWithHdfsInfoBak();
        Integer countHDFS = list1.size() + list2.size();
        Map<String,Object> data = new HashMap<String, Object>();
        data.put("data",countHDFS);
        request.getSession().setAttribute("countHDFS",countHDFS);
        return data;
    }

}
