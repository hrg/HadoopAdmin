package com.ideal.hadoopadmin.web.controller.cluster;

import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.entity.cluster.ClusterType;
import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import com.ideal.hadoopadmin.entity.cluster.DataTransfer;
import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsInfo;
import com.ideal.hadoopadmin.service.cluster.ClusterTypeService;
import com.ideal.hadoopadmin.service.cluster.ClusterUserService;
import com.ideal.hadoopadmin.service.cluster.DataTransferService;
import com.ideal.hadoopadmin.service.meta.hdfs.MetaHdfsInfoService;
import com.ideal.hadoopadmin.web.controller.UIController;
import com.ideal.hadoopadmin.framework.page.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 袁颖 on 2016/4/19.
 */
@Controller
@RequestMapping(DataTranferController.PORTAL_PREFIX)
public class DataTranferController extends UIController{
    public static final String PORTAL_PREFIX = "/cluster/transfer";
    @Resource
    private DataTransferService dataTransferService;
    @Resource
    private ClusterTypeService clusterTypeService;
    @Resource
    private ClusterUserService clusterUserService;
    @Resource
    MetaHdfsInfoService metaHdfsInfoService;
    @RequestMapping("transfer_list")
    public void transferList(HttpServletRequest request){
        PageInfo page = dataTransferService.searchPage(Page.getCurrentPage(request), Page.DEFAULT_PAGESIZE, Page.DEFAULT_ORDER);
        List<ClusterType> clusterTypeList = clusterTypeService.findAllClusterType();
        request.setAttribute("clusterTypes",clusterTypeList);
        request.setAttribute("page",page);
    }
    @RequestMapping("findClusterUser")
    @ResponseBody
    public Map<String,Object> findClusterUserByClusterType(Long clusterTypeId){
        Map<String,Object> map = new HashMap<String, Object>();
        List<ClusterUser> clusterUsers = clusterUserService.findClusterUserByClusterType(clusterTypeId);
        List<ClusterType> clusterTypes = clusterTypeService.findOtherClusterType(clusterTypeId);
        map.put("clusterUsers",clusterUsers);
        map.put("clusterTypes",clusterTypes);
        return map;
    }
    @RequestMapping("findHdfsPath")
    @ResponseBody
    public List<MetaHdfsInfo> findHdfsPathByClusterUser(Long clusterUserId){
        List<MetaHdfsInfo> hdfsInfos = metaHdfsInfoService.findPathByUserId(clusterUserId);
        return hdfsInfos;
    }
   @RequestMapping("transferDataSource")
   public String transfer(String start,String end,String run,DataTransfer transfer) throws ParseException {
       SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
       SimpleDateFormat monthFormat = new SimpleDateFormat("yyyy-MM");
       Long startDate = 0L;
       Long endDate = 0L;
       Long runDate = 0L;
       if(StringUtils.isNotBlank(start)){
           startDate=simpleDateFormat.parse(start).getTime();
       }
       if(StringUtils.isNotBlank(end)){
           endDate = simpleDateFormat.parse(end).getTime();
       }
       if(StringUtils.isNotBlank(run)){
           runDate=monthFormat.parse(run).getTime();
       }
       if(transfer.getRunCycle()==null){
           transfer.setRunCycle(0);//暂时设置为0
       }
       transfer.setRunTime(runDate);
       transfer.setStartTime(startDate);
       transfer.setEndTime(endDate);
       transfer.setStatus(0);//插入数据即为开启状态
       dataTransferService.transfer(transfer);
       return "redirect:/cluster/transfer/transfer_list.do";
   }
    @RequestMapping("openStatus")
    public String openStatus(Long id){
        dataTransferService.openStatusById(id);
        return "redirect:/cluster/transfer/transfer_list.do";
    }
    @RequestMapping("stopStatus")
    public String stopStatus(Long id){
        dataTransferService.stopStatusById(id);
        return "redirect:/cluster/transfer/transfer_list.do";
    }
    //删除数据
    @RequestMapping("deleteTransferData")
    public String delete(Long id){
        dataTransferService.deleteById(id);
        return "redirect:/cluster/transfer/transfer_list.do";
    }
    @Override
    public String getPortalPrefix() {
        return PORTAL_PREFIX;
    }
}
