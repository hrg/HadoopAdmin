package com.ideal.hadoopadmin.web.controller.system.company;

import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.entity.cluster.ClusterCompanyQuota;
import com.ideal.hadoopadmin.entity.system.company.SystemCompany;
import com.ideal.hadoopadmin.framework.message.WebMessageLevel;
import com.ideal.hadoopadmin.framework.web.json.JsonObject;
import com.ideal.hadoopadmin.service.cluster.ClusterCompanyQuotaService;
import com.ideal.hadoopadmin.service.system.company.*;
import com.ideal.hadoopadmin.web.controller.UIController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by fwj on 16-2-22.
 * 租户管理
 */
@Controller
@RequestMapping(SystemCompanyController.PORTAL_PREFIX)
public class SystemCompanyController extends UIController {
    public final static String PORTAL_PREFIX = "/system/company";
    private Logger logger = LoggerFactory.getLogger(SystemCompanyController.class);

    @Resource
    private SystemCompanyService systemCompanyService ;
    @Resource
    private ClusterCompanyQuotaService clusterCompanyQuotaService;

    @RequestMapping("system_company")
    public void clusterCompany(HttpServletRequest request) {
        try{
            Map<String,Object> searchParams= WebUtils.getParametersStartingWith(request, "Q_");
            searchParams.put("EQ_delTag",0);
            PageInfo page=systemCompanyService.queryClusterCompany(searchParams,request);
            request.setAttribute("page",page);
        }catch (Exception e){
            e.printStackTrace();
            logger.info(e.getMessage());
        }
    }

    @RequestMapping("edit_customer_pop")
    public void edit(Model model, @RequestParam(value = "id", required = false) Long id) {
        SystemCompany customerInfo = null;
        ClusterCompanyQuota clusterCompanyQuota = null;
        if (id != null) {
            customerInfo = systemCompanyService.findSystemCompanyById(id);
            clusterCompanyQuota = clusterCompanyQuotaService.findSystemCompanyById(id);
        }
        model.addAttribute("customerInfo", customerInfo);
        model.addAttribute("clusterCompanyQuota", clusterCompanyQuota);
    }

    @RequestMapping("save_systemCompany")
    @ResponseBody
    public JsonObject save(Model model,SystemCompany systemCompany,ClusterCompanyQuota clusterCompanyQuota,HttpServletRequest request){
        try{
            String clusterCompanyQuotaId = request.getParameter("clusterCompanyQuotaId");
            systemCompanyService.saveSystemCompany(systemCompany,clusterCompanyQuota,clusterCompanyQuotaId);
            return JsonObject.alert("新建/修改租户成功!", WebMessageLevel.SUCCESS);
        }catch (Exception e){
            e.printStackTrace();
            logger.info(e.getMessage());
        }
        return JsonObject.error("添加租户信息失败!");
    }

    @RequestMapping("delete_customer")
    @ResponseBody
    public JsonObject delete(@RequestParam(value = "id") Long[] ids) {
        if(ids != null) {
            systemCompanyService.deleteSystemCompanById(ids);
        }
        return JsonObject.alert("删除成功", WebMessageLevel.SUCCESS);
    }
    @Override
    public String getPortalPrefix() {
        return PORTAL_PREFIX;
    }
}
