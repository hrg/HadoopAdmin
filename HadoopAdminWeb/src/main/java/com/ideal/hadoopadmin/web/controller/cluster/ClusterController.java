package com.ideal.hadoopadmin.web.controller.cluster;

import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.entity.cluster.ClusterType;
import com.ideal.hadoopadmin.framework.message.WebMessageLevel;
import com.ideal.hadoopadmin.framework.web.json.JsonObject;
import com.ideal.hadoopadmin.service.cluster.ClusterTypeService;
import com.ideal.hadoopadmin.web.controller.UIController;
import com.ideal.hadoopadmin.framework.page.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by 袁颖 on 2016/4/7.
 */
@Controller
@RequestMapping(ClusterController.PORTAL_PREFIX)
public class ClusterController extends UIController {
    public static final String PORTAL_PREFIX = "cluster/type";
    @Resource
    private ClusterTypeService clusterTypeService;

    //查找所有的cluster_cluster_type数据
    @RequestMapping("cluster_list")
    public void clusterList(HttpServletRequest request) {
        PageInfo pageInfo = clusterTypeService.pageCluster(null, Page.getCurrentPage(request), Page.DEFAULT_PAGESIZE);
        request.setAttribute("page", pageInfo);
    }

    @RequestMapping("edit_cluster_pop")
    public void edit(Long id, HttpServletRequest request) {
        ClusterType clusterType = clusterTypeService.findById(id);
        request.setAttribute("clusterType",clusterType);
    }

    //根据ids进行删除
    @RequestMapping("delete_cluster")
    @ResponseBody
    public JsonObject deleteByIds(@RequestParam(value = "id")Long[] ids) {
        for (int i = 0; i < ids.length; i++) {
            clusterTypeService.deleteById(ids[i]);
        }
        return JsonObject.alert("删除成功", WebMessageLevel.SUCCESS);
    }

    //保存
    @RequestMapping("save_cluster")
    @ResponseBody
    public JsonObject save(ClusterType clusterType) {
        //判断是修改还是新建
        if(clusterType.getId()==null){
            //设置clusterTypeId查出来的最大clusterTypeId+1
            Long maxClusterTypeId = clusterTypeService.findMaxClusterTypeIds();
            clusterType.setClusterTypeId(maxClusterTypeId+1);
            clusterTypeService.save(clusterType);
        }else {
            clusterTypeService.update(clusterType);
        }
       return JsonObject.success();
    }

//    //更新
//    public void update(ClusterType clusterType) {
//        clusterTypeService.update(clusterType);
//    }

    @Override
    public String getPortalPrefix() {
        return PORTAL_PREFIX;
    }
}
