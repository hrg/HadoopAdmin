package com.ideal.hadoopadmin.web.controller.report;
import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.entity.cluster.user.Queue;
import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsInfo;
import com.ideal.hadoopadmin.framework.page.Page;
import com.ideal.hadoopadmin.service.meta.hdfs.MetaHdfsInfoService;
import com.ideal.hadoopadmin.web.controller.UIController;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by 袁颖 on 2016/5/9.
 */
@Controller
@RequestMapping(HdfsPathAccessController.PORTAL_PREFIX)
public class HdfsPathAccessController extends UIController{
    public static final String PORTAL_PREFIX = "/report/hdfsPathAccess";
    @Resource
    private MetaHdfsInfoService metaHdfsInfoService;
    @Override
    public String getPortalPrefix() {
        return PORTAL_PREFIX;
    }

    /**
     * hdfs第一级数据
     * add20160721qinfengxia
     * @param request
     */
    @RequestMapping("hdfsPathAccess_list")
    public void hdfsPathParent(HttpServletRequest request){
        Map<String,Object> searchParam = WebUtils.getParametersStartingWith(request,"Q_");
        //默认不加载数据，只有当点击搜索的时候，才加载数据
        if(null != request.getParameter("isQuery") && "query".equals(request.getParameter("isQuery").toString())){
            PageInfo hdfsInfoCustoms = metaHdfsInfoService.searchReportPage(Page.getCurrentPage(request), Page.DEFAULT_PAGESIZE, searchParam);
            request.setAttribute("page",hdfsInfoCustoms);
        }
        List<MetaHdfsInfo> metaHdfsInfos = metaHdfsInfoService.findAllHdfsInfo();
        request.setAttribute("metaHdfsInfos",metaHdfsInfos);
    }

    /**
     * hdfs第二级数据（权限）
     * add20160721qinfengxia
     * @param clusterUserId
     * @param hdfsPath
     * @return
     */
    @RequestMapping("hdfsPathAccessChild_list")
    @ResponseBody
    public List<Map<String,Object>> hdfsPathChild(Integer clusterUserId,String hdfsPath){
        List<Map<String,Object>> hdfsPathAccessChild = metaHdfsInfoService.searchReportChildPage(clusterUserId,hdfsPath);
        return hdfsPathAccessChild;
    }

    /**
     * 下载结果
     * add20160721qinfengxia
     * @param response
     * @param hdfsPath
     */
    @RequestMapping("down_all")
    public void downAll(HttpServletResponse response,String hdfsPath) {
        List<Map<String,Object>> hdfsList = metaHdfsInfoService.queryHdfsByDown(hdfsPath);
        OutputStream outputStream = null;
        try {
            response.setCharacterEncoding("utf-8");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition", "attachment;fileName=hdfs_list.xls");
            HSSFWorkbook hssfWorkbook = metaHdfsInfoService.creatSheet(hdfsList);
            outputStream = response.getOutputStream();
            hssfWorkbook.write(outputStream);
            response.flushBuffer();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 下载结果
     * add20160918qinfengxia
     * @param response
     */
    @RequestMapping("down_all_hdfs_user")
    public void downAllHdfsUser(HttpServletResponse response,String userName) {
        List<Map<String,Object>> hdfsList = metaHdfsInfoService.queryHdfsUserByDown(userName);
        OutputStream outputStream = null;
        try {
            response.setCharacterEncoding("utf-8");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition", "attachment;fileName=hdfs_list.xls");
            HSSFWorkbook hssfWorkbook = metaHdfsInfoService.creatSheetHdfsUser(hdfsList);
            outputStream = response.getOutputStream();
            hssfWorkbook.write(outputStream);
            response.flushBuffer();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * hdfs(用户主导)第一级数据
     * add20160918qinfengxia
     * @param request
     */
    @RequestMapping("hdfsUserAccess_list")
    public void hdfsUserParent(HttpServletRequest request){
        Map<String,Object> searchParam = WebUtils.getParametersStartingWith(request,"Q_");
        //默认不加载数据，只有当点击搜索的时候，才加载数据
        if(null != request.getParameter("isQuery") && "query".equals(request.getParameter("isQuery").toString())){
            PageInfo hdfsInfoCustoms = metaHdfsInfoService.searchHdfsUserPage(Page.getCurrentPage(request), Page.DEFAULT_PAGESIZE, searchParam);
            request.setAttribute("page",hdfsInfoCustoms);
        }
    }


    /**
     * hdfs（用户主导）第二级数据
     * add20160721qinfengxia
     * @param clusterUserId
     * @return
     */
    @RequestMapping("hdfsUserChild_list")
    @ResponseBody
    public List<Map<String,Object>> hdfsUserChild(Integer clusterUserId){
        List<Map<String,Object>> hdfsUserChild = metaHdfsInfoService.searchHdfsUserChildPage(clusterUserId);
        return hdfsUserChild;
    }
}
