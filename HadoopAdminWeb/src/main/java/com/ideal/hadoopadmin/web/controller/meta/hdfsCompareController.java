package com.ideal.hadoopadmin.web.controller.meta;

import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsInfo;
import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsInfoBak;
import com.ideal.hadoopadmin.framework.web.json.JsonObject;
import com.ideal.hadoopadmin.service.meta.hdfs.MetaHdfsInfoBakService;
import com.ideal.hadoopadmin.service.meta.hdfs.MetaHdfsInfoService;
import com.ideal.hadoopadmin.web.controller.UIController;
import com.ideal.hadoopadmin.framework.page.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 袁颖 on 2016/4/26.
 */
@Controller
@RequestMapping(hdfsCompareController.PORTAL_PREFIX)
public class hdfsCompareController extends UIController{
    public static final String PORTAL_PREFIX = "/meta/hdfsCompare";
    @Resource
    private MetaHdfsInfoBakService metaHdfsInfoBakService;
    @Resource
    private MetaHdfsInfoService metaHdfsInfoService;
    @RequestMapping("compareHdfs")
    public void CompareHdfsList(){
    }

    /**
     * dbList 表
     * @param request
     */
    @RequestMapping("query_dbList_pop")
    public void dbList(HttpServletRequest request,String Q_LIKE_hdfsPath) {
        Map<String,Object> searchParam =new HashMap<String, Object>();
        if(StringUtils.isNotBlank(Q_LIKE_hdfsPath)){
            searchParam.put("LIKE_hdfsInfoBak.hdfsPath",Q_LIKE_hdfsPath);
        }
        PageInfo page = metaHdfsInfoBakService.hdfsCompare(Page.getCurrentPage(request), searchParam);
        request.setAttribute("hdfsPageInfoBak",page);
        request.setAttribute("divId","dbList");
    }
    /**
     * 添加hdfs 在hdfs_info插入数据即可
     * @param id
     * @return
     */
    @RequestMapping("addHdfs")
    @ResponseBody
    public JsonObject addHDFS(Long id) {
        //通过id,查找出对应的hdfsInfoBak数据放入hdfsInfo表中
        MetaHdfsInfoBak metaHdfsInfoBak = metaHdfsInfoBakService.queryMetaHdfsInfoBakById(id);
        MetaHdfsInfo metaHdfsInfo =  new MetaHdfsInfo();
        metaHdfsInfo.setHdfsPath(metaHdfsInfoBak.getHdfsPath());
        metaHdfsInfo.setUserId(metaHdfsInfoBak.getClusterUserId());
        metaHdfsInfo.setHdfsGroup(metaHdfsInfoBak.getHdfsGroup());
        metaHdfsInfo.setHdfsOwner(metaHdfsInfoBak.getHdfsOwner());
        metaHdfsInfo.setHdfsPerm(metaHdfsInfoBak.getHdfsPerm());
        metaHdfsInfo.setCreateTime(System.currentTimeMillis());
        metaHdfsInfo.setNote("");
        metaHdfsInfoService.saveHdfsDB(metaHdfsInfo);
        return JsonObject.success("添加到hdfsInfo成功!");
    }
    /**
     * 删除hdfs 删除数据库 并调用接口真实删除
     * @param id
     * @return
     */
    @RequestMapping("delHdfsBak")
    @ResponseBody
    public JsonObject delHdfsBak(Long id) {
        List<String> messageList = metaHdfsInfoBakService.delHdfsPubDir(id);
        return JsonObject.success(StringUtils.strip(messageList.toString(),"[]"));
    }
    /**
     * hdfsList  表
     * @param request
     */
    @RequestMapping("query_hdfsList_pop")
    public void hdfsList(HttpServletRequest request,String Q_LIKE_hdfsPath) {
        Map<String,Object> searchParam = new HashMap<String, Object>();
        if(StringUtils.isNotBlank(Q_LIKE_hdfsPath)){
            searchParam.put("LIKE_hdfsInfo.hdfsPath",Q_LIKE_hdfsPath);
        }
        PageInfo page = metaHdfsInfoService.hdfsCompare(Page.getCurrentPage(request), searchParam);
        request.setAttribute("hdfsPageInfo",page);
        request.setAttribute("divId","hdfsList");
    }


    /**
     * 删除hdfsInfo数据 只在数据库里删除
     * @param id
     * @return
     */
    @RequestMapping("delHdfs")
    @ResponseBody
    public JsonObject delHdfs(Long id) {
        metaHdfsInfoService.deleteMetaHdfsInfoById(id);
        return JsonObject.success("删除hdfsInfo成功");
    }

    /**
     * 添加DB 在hdfs_info_bak插入数据并调用接口
     * @param id
     * @return
     */
    @RequestMapping("addHdfsInfoBak")
    @ResponseBody
    public JsonObject addDB(Long id) {
        List<String> messageList = metaHdfsInfoBakService.makeHdfsPubDir(id);
        return JsonObject.success(StringUtils.strip(messageList.toString(),"[]"));
    }


    @Override
    public String getPortalPrefix() {
        return PORTAL_PREFIX;
    }
}
