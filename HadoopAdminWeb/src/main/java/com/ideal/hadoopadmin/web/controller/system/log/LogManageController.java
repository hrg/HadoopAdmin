package com.ideal.hadoopadmin.web.controller.system.log;

import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.service.system.WebLogService;
import com.ideal.hadoopadmin.web.controller.UIController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by fwj on 16-3-15.
 * 日志管理
 */
@Controller
@RequestMapping(LogManageController.PORTAL_PREFIX)
public class LogManageController extends UIController {

    public final static String PORTAL_PREFIX = "/system/log";
    private Logger logger = LoggerFactory.getLogger(LogManageController.class);
    @Resource
    private WebLogService webLogService;


    @RequestMapping("view_daily")
    public void index(HttpServletRequest request, Model model) {
        try {
            Map<String,Object> searchParams= WebUtils.getParametersStartingWith(request, "Q_");
            PageInfo infoWebappLogPage=webLogService.queryWebLogList(searchParams,request);
            model.addAttribute("infoWebappLogPage",infoWebappLogPage);
            model.addAttribute("val",searchParams.get("LIKE_op.operationName"));
            model.addAttribute("userName",searchParams.get("LIKE_log.userName"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getPortalPrefix() {
        return PORTAL_PREFIX;
    }

}
