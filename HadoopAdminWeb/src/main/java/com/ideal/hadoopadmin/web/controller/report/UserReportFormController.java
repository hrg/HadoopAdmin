package com.ideal.hadoopadmin.web.controller.report;
import com.ideal.hadoopadmin.entity.report.AuditLog;
import com.ideal.hadoopadmin.entity.report.AuditLogCustom;
import com.ideal.hadoopadmin.service.report.AuditLogService;
import com.ideal.hadoopadmin.web.controller.UIController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.sound.sampled.AudioSystem;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by fwj on 16/3/3.
 * 用户报表
 */
@Controller
@RequestMapping(UserReportFormController.PORTAL_PREFIX)
public class UserReportFormController  extends UIController {

    public final static String PORTAL_PREFIX = "/report/userFormReport";
    private Logger logger = LoggerFactory.getLogger(UserReportFormController.class);
    @Resource
    AuditLogService auditLogService;
    /**
     * 用户报表
     * @param request
     * @param model
     */
    @RequestMapping("user_report_form")
    public void HDFS_list(HttpServletRequest request, Model model,String datetime) {
       try{
           String date_time="";
//           String date_time=null;
           SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
           if(null==datetime){
               model.addAttribute("date","new Date()");
               Date today=new Date();
               date_time=sdf.format(today);
           }else{
               String[] dt=datetime.split("-");
               int date=Integer.parseInt(dt[2]);
               int month=Integer.parseInt(dt[1])-1;
               int year=Integer.parseInt(dt[0]);
               model.addAttribute("date","new Date("+year+","+month+","+date+")");
               Date dateTime=sdf.parse(datetime);
               date_time=sdf.format(dateTime);
           }
            List<AuditLogCustom> auditLogCustoms = auditLogService.findAccessPattern(date_time);
           List<String> numList = new ArrayList<String>();
           List<String> userList = new ArrayList<String>();
           //获取所有的用户List并且拼成数组类型的字符串
           //获取所有的用户对应的次数拼成数组类型字符串,并且用户和次数在数组中的位置互相对应
           for(AuditLogCustom auditLogCustom:auditLogCustoms){
               numList.add(auditLogCustom.getNum());
               userList.add(auditLogCustom.getUgi());
           }
           String nums = numList==null?"":numList.toString();
           String users = userList==null?"":userList.toString();
           //series 拼接成[{name:user},{data:nums}]类型传到前台
//           model.addAttribute("user",users);
//           model.addAttribute("series",nums);
//            model.addAttribute("auditLogCustoms",auditLogCustoms);
//           HDFSAuditLog hdfsAuditLog=new HDFSAuditLog();
//           Map<String,String> params=new HashMap<String, String>();
//           params.put(HDFSAuditLog.HDFS_DATE,date_time);
//           Map<String,List<String>> rsMap=hdfsAuditLog.getHDFSAuditLogInfo(params);
//           String userListStr=hdfsAuditLog.getUserListString(rsMap);
//           String tableListStr= hdfsAuditLog.getTableListString(rsMap);
//           model.addAttribute("user",userListStr);
//           model.addAttribute("series",tableListStr);

       }catch (Exception e){
           e.printStackTrace();
       }
    }

    @Override
    public String getPortalPrefix() {
        return PORTAL_PREFIX;
    }
}
