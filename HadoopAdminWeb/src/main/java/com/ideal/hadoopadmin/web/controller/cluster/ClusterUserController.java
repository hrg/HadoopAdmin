package com.ideal.hadoopadmin.web.controller.cluster;

import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.api.tools.Tool;
import com.ideal.hadoopadmin.common.entity.Result;
import com.ideal.hadoopadmin.common.util.JacksonUtil;
import com.ideal.hadoopadmin.entity.cluster.ClusterMachine;
import com.ideal.hadoopadmin.entity.cluster.ClusterType;
import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import com.ideal.hadoopadmin.entity.cluster.Parameter;
import com.ideal.hadoopadmin.entity.cluster.user.*;
import com.ideal.hadoopadmin.entity.cluster.user.Queue;
import com.ideal.hadoopadmin.entity.system.company.SystemCompany;
import com.ideal.hadoopadmin.framework.message.WebMessageLevel;
import com.ideal.hadoopadmin.framework.web.HttpRequests;
import com.ideal.hadoopadmin.framework.web.json.JsonObject;
import com.ideal.hadoopadmin.service.cluster.*;
import com.ideal.hadoopadmin.service.cluster.user.*;
import com.ideal.hadoopadmin.service.system.company.SystemCompanyService;
import com.ideal.hadoopadmin.web.controller.UIController;
import com.ideal.tools.ssh.common.PropertyBox;
import com.ideal.tools.ssh.common.PropertyDictory;
import com.ideal.tools.ssh.result.LinuxResult;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by CC on 2016/2/18.
 */
@Controller
@RequestMapping(ClusterUserController.PORTAL_PREFIX)
public class ClusterUserController extends UIController {
    public final static String PORTAL_PREFIX = "/cluster/user";
    private final static Logger logger = LoggerFactory.getLogger(ClusterUserController.class);

    @Override
    public String getPortalPrefix() {
        return PORTAL_PREFIX;
    }

    @Resource
    private ClusterUserService clusterUserService;
    @Resource
    private ClusterMachineService clusterMachineService;
    @Resource
    private UserTypeService userTypeService;
    @Resource
    private KbrconfigService kbrconfigService;
    @Resource
    private HdfsquotaService hdfsquotaService;
    @Resource
    private ClientquotaService clientquotaService;
    @Resource
    private SystemCompanyService systemCompanyService;
    @Resource
    private ClusterTypeService clusterTypeService;
    @Resource
    private QueueService queueService;
    @Resource
    private WizardService wizardService;
    @Autowired
    ClusterUserServiceNew clusterUserServiceNew;
    @Resource
    private ParameterService parameterService;
    //主页面,查询所有的集群用户,和所有的公司
    @RequestMapping("/query_clusterUser_list")
    public void queryClusterUserList(HttpServletRequest request, Model model) {
        //过滤条件
        Map<String, Object> searchParams = HttpRequests.getParametersStartingWith(request, "Q_");
        PageInfo page = clusterUserService.searchUser(searchParams, request);
        //查询所有的公司
        List<SystemCompany> systemCompanies = systemCompanyService.queryClusterCompany();
        //查询所有的用户类型
        List<UserType> userTypes = userTypeService.findUserType();
        request.setAttribute("page", page);
        request.setAttribute("systemCompanies", systemCompanies);
        request.setAttribute("userTypes", userTypes);
    }

    //新增或者修改
    @RequestMapping("/add_update_see_clusterUser_wss")
    public void editeClusterUser(String userIds, Model model, String operate) {
        //所有的公司
        List<SystemCompany> systemCompanies = systemCompanyService.queryClusterCompany();
        //所有的集群类型
        List<ClusterType> clusterTypes = clusterTypeService.findAllClusterType();
        //所有的用户类型
        List<UserType> userTypes = userTypeService.findUserType();
        //获取所有的userIds
        List<Long> ids = new ArrayList<Long>();
        if (!StringUtils.isBlank(userIds)) {
            String[] idArray = userIds.split(",");
            for (String id : idArray) {
                ids.add(Long.parseLong(id));
            }
        }
        Long userId = null;
        //当userId唯一时,需要载入以下数据
        if (ids.size() == 1) {
            userId = ids.get(0);
            ClusterUser clusterUser = getUser(userId);
            Kbrconfig kbrconfig = getKbr(userId);
            Hdfsquota hdfsquota = getHdfs(userId);
            List<Queue> queues = getQueue(userId);
            //Clientquota clientquota = getClient(userId);//获取client配置信息update20170715qinfengxia
            //通过kbr来确定哪些machine被选中
            List<Long> machineIds = getMachineIds(kbrconfig);
            //剩余时间
            Long laveDay = getLaveDay(kbrconfig);
            model.addAttribute("clusterUser", clusterUser);
            model.addAttribute("kbrconfig", kbrconfig);
            model.addAttribute("hdfsquota", hdfsquota);
            model.addAttribute("queues", queues);
            //model.addAttribute("clientquota",clientquota);
            model.addAttribute("laveDay", laveDay);
            model.addAttribute("machineIds", machineIds);
        }
        List<Map<String,Object>> clusterMachines  = clusterMachineService.findMachineAndClientquota(3L,userId);

        model.addAttribute("systemCompanies", systemCompanies);
        model.addAttribute("clusterTypes", clusterTypes);
        model.addAttribute("userTypes", userTypes);
        model.addAttribute("clusterMachines", clusterMachines);
        model.addAttribute("operate", operate);
        model.addAttribute("ids", ids);
    }

    //新建是进行统一保存  需要参数 租户,kbr,hdfs,queue,剩余时间days
    @RequestMapping("save_clusterUser")
    @ResponseBody
    public JsonObject saveClusterUser(String sclusterUser, String shdfs, String validDay,
                                      String machineIds, String squeueList, String status,String start,String end,String machineClientQuotaList) {
        JacksonUtil jacksonUtil = new JacksonUtil();
        ClusterUser clusterUser = handlerClusterUser(sclusterUser, jacksonUtil);
        Hdfsquota hdfsquota = handlerHdfs(shdfs, clusterUser, jacksonUtil);
        Kbrconfig kbrconfig = handlerKbrconfig(validDay, machineIds, status,start,end);
        List<Queue> queues = handlerQueueList(squeueList, jacksonUtil, clusterUser.getId());
        List<Clientquota> clientquotas = handlerClientList(machineClientQuotaList,jacksonUtil);
        //    进行保存 注释掉旧方法，调用新方法update20160802qinfengxia
        //Result result = clusterUserService.saveCluserUser(clusterUser, hdfsquota, kbrconfig, queues,clientquota);
        Result result = clusterUserServiceNew.saveCluserUser(clusterUser, hdfsquota, kbrconfig, queues,clientquotas);
        String messageList = StringUtils.strip(result.getMessageList().toString(),"[]");
        if(result.getFlag()){
            return JsonObject.alert(messageList, WebMessageLevel.SUCCESS);
        }
        return JsonObject.alert(messageList,WebMessageLevel.ERROR);

    }

    // 保存已经修改的租户
    @ResponseBody
    @RequestMapping(value = "/update_clusterUser_byId", method = RequestMethod.POST)
    public JsonObject update_clusterUser_byId(HttpServletRequest request, Model model, ClusterUser clusterUser) {
//        clusterUserService.update(clusterUser);
        clusterUserService.updateClusterUser(clusterUser);
        return JsonObject.success("修改租户配置成功");
    }

    //保存已经修改的kerbers
    @ResponseBody
    @RequestMapping(value = "/update_kerbers_byId", method = RequestMethod.POST)
    public JsonObject update_kerbers_byId(Kbrconfig kbrconfig, String userIds,String start,String end,String machineClientQuotaList) {
        userIds = userIds.replace("[", "");
        userIds = userIds.replace("]", "");
        String[] id = userIds.split(",");
        List<Long> ids = new ArrayList<Long>();
        //保存kers的id
        for (int i = 0; i < id.length; i++) {
            ids.add(Long.parseLong(id[i].trim()));
        }
        kbrconfig.setStartTime(stringToLong(start, "yyyy-MM-dd HH:mm:ss"));
        kbrconfig.setEndTime(stringToLong(end,"yyyy-MM-dd HH:mm:ss"));
        kbrconfig.setChangeTime(System.currentTimeMillis());
        JacksonUtil jacksonUtil = new JacksonUtil();
        List<Clientquota> clientquotas = handlerClientList(machineClientQuotaList,jacksonUtil);
        List<String> messageList = kbrconfigService.updateKbrconfig(ids, kbrconfig,clientquotas);
        return JsonObject.alert(StringUtils.strip(messageList.toString(), "[]"), WebMessageLevel.SUCCESS);
    }

    //更新修改的hdfs
    @RequestMapping("update_hdfs_byId")
    @ResponseBody
    public JsonObject update_hdfs_byId(String hdfs, Long[] userIds) {
        JacksonUtil jacksonUtil = new JacksonUtil();
        //如果为空相当于删除hdfs
        Hdfsquota hdfsquota = jacksonUtil.fromJson(hdfs, Hdfsquota.class);
        List<String> messageList= hdfsquotaService.updateHDFS(userIds, hdfsquota);
        return JsonObject.alert(StringUtils.strip(messageList.toString(),"[]"),WebMessageLevel.SUCCESS);
    }

    /**
     * 更新修改的client
     * add20160715qinfengxia
     * @param client
     * @param userIds
     * @return
     */
    @RequestMapping("update_client_byId")
    @ResponseBody
    public JsonObject update_client_byId(String client, Long[] userIds) {
        JacksonUtil jacksonUtil = new JacksonUtil();
        //如果为空相当于删除clientquota
        Clientquota clientquota = jacksonUtil.fromJson(client, Clientquota.class);
        List<String> messageList= clientquotaService.updateClient(userIds, clientquota);
        return JsonObject.alert(StringUtils.strip(messageList.toString(),"[]"),WebMessageLevel.SUCCESS);
    }

    //检查队列是否冲突
    @ResponseBody
    @RequestMapping("check_queue")
    public String checkQueue(Queue queue) {
        boolean flag = queueService.checkQueue(queue);
        if (flag) {
            return "yes";//冲突
        }
        return "no";//不冲突
    }

    //修改后更新queue
    @RequestMapping("save_queue")
    @ResponseBody
    public JsonObject save_queue(Queue queue) {
        queueService.editQueue(queue);
        return JsonObject.success();
    }

    //删除租户
    @RequestMapping("delete_clusterUser")
    @ResponseBody
    public JsonObject delete_clusterUser(Long[] id) {
        List<String> messageList = new ArrayList<String>();
        for(int i=0;i<id.length;i++){
            //注释掉旧方法，调用新方法update20160802qinfengxia
            //messageList.addAll(clusterUserService.deleteClusterUser(id[i]));
            messageList.addAll(clusterUserServiceNew.deleteClusterUser(id[i]));
        }
        return JsonObject.alert(StringUtils.strip(messageList.toString(),"[]"), WebMessageLevel.SUCCESS);
    }

    //重置密码界面
    @RequestMapping("reset_password_pop")
    public void resetPasswordPop(Model model, Long id) {
        if (id != null) {
            ClusterUser clusterUser = clusterUserService.queryClusterUserById(id);
            model.addAttribute("clusterUser", clusterUser);
        }
    }

    //对密码进行重置
    @RequestMapping("reset_password")
    @ResponseBody
    public JsonObject resetPassword(Long id, String clientPW) {
        //注释掉旧方法，调用新方法update20160802qinfengxia
        //List<String> messageList = clusterUserService.resetPassword(id, clientPW);
        List<String> messageList = clusterUserServiceNew.resetPassword(id, clientPW);
        return JsonObject.alert(StringUtils.strip(messageList.toString(),"[]"), WebMessageLevel.SUCCESS);
    }

    //用户启用
    @RequestMapping("open_user")
    @ResponseBody
    public JsonObject openUser(Long id) {
        List<String> messageList = clusterUserService.openUser(id);
        return JsonObject.alert(StringUtils.strip(messageList.toString(),"[]"), WebMessageLevel.SUCCESS);
    }

    //用户暂停
    @RequestMapping("pause_user")
    @ResponseBody
    public JsonObject pauseUser(Long id) {
        List<String> messageList = clusterUserService.pauseUser(id);
        return JsonObject.alert(StringUtils.strip(messageList.toString(), "[]"), WebMessageLevel.SUCCESS);
    }

    //删除队列
    @ResponseBody
    @RequestMapping("del_queue")
    public Map<String, Object> delMr(Long id) {
        Map<String, Object> result = new HashMap<String, Object>();
        queueService.delQueue(id);
        result.put("success", "success");
        result.put("ErrorMessage", "");
        return result;
    }

    //wizard
    @RequestMapping("modal-wizard")
    public void getModalWizard(HttpServletRequest request) {
        List<Wizard> wizards = wizardService.findAllWizard();
        request.setAttribute("wizards", wizards);
    }

    /**
     * ********************************************************************************************
     */
    //租户管理
    private ClusterUser getUser(Long id) {
        ClusterUser clusterUser = new ClusterUser();
        if (id != null) {
            clusterUser = clusterUserService.queryClusterUserById(id);
        }
        return clusterUser;
    }

    //处理前台的获取的参数,并以实体的形式返回
    private ClusterUser handlerClusterUser(String sclusterUser, JacksonUtil jacksonUtil) {
        //租户配置
        ClusterUser clusterUser = jacksonUtil.fromJson(sclusterUser, ClusterUser.class);
        clusterUser.setCreateTime(System.currentTimeMillis());
        clusterUser.setChangeTime(System.currentTimeMillis());
        clusterUser.setSystemPW(Tool.GeneratRandomPW());
//        clusterUser.setSystemPW(PassWordGenerator.GeneratRandomPW());
        return clusterUser;
    }

    /**
     * *****************************************************************************************
     */
    //kerberos管理
    //根据userId获取获取kbrs
    private Kbrconfig getKbr(Long userId) {
        Kbrconfig kbrconfig = new Kbrconfig();
        if (userId != null) {
            kbrconfig = kbrconfigService.findByUserId(userId);
        }
        return kbrconfig;
    }

    //计算剩余时间
    private Long getLaveDay(Kbrconfig kbrconfig) {
        Long laveDay = 0L;
        if (kbrconfig != null && kbrconfig.getEndTime() != null) {
            laveDay = (kbrconfig.getEndTime() - System.currentTimeMillis()) / (24 * 60 * 60 * 1000);
        }
        return laveDay;
    }

    //保存kbrs
    private Kbrconfig handlerKbrconfig(String validDay, String machineIds, String status,String startTime,String endTime) {
        Kbrconfig kbrconfig = new Kbrconfig();
        //将valiDay添加至kbrconfig里面
        if (validDay == null || "".equals(validDay)) {
            kbrconfig.setValidDay(0L);
        } else {
            kbrconfig.setValidDay(Long.parseLong(validDay));
        }
        kbrconfig.setStatus(Integer.parseInt(status));
        kbrconfig.setMachineIds(machineIds.trim());
        kbrconfig.setCreateTime(System.currentTimeMillis());
        kbrconfig.setChangeTime(System.currentTimeMillis());
        kbrconfig.setStartTime(stringToLong(startTime, "yyyy-MM-dd HH:mm:ss"));
        kbrconfig.setEndTime(stringToLong(endTime, "yyyy-MM-dd HH:mm:ss"));
        return kbrconfig;
    }

    /**
     * *****************************************************************************************
     */
    //获取机器类型为client的机器,即machineTypeId=3L的所有机器
    private List<ClusterMachine> getClusterMahine() {
        return clusterMachineService.findByMachineTypeId(3L);
    }

    //通过kbs的machineIds获取机器所选中机器id
    private List<Long> getMachineIds(Kbrconfig kbrconfig) {
        List<Long> machineIds = new ArrayList<Long>();
        if (kbrconfig != null && kbrconfig.getMachineIds() != null) {
            String[] ids = (kbrconfig.getMachineIds().split("\\,"));
            for (int i = 0; i < ids.length; i++) {
                machineIds.add(Long.parseLong(ids[i]));
            }
        }
        return machineIds;
    }

    /**
     * ***************************************************************************************
     */
    //hdfs管理
    private Hdfsquota getHdfs(Long userId) {
        Hdfsquota hdfsquota = new Hdfsquota();
        if (userId != null) {
            hdfsquota = hdfsquotaService.findByUserId(userId);
        }
        return hdfsquota;
    }

    /**
     * ***************************************************************************************
     */
    //hdfs管理
    private Clientquota getClient(Long userId) {
        Clientquota clientquota = new Clientquota();
        if (userId != null) {
            clientquota = clientquotaService.findByUserId(userId);
        }
        return clientquota;
    }
    //新建时处理hdfs,可能为null
    private Hdfsquota handlerHdfs(String shdfs, ClusterUser clusterUser, JacksonUtil jacksonUtil) {
        if (!StringUtils.isBlank(shdfs)) {
            Hdfsquota hdfsquota = jacksonUtil.fromJson(shdfs, Hdfsquota.class);
            hdfsquota.setHdfsPath("/user/" + clusterUser.getUserName());
            return hdfsquota;
        }
        return null;
    }

    /**
     * 新建时处理client,可能为null
     * add20160715qinfengxia
     * @param sclient
     * @param jacksonUtil
     * @return
     */
    private Clientquota handlerClient(String sclient, JacksonUtil jacksonUtil) {
        if (!StringUtils.isBlank(sclient)) {
            Clientquota clientquota = jacksonUtil.fromJson(sclient, Clientquota.class);
            return clientquota;
        }
        return null;
    }


    /**
     * ***************************************************************************************
     */
    //队列管理
    private List<Queue> getQueue(Long userId) {
        return queueService.findByUserId(userId);
    }

    //新建时保存
    private List<Queue> handlerQueueList(String squeueList, JacksonUtil jacksonUtil, Long userId) {
        List<Queue> queues = new ArrayList<Queue>();
        String[] queueList = squeueList.split(";");
        for (String squeue : queueList) {
            Queue queue = jacksonUtil.fromJson(squeue, Queue.class);
            queue.setModifyTime(System.currentTimeMillis());
            queues.add(queue);
        }
        return queues;
    }

    //新建时保存
    private List<Clientquota> handlerClientList(String machineClientQuotaList, JacksonUtil jacksonUtil) {
        List<Clientquota> clientquota = new ArrayList<Clientquota>();
        if(StringUtils.isNotBlank(machineClientQuotaList)){
            String[] clientquotaList = machineClientQuotaList.split(";");
            for (String client : clientquotaList) {
                Clientquota quota = jacksonUtil.fromJson(client, Clientquota.class);
                clientquota.add(quota);
            }
        }

        return clientquota;
    }

    //新建时保存
    private List<Hdfsquota> handlerHdfsList(String hdfsList, JacksonUtil jacksonUtil) {
        List<Hdfsquota> hdfsquotaList = new ArrayList<Hdfsquota>();
        if(StringUtils.isNotBlank(hdfsList)){
            String[] hdfsquota = hdfsList.split(";");
            for (String hdfs : hdfsquota) {
                Hdfsquota quota = jacksonUtil.fromJson(hdfs, Hdfsquota.class);
                quota.setHdfsPath("/user/" + clusterUserService.queryClusterUserById(quota.getUserId()).getUserName());
                hdfsquotaList.add(quota);
            }
        }
        return hdfsquotaList;
    }

    //新建时保存
    private List<Kbrconfig> handlerKbrList(String kerList, JacksonUtil jacksonUtil) {
        List<Kbrconfig> kerbersList = new ArrayList<Kbrconfig>();
        String[] kerber = kerList.split(";");
        for (String ker : kerber) {
            net.sf.json.JSONObject jsonobject = net.sf.json.JSONObject.fromObject(ker);
            String startTime = jsonobject.get("startTime").toString();
            String endTime = jsonobject.get("endTime").toString();
            jsonobject.remove("startTime");
            jsonobject.remove("endTime");
            ker = jsonobject.toString();
            Kbrconfig kbrconfig = jacksonUtil.fromJson(ker, Kbrconfig.class);
            if(jsonobject.get("validDay").toString().equals("0")){
                kbrconfig.setStartTime(0l);
                kbrconfig.setEndTime(0l);
            }else{
                kbrconfig.setStartTime(stringToLong(startTime, "yyyy-MM-dd HH:mm:ss"));
                kbrconfig.setEndTime(stringToLong(endTime,"yyyy-MM-dd HH:mm:ss"));
            }
            kbrconfig.setChangeTime(System.currentTimeMillis());
            kerbersList.add(kbrconfig);
        }
        return kerbersList;
    }
    /**************************字符串转换成时间**********************************/
    public Long stringToLong(String sdate,String formate){
        Long time = 0l;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formate);
        try {
            if(null != sdate && !"".equals(sdate)){
                if("0".equals(sdate)){
                    time = 0l;
                }else {
                    time = simpleDateFormat.parse(sdate).getTime();
                }
            }
            return time;
        }catch (Exception e){
            logger.error("字符串转成成date出错"+e);
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 根据companyId(租户id)获取租户信息
     * add20160714qinfengxia
     * @param companyId
     * @return
     */
    @RequestMapping("findCompanyParams")
    @ResponseBody
    public Map<String ,Object> findCompanyParams(Long companyId){
        Map<String,Object> map = clusterUserService.findCompanyParams(companyId);
        return map;
    }

    /**
     * 查找用户目录的默认配置
     * add20160830qinfengxia
     * @param userName
     * @return
     */
    @RequestMapping("findClientHome")
    @ResponseBody
    private List<String> findClientHome(String userName){
        List<String> clientHomeList = new ArrayList<String>();
        String clientUserHome = "CLIENT_USER_HOME_";
        //List<Parameter> parameters = parameterService.findParameterByParam(clientUserHome);
        List<String> proList = PropertyBox.getVals(clientUserHome, "");
        for(String str: proList){
                clientHomeList.add(str.replace("@username@", userName));
        }
        return clientHomeList;
    }

    /**
     *
     * add20160810qinfengxia
     */
    @RequestMapping("query_batch_update_clusterUser_pop")
    public void queryBatchUpdateClusterUser(String userIds, HttpServletRequest request) {
        request.setAttribute("userIds", userIds);
    }


    /**
     *
     * add20160810qinfengxia
     */
    @RequestMapping("batch_update_clusterUser_pop")
    public void batchUpdateClusterUser(HttpServletRequest request,String userIds) {
        PageInfo page = clusterUserServiceNew.findUserAndHdfsList(userIds,request);
        request.setAttribute("page", page);
        request.setAttribute("userIds", userIds);
    }

    /**
     * 第二级数据
     * add20160810qinfengxia
     * @return
     */
    @RequestMapping("findMachineAndClientquota")
    @ResponseBody
    public List<Map<String,Object>> findMachineAndClientquota(String userId){
        List<Map<String,Object>> clusterMachines  = clusterMachineService.findMachineAndClientquota(3L, Long.valueOf(userId));
        Kbrconfig kbrconfig = kbrconfigService.findByUserId(Long.valueOf(userId));
        String machineIds = ","+ kbrconfig.getMachineIds()+",";
        for(int i= 0 ;i<clusterMachines.size();i++){
            String id = clusterMachines.get(i).get("id").toString();
            if(machineIds.contains(","+id+",")){
                clusterMachines.get(i).put("check","true");
            }else{
                clusterMachines.get(i).put("check","false");
            }
        }
        return clusterMachines;
    }

    //保存已经修改的kerbers
    @ResponseBody
    @RequestMapping(value = "/saveBatchUpdate", method = RequestMethod.POST)
    public JsonObject saveBatchUpdate(String hdfsList,String clientList,String kerList,String noSelectList) {
        JacksonUtil jacksonUtil = new JacksonUtil();
        List<Clientquota> clientquotas = handlerClientList(clientList,jacksonUtil);
        List<Hdfsquota> hdfsquotas = handlerHdfsList(hdfsList, jacksonUtil);
        List<Kbrconfig> kbrconfigs = handlerKbrList(kerList,jacksonUtil);
        Result result = kbrconfigService.saveBatchUpdate(kbrconfigs, noSelectList,clientquotas, hdfsquotas);
        String messageList =StringUtils.strip(result.getMessageList().toString(), "[]");
        if(result.getFlag()){
            return JsonObject.alert(messageList, WebMessageLevel.SUCCESS);
        }
        return JsonObject.alert(messageList,WebMessageLevel.ERROR);
    }

}
