package com.ideal.hadoopadmin.web.controller.cluster;
import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.entity.cluster.ClusterType;
import com.ideal.hadoopadmin.entity.cluster.Parameter;
import com.ideal.hadoopadmin.framework.web.json.JsonObject;
import com.ideal.hadoopadmin.service.cluster.ClusterTypeService;
import com.ideal.hadoopadmin.service.cluster.ParameterService;
import com.ideal.hadoopadmin.web.controller.UIController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * Created by 袁颖 on 2016/2/22.
 */
@RequestMapping(ParameterController.PORTAL_PREFIX)
@Controller
public class ParameterController extends UIController{
    public final static String PORTAL_PREFIX = "/cluster/parameter";
    @Resource
    private ParameterService parameterService;
    @Resource
    private ClusterTypeService clusterTypeService;
    /**
     *  返回参数页面，子参数不显示
     */
    @RequestMapping("parameter_list")
    public void parameterList(HttpServletRequest request){
        String parentPageNum = request.getParameter("pageNum")==null?"1":request.getParameter("pageNum");
        //查询出所有的集群类型add20160713qinfengxia
        List<ClusterType> clusterTypes = clusterTypeService.findAllClusterType();
        HttpSession session = request.getSession();
        request.setAttribute("parentPageNum",parentPageNum);
        session.setAttribute("clusterTypes", clusterTypes);
//        Map<String,Object> searchParams = WebUtils.getParametersStartingWith(request,"Q_");
//        //只显示所有的父参数
//        searchParams.put("EQ_parentId",0);
//        PageInfo page = parameterService.searchParameter(request, searchParams);
//        model.addAttribute("parameterNewPage", page);
//        HttpSession session = request.getSession();
//        session.removeAttribute("parentId");
//        model.addAttribute("divId", "parentId");
    }

    /**
     * 显示父参数页面
     * @param request
     * @param model
     */
    @RequestMapping("parameter_parent_pop")
    public void parameterParent(HttpServletRequest request, Model model) {
        Map<String,Object> searchParams = WebUtils.getParametersStartingWith(request,"Q_");
        //只显示所有的父参数
        searchParams.put("EQ_parentId",Parameter.parent);
        PageInfo page = parameterService.searchParameter(request,searchParams);
        model.addAttribute("parameterNewPage",page);
        model.addAttribute("divId","parentId");
    }

    /**
     * 显示对应父参数下的所有子参数
     * @param parentId
     * @param request
     * @param model
     */
    @RequestMapping("parameter_child_pop")
    public void parameterChild(Long parentId,HttpServletRequest request,Model model){
        Map<String, Object> searchParams = WebUtils.getParametersStartingWith(request,"Q_");
        searchParams.put("EQ_parentId",parentId);
        PageInfo page = parameterService.searchParameter(request, searchParams);
        model.addAttribute("parameterNewPageChild",page);
        HttpSession session = request.getSession();
        session.setAttribute("parentId",parentId);
        model.addAttribute("divId","childrenId");
    }

    /**
     * 增加或者修改
     * @param parentId
     * @param model
     * @param id
     * @param rel
     */
    @RequestMapping("add_parameterNew_pop")
    public void addParameter(Long parentId,Model model,Long id,String rel){
        if(id != null){
            Parameter parameter = parameterService.findById(id);
            model.addAttribute("parameterNew",parameter);
        }
        model.addAttribute("parentId",parentId);
        model.addAttribute("rel",rel);
        model.addAttribute("id",id);
    }

    /**
     * 保存或者更新
     * @param parameter
     * @return
     */
    @RequestMapping("save_parameterNew")
    @ResponseBody
    public JsonObject saveParameter(Parameter parameter){
        parameterService.saveOrUpdate(parameter);
        return JsonObject.success();
    }

    /**
     * 根据id删除子参数
     * @param id
     * @return
     */
    @RequestMapping("delete_parameterNew")
    @ResponseBody
    public JsonObject deleteParameter(Long id){
        parameterService.deleteById(id);
        return JsonObject.success();
    }

    /**
     * 检查是否存在相同的key
     * @param parameterKey
     * @param id
     * @return
     */
    @RequestMapping("check_key")
    @ResponseBody
    public String checkKey(String parameterKey,Long id){
        Parameter parameter = parameterService.findByKey(parameterKey);
        if(parameter!=null&&!parameter.getId().equals(id)){
            return "success";//存在冲突，不能更新或者插入数据
        }
        return "error";
    }

    /**
     * 删除父参数并返回主页面
     * @param id
     * @return
     */
    @RequestMapping("delete_parent")
    public String delParent(Long id,HttpServletRequest request){
        parameterService.deleteById(id);
        parameterService.deleteByParentId(id);
        String pageNum = request.getParameter("pageNum").isEmpty()?"1":request.getParameter("pageNum");
        return "redirect:/cluster/parameter/parameter_list.do?pageNum="+pageNum;
    }
    /**
     * 刷新
     */
    @RequestMapping("flush")
    @ResponseBody
    public void flush() {
//        ConfigManager.instance().refresh();
    }
    @Override
    public String getPortalPrefix() {
        return PORTAL_PREFIX;
    }
}
