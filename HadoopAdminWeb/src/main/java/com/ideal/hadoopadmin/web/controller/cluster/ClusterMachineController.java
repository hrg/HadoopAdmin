package com.ideal.hadoopadmin.web.controller.cluster;

import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.entity.cluster.ClusterMachine;
import com.ideal.hadoopadmin.entity.cluster.ClusterType;
import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import com.ideal.hadoopadmin.entity.cluster.MachineType;
import com.ideal.hadoopadmin.framework.message.WebMessageLevel;
import com.ideal.hadoopadmin.framework.web.json.JsonObject;
import com.ideal.hadoopadmin.service.cluster.ClusterMachineService;
import com.ideal.hadoopadmin.service.cluster.ClusterTypeService;
import com.ideal.hadoopadmin.service.cluster.ClusterUserService;
import com.ideal.hadoopadmin.service.cluster.MachineTypeService;
import com.ideal.hadoopadmin.web.controller.UIController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * Created by 袁颖 on 2016/2/19.
 */
@RequestMapping(ClusterMachineController.PORTAL_PREFIX)
@Controller
public class ClusterMachineController extends UIController {
    public final static String PORTAL_PREFIX = "cluster/clusterMachine";
    @Resource
    private ClusterMachineService clusterMachineService;
    @Resource
    private ClusterTypeService clusterTypeService;
    @Resource
    private MachineTypeService machineTypeService;
    @Resource
    private ClusterUserService clusterUserService;

    /**
     * 根据条件查找所有的集群机器并分页显示
     */
    @RequestMapping("clusterMachine_list")
    public void clusterMachineList(HttpServletRequest request) {
        Map<String, Object> searchParams = WebUtils.getParametersStartingWith(request, "Q_");
        //查询出来的结果集
        PageInfo page = clusterMachineService.searchClusterMachine(searchParams, request);
        //查询所有的机器类型
        List<MachineType> machineTypes = machineTypeService.findAllMachineType();
        //查询出所有的集群类型
        List<ClusterType> clusterTypes = clusterTypeService.findAllClusterType();
        //查询某个集群所有机器的IP
        List<String> clusterMachineIps = clusterMachineService.findAllIp();
        //查找某个集群所有机器的登录用户
        List<String> clusterMachineUsers = clusterMachineService.findAllUser();
        request.setAttribute("page", page);
        HttpSession session = request.getSession();
        session.setAttribute("machineTypes", machineTypes);
        session.setAttribute("clusterTypes", clusterTypes);
        request.setAttribute("clusterMachineIps", clusterMachineIps);
        request.setAttribute("clusterMachineUsers", clusterMachineUsers);
    }

    //编辑某一条机器信息
    @RequestMapping("edit_clusterMachine_pop")
    public void editClusterMachine(Long id, HttpServletRequest request) {
        ClusterMachine clusterMachine = clusterMachineService.findById(id);
        String page = request.getParameter("page");
        request.setAttribute("savePage", page);
        request.setAttribute("clusterMachine", clusterMachine);
    }

    //新增集群机器
    @RequestMapping("add_clusterMachine_pop")
    public void addMachine(ClusterMachine machine) {

    }

    //保存集群机器
    @RequestMapping("save_clusterMachine")
    public String saveMachine(ClusterMachine clusterMachine, HttpServletRequest request) {
        int currentPage = Integer.parseInt(StringUtils.isEmpty(request.getParameter("page"))? "1" : request.getParameter("page"));
        clusterMachineService.saveClusterMachine(clusterMachine);
        return "redirect:/cluster/clusterMachine/clusterMachine_list.do?page=" + currentPage;
    }

    //删除机器信息
    @RequestMapping("delete_clusterMachine")
    @ResponseBody
    public JsonObject deleteMachine(Long[] id) {
        for (int i = 0; i < id.length; i++) {
            clusterMachineService.deleteClusterMachine(id[i]);
        }
        return JsonObject.alert("删除成功", WebMessageLevel.SUCCESS);
    }

    //检查机器的ip，命名和集群id是否全部相同，如相同，则冲突返回seccess
    @RequestMapping("check_name_ip")
    @ResponseBody
    public String checkNameIp(ClusterMachine clusterMachine) {
        String message = clusterMachineService.checkClusterMachine(clusterMachine);
        return message;
    }

    @RequestMapping("synchrony_clusterMachine_pop")
    public void synchrony(@RequestParam(value = "id") Long[] ids, HttpServletRequest request) {
        //查找所有的用户
        List<ClusterUser> clusterUsers = clusterUserService.queryClusterUser();
        String sids = StringUtils.strip(Arrays.asList(ids).toString(),"[]");
        request.setAttribute("ids", sids);
        request.setAttribute("clusterUsers", clusterUsers);
    }

    //保存
    @RequestMapping("save_synchrony_clusterUser")
    @ResponseBody
    public JsonObject saveSynchrony(HttpServletRequest request, String ids, String userId) {
        List<String> messageList = new ArrayList<String>();
        String [] userIds = userId.trim().split(",");
        for(int i=0;i<userIds.length;i++){
            messageList.addAll(clusterMachineService.saveSynchrony(Long.parseLong(userIds[i]), ids));
        }
        //根据machineId及获取的
        return JsonObject.alert(StringUtils.strip(messageList.toString(),"[]"), WebMessageLevel.SUCCESS);
    }

    @Override
    public String getPortalPrefix() {
        return PORTAL_PREFIX;
    }
}
