package com.ideal.hadoopadmin.framework.web.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by 袁颖 on 2016/5/3.
 */
public class MyHandlerExeption implements HandlerExceptionResolver{
    private static Logger logger = LoggerFactory.getLogger(MyHandlerExeption.class);
    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        logger.error(e.getMessage(),e);
        httpServletRequest.setAttribute("ex",e);
        return new ModelAndView("error");
    }
}
