package com.ideal.hadoopadmin.framework.page;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by 袁颖 on 2016/4/7.
 * 页面进行分页时所需要的数据
 */
public class Page {
    //每页默认显示10页
    public static final int DEFAULT_PAGESIZE = 10;
    //每页默认根据id倒序
    public static final String DEFAULT_ORDER = "id desc";
    //获取当前页面
    public static int getCurrentPage(HttpServletRequest request){
        int currentPage = Integer.parseInt(request.getParameter("page") == null ? "1" : request.getParameter("page"));
        return currentPage;
    }
}
