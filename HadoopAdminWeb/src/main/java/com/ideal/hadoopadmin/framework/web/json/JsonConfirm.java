package com.ideal.hadoopadmin.framework.web.json;

/**
 * Created by wanghongbo@163.com
 */
public class JsonConfirm {
    private String message;
    private String url;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
