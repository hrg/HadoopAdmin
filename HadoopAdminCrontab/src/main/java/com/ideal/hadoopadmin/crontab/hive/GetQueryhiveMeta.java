package com.ideal.hadoopadmin.crontab.hive;

import com.ideal.hadoopadmin.crontab.db.ConnectionManager;
import com.ideal.hadoopadmin.crontab.tool.Tools;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

public class GetQueryhiveMeta {
    private String dbName;
    private String tbName;
    private String tbl_id;
    private String serde_id;
    private String cd_id;
    private String location;
    private String input_format;
    private String output_format;
    private StringBuffer hql;

    public GetQueryhiveMeta(String dbName, String tbName, String tbl_id, String serde_id, String cd_id, String location, String input_format, String output_format, StringBuffer hql) {
        this.dbName = dbName;
        this.tbName = tbName;
        this.tbl_id = tbl_id;
        this.serde_id = serde_id;
        this.cd_id = cd_id;
        this.location = location;
        this.input_format = input_format;
        this.output_format = output_format;
        this.hql = hql;
    }

    public String getTbl_id() {
        return tbl_id;
    }

    public String getSerde_id() {
        return serde_id;
    }

    public String getCd_id() {
        return cd_id;
    }

    public String getLocation() {
        return location;
    }

    public String getInput_format() {
        return input_format;
    }

    public String getOutput_format() {
        return output_format;
    }

    public GetQueryhiveMeta invoke() {
        String sd_id;
        Tools.getCreateSqlParam(hql, " select tbls.tbl_id,tbls.sd_id,sds.serde_id,sds.cd_id ,"
                , "sds.input_format,sds.output_format,sds.location from  dbs,tbls,sds"
                , " where dbs.db_id=tbls.db_id and tbls.sd_id =sds.sd_id and dbs.name"
                , "='" + dbName + "' and tbls.tbl_name='" + tbName + "'");
        Connection conn = ConnectionManager.getHiveConnection();
        List<Map<String, Object>> resAll = ConnectionManager.queryDB(conn, hql.toString());
        for (Map<String, Object> metaData : resAll) {
            sd_id = metaData.get("SD_ID").toString();
            tbl_id = metaData.get("TBL_ID").toString();
            serde_id = metaData.get("SERDE_ID").toString();
            cd_id = metaData.get("CD_ID").toString();
            location = metaData.get("LOCATION").toString();
            input_format = metaData.get("INPUT_FORMAT").toString();
            output_format = metaData.get("OUTPUT_FORMAT").toString();
        }
        return this;
    }

}