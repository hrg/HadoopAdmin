package com.ideal.hadoopadmin.crontab.property;

import com.ideal.hadoopadmin.crontab.db.ConnectionManager;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CC on 2015/9/1.
 * 这个类中的所有属性 都需要和 sql数据库中的 配置key 相匹配
 */
public class Properties {
    private static Properties properties;
    private static java.util.Properties proper;
    private static boolean hasInitMysql= false;

    /**mysql tables**/
    public static String KDC_CONFIG_TABLE="cluster_user_kbrconfig";
    public static String MACHINE_TABLE="cluster_machine";
    public static String CLUSTER_PARAMETER_TABLE="cluster_parameter";
        public static String PARAMETER_COL_KEY="PARAMETERKEY";
        public static String PARAMETER_COL_VAL="PARAMETERVAL";

    /**linux**/
    public static String Linux_Delimited="\n";
    public static String HDFS_INFO_FILENAME="hdfsMetaData";

    public static String Kerberos_EXE_FileName="kerberos_exe_shell";

    public static String Kerberos_CP_FileName="kerberos_cp_shell";

    public static String Kerberos_INFO_FILENAME="kerberos_flush_list";

    public static String Linux_Prompt_FileName="initLinuxPrompt";

    public static String Kerberos_Start="start_kerberos_refresh";

    public static String Kerberos_End="end_kerberos_refresh";

    public static String Linux_Group_Path="Linux_Group_Path";

    /**property 属性*/
    public static final String Property_Path="/property.properties";

    //webapp
    public static final String WEB_APP_DB_IP="webapp.ip";
    public static final String WEB_APP_DB_DB="webapp.database";
    public static final String WEB_APP_DB_USER="webapp.username";
    public static final String WEB_APP_DB_PW="webapp.password";
    //hivemeta
    public static final String HIVE_META_IP = "hive.ip";
    public static final String HIVE_META_DB = "hive.database";
    public static final String HIVE_META_PW = "hive.password";
    public static final String HIVE_META_USER = "hive.username";
    //sentry
    public static final String SENTRY_META_IP = "sentry.ip";
    public static final String SENTRY_META_DB = "sentry.database";
    public static final String SENTRY_META_PW = "sentry.password";
    public static final String SENTRY_META_USER = "sentry.username";
    //remote syn db
    public static final String REMOTE_SYN_IP = "syn.ip";
    public static final String REMOTE_SYN_DB = "syn.database";
    public static final String REMOTE_SYN_PW = "syn.password";
    public static final String REMOTE_SYN_USER = "syn.username";


    private Properties(){
        initProperties();

    }

    private void initProperties(){
        proper=new java.util.Properties();
        InputStream in=getClass().getResourceAsStream(Properties.Property_Path);
        try {
            proper.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 不指定 强行载入mysql 信息
     * @return
     */
    public static Properties instance(){
        return instance(true);
    }

    /**
     * @param isInitMysqlParameter
     * @return
     */
    public static Properties instance(boolean isInitMysqlParameter){
        if(properties==null) {
            properties = new Properties();
        }

        return properties;
    }


    public String getPropertyByKey(String key,String defaultVal){
        String val="";
        val=proper.getProperty(key,defaultVal);
        return val;
    }

    /**
     * 这里需要手动调用一次 如果需要 数据库的 初始信息的话
     */
    public static void initMySQL(){
        if(!hasInitMysql) {
            String sql = "select " + PARAMETER_COL_KEY + "," + PARAMETER_COL_VAL + " from " + CLUSTER_PARAMETER_TABLE;

            Connection conn = ConnectionManager.getConnection();

            List<Map<String, Object>> rsList = ConnectionManager.queryDB(conn, sql);

            for (Map<String, Object> map : rsList) {
                String key = map.get(PARAMETER_COL_KEY).toString();
                String val = map.get(PARAMETER_COL_VAL).toString();
                proper.put(key, val);
            }
            hasInitMysql=true;
        }
    }

}
