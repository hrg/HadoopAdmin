package com.ideal.hadoopadmin.crontab.changeDataBase;

public enum TableEnum {
    cluster_user_queue("fair_scheduler_allocation"),
    cluster_user_kbrconfig("kdc_config"),
    cluster_user_kbrauth("kdc_auth"),
    cluster_user_hdfsquota("hadoopuser_hdfs"),
    system_web_operations(""),
    cluster_parameter(""),
    cluster_machine_type("d_machin_type"),
    cluster_machine("machine_info"),
    system_user(""),
    meta_hdfs_access("hdfs_visiters"),
    system_web_log(""),
    system_company("t_customer"),
    system_user_role(""),
    meta_hive_access("hive_visiters"),
    system_role_menu(""),
    cluster_cluster_type("cluster_machine"),
    meta_hive_info(""),
    cluster_user_type(""),
    meta_hdfs_info_bak(""),
    meta_hdfs_info("hdfs_info"),
    cluster_user("hadoopuser"),
    system_menu(""),
    meta_hive_sql("");

    String tabN;

    TableEnum(String tab) {
        tabN = tab;
    }

    public static   String getTab(String table) {
        String resTable = "";
        for(TableEnum t : TableEnum.values()){
            if(table.equals(t.toString())){
                resTable = t.tabN;
                break;
            }
        }
        return resTable;
    }

}
    