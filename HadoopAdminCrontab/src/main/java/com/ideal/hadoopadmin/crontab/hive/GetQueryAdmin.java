package com.ideal.hadoopadmin.crontab.hive;



import com.ideal.hadoopadmin.crontab.db.ConnectionManager;
import com.ideal.hadoopadmin.crontab.tool.Tools;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

public class GetQueryAdmin {
    private int hdsfId;
    private String dbName;
    private String tbName;
    private StringBuffer hql;

    public GetQueryAdmin(int hdsfId, String dbName, String tbName, StringBuffer hql) {
        this.hdsfId = hdsfId;
        this.dbName = dbName;
        this.tbName = tbName;
        this.hql = hql;
    }

    public String getDbName() {
        return dbName;
    }

    public String getTbName() {
        return tbName;
    }

    public GetQueryAdmin invoke() {
        Tools.getCreateSqlParam(hql, " SELECT dbName,tableName from meta_hive_info", " where id='" + hdsfId + "'");
        Connection connHiveInfo = ConnectionManager.getConnection();
        List<Map<String, Object>> hvieInfos = ConnectionManager.queryDB(connHiveInfo, hql.toString());
        for (Map<String, Object> res : hvieInfos) {
            dbName = res.get("DBNAME").toString();
            tbName = res.get("TABLENAME").toString();
        }
        return this;
    }
}
