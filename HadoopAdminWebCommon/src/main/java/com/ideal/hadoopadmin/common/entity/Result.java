package com.ideal.hadoopadmin.common.entity;

import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.result.LinuxResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 袁颖 on 2016/3/1.
 */
public class Result {
    String message;//传递单条信息
    List<String> messageList = new ArrayList<String>();//传递多条信息
    Boolean flag;//判断是否成功

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<String> messageList) {
        this.messageList = messageList;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }


}
