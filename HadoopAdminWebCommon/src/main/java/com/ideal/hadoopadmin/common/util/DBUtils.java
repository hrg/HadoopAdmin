package com.ideal.hadoopadmin.common.util;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by CC on 2016/2/29.
 */
public class DBUtils {

    public final static SimpleDateFormat simpleDateFormat=new SimpleDateFormat();


    public final static String FormatDateStr="yyyy-MM-dd HH:mm:ss";


    /**
     * 格式化时间格式
     * @param dateFormat
     * @param time
     * @return
     */
    public static Date formatDate(String dateFormat,Long time) {
//        if (dateFormat == null || StringUtils.isBlank(dateFormat)) {
//            dateFormat = DBUtils.FormatDateStr;
//        }
//        simpleDateFormat.applyPattern(dateFormat);
        return new Date(time);
    }


    public static Long parseDate(String dateFormat,Date date) throws ParseException {
//        if (dateFormat == null || StringUtils.isBlank(dateFormat)) {
//            dateFormat = DBUtils.FormatDateStr;
//        }
        return date.getTime();
    }



}
