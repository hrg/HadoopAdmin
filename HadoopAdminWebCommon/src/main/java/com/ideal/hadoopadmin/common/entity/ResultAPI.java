package com.ideal.hadoopadmin.common.entity;

import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.result.LinuxResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 袁颖 on 2016/4/21.
 */
public class ResultAPI {
    public static boolean flag ;//hdfs判断,为false代表失败
    public static List<String> messageList;

    public static void initAPIResult(List<LinuxResult> resultList) {
        messageList = new ArrayList<String>();
        flag = true;
        if(null == resultList || resultList.isEmpty()) return;
        for (LinuxResult result : resultList) {
            if (result.isSuccess()) {
                messageList.add("<div>成功信息:" + result.getNote() + "</div>");
            } else {
                messageList.add("<div style= 'color:red'>错误信息:" + result.getNote()  + "</div>");
                flag = false;//调用失败
            }
        }
    }

    public List<String> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<String> messageList) {
        this.messageList = messageList;
    }

    //拼接好的table
    //                messageTable = "<table class='center' style='width:600px'>" +
//                        "       <tr>" +
//                        "           <td style='width:35%'>标准输出:</td>" +
//                        "           <td>" + result.getStdOut() + "</td>" +
//                        "       </tr>" +
//                        "       <tr>" +
//                        "           <td style='width:35%'>错误:</td>" +
//                        "           <td style='color:red'>" + result.getErrOut() + "</td>" +
//                        "       </tr>" +
//                        "       <tr>" +
//                        "           <td style='width:35%'>异常:</td>" +
//                        "           <td style='color:red'>" + result.getException() + "</td>" +
//                        "       </tr>" +
//                        "       <tr>" +
//                        "           <td style='width:35%'>返回码:</td>" +
//                        "           <td>" + result.getExitCode() + "</td>" +
//                        "       </tr>" +
//                        "</table>";
}
