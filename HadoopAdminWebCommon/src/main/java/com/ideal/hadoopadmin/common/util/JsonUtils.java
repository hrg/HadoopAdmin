package com.ideal.hadoopadmin.common.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
public class JsonUtils {

	private static Gson gson = null;
	/**
	 * 通过单例获取gson
	 * @return
	 */
	public static Gson getGson(){
		if(gson==null){
			return new Gson();
		}
		return gson;
	}

	/**
	 * 把java对象转换为JSON
	 * @param obj 要转换的对象
	 * @return
	 * @throws java.io.IOException
	 */
	public static String toJson(Object obj){
		return getGson().toJson(obj);
	}
	/**
	 * 把JSON转换为java对象
	 * @param str JSON字符串
	 * @param obj java类型
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static Object toObject(String str,TypeToken obj ){
		return getGson().fromJson(str, obj.getType());
	}
    public static <T> T objectFromJson(String json, Class<T> clz) {
        return getGson().fromJson(json, clz);
    }

}
