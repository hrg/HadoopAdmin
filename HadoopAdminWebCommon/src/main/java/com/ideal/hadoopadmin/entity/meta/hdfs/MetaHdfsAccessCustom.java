package com.ideal.hadoopadmin.entity.meta.hdfs;

import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import org.apache.ibatis.type.Alias;

/**
 * Created by 袁颖 on 2016/5/9.
 */
@Alias("hdfsAccessCustom")
public class MetaHdfsAccessCustom extends MetaHdfsAccess{
    //添加属性,进行关联查询时使用
    private ClusterUser accessUser;//可访问用户

    public ClusterUser getAccessUser() {
        return accessUser;
    }

    public void setAccessUser(ClusterUser accessUser) {
        this.accessUser = accessUser;
    }
}
