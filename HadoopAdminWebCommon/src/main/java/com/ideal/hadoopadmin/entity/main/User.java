package com.ideal.hadoopadmin.entity.main;

import com.ideal.hadoopadmin.entity.system.Role;
import com.ideal.hadoopadmin.entity.system.company.SystemCompany;
import org.apache.ibatis.type.Alias;

import java.util.List;

/**
 * Created by CC on 2016/2/18.
 */
@Alias("user")
public class User {

    private Long id;
    private String userName;
    private String passWord;
    private String email;
    private String phoneNumber;
    private String remark;
    private Long createTime;
    private Long changeTime;
    private long systemCompanyId;
    private SystemCompany systemCompany;
    private String role;

    public long getSystemCompanyId() {
        return systemCompanyId;
    }

    public void setSystemCompanyId(long systemCompanyId) {
        this.systemCompanyId = systemCompanyId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public SystemCompany getSystemCompany() {
        return systemCompany;
    }

    public void setSystemCompany(SystemCompany systemCompany) {
        this.systemCompany = systemCompany;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Long changeTime) {
        this.changeTime = changeTime;
    }
}
