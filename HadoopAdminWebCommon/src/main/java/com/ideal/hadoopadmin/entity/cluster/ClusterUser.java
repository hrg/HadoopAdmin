package com.ideal.hadoopadmin.entity.cluster;

import com.ideal.hadoopadmin.entity.cluster.user.UserType;
import com.ideal.hadoopadmin.entity.system.company.SystemCompany;
import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * Created by CC on 2016/2/19.
 * 集群用户 实体类
 */
@Alias("clusterUser")//设置别名
public class ClusterUser {
    public static int onStatus = 0;//启用
    public static int unStatus =1;//停用
    private Long id;//唯一标识
    private String userName;//用户名
    private String clientPW;//用户客户端密码
    private String systemPW;//用户服务器密码
    private Long companyId;//用户所属公司id
    private SystemCompany systemCompany;//用户所属公司,根据字段companyId得出
    private String contactPerson;//联系人
    private String phoneNumber;//联系电话
    private  String email;//用户邮箱
    private String remark;//用户备注
    private Long userTypeId;//用户类型id,外键
    private UserType userType;//用户类型,对应字段userTypeId
    private int  status;//用户状态:    0-启用,1-停用
    private Long clusterTypeId;//用户的集群id
    private ClusterType clusterType;//用户所属集群,对应字段clusterTypeId
    private Long changeTime;//修改时间
    private Long createTime;//创建时间
    private String itsmNumber;//itsm工单号

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getClusterTypeId() {
        return clusterTypeId;
    }

    public void setClusterTypeId(Long clusterTypeId) {
        this.clusterTypeId = clusterTypeId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getClientPW() {
        return clientPW;
    }

    public void setClientPW(String clientPW) {
        this.clientPW = clientPW;
    }

    public String getSystemPW() {
        return systemPW;
    }

    public void setSystemPW(String systemPW) {
        this.systemPW = systemPW;
    }

    public SystemCompany getSystemCompany() {
        return systemCompany;
    }

    public void setSystemCompany(SystemCompany systemCompany) {
        this.systemCompany = systemCompany;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Long userTypeId) {
        this.userTypeId = userTypeId;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ClusterType getClusterType() {
        return clusterType;
    }

    public void setClusterType(ClusterType clusterType) {
        this.clusterType = clusterType;
    }

    public Long getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Long changeTime) {
        this.changeTime = changeTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getItsmNumber() {
        return itsmNumber;
    }

    public void setItsmNumber(String itsmNumber) {
        this.itsmNumber = itsmNumber;
    }
}
