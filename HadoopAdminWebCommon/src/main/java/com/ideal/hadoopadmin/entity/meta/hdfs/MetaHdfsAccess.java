package com.ideal.hadoopadmin.entity.meta.hdfs;

import org.apache.ibatis.type.Alias;

/**
 * Created by fwj on 16-2-26.
 *  hdfs访问表
 */
@Alias("hdfsAccess")
public class MetaHdfsAccess {
    private Long id;
    //集群用户标识
    private Long clusterUserId;
    //hdfspath信息
    private Long hdfsInfoId;
    //创建时间
    private Long createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClusterUserId() {
        return clusterUserId;
    }

    public void setClusterUserId(Long clusterUserId) {
        this.clusterUserId = clusterUserId;
    }

    public Long getHdfsInfoId() {
        return hdfsInfoId;
    }

    public void setHdfsInfoId(Long hdfsInfoId) {
        this.hdfsInfoId = hdfsInfoId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }
}
