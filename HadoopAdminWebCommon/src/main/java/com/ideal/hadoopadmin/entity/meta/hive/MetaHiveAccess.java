package com.ideal.hadoopadmin.entity.meta.hive;

/**
 * Created by fwj on 16-2-26.
 *  访问表
 */
public class MetaHiveAccess {
    private Long id;
    //集群用户标识
    private Long clusterUserId;
    //hive数据标识
    private Long hiveInfoId;
    //创建时间
    private Long createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClusterUserId() {
        return clusterUserId;
    }

    public void setClusterUserId(Long clusterUserId) {
        this.clusterUserId = clusterUserId;
    }

    public Long getHiveInfoId() {
        return hiveInfoId;
    }

    public void setHiveInfoId(Long hiveInfoId) {
        this.hiveInfoId = hiveInfoId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }
}
