package com.ideal.hadoopadmin.entity.report;

import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import org.apache.ibatis.type.Alias;
import java.util.Date;

/**
 * Created by fwj on 16-3-3.
 * 队列资源访问列表
 */

@Alias("fairSchedulerAllocation")
public class FairSchedulerAllocation {
    private Long id;
    private ClusterUser clusterUser;
    private Integer minMemory;
    private Integer minCPU;
    private Integer maxMemory;
    private Integer maxCPU;
    private Integer maxApps;
    private Float weight;
    private Date updateTime;
    private String acl = "*";
    private String queue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClusterUser getClusterUser() {
        return clusterUser;
    }

    public void setClusterUser(ClusterUser clusterUser) {
        this.clusterUser = clusterUser;
    }

    public Integer getMinMemory() {
        return minMemory;
    }

    public void setMinMemory(Integer minMemory) {
        this.minMemory = minMemory;
    }

    public Integer getMinCPU() {
        return minCPU;
    }

    public void setMinCPU(Integer minCPU) {
        this.minCPU = minCPU;
    }

    public Integer getMaxMemory() {
        return maxMemory;
    }

    public void setMaxMemory(Integer maxMemory) {
        this.maxMemory = maxMemory;
    }

    public Integer getMaxCPU() {
        return maxCPU;
    }

    public void setMaxCPU(Integer maxCPU) {
        this.maxCPU = maxCPU;
    }

    public Integer getMaxApps() {
        return maxApps;
    }

    public void setMaxApps(Integer maxApps) {
        this.maxApps = maxApps;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getAcl() {
        return acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }
}
