package com.ideal.hadoopadmin.entity.meta.privilege;

import org.apache.ibatis.type.Alias;

/**
 * Created by qinfengxia on 2016/8/5.
 * 数据对应权限
 */
@Alias("metaPrivileges")
public class MetaPrivileges {
    private Long id;
    private Long accessId;
    private Long privilegeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccessId() {
        return accessId;
    }

    public void setAccessId(Long accessId) {
        this.accessId = accessId;
    }

    public Long getPrivilegeId() {
        return privilegeId;
    }

    public void setPrivilegeId(Long privilegeId) {
        this.privilegeId = privilegeId;
    }
}
