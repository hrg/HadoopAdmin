package com.ideal.hadoopadmin.entity.meta.privilege;

import org.apache.ibatis.type.Alias;

/**
 * Created by qinfengxia on 2016/8/5.
 * 权限类型
 */
@Alias("metaAccessPrivilege")
public class MetaAccessPrivilege {
    private Long id;
    private String type;
    private String privilegeType;
    private String note;
    private int isHidden;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrivilegeType() {
        return privilegeType;
    }

    public void setPrivilegeType(String privilegeType) {
        this.privilegeType = privilegeType;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(int isHidden) {
        this.isHidden = isHidden;
    }
}
