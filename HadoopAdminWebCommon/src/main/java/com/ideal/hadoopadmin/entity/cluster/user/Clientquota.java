package com.ideal.hadoopadmin.entity.cluster.user;

/**
 * Created by qinfengxia on 2016/7/14.
 */
public class Clientquota {
    private Long id;
    private Long userId;//用户id,外键,关联clusterUser表
    private String clientCatalog;//用户目录
    private Integer clientCatalogSize; //用户目录配置额
    private String clientCatalogUnit;//用户目录配置额单位
    private String machineId;//机器id

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getClientCatalog() {
        return clientCatalog;
    }

    public void setClientCatalog(String clientCatalog) {
        this.clientCatalog = clientCatalog;
    }

    public Integer getClientCatalogSize() {
        return clientCatalogSize;
    }

    public void setClientCatalogSize(Integer clientCatalogSize) {
        this.clientCatalogSize = clientCatalogSize;
    }

    public String getClientCatalogUnit() {
        return clientCatalogUnit;
    }

    public void setClientCatalogUnit(String clientCatalogUnit) {
        this.clientCatalogUnit = clientCatalogUnit;
    }

    public String getMachineId() {
        return machineId;
    }

    public void setMachineId(String machineId) {
        this.machineId = machineId;
    }
}
