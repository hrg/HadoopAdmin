package com.ideal.hadoopadmin.entity.report;

import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import com.ideal.hadoopadmin.entity.meta.hive.MetaHiveAccess;
import com.ideal.hadoopadmin.entity.meta.hive.MetaHiveAccessCustom;
import com.ideal.hadoopadmin.entity.meta.hive.MetaHiveInfo;
import org.apache.ibatis.type.Alias;

/**
 * Created by 袁颖 on 2016/5/10.
 */
@Alias("hiveInfoCustom")
public class HiveInfoCustom extends MetaHiveInfo{
    private MetaHiveAccessCustom metaHiveAccess;
    private ClusterUser ownerUser;

    public MetaHiveAccessCustom getMetaHiveAccess() {
        return metaHiveAccess;
    }

    public void setMetaHiveAccess(MetaHiveAccessCustom metaHiveAccess) {
        this.metaHiveAccess = metaHiveAccess;
    }

    public ClusterUser getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(ClusterUser ownerUser) {
        this.ownerUser = ownerUser;
    }
}
