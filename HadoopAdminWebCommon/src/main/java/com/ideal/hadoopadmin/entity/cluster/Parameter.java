package com.ideal.hadoopadmin.entity.cluster;

import org.apache.ibatis.type.Alias;

import java.util.List;

/**
 * Created by 袁颖 on 2016/2/22.
 */
@Alias("parameter")
public class Parameter {
    public static long parent = 0L;//父参数
    private Long id;
    private String parameterKey;//key值
    private String parameterVal;//val值
    private Long parentId;//父参数id,0表示是父参数
    private String note;//备注
    private List<Parameter> children;//所有父参数下的子参数
    Long clusterTypeId;
    ClusterType clusterType;//集群类型

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParameterKey() {
        return parameterKey;
    }

    public void setParameterKey(String parameterKey) {
        this.parameterKey = parameterKey;
    }

    public String getParameterVal() {
        return parameterVal;
    }

    public void setParameterVal(String parameterVal) {
        this.parameterVal = parameterVal;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<Parameter> getChildren() {
        return children;
    }

    public void setChildren(List<Parameter> children) {
        this.children = children;
    }

    public Long getClusterTypeId() {
        return clusterTypeId;
    }

    public void setClusterTypeId(Long clusterTypeId) {
        this.clusterTypeId = clusterTypeId;
    }

    public ClusterType getClusterType() {
        return clusterType;
    }

    public void setClusterType(ClusterType clusterType) {
        this.clusterType = clusterType;
    }
}
