package com.ideal.hadoopadmin.entity.system;


/**
 * Created by fwj on 16-3-8.
 * 角色管理
 */
public class Role {
    private Long id;
    private String name;//角色名
    private String status;//生效0 无效1
    private Long createTime;//cnName

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }


}
