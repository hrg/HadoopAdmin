package com.ideal.hadoopadmin.entity.cluster.user;

import com.ideal.hadoopadmin.entity.cluster.ClusterMachine;
import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import com.ideal.hadoopadmin.entity.cluster.MachineType;
import org.apache.ibatis.type.Alias;

import java.util.Date;
import java.util.List;

/**
 * Created by 袁颖 on 2016/2/26.
 */
@Alias("kbrconfig")
public class Kbrconfig {
    public static int onStatus = 0;//启用
    public static int unStatus = 1;//关闭
    Long id;
    Long userId;//用户id,外键关联cluster_user表
    Long validDay;//kbr有效期
    String machineIds;//机器列表,根据id
    int status;//状态 0启用 1停用
    Long changeTime;//修改时间
    Long createTime;//创建时间
    ClusterUser clusterUser;//根据userId注入的实体user,俩表进行关联时使用
    Long startTime;//有效期开始日期
    Long endTime;//结束日期
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getValidDay() {
        return validDay;
    }

    public void setValidDay(Long validDay) {
        this.validDay = validDay;
    }

    public String getMachineIds() {
        return machineIds;
    }

    public void setMachineIds(String machineIds) {
        this.machineIds = machineIds;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Long changeTime) {
        this.changeTime = changeTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public ClusterUser getClusterUser() {
        return clusterUser;
    }

    public void setClusterUser(ClusterUser clusterUser) {
        this.clusterUser = clusterUser;
    }

}
