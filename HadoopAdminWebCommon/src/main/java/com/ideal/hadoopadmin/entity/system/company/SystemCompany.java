package com.ideal.hadoopadmin.entity.system.company;

import org.apache.ibatis.type.Alias;

/**
 * Created by fwj on 16-2-22.
 * 公司管理
 */
@Alias("systemCompany")
public class SystemCompany {
    private Long id;
    private String companyName;
    private String note;
    private String contactName;
    private String contactMobile;
    private int delTag;
    /*private Integer maxUserNum;
    private Integer maxResource;
    private Integer hdfsFileCount;
    private Integer maxCpu;
    private Integer hdfsSpace;*/
    public int getDelTag() {
        return delTag;
    }

    public void setDelTag(int delTag) {
        this.delTag = delTag;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactMobile() {
        return contactMobile;
    }

    public void setContactMobile(String contactMobile) {
        this.contactMobile = contactMobile;
    }

   /* public Integer getMaxUserNum() {
        return maxUserNum;
    }

    public void setMaxUserNum(Integer maxUserNum) {
        this.maxUserNum = maxUserNum;
    }

    public Integer getMaxResource() {
        return maxResource;
    }

    public void setMaxResource(Integer maxResource) {
        this.maxResource = maxResource;
    }

    public Integer getHdfsFileCount() {
        return hdfsFileCount;
    }

    public void setHdfsFileCount(Integer hdfsFileCount) {
        this.hdfsFileCount = hdfsFileCount;
    }

    public Integer getMaxCpu() {
        return maxCpu;
    }

    public void setMaxCpu(Integer maxCpu) {
        this.maxCpu = maxCpu;
    }

    public Integer getHdfsSpace() {
        return hdfsSpace;
    }

    public void setHdfsSpace(Integer hdfsSpace) {
        this.hdfsSpace = hdfsSpace;
    }*/
}
