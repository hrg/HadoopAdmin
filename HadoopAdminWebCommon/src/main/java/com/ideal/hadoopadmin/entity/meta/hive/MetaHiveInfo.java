package com.ideal.hadoopadmin.entity.meta.hive;

import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsInfo;
import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsInfoBak;
import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * Created by fwj on 16-3-1.
 * hive元数据实体类
 */
@Alias("hiveInfo")
public class MetaHiveInfo {
    private Long id;
    //HIVE数据库名称
    private String dbName;
    //HIVE表名
    private String tableName;
    private Long hdfsInfoBakId;//单表时使用
    //hdfs信息
    private MetaHdfsInfoBak hdfsInfoBak;//关联时使用
    private Long clusterUserId;//
    //集群用户
    private ClusterUser clusterUser;//
    //创建时间
    private Long createTime;

    private long countByCluster;

    public Long getHdfsInfoBakId() {
        return hdfsInfoBakId;
    }

    public void setHdfsInfoBakId(Long hdfsInfoBakId) {
        this.hdfsInfoBakId = hdfsInfoBakId;
    }

    public Long getClusterUserId() {
        return clusterUserId;
    }

    public void setClusterUserId(Long clusterUserId) {
        this.clusterUserId = clusterUserId;
    }

    public ClusterUser getClusterUser() {
        return clusterUser;
    }

    public void setClusterUser(ClusterUser clusterUser) {
        this.clusterUser = clusterUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public MetaHdfsInfoBak getHdfsInfoBak() {
        return hdfsInfoBak;
    }

    public void setHdfsInfoBak(MetaHdfsInfoBak hdfsInfoBak) {
        this.hdfsInfoBak = hdfsInfoBak;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public long getCountByCluster() {
        return countByCluster;
    }

    public void setCountByCluster(long countByCluster) {
        this.countByCluster = countByCluster;
    }
}
