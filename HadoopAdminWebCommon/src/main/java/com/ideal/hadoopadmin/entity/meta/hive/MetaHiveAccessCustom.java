package com.ideal.hadoopadmin.entity.meta.hive;

import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import org.apache.ibatis.type.Alias;

/**
 * Created by 袁颖 on 2016/5/10.
 */
@Alias("hiveAccessCustom")
public class MetaHiveAccessCustom extends MetaHiveAccess{
    private ClusterUser accessUser;

    public ClusterUser getAccessUser() {
        return accessUser;
    }

    public void setAccessUser(ClusterUser accessUser) {
        this.accessUser = accessUser;
    }
}
