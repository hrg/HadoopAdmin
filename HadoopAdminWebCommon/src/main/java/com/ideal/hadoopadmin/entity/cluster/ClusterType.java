package com.ideal.hadoopadmin.entity.cluster;

import org.apache.ibatis.type.Alias;

/**
 * Created by 袁颖 on 2016/2/22.
 */
@Alias("clusterType")
public class ClusterType {
    Long id;
    Long clusterTypeId;//集群类型id,外键
    String clusterTypeName;//集群类型名称
    String note;//备注

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClusterTypeId() {
        return clusterTypeId;
    }

    public void setClusterTypeId(Long clusterTypeId) {
        this.clusterTypeId = clusterTypeId;
    }

    public String getClusterTypeName() {
        return clusterTypeName;
    }

    public void setClusterTypeName(String clusterTypeName) {
        this.clusterTypeName = clusterTypeName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
