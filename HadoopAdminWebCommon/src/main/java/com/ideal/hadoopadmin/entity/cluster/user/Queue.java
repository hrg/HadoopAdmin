package com.ideal.hadoopadmin.entity.cluster.user;

import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * Created by 袁颖 on 2016/2/29.
 */
@Alias("queue")
public class Queue {
    private Long id;//唯一标示id
    private Long userId;//集群用户id
    private ClusterUser clusterUser;//集群用户
    private String queueName;//队列名称
    private Long minResource;//最小资源值
    private Long maxResource;//最大资源值
    private Long maxApp;//最大app数
    private Float weight;//权重值
    private String acl;//
    private Long minCpu;//最小cpu值
    private Long maxCpu;//最大cpu值
    private Long modifyTime;//修改时间

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public ClusterUser getClusterUser() {
        return clusterUser;
    }

    public void setClusterUser(ClusterUser clusterUser) {
        this.clusterUser = clusterUser;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public Long getMinResource() {
        return minResource;
    }

    public void setMinResource(Long minResource) {
        this.minResource = minResource;
    }

    public Long getMaxResource() {
        return maxResource;
    }

    public void setMaxResource(Long maxResource) {
        this.maxResource = maxResource;
    }

    public Long getMaxApp() {
        return maxApp;
    }

    public void setMaxApp(Long maxApp) {
        this.maxApp = maxApp;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getAcl() {
        return acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    public Long getMinCpu() {
        return minCpu;
    }

    public void setMinCpu(Long minCpu) {
        this.minCpu = minCpu;
    }

    public Long getMaxCpu() {
        return maxCpu;
    }

    public void setMaxCpu(Long maxCpu) {
        this.maxCpu = maxCpu;
    }

    public Long getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Long modifyTime) {
        this.modifyTime = modifyTime;
    }
}
