package com.ideal.hadoopadmin.entity.meta.hive;

import org.apache.ibatis.type.Alias;

/**
 * Created by 袁颖 on 2016/3/11.
 */
@Alias("hiveSql")
public class MetaHiveSql {
    Long id;
    String hiveSql;
    Long hiveInfoId;

    public Long getHiveInfoId() {
        return hiveInfoId;
    }

    public void setHiveInfoId(Long hiveInfoId) {
        this.hiveInfoId = hiveInfoId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHiveSql() {
        return hiveSql;
    }

    public void setHiveSql(String hiveSql) {
        this.hiveSql = hiveSql;
    }
}
