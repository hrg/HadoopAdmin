package com.ideal.hadoopadmin.entity.report;

import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsAccess;
import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsAccessCustom;
import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsInfo;
import org.apache.ibatis.type.Alias;

/**
 * Created by 袁颖 on 2016/5/9.
 */
@Alias("hdfsInfoCustom")
public class HdfsInfoCustom extends MetaHdfsInfo {

    private MetaHdfsAccessCustom metaHdfsAccess;
    private ClusterUser ownerUser;//所属用户

    public MetaHdfsAccessCustom getMetaHdfsAccess() {
        return metaHdfsAccess;
    }

    public void setMetaHdfsAccess(MetaHdfsAccessCustom metaHdfsAccess) {
        this.metaHdfsAccess = metaHdfsAccess;
    }

    public ClusterUser getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(ClusterUser ownerUser) {
        this.ownerUser = ownerUser;
    }
}
