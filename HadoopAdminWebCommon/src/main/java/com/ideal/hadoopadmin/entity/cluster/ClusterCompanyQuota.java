package com.ideal.hadoopadmin.entity.cluster;

import org.apache.ibatis.type.Alias;

/**
 * Created by fwj on 16-2-22.
 * 公司管理
 */
@Alias("clusterCompanyQuota")
public class ClusterCompanyQuota {
    private Long id;
    private Long companyId;
    private Integer maxUserNum;
    private Integer maxResource;
    private Integer hdfsFileCount;
    private Integer maxCpu;
    private Integer hdfsSpace;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMaxUserNum() {
        return maxUserNum;
    }

    public void setMaxUserNum(Integer maxUserNum) {
        this.maxUserNum = maxUserNum;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Integer getMaxResource() {
        return maxResource;
    }

    public void setMaxResource(Integer maxResource) {
        this.maxResource = maxResource;
    }

    public Integer getHdfsFileCount() {
        return hdfsFileCount;
    }

    public void setHdfsFileCount(Integer hdfsFileCount) {
        this.hdfsFileCount = hdfsFileCount;
    }

    public Integer getMaxCpu() {
        return maxCpu;
    }

    public void setMaxCpu(Integer maxCpu) {
        this.maxCpu = maxCpu;
    }

    public Integer getHdfsSpace() {
        return hdfsSpace;
    }

    public void setHdfsSpace(Integer hdfsSpace) {
        this.hdfsSpace = hdfsSpace;
    }
}
