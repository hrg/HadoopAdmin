package com.ideal.hadoopadmin.entity.cluster;

import org.apache.ibatis.type.Alias;

/**
 * Created by 袁颖 on 2016/2/22.
 */
@Alias("machineType")
public class MachineType {
    Long id;
    Long machineTypeId;//机器类型id,外键
    String machineTypeName;//机器类型名称
    String note;//备注
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMachineTypeId() {
        return machineTypeId;
    }

    public void setMachineTypeId(Long machineTypeId) {
        this.machineTypeId = machineTypeId;
    }

    public String getMachineTypeName() {
        return machineTypeName;
    }

    public void setMachineTypeName(String machineTypeName) {
        this.machineTypeName = machineTypeName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
