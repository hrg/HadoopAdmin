package com.ideal.hadoopadmin.entity.cluster;

import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsInfo;
import org.apache.ibatis.type.Alias;

/**
 * Created by 袁颖 on 2016/4/18.
 */
@Alias("dataTransfer")
public class DataTransfer {
    public static final Integer ON_STATUS = 0;//启用
    public static final Integer UN_STATUS = 1;//暂停
    private Long id;
    private Long sourceClusterTypeId;
    private ClusterType sourceClusterType;
    private Long targetClusterTypeId;
    private ClusterType targetClusterType;
    private Long clusterUserId;
    private ClusterUser clusterUser;
    private Long hdfsInfoId;
    private MetaHdfsInfo hdfsInfo;
    private Long startTime;
    private String hdfsPath;
    private Long endTime;
    private Long runTime;
    private Integer runCycle;
    private Integer status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSourceClusterTypeId() {
        return sourceClusterTypeId;
    }

    public void setSourceClusterTypeId(Long sourceClusterTypeId) {
        this.sourceClusterTypeId = sourceClusterTypeId;
    }

    public ClusterType getSourceClusterType() {
        return sourceClusterType;
    }

    public void setSourceClusterType(ClusterType sourceClusterType) {
        this.sourceClusterType = sourceClusterType;
    }

    public Long getTargetClusterTypeId() {
        return targetClusterTypeId;
    }

    public void setTargetClusterTypeId(Long targetClusterTypeId) {
        this.targetClusterTypeId = targetClusterTypeId;
    }

    public ClusterType getTargetClusterType() {
        return targetClusterType;
    }

    public void setTargetClusterType(ClusterType targetClusterType) {
        this.targetClusterType = targetClusterType;
    }

    public Long getClusterUserId() {
        return clusterUserId;
    }

    public void setClusterUserId(Long clusterUserId) {
        this.clusterUserId = clusterUserId;
    }

    public ClusterUser getClusterUser() {
        return clusterUser;
    }

    public void setClusterUser(ClusterUser clusterUser) {
        this.clusterUser = clusterUser;
    }

    public Long getHdfsInfoId() {
        return hdfsInfoId;
    }

    public void setHdfsInfoId(Long hdfsInfoId) {
        this.hdfsInfoId = hdfsInfoId;
    }

    public MetaHdfsInfo getHdfsInfo() {
        return hdfsInfo;
    }

    public void setHdfsInfo(MetaHdfsInfo hdfsInfo) {
        this.hdfsInfo = hdfsInfo;
    }

    public String getHdfsPath() {
        return hdfsPath;
    }

    public void setHdfsPath(String hdfsPath) {
        this.hdfsPath = hdfsPath;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getRunTime() {
        return runTime;
    }

    public void setRunTime(Long runTime) {
        this.runTime = runTime;
    }

    public Integer getRunCycle() {
        return runCycle;
    }

    public void setRunCycle(Integer runCycle) {
        this.runCycle = runCycle;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
