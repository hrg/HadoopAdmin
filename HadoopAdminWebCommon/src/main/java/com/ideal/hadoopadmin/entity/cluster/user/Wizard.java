package com.ideal.hadoopadmin.entity.cluster.user;

/**
 * Created by 袁颖 on 2016/3/17.
 *向导界面动态载入值
 */
public class Wizard {
    private Long id ;
    private String title;
    private String url;//内容jsp所在路径
    private int sort;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }
}
