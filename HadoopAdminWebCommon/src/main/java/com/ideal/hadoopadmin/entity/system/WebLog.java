package com.ideal.hadoopadmin.entity.system;

/**
 * 系统日志表
 * Created by fwj on 16-3-15.
 */
public class WebLog {
    private Long id;
    private String userName;//用户名
    private Long operationId;//日志参数id
    private WebOperations webOperations;
    private Long createtime;//创建时间
    private String parameter;//参数

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getOperationId() {
        return operationId;
    }

    public void setOperationId(Long operationId) {
        this.operationId = operationId;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public WebOperations getWebOperations() {
        return webOperations;
    }

    public void setWebOperations(WebOperations webOperations) {
        this.webOperations = webOperations;
    }
}
