package com.ideal.hadoopadmin.entity.system;

/**
 * 日志参数
 * Created by fwj on 16-3-15.
 */
public class WebOperations {
    private Long id;
    private String url;
    private String operationName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }
}
