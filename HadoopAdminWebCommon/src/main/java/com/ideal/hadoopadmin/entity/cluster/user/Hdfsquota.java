package com.ideal.hadoopadmin.entity.cluster.user;

import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import org.apache.ibatis.type.Alias;

/**
 * Created by 袁颖 on 2016/2/26.
 */
@Alias("hdfsquota")
public class Hdfsquota {
    private Long id;
    private Long userId;//用户id,外键,关联clusterUser表
    private Integer hdfsSpace;//hdfs用户空间大小
    private Integer hdfsFileCount;//hdfs文件数量
    private String hdfsSpaceUnit;//hdfs空间单位
    private String hdfsPath;//hdfs地址
    private ClusterUser clusterUser;//实体,关联表cluster_user的对照实体

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getHdfsPath() {
        return hdfsPath;
    }

    public void setHdfsPath(String hdfsPath) {
        this.hdfsPath = hdfsPath;
    }

    public ClusterUser getClusterUser() {
        return clusterUser;
    }

    public void setClusterUser(ClusterUser clusterUser) {
        this.clusterUser = clusterUser;
    }

    public Integer getHdfsSpace() {
        return hdfsSpace;
    }

    public void setHdfsSpace(Integer hdfsSpace) {
        this.hdfsSpace = hdfsSpace;
    }

    public Integer getHdfsFileCount() {
        return hdfsFileCount;
    }

    public void setHdfsFileCount(Integer hdfsFileCount) {
        this.hdfsFileCount = hdfsFileCount;
    }

    public String getHdfsSpaceUnit() {
        return hdfsSpaceUnit;
    }

    public void setHdfsSpaceUnit(String hdfsSpaceUnit) {
        this.hdfsSpaceUnit = hdfsSpaceUnit;
    }
}
