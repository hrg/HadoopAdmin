package com.ideal.hadoopadmin.entity.meta.hdfs;

import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import com.ideal.hadoopadmin.entity.main.User;
import org.apache.ibatis.type.Alias;

/**
 * Created by fwj on 16-2-22.
 * hdfs元数据
 */
@Alias("metaHdfsInfo")
public class MetaHdfsInfo {
    private Long id;
    //hdfs地址
    private String hdfsPath;
    //根据地址截取的用户
    private ClusterUser clusterUserId;
    //y用户
    private Long userId;

    //地址所属组
    private String hdfsGroup;
    //地址的权限所属人
    private String hdfsOwner;
    //hdfsPerm
    private String hdfsPerm;
    //note
    private String note;
    //创建时间
    private Long createTime;

//    //添加人
//    private User userId;

    private long countByCluster;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHdfsPath() {
        return hdfsPath;
    }

    public void setHdfsPath(String hdfsPath) {
        this.hdfsPath = hdfsPath;
    }

    public ClusterUser getClusterUserId() {
        return clusterUserId;
    }

    public void setClusterUserId(ClusterUser clusterUserId) {
        this.clusterUserId = clusterUserId;
    }

    public String getHdfsGroup() {
        return hdfsGroup;
    }

    public void setHdfsGroup(String hdfsGroup) {
        this.hdfsGroup = hdfsGroup;
    }

    public String getHdfsOwner() {
        return hdfsOwner;
    }

    public void setHdfsOwner(String hdfsOwner) {
        this.hdfsOwner = hdfsOwner;
    }

    public String getHdfsPerm() {
        return hdfsPerm;
    }

    public void setHdfsPerm(String hdfsPerm) {
        this.hdfsPerm = hdfsPerm;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

//    public User getUserId() {
//        return userId;
//    }
//
//    public void setUserId(User userId) {
//        this.userId = userId;
//    }


    public long getCountByCluster() {
    return countByCluster;
}

    public void setCountByCluster(long countByCluster) {
        this.countByCluster = countByCluster;
    }
}
