package com.ideal.hadoopadmin.entity.main;

import org.apache.ibatis.type.Alias;

/**
 * Created by 袁颖 on 2016/2/18.
 * system_role_menu对应实体
 */
@Alias("roleMenu")//设置别名，用于result映射
public class RoleMenu {
    private long id;
    private long roleId;
    private long menuId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public long getMenuId() {
        return menuId;
    }

    public void setMenuId(long menuId) {
        this.menuId = menuId;
    }
}
