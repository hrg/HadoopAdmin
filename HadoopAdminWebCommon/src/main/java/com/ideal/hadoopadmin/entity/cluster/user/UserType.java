package com.ideal.hadoopadmin.entity.cluster.user;

import org.apache.ibatis.type.Alias;

/**
 * Created by 袁颖 on 2016/2/29.
 */
@Alias("userType")
public class UserType {
    private Long id;//
    private Long userTypeId;//用户类型id
    private String userTypeName;//用户类型名
    private String note;//备注

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Long userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getUserTypeName() {
        return userTypeName;
    }

    public void setUserTypeName(String userTypeName) {
        this.userTypeName = userTypeName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
