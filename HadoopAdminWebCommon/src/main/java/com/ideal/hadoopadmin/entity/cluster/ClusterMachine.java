package com.ideal.hadoopadmin.entity.cluster;

import org.apache.ibatis.type.Alias;

/**
 * Created by 袁颖 on 2016/2/19.
 */
@Alias("clusterMachine")//设置别名，用于mapper映射
public class ClusterMachine {
    public static int addUser= 0 ;//添加用户
    public static int unAddUser = 1;//不添加用户
    Long id;
    String machineIp;//机器ip地址
    String loginUserName;//机器登陆用户
    String loginPassWord;//机器登陆密码
    Long clusterTypeId;
    ClusterType clusterType;//集群类型
    Long machineTypeId;
    MachineType machineType;//机器类型
    int isAddUser;//是否添加用户:0-添加,1-不添加
    int status;//机器状态:0-正常,1-停机
    String note;//机器备注

    public Long getClusterTypeId() {
        return clusterTypeId;
    }

    public void setClusterTypeId(Long clusterTypeId) {
        this.clusterTypeId = clusterTypeId;
    }

    public Long getMachineTypeId() {
        return machineTypeId;
    }

    public void setMachineTypeId(Long machineTypeId) {
        this.machineTypeId = machineTypeId;
    }

    public ClusterType getClusterType() {
        return clusterType;
    }

    public void setClusterType(ClusterType clusterType) {
        this.clusterType = clusterType;
    }

    public MachineType getMachineType() {
        return machineType;
    }

    public void setMachineType(MachineType machineType) {
        this.machineType = machineType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMachineIp() {
        return machineIp;
    }

    public void setMachineIp(String machineIp) {
        this.machineIp = machineIp;
    }

    public String getLoginUserName() {
        return loginUserName;
    }

    public void setLoginUserName(String loginUserName) {
        this.loginUserName = loginUserName;
    }

    public String getLoginPassWord() {
        return loginPassWord;
    }

    public void setLoginPassWord(String loginPassWord) {
        this.loginPassWord = loginPassWord;
    }

    public int getIsAddUser() {
        return isAddUser;
    }

    public void setIsAddUser(int isAddUser) {
        this.isAddUser = isAddUser;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
