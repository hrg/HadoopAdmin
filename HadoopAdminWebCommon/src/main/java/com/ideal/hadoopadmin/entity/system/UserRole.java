package com.ideal.hadoopadmin.entity.system;


/**
 * Created by fwj on 16-3-9.
 * 用户角色管理
 */
public class UserRole {
    private Long id;
    private Long userId;//用户id
    private Long roleId;//角色id
    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}
