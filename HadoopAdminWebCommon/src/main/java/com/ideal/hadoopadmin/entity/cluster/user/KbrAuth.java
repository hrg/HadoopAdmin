package com.ideal.hadoopadmin.entity.cluster.user;
import com.ideal.hadoopadmin.entity.cluster.ClusterMachine;
import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import org.apache.ibatis.type.Alias;

/**
 * Created by fwj on 2016/3/4.
 * 用户认证报表
 */
@Alias("kbrauth")
public class KbrAuth {

    Long id;
    ClusterUser userId;
    ClusterMachine machineId;
    Long startTime;
    Long endTime;
    String principal;
    String ticketPath;
    int status;
    Long createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClusterUser getUserId() {
        return userId;
    }

    public void setUserId(ClusterUser userId) {
        this.userId = userId;
    }

    public ClusterMachine getMachineId() {
        return machineId;
    }

    public void setMachineId(ClusterMachine machineId) {
        this.machineId = machineId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getTicketPath() {
        return ticketPath;
    }

    public void setTicketPath(String ticketPath) {
        this.ticketPath = ticketPath;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }
}
