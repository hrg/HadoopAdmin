package com.ideal.hadoopadmin.entity.main;

import org.apache.ibatis.type.Alias;

import java.util.List;

/**
 * Created by CC on 2016/2/16.
 * 对应菜单表的实例
 *
 */

@Alias("menu")  //设置别名 用于result映射
public class Menu {

    public static int ParentMenu=0;
    public static int ButtonType=2;
    public static int MenuType=1;

    private Long id;
    private String title;
    private String url;
    private Long parentId;
    private int sort;
    private Long createTime;
    private List<Menu> subMenu;
    private String requestUri;
    private int isParent;
    private String cssClass;
    private int type ;
    private String code;
    private int isHidden;
    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public List<Menu> getSubMenu() {
        return subMenu;
    }

    public void setSubMenu(List<Menu> subMenu) {
        this.subMenu = subMenu;
    }

    public String getRequestUri() {
        return requestUri;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public int getIsParent() {
        return isParent;
    }

    public void setIsParent(int isParent) {
        this.isParent = isParent;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    //判断是否是点击url
    public boolean getIsActive() {
        //for jstl 1
        if (requestUri == null || requestUri.equals("")
                || url == null || url.equals("")) {
            return false;
        }
        if(url.startsWith(requestUri)){
            return true;
        }else{
            return false;
        }
    }

    public int getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(int isHidden) {
        this.isHidden = isHidden;
    }
}
