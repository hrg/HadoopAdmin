package com.ideal.hadoopadmin.service.cluster.user;

import com.ideal.hadoopadmin.entity.cluster.user.UserType;
import com.ideal.hadoopadmin.mapper.webdb.cluster.user.UserTypeMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by 袁颖 on 2016/2/29.
 */
@Service
public class UserTypeService {
    @Resource
    private UserTypeMapper userTypeMapper;
    //查找所有的用户类型
    public List<UserType> findUserType(){
        return userTypeMapper.findUserType();
    }
}
