package com.ideal.hadoopadmin.service.cluster.user;

import com.ideal.hadoopadmin.api.hdfs.HDFSAPI;
import com.ideal.hadoopadmin.api.linux.UserAPI;
import com.ideal.hadoopadmin.common.entity.ResultAPI;
import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import com.ideal.hadoopadmin.entity.cluster.user.Clientquota;
import com.ideal.hadoopadmin.entity.cluster.user.Hdfsquota;
import com.ideal.hadoopadmin.entity.cluster.user.Queue;
import com.ideal.hadoopadmin.mapper.webdb.cluster.ClusterUserMapper;
import com.ideal.hadoopadmin.mapper.webdb.cluster.user.ClientquotaMapper;
import com.ideal.hadoopadmin.mapper.webdb.cluster.user.HdfsquotaMapper;
import com.ideal.hadoopadmin.service.cluster.ClusterMachineService;
import com.ideal.hadoopadmin.service.cluster.ParameterService;
import com.ideal.tools.ssh.common.CommonProperties;
import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxMachine;
import com.ideal.tools.ssh.result.LinuxResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by 秦凤霞 on 2016/7/14.
 */
@Service
public class ClientquotaService {
    @Resource
    private ClientquotaMapper clientquotaMapper;
    @Resource
    private ClusterUserMapper clusterUserMapper;

    /**
     * 根据userId找到对应的client配置信息
     * add20160714qinfengxia
     * @param userId
     * @return
     */
    public Clientquota findByUserId(Long userId) {
        return clientquotaMapper.findByUserId(userId);
    }

    /**
     * 保存client配置信息
     * add20160714qinfengxia
     * 入库,前台可能不传任何值
     * @param clusterUser
     */
    public void saveClientDB(ClusterUser clusterUser,List<Clientquota> clientquotaList) {
        for (Clientquota client : clientquotaList) {
            if(null != clusterUser){
                client.setUserId(clusterUser.getId());
            }
            if(null != client.getId() && !"".equals(client.getId())){
                clientquotaMapper.update(client);
            }else{
                clientquotaMapper.save(client);
            }
        }
    }

    /**
     * 更新client配置信息
     * 更新数据库,比较特殊的是可能新建的时候并没有添加clientquota,所以需要先进行判断是否存在这条数据
     * add20160714qinfengxia
     * @param clusterUser
     * @param clientquota
     */
    public void updateClientDB(ClusterUser clusterUser,Clientquota clientquota) {
        Clientquota existClientquota = clientquotaMapper.findByUserId(clientquota.getUserId());
        if (existClientquota == null) {
            //saveClientDB(clusterUser,clientquota);
        } else {
            clientquotaMapper.update(clientquota);
        }
    }

    public void saveClientDB(Clientquota client) {
            if(null != client.getId() && !"".equals(client.getId())){
                clientquotaMapper.update(client);
            }else{
                clientquotaMapper.save(client);
            }
    }

    /**
     * 根据userId进行删除
     * add20160714qinfengxia
     * @param userId
     */
    public void deleteClient(Long userId) {
        clientquotaMapper.deleteByUserId(userId);
    }

    /**
     * 批量更新client配置
     * add20160714qinfengxia
     * @param userIds
     * @param clientquota
     * @return
     */
    public List<String> updateClient(Long[] userIds, Clientquota clientquota) {
        List<String> messageList = new ArrayList<String>();
        //通过client查询出对应的userName
        for (int i = 0; i < userIds.length; i++) {
            clientquota.setUserId(userIds[i]);
            ClusterUser clusterUser = clusterUserMapper.findById(userIds[i]);
            //todo 请补全需要的接口信息add20160715qinfengxia
            if (clientquota.getClientCatalog()== null && clientquota.getClientCatalogSize()== null) {//如果这俩个数据为空,即把对应用户里面的client删除
                deleteClient(userIds[i]);
            } else {
                updateClientDB(clusterUser,clientquota);
            }
            messageList.add("<div>数据库信息:client修改成功!</div>");
        }
        return messageList;
    }
}
