package com.ideal.hadoopadmin.service.system;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.common.framework.orm.SearchFilter;
import com.ideal.hadoopadmin.entity.system.Role;
import com.ideal.hadoopadmin.entity.system.UserRole;
import com.ideal.hadoopadmin.mapper.webdb.system.RoleMapper;
import com.ideal.hadoopadmin.mapper.webdb.system.UserRoleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by fwj on 2016/3/3.
 * 队列资源访问报表
 */
@Service
public class UserRoleService {

    private static Logger logger = LoggerFactory.getLogger(UserRoleService.class);

    @Resource
    private UserRoleMapper userRoleMapper;

    public List<UserRole> queryUserRoleByRoleId(Long roleId){
        return userRoleMapper.queryUserRoleByRoleId(roleId);
    }
    /**
     *  新增
     **/
    public void saveUserRole(UserRole userRole){
        userRoleMapper.saveUserRole(userRole);
    }
    public List<Long> queryRoleIdByUserId(Long userId){
        return userRoleMapper.queryRoleIdByUserId(userId);
    }
    public void deleteUserRoleByUserId(Long userId){
        userRoleMapper.deleteUserRoleByUserId(userId);
    }

    public List<String> queryUserRoleByUserId(Long userId){
        return userRoleMapper.queryUserRoleByUserId(userId);
    }
}
