package com.ideal.hadoopadmin.service.cluster;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.entity.cluster.ClusterType;
import com.ideal.hadoopadmin.mapper.webdb.cluster.ClusterTypeMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by 袁颖 on 2016/2/22.
 */
@Service
public class ClusterTypeService {
    @Resource
    private ClusterTypeMapper clusterTypeMapper;
    public List<ClusterType> findAllClusterType(){
        return clusterTypeMapper.findAllClusterType();
    }
    //分页及查询
    public PageInfo pageCluster(Map<String,Object> searchParams,int currentPage,int defaultSize){
        PageHelper.startPage(currentPage,defaultSize);
        List<ClusterType> clusterTypes = clusterTypeMapper.findAllClusterType();
        PageInfo pageInfo = new PageInfo(clusterTypes);
        return  pageInfo;
    }
    //删除
    public void deleteById(Long id){
        clusterTypeMapper.deleteById(id);
    }

    //更新
    public void update(ClusterType clusterType){
        clusterTypeMapper.updateById(clusterType);
    }

    //保存
    public void save(ClusterType clusterType){
        clusterTypeMapper.insert(clusterType);
    }

    //通过id进行查找
    public ClusterType findById(Long id){
       return clusterTypeMapper.findById(id);
    }

    //查找所有的clusterTypeId
    public Long findMaxClusterTypeIds(){
      return clusterTypeMapper.findMaxClusterTypeId();
    }
    /*查找出clusterType不为当前clusterTypeId的所有clusterType*/
    public List<ClusterType> findOtherClusterType(Long clusterTypeId){
        return clusterTypeMapper.findOtherClusterType(clusterTypeId);
    }
}
