package com.ideal.hadoopadmin.service.cluster;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.common.framework.orm.SearchFilter;
import com.ideal.hadoopadmin.entity.cluster.ClusterCompanyQuota;
import com.ideal.hadoopadmin.entity.system.company.SystemCompany;
import com.ideal.hadoopadmin.mapper.webdb.cluster.ClusterCompanyQuotaMapper;
import com.ideal.hadoopadmin.mapper.webdb.system.company.SystemCompanyMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by fwj on 2016/2/22.
 * 公司管理
 */
@Service
public class ClusterCompanyQuotaService {



    @Resource
    private ClusterCompanyQuotaMapper clusterCompanyQuotaMapper;
    /**
     * 根据id查询公司信息
     * */
    public ClusterCompanyQuota findSystemCompanyById(Long companyId){
        return clusterCompanyQuotaMapper.findClusterCompanyQuotaById(companyId);
    }

    /**
     * 根据id删除公司信息
     * */
    public void deleteSystemCompanById(Long[] ids){
        for(int i=0;i<ids.length;i++){
            //systemCompanyMapper.deleteSystemCompanById(ids[i]);
        }
    }
}
