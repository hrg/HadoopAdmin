package com.ideal.hadoopadmin.service.cluster.user;

import com.ideal.hadoopadmin.entity.cluster.user.Wizard;
import com.ideal.hadoopadmin.mapper.webdb.cluster.user.WizardMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by 袁颖 on 2016/3/17.
 */
@Service
public class WizardService {
    @Resource
    private WizardMapper wizardMapper;
    public List<Wizard> findAllWizard(){
        return wizardMapper.findAllWizard();
    }
}
