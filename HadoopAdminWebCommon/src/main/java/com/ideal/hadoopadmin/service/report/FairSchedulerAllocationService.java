package com.ideal.hadoopadmin.service.report;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.common.framework.orm.SearchFilter;
import com.ideal.hadoopadmin.entity.report.FairSchedulerAllocation;
import com.ideal.hadoopadmin.mapper.webdb.report.FairSchedulerAllocationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by fwj on 2016/3/3.
 * 队列资源访问报表
 */
@Service
public class FairSchedulerAllocationService {

    private static Logger logger = LoggerFactory.getLogger(FairSchedulerAllocationService.class);

    @Resource
    private FairSchedulerAllocationMapper fairSchedulerAllocationMapper;

    public PageInfo queryFairSchedulerAllocationList(Map<String, Object> searchParams,HttpServletRequest request) {
        int currentPage = Integer.parseInt(request.getParameter("page") == null ? "1" : request.getParameter("page"));
        //过滤器
        String where = SearchFilter.parseToString(searchParams);
        //使用分页
        PageHelper.startPage(currentPage, 10);
        //加入组织好的where 条件
        PageHelper.setAppendWhere(where);
        List<FairSchedulerAllocation> metaHdfsInfoList = fairSchedulerAllocationMapper.queryFairSchedulerAllocationList();
        PageInfo pageInfo = new PageInfo(metaHdfsInfoList);
        return pageInfo;
    }

    public List<FairSchedulerAllocation> queryFairSchedulerAllocationListByDown(Long clusterId,String queue) {
        List<FairSchedulerAllocation> metaHdfsInfoList = fairSchedulerAllocationMapper.queryFairSchedulerAllocationListBySearchParam(clusterId,queue);
        return metaHdfsInfoList;
    }



    public List<String> findQueue(Long clusterUserId) {
        List<String> queues = new ArrayList<String>();
        if (clusterUserId==null) {
            queues = fairSchedulerAllocationMapper.findAllQueue();
        } else {
            queues = fairSchedulerAllocationMapper.findQueueByUsername(clusterUserId);
        }
        return queues;
    }
}
