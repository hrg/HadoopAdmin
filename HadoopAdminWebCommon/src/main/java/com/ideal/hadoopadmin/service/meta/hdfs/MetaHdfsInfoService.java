package com.ideal.hadoopadmin.service.meta.hdfs;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.api.better.hdfs.HadoopHDFSAPI;
import com.ideal.hadoopadmin.api.hdfs.HDFSAPI;
import com.ideal.hadoopadmin.api.linux.UserAPI;
import com.ideal.hadoopadmin.common.entity.Result;
import com.ideal.hadoopadmin.common.entity.ResultAPI;
import com.ideal.hadoopadmin.common.framework.orm.SearchFilter;
import com.ideal.hadoopadmin.entity.cluster.ClusterMachine;
import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import com.ideal.hadoopadmin.entity.cluster.user.*;
import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsAccess;
import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsInfo;
import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsInfoBak;
import com.ideal.hadoopadmin.entity.report.HdfsInfoCustom;
import com.ideal.hadoopadmin.mapper.webdb.cluster.ClusterUserMapper;
import com.ideal.hadoopadmin.mapper.webdb.meta.MetaHdfsAccessMapper;
import com.ideal.hadoopadmin.mapper.webdb.meta.MetaHdfsInfoBakMapper;
import com.ideal.hadoopadmin.mapper.webdb.meta.MetaHdfsInfoMapper;
import com.ideal.hadoopadmin.service.cluster.ClusterMachineService;
import com.ideal.hadoopadmin.service.cluster.ParameterService;
import com.ideal.tools.ssh.common.CommonProperties;
import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.ContextResult;
import com.ideal.tools.ssh.entity.LinuxMachine;
import com.ideal.tools.ssh.result.LinuxResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.Queue;
import java.util.logging.Filter;

/**
 * Created by fwj on 2016/2/22.
 * hdfs元数据
 */
@Service
@Transactional
public class MetaHdfsInfoService {

    private static Logger logger = LoggerFactory.getLogger(MetaHdfsInfoService.class);

    @Resource
    private MetaHdfsInfoMapper metaHdfsInfoMapper;
    @Resource
    private ClusterUserMapper clusterUserMapper;
    @Resource
    private MetaHdfsInfoBakMapper metaHdfsInfoBakMapper;
    @Resource
    private MetaHdfsAccessMapper metaHdfsAccessMapper;
    @Resource
    ParameterService parameterService;
    @Resource
    ClusterMachineService clusterMachineService;

    public PageInfo queryMetaHdfsInfo(Map<String, Object> searchParams, HttpServletRequest request) {
        int currentPage = Integer.parseInt(request.getParameter("page") == null ? "1" : request.getParameter("page"));
        //过滤器
        String where = SearchFilter.parseToString(searchParams);
        //使用分页
        PageHelper.startPage(currentPage, 10);
        //加入组织好的where 条件
        PageHelper.setAppendWhere(where);
        List<MetaHdfsInfo> metaHdfsInfoList = metaHdfsInfoMapper.queryMetaHdfsInfo();
        PageInfo pageInfo = new PageInfo(metaHdfsInfoList);
        return pageInfo;
    }

    /**
     * 根据id查询
     */
    public MetaHdfsInfo queryMetaHdfsInfoById(Long id) {
        return metaHdfsInfoMapper.queryMetaHdfsInfoById(id);
    }

    //添加hdfs
    public List<String> addHdfs(MetaHdfsInfo metaHdfsInfo) {
        List<String> messageList = new ArrayList<String>();
        //注释掉旧方法，用新方法update20160802qinfengxia
        //List<LinuxResult> linuxResults = saveHdfsAPI(metaHdfsInfo.getUserId(), metaHdfsInfo.getHdfsPath());
        List<LinuxResult> linuxResults = saveHdfsAPINew(metaHdfsInfo.getUserId(), metaHdfsInfo.getHdfsPath());
        ResultAPI.initAPIResult(linuxResults);
        messageList.addAll(ResultAPI.messageList);
        if (ResultAPI.flag) {
            saveHdfs(metaHdfsInfo);
            messageList.add("<div>数据库信息:hdfs添加成功!</div>");
        }
        return messageList;
    }

    //保存hdfs数据
    public void saveHdfs(MetaHdfsInfo metaHdfsInfo) {
        ClusterUser clusterUser = clusterUserMapper.queryClusterUserById(metaHdfsInfo.getUserId());
        String owner = clusterUser.getUserName();
        String entity = metaHdfsInfo.getHdfsPath();
        metaHdfsInfo.setHdfsPath("/user/" + owner + "/public/" + entity);
        metaHdfsInfo.setHdfsPerm("drwxr-x---");//默认权限750
        metaHdfsInfo.setHdfsOwner(owner);
        metaHdfsInfo.setHdfsGroup(owner + "_" + entity);
        metaHdfsInfoMapper.saveMetaHdfsInfo(metaHdfsInfo);
    }

    public void saveHdfsDB(MetaHdfsInfo metaHdfsInfo) {
        metaHdfsInfoMapper.saveMetaHdfsInfo(metaHdfsInfo);
    }

    //调用保存接口
    public List<LinuxResult> saveHdfsAPI(Long userId, String hdfsPath) {
        ClusterUser clusterUser = clusterUserMapper.findById(userId);
        ClusterContext context = initHdfsAPI(clusterUser.getUserName(), hdfsPath);
        HDFSAPI.MakeHDFSPubDir(context);
        return context.getContextResult().getLastResult();
    }

    /**
     * 调用保存接口
     * @param userId
     * @param hdfsPath
     * @return
     */
    public List<LinuxResult> saveHdfsAPINew(Long userId, String hdfsPath) {
        ClusterUser clusterUser = clusterUserMapper.findById(userId);
        Map<String, Object> params = initHdfsAPINew(clusterUser.getUserName(), hdfsPath);
        ContextResult contextResult = HadoopHDFSAPI.CreateHDFSPubDir(params);
        return contextResult.getLastResult();
    }

    /**
     * 修改,只能修改描述
     */
    public void updateMetaHdfsInfoById(MetaHdfsInfo metaHdfsInfo) {
        metaHdfsInfoMapper.updateMetaHdfsInfoById(metaHdfsInfo);
    }

    //进行hdfs删除
    public List<String> deleteHdfs(Long hdfsId) {
        List<String> messageList = new ArrayList<String>();
        //注释掉旧方法，用新的方法update20160802qinfengxia
        //List<LinuxResult> linuxResults = deleteHdfsAPI(hdfsId);
        List<LinuxResult> linuxResults = deleteHdfsAPINew(hdfsId);
        ResultAPI.initAPIResult(linuxResults);
        messageList.addAll(ResultAPI.messageList);
        if (ResultAPI.flag) {
            deleteMetaHdfsInfoById(hdfsId);
            messageList.add("<div>数据库信息:hdfs删除成功!</div>");
        }
        return messageList;
    }

    public ClusterContext initHdfsAPI(String userName, String hdfsPath) {
        Map<String, String> map = parameterService.getAllParameter();
        List<LinuxMachine> linuxMachines = clusterMachineService.getMachineList(null);
        CommonProperties commonProperties = new CommonProperties(map);
        commonProperties.setArgument(UserAPI.Cluster_User_Name, userName);
        commonProperties.setArgument(HDFSAPI.HDFS_PUB_TABLE_DIR, hdfsPath);
        ClusterContext context = new ClusterContext(commonProperties);
        context.setOriginalList(linuxMachines);
        return context;
    }


    public Map<String, Object> initHdfsAPINew(String userName, String hdfsPath) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(UserAPI.Cluster_User_Name, userName);
        params.put(HDFSAPI.HDFS_PUB_TABLE_DIR, hdfsPath);
        return params;
    }
    public ClusterContext initHdfsAPI(String path_owner, String userName, String hdfsPath) {
        Map<String, String> map = parameterService.getAllParameter();
        List<LinuxMachine> linuxMachines = clusterMachineService.getMachineList(null);
        CommonProperties commonProperties = new CommonProperties(map);
        commonProperties.setArgument(UserAPI.Cluster_User_Name, userName);
        commonProperties.setArgument(UserAPI.Cluster_HDFS_PATH_OWNER, path_owner);
        commonProperties.setArgument(HDFSAPI.HDFS_PUB_TABLE_DIR, hdfsPath);
        ClusterContext context = new ClusterContext(commonProperties);
        context.setOriginalList(linuxMachines);
        return context;
    }

    public Map<String, Object> initHdfsAPINew(String path_owner, String userName, String hdfsPath) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(UserAPI.Cluster_User_Name, userName);
        params.put(UserAPI.Cluster_HDFS_PATH_OWNER, path_owner);
        params.put(HDFSAPI.HDFS_PUB_TABLE_DIR, hdfsPath);
        return params;
    }
    public ClusterContext initHdfsAPI(String path_owner, String userName, String hdfsPath, String otherAcess) {
        Map<String, String> map = parameterService.getAllParameter();
        List<LinuxMachine> linuxMachines = clusterMachineService.getMachineList(null);
        CommonProperties commonProperties = new CommonProperties(map);
        commonProperties.setArgument(HDFSAPI.HDFS_HAS_OTHER_ACCESS, otherAcess);
        commonProperties.setArgument(UserAPI.Cluster_User_Name, userName);
        commonProperties.setArgument(UserAPI.Cluster_HDFS_PATH_OWNER, path_owner);
        commonProperties.setArgument(HDFSAPI.HDFS_PUB_TABLE_DIR, hdfsPath);
        ClusterContext context = new ClusterContext(commonProperties);
        context.setOriginalList(linuxMachines);
        return context;
    }

    public Map<String, Object> initHdfsAPINew(String path_owner, String userName, String hdfsPath, String otherAcess) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(HDFSAPI.HDFS_HAS_OTHER_ACCESS, otherAcess);
        params.put(UserAPI.Cluster_User_Name, userName);
        params.put(UserAPI.Cluster_HDFS_PATH_OWNER, path_owner);
        params.put(HDFSAPI.HDFS_PUB_TABLE_DIR, hdfsPath);
        return params;
    }


    public List<LinuxResult> deleteHdfsAPI(Long hdfsId) {
        MetaHdfsInfo metaHdfsInfo = metaHdfsInfoMapper.queryMetaHdfsInfoById(hdfsId);
        String hdfsPath = metaHdfsInfo.getHdfsPath();
        String path = ordinalSubstring(hdfsPath, "/", 4);
        ClusterContext context = initHdfsAPI(metaHdfsInfo.getClusterUserId().getUserName(), path);
        HDFSAPI.RMHDFSPubDir(context);
        return context.getContextResult().getLastResult();
    }

    /**
     * 删除HDFS
     * update20160802qinfengxia
     * @param hdfsId
     * @return
     */
    public List<LinuxResult> deleteHdfsAPINew(Long hdfsId) {
        MetaHdfsInfo metaHdfsInfo = metaHdfsInfoMapper.queryMetaHdfsInfoById(hdfsId);
        String hdfsPath = metaHdfsInfo.getHdfsPath();
        String path = ordinalSubstring(hdfsPath, "/", 4);
        Map<String, Object> params = initHdfsAPINew(metaHdfsInfo.getClusterUserId().getUserName(), path);
        ContextResult contextResult = HadoopHDFSAPI.RemoveHDFSPubDir(params);
        return contextResult.getLastResult();
    }
    /**
     * 根据id删除数据
     * *
     */
    public void deleteMetaHdfsInfoById(Long id) {
        metaHdfsInfoMapper.deleteMetaHdfsInfoById(id);
    }

    //获取组内用户
    public PageInfo findGroupInner(Long resourceId, Map<String, Object> searchParams, int currenPage, int defaultNum) {
        //防止sql太长,直接先查询出自身的userId
        Long userId = metaHdfsInfoMapper.findUserIdByHdfsInfoId(resourceId);
        String where = SearchFilter.parseToString(searchParams);
        //通过resource
        PageHelper.startPage(currenPage, 8);
        PageHelper.setAppendWhere(where);
        //获取所有的组内用户
        List<ClusterUser> innerUser = clusterUserMapper.findInnerByHdfsInfo(userId, resourceId);
//        List<ClusterUser> innerUser = clusterUserMapper.findInnerByHdfsInfoId(resourceId);
        return new PageInfo(innerUser);
    }

    //获取组外用户
    public PageInfo findGroupOuter(Long resourceId, Map<String, Object> searchParams, int currenPage, int defaultNum) {
        Long userId = metaHdfsInfoMapper.findUserIdByHdfsInfoId(resourceId);
        String where = SearchFilter.parseToString(searchParams);
        PageHelper.startPage(currenPage, 8);
        PageHelper.setAppendWhere(where);
        List<ClusterUser> outerUser = clusterUserMapper.findOuterByHdfsInfoId(userId, resourceId);
        return new PageInfo(outerUser);
    }

    //移除组内用户
    public List<String> deleteInnerUser(Long[] userIds, Long hdfsId) {
        List<String> messageList = new ArrayList<String>();
        for (int i = 0; i < userIds.length; i++) {
            //注释掉旧方法，调用新方法update20160802qinfengxia
            //List<LinuxResult> linuxResults = deleteInnerUserAPI(userIds[i], hdfsId);
            List<LinuxResult> linuxResults = deleteInnerUserAPINew(userIds[i], hdfsId);
            ResultAPI.initAPIResult(linuxResults);
            messageList.addAll(ResultAPI.messageList);
            if (ResultAPI.flag) {
                deleteInnerByUserId(userIds[i]);
                messageList.add("<div>数据库信息:移除组内用户成功!</div>");
            }
        }
        return messageList;
    }

    public List<LinuxResult> deleteInnerUserAPI(long userId, Long hdfsId) {
        //被移除的用户是否还能访问 目录所有者的其他目录
        int count = metaHdfsAccessMapper.findHdfsHasOtherAcess(userId, hdfsId);
        String otherAcess = HDFSAPI.HDFS_FALSE;
        if (count > 1) {
            otherAcess = HDFSAPI.HDFS_TRUE;
        }
        MetaHdfsInfo metaHdfsInfo = metaHdfsInfoMapper.queryMetaHdfsInfoById(hdfsId);
        String hdfsPath = metaHdfsInfo.getHdfsPath();
        String path = ordinalSubstring(hdfsPath, "/", 4);
        ClusterUser clusterUser = clusterUserMapper.findById(userId);
        ClusterContext context = initHdfsAPI(metaHdfsInfo.getClusterUserId().getUserName(), clusterUser.getUserName(), path, otherAcess);
        HDFSAPI.refuseUserToGroup(context);
        return context.getContextResult().getLastResult();
    }

    /**
     * update20160802qinfengxia
     * @param userId
     * @param hdfsId
     * @return
     */
    public List<LinuxResult> deleteInnerUserAPINew(long userId, Long hdfsId) {
        //被移除的用户是否还能访问 目录所有者的其他目录
        int count = metaHdfsAccessMapper.findHdfsHasOtherAcess(userId, hdfsId);
        String otherAcess = HDFSAPI.HDFS_FALSE;
        if (count > 1) {
            otherAcess = HDFSAPI.HDFS_TRUE;
        }
        MetaHdfsInfo metaHdfsInfo = metaHdfsInfoMapper.queryMetaHdfsInfoById(hdfsId);
        String hdfsPath = metaHdfsInfo.getHdfsPath();
        String path = ordinalSubstring(hdfsPath, "/", 4);
        ClusterUser clusterUser = clusterUserMapper.findById(userId);
        Map<String, Object> params = initHdfsAPINew(metaHdfsInfo.getClusterUserId().getUserName(), clusterUser.getUserName(), path, otherAcess);
        ContextResult contextResult = HadoopHDFSAPI.RemoveUserFromGroup(params);
        return contextResult.getLastResult();
    }

    //移除组内用户数据
    public void deleteInnerByUserId(Long userId) {
        metaHdfsAccessMapper.deleteByUserId(userId);
    }

    //添加组外用户
    public List<String> addOuterUser(Long[] userIds, Long hdfsId) {
        List<String> messageList = new ArrayList<String>();
        MetaHdfsAccess metaHdfsAccess = new MetaHdfsAccess();
        metaHdfsAccess.setHdfsInfoId(hdfsId);
        //根据hdfsId查出来hdfsPath并且切割
        MetaHdfsInfo metaHdfsInfo = metaHdfsInfoMapper.queryMetaHdfsInfoById(hdfsId);
        String hdfsPath = metaHdfsInfo.getHdfsPath();
        String path = ordinalSubstring(hdfsPath, "/", 4);
        for (int i = 0; i < userIds.length; i++) {
            metaHdfsAccess.setCreateTime(System.currentTimeMillis());
            metaHdfsAccess.setClusterUserId(userIds[i]);
            ClusterUser clusterUser = clusterUserMapper.findById(userIds[i]);
            //todo 暂时注释掉
            //注释掉旧方法，调用新方法update20160802qinfengxia
            //List<LinuxResult> linuxResults = addOuterUserAPI(metaHdfsInfo.getClusterUserId().getUserName(), clusterUser.getUserName(), path);
            List<LinuxResult> linuxResults = addOuterUserAPINew(metaHdfsInfo.getClusterUserId().getUserName(), clusterUser.getUserName(), path);
            ResultAPI.initAPIResult(linuxResults);
            messageList.addAll(ResultAPI.messageList);
            if (ResultAPI.flag) {
                addOuterUserById(metaHdfsAccess);
                messageList.add("<div>数据库信息:添加组外用户成功!</div>");
            }
        }
        return messageList;
    }

    //添加组外用户数据
    public void addOuterUserById(MetaHdfsAccess metaHdfsAccess) {
        metaHdfsAccessMapper.saveMetaHdfsAccess(metaHdfsAccess);
    }

    public List<LinuxResult> addOuterUserAPI(String path_owner, String userName, String path) {
        ClusterContext context = initHdfsAPI(path_owner, userName, path);
        HDFSAPI.accessUserToGroup(context);
        return context.getContextResult().getLastResult();
    }

    /**
     * update20160802qinfengxia
     * @param path_owner
     * @param userName
     * @param path
     * @return
     */
    public List<LinuxResult> addOuterUserAPINew(String path_owner, String userName, String path) {
        Map<String, Object> params = initHdfsAPINew(path_owner, userName, path);
        ContextResult contextResult = HadoopHDFSAPI.AccessUserToGroup(params);
        return contextResult.getLastResult();
    }

    //
    public List<MetaHdfsInfo> findPathByUserId(Long userId) {
        return metaHdfsInfoMapper.findPathByUserId(userId);
    }

    public List<MetaHdfsInfo> findPathByUserAndTbl(String hdfsPath, Long clusterUserId) {
        return metaHdfsInfoMapper.findPathByUserAndTbl(hdfsPath, clusterUserId);
    }


    public PageInfo hdfsCompare(int currentNum, Map<String, Object> searchParam) {
        String where = SearchFilter.parseToString(searchParam);
        PageHelper.startPage(currentNum, 5);
        PageHelper.setAppendWhere(where);
        List<MetaHdfsInfo> metaHdfsInfos = metaHdfsInfoMapper.compareWithHdfsInfoBak();
        PageInfo pageInfo = new PageInfo(metaHdfsInfos);
        return pageInfo;
    }

    public Long findHdfsInfoIdByHiveInfoId(Long hdfsInfoBakId) {
        return metaHdfsInfoMapper.findHdfsInfoIdByHiveInfoId(hdfsInfoBakId);
    }

    public List<MetaHdfsInfo> compareWithHdfsInfoBak() {
        return metaHdfsInfoMapper.compareWithHdfsInfoBak();
    }

    /**
     * hdfs第一级数据
     * add20160721qinfengxia
     * @param currentPage
     * @param defaultSize
     * @param searchParam
     * @return
     */
    public PageInfo searchReportPage(int currentPage,int defaultSize,Map<String,Object> searchParam) {
        String where = SearchFilter.parseToString(searchParam);
        PageHelper.startPage(currentPage,defaultSize);
        PageHelper.setAppendWhere(where);
        List<MetaHdfsInfo> metaHdfsInfo = metaHdfsInfoMapper.hdfsParentList();
        return new PageInfo(metaHdfsInfo);
    }

    /**
     * hdfs（用户主导）第一级数据
     * add20160918qinfengxia
     * @param currentPage
     * @param defaultSize
     * @param searchParam
     * @return
     */
    public PageInfo searchHdfsUserPage(int currentPage,int defaultSize,Map<String,Object> searchParam) {
        String where = SearchFilter.parseToString(searchParam);
        PageHelper.startPage(currentPage,defaultSize);
        PageHelper.setAppendWhere(where);
        List<Map<String,Object>> metaHdfsInfo = metaHdfsInfoMapper.hdfsUserParentList();
        return new PageInfo(metaHdfsInfo);
    }
    /**
     * hdfs第二级数据（权限）
     * add20160721qinfengxia
     * @param clusterUserId
     * @param hdfsPath
     * @return
     */
    public List<Map<String,Object>>  searchReportChildPage(int clusterUserId,String hdfsPath) {
        Map<String, Object> paraMap = new HashMap<String, Object>();
        if(null != hdfsPath && !"".equals(hdfsPath)){
            paraMap.put("hdfsPath",hdfsPath);
        }
        paraMap.put("clusterUserId",clusterUserId);
        List<Map<String,Object>> list = metaHdfsInfoMapper.hdfsPathAccessList(paraMap);
        return list;
    }

    /**
     * hdfs（用户主导）第二级数据
     * add20160721qinfengxia
     * @param clusterUserId
     * @return
     */
    public List<Map<String,Object>>  searchHdfsUserChildPage(int clusterUserId) {
        Map<String, Object> paraMap = new HashMap<String, Object>();
        paraMap.put("clusterUserId",clusterUserId);
        List<Map<String,Object>> list = metaHdfsInfoMapper.hdfsUserChildList(paraMap);
        return list;
    }
    public List<MetaHdfsInfo> findAllHdfsInfo(){
        return metaHdfsInfoMapper.queryMetaHdfsInfo();
    }
    public List<MetaHdfsInfo> findAllHdfsPath(){
        return metaHdfsInfoMapper.queryMetaHdfsPath();
    }
/*********************************************一些公用方法**********************************************************/
    /**
     * @param string 需要进行处理的string
     * @param search 根据查找的字符串
     * @param count  字符串出现的次数
     * @return 获取search出现count次位置之后的string子字符串, string为null或者查找不到, 返回空字符串
     */
    public static String ordinalSubstring(String string, String search, int count) {
        if (string.startsWith("hdfs:/")) {
            string = string.replace("hdfs:/", "");
            count++;
        }
        int index = StringUtils.ordinalIndexOf(string, search, count);//获取"/"第四次出现的位置,查找不到返回-1
        String path = "";
        if (index != -1) {
            path = StringUtils.substring(string, index + 1);
        }
        return path;
    }
    /**
     * 根据条件查询hdfs信息
     * add20160721qinfengxia
     */
    public  List<Map<String,Object>> queryHdfsByDown(String hdfsPath) {
        Map<String, Object> paraMap = new HashMap<String, Object>();
        if(null != hdfsPath && !"".equals(hdfsPath)){
            paraMap.put("hdfsPath",hdfsPath);
        }
        List<Map<String,Object>> list = metaHdfsInfoMapper.hdfsPathAccessList(paraMap);
        return list;
    }

    /**
     * 根据条件查询hdfs信息
     * add20160918qinfengxia
     */
    public  List<Map<String,Object>> queryHdfsUserByDown(String userName) {
        Map<String, Object> paraMap = new HashMap<String, Object>();
        if(null != userName && !"".equals(userName)){
            paraMap.put("userName",userName);
        }
        List<Map<String,Object>> list = metaHdfsInfoMapper.hdfsUserChildList(paraMap);
        return list;
    }

    /**
     * 创建excel工作薄
     * @param list
     * @return
     */
    public HSSFWorkbook creatSheet(List<Map<String,Object>> list) {
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook();//创建excel工作薄
        HSSFCellStyle cellStyle = hssfWorkbook.createCellStyle(); //创建表格样式
        cellStyle.setWrapText(true);                        //自动换行
        HSSFSheet hssfSheet = hssfWorkbook.createSheet();//创建excel表格
        hssfSheet.setColumnWidth(0, 4000);
        hssfSheet.setColumnWidth(1, 10000);
        hssfSheet.setColumnWidth(2, 4000);
        HSSFRow hssfRow1 = hssfSheet.createRow(0);//创建行 标题
        HSSFRow hssfRow2 = hssfSheet.createRow(1);//创建行 th名
        HSSFCell cell1 = hssfRow2.createCell(0);
        HSSFCell cell2 = hssfRow2.createCell(1);
        HSSFCell cell3 = hssfRow2.createCell(2);
        cell1.setCellValue(new HSSFRichTextString("所属用户"));
        cell2.setCellValue(new HSSFRichTextString("hdfs地址"));
        cell3.setCellValue(new HSSFRichTextString("可访问用户"));

        HSSFCellStyle thStyle = hssfWorkbook.createCellStyle();       //创建样式
        thStyle.setFillPattern(HSSFCellStyle.FINE_DOTS);
        thStyle.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
        thStyle.setFillBackgroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
        cell1.setCellStyle(thStyle);
        cell2.setCellStyle(thStyle);
        cell3.setCellStyle(thStyle);
        for (int i = 0; i < list.size(); i++) {
            Map<String,Object> map = list.get(i);
            HSSFRow hssfRow = hssfSheet.createRow(i + 2);//创建行
            HSSFCell hssfCell1 = hssfRow.createCell(0);
            HSSFCell hssfCell2 = hssfRow.createCell(1);
            HSSFCell hssfCell3 = hssfRow.createCell(2);
            hssfCell1.setCellStyle(cellStyle);
            hssfCell2.setCellStyle(cellStyle);
            hssfCell3.setCellStyle(cellStyle);
            hssfCell1.setCellValue(new HSSFRichTextString(map.get("ownerUserName").toString()));
            hssfCell2.setCellValue(new HSSFRichTextString(map.get("hdfsPath").toString()));
            hssfCell3.setCellValue(new HSSFRichTextString(map.get("accessUserName").toString()));
        }
        return hssfWorkbook;
    }

    /**
     * 创建excel工作薄
     * @param list
     * @return
     */
    public HSSFWorkbook creatSheetHdfsUser(List<Map<String,Object>> list) {
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook();//创建excel工作薄
        HSSFCellStyle cellStyle = hssfWorkbook.createCellStyle(); //创建表格样式
        cellStyle.setWrapText(true);                        //自动换行
        HSSFSheet hssfSheet = hssfWorkbook.createSheet();//创建excel表格
        hssfSheet.setColumnWidth(0, 4000);
        hssfSheet.setColumnWidth(1, 10000);
        HSSFRow hssfRow1 = hssfSheet.createRow(0);//创建行 标题
        HSSFRow hssfRow2 = hssfSheet.createRow(1);//创建行 th名
        HSSFCell cell1 = hssfRow2.createCell(0);
        HSSFCell cell2 = hssfRow2.createCell(1);
        cell1.setCellValue(new HSSFRichTextString("用户名"));
        cell2.setCellValue(new HSSFRichTextString("hdfs地址"));

        HSSFCellStyle thStyle = hssfWorkbook.createCellStyle();       //创建样式
        thStyle.setFillPattern(HSSFCellStyle.FINE_DOTS);
        thStyle.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
        thStyle.setFillBackgroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
        cell1.setCellStyle(thStyle);
        cell2.setCellStyle(thStyle);
        for (int i = 0; i < list.size(); i++) {
            Map<String,Object> map = list.get(i);
            HSSFRow hssfRow = hssfSheet.createRow(i + 2);//创建行
            HSSFCell hssfCell1 = hssfRow.createCell(0);
            HSSFCell hssfCell2 = hssfRow.createCell(1);
            hssfCell1.setCellStyle(cellStyle);
            hssfCell2.setCellStyle(cellStyle);

            hssfCell1.setCellValue(new HSSFRichTextString(map.get("userName")==null||map.get("userName")==""?"":map.get("userName").toString()));
            hssfCell2.setCellValue(new HSSFRichTextString(map.get("hdfsPath")==null||map.get("hdfsPath")==""?"":map.get("hdfsPath").toString()));
        }
        return hssfWorkbook;
    }

}
