package com.ideal.hadoopadmin.service.system.company;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.common.framework.orm.SearchFilter;
import com.ideal.hadoopadmin.entity.cluster.ClusterCompanyQuota;
import com.ideal.hadoopadmin.entity.system.company.SystemCompany;
import com.ideal.hadoopadmin.mapper.webdb.cluster.ClusterCompanyQuotaMapper;
import com.ideal.hadoopadmin.mapper.webdb.system.company.SystemCompanyMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by fwj on 2016/2/22.
 * 公司管理
 */
@Service
public class SystemCompanyService {


    @Resource
    private SystemCompanyMapper systemCompanyMapper;
    @Resource
    private ClusterCompanyQuotaMapper clusterCompanyQuotaMapper;
    public PageInfo queryClusterCompany(Map<String,Object> searchParams,HttpServletRequest request){
        //从前台获取当前页码
        int currentPage = Integer.parseInt(request.getParameter("page")==null?"1":request.getParameter("page"));
        //过滤器
        String where = SearchFilter.parseToString(searchParams);
        //使用分页
        PageHelper.startPage(currentPage, 10);
        //加入组织好的where 条件
        PageHelper.setAppendWhere(where);
        List<SystemCompany> clusterCompanyList=systemCompanyMapper.queryClusterCompany();
        PageInfo page = new PageInfo(clusterCompanyList);
        return page;
    }
    /**
     * 根据id查询公司信息
     * */
    public SystemCompany findSystemCompanyById(Long id){
        return systemCompanyMapper.findSystemCompanyById(id);
    }
    /**
     * 添加公司信息
     * */
    public void saveSystemCompany(SystemCompany systemCompany,ClusterCompanyQuota clusterCompanyQuota,String clusterCompanyQuotaId){
        if(null==systemCompany.getId()){
            systemCompanyMapper.saveSystemCompany(systemCompany);
            //同步往cluster_company_quota新增数据
            clusterCompanyQuota.setCompanyId(systemCompany.getId());
            clusterCompanyQuotaMapper.saveClusterCompanyQuota(clusterCompanyQuota);
        }else{
            systemCompanyMapper.updateSystemCompanyById(systemCompany);
            //修改时保存cluster_company_quota   add20160804qinfengxia
            if(null != clusterCompanyQuotaId && !"".equals(clusterCompanyQuotaId)){
                clusterCompanyQuota.setId(Long.valueOf(clusterCompanyQuotaId.toString()));
                clusterCompanyQuota.setCompanyId(systemCompany.getId());
                clusterCompanyQuotaMapper.updateClusterCompanyQuotaById(clusterCompanyQuota);
            }else{
                clusterCompanyQuota.setCompanyId(systemCompany.getId());
                clusterCompanyQuotaMapper.saveClusterCompanyQuota(clusterCompanyQuota);
            }
        }
    }
    /**
     * 根据id删除公司信息
     * */
    public void deleteSystemCompanById(Long[] ids){
        for(int i=0;i<ids.length;i++){
            systemCompanyMapper.deleteSystemCompanById(ids[i]);
            //同步删除cluster_company_quota信息 add20160804qinfengxia
            clusterCompanyQuotaMapper.deleteClusterCompanyQuotaByCompanyId(ids[i]);
        }
    }

    public List<SystemCompany> queryClusterCompany(){
        return systemCompanyMapper.queryClusterCompany();
    }

}
