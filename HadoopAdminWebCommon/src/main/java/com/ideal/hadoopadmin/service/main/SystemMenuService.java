package com.ideal.hadoopadmin.service.main;

import com.ideal.hadoopadmin.common.util.JsonUtils;
import com.ideal.hadoopadmin.common.util.MenuTree;
import com.ideal.hadoopadmin.entity.main.Menu;
import com.ideal.hadoopadmin.entity.main.User;
import com.ideal.hadoopadmin.mapper.webdb.main.MenuMapper;
import com.ideal.hadoopadmin.mapper.webdb.main.RoleMenuMapper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by CC on 2016/2/16.
 */
@Service
public class SystemMenuService {

    private static Logger logger = LoggerFactory.getLogger(SystemMenuService.class);

    @Resource
    MenuMapper menuMapper;
    //    @Resource
//    RoleMenuMapper roleMenuMapper;
    @Resource
    SystemUserService systemUserService;

    @Resource
    RoleMenuMapper roleMenuMapper;


//    public Menu getMenu(Long id){
//        return menuMapper.getMenu(id);
////    }

    /**
     * 根据当前的登陆用户获取
     *
     * @return
     */
    public List<Menu> getLoginUserMenu(String requestUri) {
        User loginUser = systemUserService.getLoginUser();
        List<Menu> menuList = new ArrayList<Menu>();
        List<Menu> allMenu = null;
        //获取用户角色下的所有能访问的菜单
        List<Long> userRoles = systemUserService.getUserRoles(loginUser.getId());
        if (userRoles == null || userRoles.isEmpty()) {
            allMenu = new ArrayList<Menu>();
        } else {
            allMenu = this.getMenuByRoles(userRoles);
        }
        //组织成一个树的形式
        for (Menu menu : allMenu) {
            menu.setRequestUri(requestUri);
            //对父节点做处理
            if (menu.getIsParent() == 0) {
                menuList.add(processMenu(menu, allMenu));
            }
        }
        return menuList;
    }

    /**
     * 根据用户的角色获取用户能够访问的全部菜单
     *
     * @param roles
     * @return
     */
    public List<Menu> getMenuByRoles(List<Long> roles) {
        //这里获取的可能有重复menu（也可以直接在sql中distinct一下）
//        List<Long> menuIds = roleMenuMapper.getMenuById(roles);
        return menuMapper.getMenuByRoles(roles);
    }


    /**===============================private method===================================**/

    /**
     * 把子菜单放入指定的父菜单
     *
     * @param parentMenu
     * @param allMenu
     * @return
     */
    private Menu processMenu(Menu parentMenu, List<Menu> allMenu) {
        for (Menu curMenu : allMenu) {
            //只对当前父类的子菜单做处理
            if (curMenu.getParentId() == parentMenu.getId()) {
                if (curMenu.getIsParent() == 0) {
                    processMenu(curMenu, allMenu);
                } else {
                    List<Menu> subMenu = parentMenu.getSubMenu();
                    if (subMenu == null) {
                        subMenu = new ArrayList<Menu>();
                        parentMenu.setSubMenu(subMenu);
                    }
                    subMenu.add(curMenu);
                }
            }
        }
        return parentMenu;
    }

    /*
    * 查询全部菜单
    * **/
    public String quereyMenuList() {
        JsonUtils json = new JsonUtils(); //转换为json格式字符串
        List<Menu> menuList = menuMapper.quereyMenuList();
        List<MenuTree> menuTreeList = new ArrayList<MenuTree>();
        for (int i = 0; i < menuList.size(); i++) {
            MenuTree menuTree =new MenuTree();
            menuTree.setId(menuList.get(i).getId());
            String name = menuList.get(i).getTitle();
            if (menuList.get(i).getParentId() != 0) {
                name  +=  "_" + menuList.get(i).getSort();
                if(menuList.get(i).getIsHidden() == 1){
                    name += "(隐藏)";
                }
                menuTree.setName(name);
            } else {
                menuTree.setName(name);
            }

            if(menuList.get(i).getType()==2){
                menuTree.setIcon("../../static/images/button.png");
            }
            menuTree.setpId(menuList.get(i).getParentId());
            menuTreeList.add(menuTree);
        }
        String menuTree = json.toJson(menuTreeList);
        return menuTree;
    }

    /**
     *
     * 查看全部
     * */
    public List<Menu> queryMenuAllList(){
        return menuMapper.quereyMenuList();
    }

    /**
     * 根据id查询
     */
    public Menu getMenu(Long id) {
        return menuMapper.getMenu(id);
    }

    /**
     * 查询一级菜单
     */
    public List<Menu> getMenuByParentId() {
        return menuMapper.getMenuByParentId();
    }

    /**
     * 查询二级菜单
     */
    public List<Menu> getMenuBySecond() {
        return menuMapper.getMenuBySecond();
    }

    /**
     * 添加菜单
     */
    public void saveMenu(Menu menu) {
        menuMapper.saveMenu(menu);
    }

    /**
     * 修改菜单
     */
    public void updateMenu(Menu menu) {
        menuMapper.updateMenu(menu);
    }

    /**
     * 删除菜单
     */
    public String deleteMenu(Long id) {
        List<Menu> menuList = menuMapper.queryMenuByParentId(id);
        if (menuList.size() > 0) {
            return "0";
        } else {
            menuMapper.deleteMenu(id);
            roleMenuMapper.deleteRoleMenu(id);
            return "1";
        }
    }

}
