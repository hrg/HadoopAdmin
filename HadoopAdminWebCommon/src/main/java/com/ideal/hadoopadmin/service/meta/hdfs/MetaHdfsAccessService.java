package com.ideal.hadoopadmin.service.meta.hdfs;

import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsAccess;
import com.ideal.hadoopadmin.mapper.webdb.meta.MetaHdfsAccessMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by fwj on 16-2-26.
 * hdfs访问service
 */
@Service
public class MetaHdfsAccessService {
    @Resource
    private MetaHdfsAccessMapper metaHdfsAccessMapper;

    public  void saveMetaHdfsAccess(MetaHdfsAccess metaHdfsAccess){
        metaHdfsAccessMapper.saveMetaHdfsAccess(metaHdfsAccess);
    }

    public  void deleteMetaHdfsAccess(Long clusterUserId,Long hdfsInfoId ){
        metaHdfsAccessMapper.deleteByParam(clusterUserId,hdfsInfoId);
    }

}
