package com.ideal.hadoopadmin.service.main;

import com.ideal.hadoopadmin.common.util.JsonUtils;
import com.ideal.hadoopadmin.common.util.MenuTree;
import com.ideal.hadoopadmin.entity.main.Menu;
import com.ideal.hadoopadmin.entity.main.RoleMenu;
import com.ideal.hadoopadmin.entity.main.User;
import com.ideal.hadoopadmin.mapper.webdb.main.MenuMapper;
import com.ideal.hadoopadmin.mapper.webdb.main.RoleMenuMapper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by CC on 2016/2/16.
 */
@Service
public class RoleMenuService {

    private static Logger logger = LoggerFactory.getLogger(RoleMenuService.class);

    @Resource
    RoleMenuMapper roleMenuMapper;
    /***
     * 根据角色id，菜单id查询
     * */
    public RoleMenu queryRoleMenuByRoleIdAndMenuId(@Param("roleId") Long roleId, @Param("menuId") Long menuId){
        return roleMenuMapper.queryRoleMenuByRoleIdAndMenuId(roleId,menuId);
    }
    /**
     * 根据角色id查询
     * */
    public List<RoleMenu> queryRoleMenuByRoleId(Long roleId){
        return roleMenuMapper.queryRoleMenuByRoleId(roleId);
    }

    public void deleteRoleMenuByRoleIdAndMeuId(List<RoleMenu> roleMenus){
        for(int i=0;i<roleMenus.size();i++){
            roleMenuMapper.deleteRoleMenuByRoleIdAndMeuId(roleMenus.get(i).getRoleId(),roleMenus.get(i).getMenuId());
        }
    }

    public void saveRoleMenu(RoleMenu roleMenu){
        roleMenuMapper.saveRoleMenu(roleMenu);
    }
}
