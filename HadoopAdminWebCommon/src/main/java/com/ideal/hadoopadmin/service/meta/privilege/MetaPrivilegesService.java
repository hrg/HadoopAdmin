package com.ideal.hadoopadmin.service.meta.privilege;

import com.ideal.hadoopadmin.entity.meta.privilege.MetaPrivileges;
import com.ideal.hadoopadmin.mapper.webdb.meta.MetaPrivilegesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by qinfengxia on 2016/8/4.
 */
@Service
@Transactional
public class MetaPrivilegesService {
    private static Logger logger = LoggerFactory.getLogger(MetaPrivilegesService.class);
    @Resource
    private MetaPrivilegesMapper metaPrivilegesMapper;

    /**
     * 查询所有的权限类型
     * @return
     */
    public List<MetaPrivileges> queryMetaPrivileges(){
        return metaPrivilegesMapper.queryMetaPrivileges();
    }

    /**
     * 查询权限对应的具体权限
     * @param accessId
     * @return
     */
    public List<MetaPrivileges> findByAccessId(Long accessId) {
        return metaPrivilegesMapper.findByAccessId(accessId);
    }

    /**
     * 增加权限
     * @param metaPrivileges
     */
    public void addMetaPrivileges (MetaPrivileges metaPrivileges) {
       metaPrivilegesMapper.saveMetaAccessType(metaPrivileges);
    }

    /**
     * 删除权限
     * @param accessId
     */
    public void deleteMetaPrivileges (Long accessId) {
        metaPrivilegesMapper.deleteByAccessId(accessId);
    }
    /**
     * 删除权限
     */
    public void deleteById (Long id) {
        metaPrivilegesMapper.deleteById(id);
    }
    /**
     * 查询权限对应的具体权限
     * @param accessId
     * @return
     */
    public List<Long> findIdByAccessId(Long accessId) {
        return metaPrivilegesMapper.findIdByAccessId(accessId);
    }

    /**
     * 查询权限对应的具体权限
     * @param id
     * @return
     */
    public MetaPrivileges findIdById(Long id) {
        return metaPrivilegesMapper.findIdById(id);
    }
}
