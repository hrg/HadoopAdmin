package com.ideal.hadoopadmin.service.cluster;

import com.ideal.hadoopadmin.entity.cluster.MachineType;
import com.ideal.hadoopadmin.mapper.webdb.cluster.MachineTypeMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by 袁颖 on 2016/2/22.
 */
@Service
public class MachineTypeService {
    @Resource
    private MachineTypeMapper machineTypeMapper;
    public List<MachineType> findAllMachineType(){
        return machineTypeMapper.findAllMachineType();
    }
}
