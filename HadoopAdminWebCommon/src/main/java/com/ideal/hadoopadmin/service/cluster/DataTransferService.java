package com.ideal.hadoopadmin.service.cluster;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.entity.cluster.ClusterType;
import com.ideal.hadoopadmin.entity.cluster.DataTransfer;
import com.ideal.hadoopadmin.mapper.webdb.cluster.DataTransferMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by 袁颖 on 2016/4/19.
 */
@Service
public class DataTransferService {
    @Resource
    DataTransferMapper dataTransferMapper;

    /**
     * 查询所有的数据,并分页
     */
    public PageInfo searchPage(int currentPage,int defaultSize,String defaultOrder){
        PageHelper.startPage(currentPage,defaultSize);
        List<DataTransfer> dataTransferList = dataTransferMapper.findAll();
        PageInfo page = new PageInfo(dataTransferList);
        return page;
    }
    //保存集群转移的相关信息
    public void transfer(DataTransfer dataTransfer){
        dataTransferMapper.save(dataTransfer);
    }
    public void openStatusById(Long id){
        dataTransferMapper.updateStatus(id, DataTransfer.ON_STATUS);
    }
    public void stopStatusById(Long id){
        dataTransferMapper.updateStatus(id,DataTransfer.UN_STATUS);
    }
    public void deleteById(Long id){
        dataTransferMapper.delete(id);
    }
}
