package com.ideal.hadoopadmin.service.system;

import com.ideal.hadoopadmin.entity.system.WebLog;
import com.ideal.hadoopadmin.entity.system.WebOperations;
import com.ideal.hadoopadmin.mapper.webdb.system.WebLogMapper;
import com.ideal.hadoopadmin.mapper.webdb.system.WebOperationsMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by fwj on 2016/3/15.
 * 系统日志参数
 */
@Service
public class WebOperationsService {
    @Resource
    private WebOperationsMapper webOperationsMapper;

    /**
     * 保存系统日志参数
     */
    public Long saveWebOperations(WebOperations webOperations) {
        return webOperationsMapper.saveWebOperations(webOperations);
    }

    /**
     * 根据路径查询
     */
    public WebOperations queryWebOperationsByOperationName(String url) {
        return webOperationsMapper.queryWebOperationsByOperationName(url);
    }
}
