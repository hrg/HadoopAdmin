package com.ideal.hadoopadmin.service.report;

import com.ideal.hadoopadmin.entity.report.AuditLog;
import com.ideal.hadoopadmin.entity.report.AuditLogCustom;
import com.ideal.hadoopadmin.mapper.webdb.report.AuditLogMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by 袁颖 on 2016/5/3.
 */
@Service
public class AuditLogService {
    @Resource
    AuditLogMapper auditLogMapper;
    public List<AuditLogCustom> findAccessPattern(String time){
        return auditLogMapper.findAccessPattern(time);
    }
}
