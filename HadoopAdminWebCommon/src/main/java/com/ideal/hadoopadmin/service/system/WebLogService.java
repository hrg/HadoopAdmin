package com.ideal.hadoopadmin.service.system;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.common.framework.orm.SearchFilter;
import com.ideal.hadoopadmin.entity.system.Role;
import com.ideal.hadoopadmin.entity.system.UserRole;
import com.ideal.hadoopadmin.entity.system.WebLog;
import com.ideal.hadoopadmin.mapper.webdb.system.UserRoleMapper;
import com.ideal.hadoopadmin.mapper.webdb.system.WebLogMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by fwj on 2016/3/15.
 * 系统日志
 */
@Service
public class WebLogService {
    @Resource
    private WebLogMapper webLogMapper;
    /**
     * 保存系统日志
     * */
    public void saveWebLog(WebLog webLog){
        webLogMapper.saveWebLog(webLog);
    }

    public PageInfo queryWebLogList(Map<String, Object> searchParams, HttpServletRequest request) {
        int currentPage = Integer.parseInt(request.getParameter("page") == null ? "1" : request.getParameter("page"));
        //过滤器
        String where = SearchFilter.parseToString(searchParams);
        //使用分页
        PageHelper.startPage(currentPage, 10);
        //加入组织好的where 条件
        PageHelper.setAppendWhere(where);
        List<WebLog> webLogList = webLogMapper.queryWebLog();
        PageInfo pageInfo = new PageInfo(webLogList);
        return pageInfo;
    }
}
