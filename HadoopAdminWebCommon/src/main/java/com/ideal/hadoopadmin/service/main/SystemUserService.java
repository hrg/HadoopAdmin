package com.ideal.hadoopadmin.service.main;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.common.framework.orm.SearchFilter;
import com.ideal.hadoopadmin.entity.main.User;
import com.ideal.hadoopadmin.entity.system.Role;
import com.ideal.hadoopadmin.entity.system.UserRole;
import com.ideal.hadoopadmin.mapper.webdb.main.UserMapper;
import com.ideal.hadoopadmin.mapper.webdb.system.UserRoleMapper;
import com.ideal.hadoopadmin.security.ShiroDbRealm;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by CC on 2016/2/18.
 */
@Service
public class SystemUserService {

    @Resource
    UserMapper userMapper;
    @Resource
    UserRoleMapper userRoleMapper;

    /**
     * 获取登陆用户,这个实在登陆以后获取用户
     *
     * @return
     */
    public User getLoginUser() {
        ShiroDbRealm.ShiroUser user = (ShiroDbRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal();
        return userMapper.getUserById(user.getId());
    }

    /**
     * 根据用户名 获取用户信息
     *
     * @param userName
     * @return
     */
    public User getUserByUserName(String userName) {
        return userMapper.getUserByUserName(userName);
    }

    /**
     * 认证用户，用户登陆时认证用户
     *
     * @param userName
     * @param passWord
     * @return
     */
    public User authenticateLoginUser(String userName, String passWord) throws Exception {
        return userMapper.getUserByNameAndPassWord(userName, passWord);
    }


    /**
     * 根据用户的id查询出用户的的所有角色
     *
     * @param id
     * @return
     */
    public List<Long> getUserRoles(Long id) {
        return userMapper.getUserRoles(id);
    }

    /**
     * 查询全部的用户
     */
    public List<User> queryAllUser() {
        return userMapper.queryAllUser();
    }

    /**
     * 分页查询
     */

    public PageInfo queryUserList(Map<String, Object> searchParams, HttpServletRequest request) {
        int currentPage = Integer.parseInt(request.getParameter("page") == null ? "1" : request.getParameter("page"));
        //过滤器
        String where = SearchFilter.parseToString(searchParams);
        //使用分页
        PageHelper.startPage(currentPage, 10);
        //加入组织好的where 条件
        PageHelper.setAppendWhere(where);
        List<User> userList = userMapper.queryAllUser();

        PageInfo pageInfo = new PageInfo(userList);
        return pageInfo;
    }


    public User getUserById(Long id) {
        return userMapper.getUserById(id);
    }

    /**
     * 保存系统用户
     */
    public void saveUsers(User user) {
        userMapper.saveUsers(user);
    }

    ;

    /**
     * 批量删除用户
     */
    public void deleteUserById(Long[] ids) {
        for (int i = 0; i < ids.length; i++) {
            userMapper.deleteUserByIds(ids[i]);
            userRoleMapper.deleteUserRoleByUserIds(ids[i]);
        }

    }

    /**
     * 修改系统用户
     * *
     */
    public void updateUsers(User user) {
        userMapper.updateUsers(user);
    }
    /**
     * 修改密码
     * */
    public void updateUserPassword(User user){
        userMapper.updateUserPassword(user);
    }

}
