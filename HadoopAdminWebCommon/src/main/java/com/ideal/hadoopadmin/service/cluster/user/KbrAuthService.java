package com.ideal.hadoopadmin.service.cluster.user;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ideal.hadoopadmin.common.entity.UserApproveForm;
import com.ideal.hadoopadmin.common.framework.orm.SearchFilter;
import com.ideal.hadoopadmin.entity.cluster.user.KbrAuth;
import com.ideal.hadoopadmin.mapper.webdb.cluster.user.KbrAuthMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by fwj on 2016/3/4.
 * 用户认证信息报表
 */
@Service
public class KbrAuthService {
    @Resource
    KbrAuthMapper kbrAuthMapper;

    public PageInfo queryKbrAuthByPage(Map<String, Object> searchParams, HttpServletRequest request) {
        int currentPage = Integer.parseInt(request.getParameter("page") == null ? "1" : request.getParameter("page"));
        //过滤器
        String where = SearchFilter.parseToString(searchParams);
        //使用分页
        PageHelper.startPage(currentPage, 10);
        //加入组织好的where 条件
        PageHelper.setAppendWhere(where);
        List<KbrAuth> kbrAuthList = kbrAuthMapper.queryUserApproveForm();

        PageInfo pageInfo = new PageInfo(kbrAuthList);
        return pageInfo;
    }

    public List<UserApproveForm> queryKbrAuthByDown(String userName){
        List<KbrAuth> kbrAuthList = kbrAuthMapper.queryUserApproveFormByDown(userName);
        List<UserApproveForm> userApproveFormList = new ArrayList<UserApproveForm>();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
        for (int i = 0; i < kbrAuthList.size(); i++) {
            UserApproveForm userApproveForm = new UserApproveForm();
            try {
                Date ct = new Date(kbrAuthList.get(i).getCreateTime());
                String createTime = sdf.format(ct);
                userApproveForm.setCreateTime(createTime);
                Date et = new Date(kbrAuthList.get(i).getEndTime());
                String endTime = sdf.format(et);
                userApproveForm.setEndTime(endTime);
                userApproveForm.setHpid(kbrAuthList.get(i).getUserId().getId().toString());
                userApproveForm.setName(kbrAuthList.get(i).getUserId().getUserName());
                Date st = new Date(kbrAuthList.get(i).getCreateTime());
                String startTime = sdf.format(st);
                userApproveForm.setStartTime(startTime);
                userApproveForm.setPrinc(kbrAuthList.get(i).getPrincipal());
                userApproveForm.setPath(kbrAuthList.get(i).getTicketPath());
                userApproveForm.setStatus(kbrAuthList.get(i).getStatus());
                userApproveForm.setIp(kbrAuthList.get(i).getMachineId().getMachineIp());
                if (kbrAuthList.get(i).getStatus() == 1) {
                    userApproveForm.setRed(true);
                } else if (kbrAuthList.get(i).getEndTime() != null) {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    Date end = new Date(kbrAuthList.get(i).getEndTime());
                    if (end.before(new Date())) {
                        userApproveForm.setRed(true);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            userApproveFormList.add(userApproveForm);
        }
        return userApproveFormList;
    }
}
