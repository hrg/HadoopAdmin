package com.ideal.hadoopadmin.security;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


/**
 * Created by CC on 2016/2/16.
 * 专门针对webservice调用的过滤器
 */
public class WebServiceFilter extends AuthenticatingFilter {

    public static final String DEFAULT_USERNAME_PARAM = "username";
    public static final String DEFAULT_PASSWORD_PARAM = "password";


    @Override
    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) throws Exception {
        String username = getUsername(request);
        String password = getPassword(request);
        return createToken(username, password, request, response);
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        return executeLogin(request,response);
    }

    protected String getUsername(ServletRequest request) {
        return WebUtils.getCleanParam(request, DEFAULT_USERNAME_PARAM);
    }

    protected String getPassword(ServletRequest request) {
        return WebUtils.getCleanParam(request, DEFAULT_PASSWORD_PARAM);
    }
}
