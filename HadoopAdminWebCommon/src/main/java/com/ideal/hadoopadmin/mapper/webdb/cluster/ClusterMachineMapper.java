package com.ideal.hadoopadmin.mapper.webdb.cluster;

import com.ideal.hadoopadmin.entity.cluster.ClusterMachine;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * Created by 袁颖 on 2016/2/19.
 */
public interface ClusterMachineMapper {
    //通过不同的条件进行查询
    List<ClusterMachine> findAllClusterMachine();
    //查找机器类型为某一个类型的机器
    List<ClusterMachine>findByMachineTypeId(@Param("machineTypeId")Long machineTypeId);
    //查找某个集群所有机器ip
    @Select("select distinct machineIp from cluster_machine")
    List<String>findAllIp();
    //查找某个集群所有机器登录用户
    @Select("select distinct loginUserName from cluster_machine")
    List<String>findAllUser();
    //通过id查找某一条machine数据
    ClusterMachine findById(Long id);
    //更新机器信息
    @Update("update cluster_machine set machineIp=#{machineIp},loginUserName=#{loginUserName},loginPassWord=#{loginPassWord}," +
            "machineTypeId=#{machineType.machineTypeId},clusterTypeId=#{clusterType.clusterTypeId},isAddUser=#{isAddUser},status=#{status},note=#{note}" +
            "where id=#{id}")
    void updateClusterMachine(ClusterMachine clusterMachine);
    //插入新的机器信息
    @Insert("insert into cluster_machine (machineIp,loginUserName,loginPassWord,machineTypeId,clusterTypeId,isAddUser,status,note)" +
            "values (#{machineIp},#{loginUserName},#{loginPassWord},#{machineType.machineTypeId},#{clusterType.clusterTypeId},#{isAddUser},#{status},#{note})")
    void saveClusterMachine(ClusterMachine clusterMachine);
    void saveOrUpdate(ClusterMachine clusterMachine);
    //删除集群机器
    @Delete("delete from cluster_machine where id=#{id}")
    void deleteClusterMahine(Long id);

    //通过用户登录名，机器ip，机器类型，集群类型来判断是否存在相同的机器
    ClusterMachine findByUserIpType(ClusterMachine clusterMachine);

    @Select("select id from cluster_machine where machineIp=#{machineIp} and machineTypeId=#{machineTypeId}")
    Long findMachineIdByIpAndType(@Param("machineIp")String machineIp,@Param("machineTypeId")Long machineTypeId);

    //查找机器类型为client的机器
    List<Map<String,Object>> findMachine(Map<String,Object> map);

    //查找机器对应的client配置
    List<Map<String,Object>> findMachineAndClientquota(Map<String,Object> map);

}
