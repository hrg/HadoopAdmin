package com.ideal.hadoopadmin.mapper.webdb.system.company;

import com.ideal.hadoopadmin.entity.cluster.ClusterMachine;
import com.ideal.hadoopadmin.entity.system.company.SystemCompany;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by fwj on 2016/2/22.
 * 公司管理
 */
public interface SystemCompanyMapper {

    //查询ClusterCompany
    @Select("select * from system_company")
    public List<SystemCompany> queryClusterCompany();

    /**
     * 根据公司id查询
     * */
    @Select("select * from system_company where id=#{id}")
    SystemCompany findSystemCompanyById(Long id);

   /* //添加公司信息
    @Insert("insert into system_company (companyName,note,contactName,contactMobile, maxUserNum, maxResource, maxCpu, hdfsFileCount, hdfsSpace) values(#{companyName},#{note},#{contactName},#{contactMobile},#{maxUserNum},#{maxResource},#{maxCpu},#{hdfsFileCount},#{hdfsSpace})")
    public void saveSystemCompany(SystemCompany systemCompany);
    //更新公司信息
    @Update("update system_company set companyName= #{companyName}, note=#{note},contactName=#{contactName},contactMobile=#{contactMobile},maxUserNum=#{maxUserNum},maxResource=#{maxResource},maxCpu=#{maxCpu},hdfsFileCount=#{hdfsFileCount},hdfsSpace=#{hdfsSpace} where id=#{id}")
    public void updateSystemCompanyById(SystemCompany systemCompany);*/

    //添加公司信息
    @Insert("insert into system_company (companyName,note,contactName,contactMobile) values(#{companyName},#{note},#{contactName},#{contactMobile})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    public void saveSystemCompany(SystemCompany systemCompany);
    //更新公司信息
    @Update("update system_company set companyName= #{companyName}, note=#{note},contactName=#{contactName},contactMobile=#{contactMobile} where id=#{id}")
    public void updateSystemCompanyById(SystemCompany systemCompany);

    //删除集群机器
    @Update("update system_company set delTag=1 where id=#{id} ")
    public void deleteSystemCompanById(Long id);
}
