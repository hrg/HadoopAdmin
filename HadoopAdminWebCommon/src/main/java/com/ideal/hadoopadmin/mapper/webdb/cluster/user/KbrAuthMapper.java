package com.ideal.hadoopadmin.mapper.webdb.cluster.user;


import com.ideal.hadoopadmin.entity.cluster.user.KbrAuth;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by fwj on 2016/3/4
 * 用户认证列表
 */
public interface KbrAuthMapper {

    public List<KbrAuth> queryUserApproveForm();

    public List<KbrAuth> queryUserApproveFormByDown(@Param("cuUserName") String cuUserName);
    @Delete("delete from cluster_user_kbrauth where userId = #{userId}")
    void deleteByUserId(Long userId);
}
