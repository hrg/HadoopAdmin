package com.ideal.hadoopadmin.mapper.webdb.system;

import com.ideal.hadoopadmin.entity.system.Role;
import com.ideal.hadoopadmin.entity.system.WebOperations;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * Created by fwj on 2016/3/15.
 * 系统日志参数管理
 */
public interface WebOperationsMapper {

    public Long saveWebOperations(WebOperations webOperations);

    /**
     *  根据路径查询
     * */
    @Select("select * from system_web_operations where url=#{url}")
    public WebOperations queryWebOperationsByOperationName(String url);
}
