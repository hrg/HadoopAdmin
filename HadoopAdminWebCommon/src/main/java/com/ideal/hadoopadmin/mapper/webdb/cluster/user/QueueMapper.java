package com.ideal.hadoopadmin.mapper.webdb.cluster.user;

import com.ideal.hadoopadmin.entity.cluster.user.Queue;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * Created by 袁颖 on 2016/2/29.
 */
public interface QueueMapper {
    //通过id进行查找
    @Select("select * from cluster_user_queue where id=#{id}")
    public Queue findById(Long id);
    //通过userId查找
    @Select("select * from cluster_user_queue where userId=#{userId}")
    public List<Queue> findByUseId(Long userId);
    @Insert("insert into cluster_user_queue(userId,queueName,minResource,maxResource,maxApp,weight,acl,minCpu,maxCpu,modifyTime) " +
            "values(#{userId},#{queueName},#{minResource},#{maxResource},#{maxApp},#{weight},#{acl},#{minCpu},#{maxCpu},#{modifyTime})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    public void save(Queue queue);
    @Update("update cluster_user_queue set queueName=#{queueName},minResource=#{minResource},maxResource=#{maxResource},maxApp=#{maxApp}," +
            "weight=#{weight},acl=#{acl},minCpu=#{minCpu},maxCpu=#{maxCpu},modifyTime=#{modifyTime} " +
            "where id=#{id}")
    public void update(Queue queue);
    @Delete("delete from cluster_user_queue where id=#{id}")
    public void deleteById(Long id);
    //通过用户id和队列名查找
    @Select("select * from cluster_user_queue where queueName=#{queueName} and userId=#{userId} and id=#{id}")
    public Queue findByNameAddUser(@Param("queueName")String queueName,@Param("userId")Long userId,@Param("id")Long id);
    //通过用户id删除
    @Delete("delete from cluster_user_queue where userId=#{userId}")
    public void deleteByUserId(Long userId);

    public List<Queue> queryQueueList();

    //获取所有的队列名
    @Select("select f.queueName from cluster_user_queue f")
    public List<String> findAllQueue();


    //根据用户名获取队列名
    @Select("select f.queueName from cluster_user_queue f where f.userId = #{clusterUser}")
    public List<String> findQueueByUsername(Long clusterId);

    //根据条件查询队列信息
    public List<Queue> queryQueueBySearchParam(@Param("cuId")Long cuId,@Param("queue")String queue);
    //通过id删除队列
    @Delete("delete from cluster_user_queue where id=#{id}")
    public void delById(Long id);
}
