package com.ideal.hadoopadmin.mapper.webdb.main;

import com.ideal.hadoopadmin.entity.main.Menu;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by CC on 2016/2/17.
 * <p/>
 * 这里的mapper必须是接口（在runtime才使用）
 * 这里展示了两种sql映射方式
 * 第一种是注解
 * 第二种是方法名映射
 * 这里说一个基本原则 注释能够完成xml可以完成的所有功能 要考虑的问题是代码美观
 * 建议复杂的写到 xml 中  简单的好排版的直接用注 释
 */
public interface MenuMapper {


    @Select("select * from system_menu where id=#{id}")
    Menu getMenu(@Param("id") Long id);

    @Select("select * from system_menu where parentId=0")
    List<Menu> getMenuByParentId();

    @Select("select * from system_menu where parentId<>0 and type=1")
    List<Menu> getMenuBySecond();

    /*
   * 查询全部菜单
   * **/
    @Select("select * from system_menu")
    List<Menu> quereyMenuList();

//    @Insert("insert into system_menu(***)"+
//             "values (****)")
//    @Options(useGeneratedKeys=true, keyProperty="id")
//    void insertOneMenu(Menu menu);

    //建议先通过system_role_menu表的roleId查找对应的menuId，再通过system_menu表的menuId查询对应的menu

    public List<Menu> getMenuByRoles(List<Long> roles);

    /**
     * 添加菜单
     */
    @Insert("insert into system_menu (title,url,cssClass,parentId,code,sort,createTime,isParent,type,isHidden) values (#{title},#{url},#{cssClass},#{parentId},#{code},#{sort},#{createTime},#{isParent},#{type},#{isHidden})")
    public void saveMenu(Menu menu);

    @Update("update system_menu set title=#{title},url=#{url},cssClass=#{cssClass},parentId=#{parentId},code=#{code},sort=#{sort},createTime=#{createTime},isParent=#{isParent},type=#{type},isHidden=#{isHidden} where id=#{id}")
    public void updateMenu(Menu menu);

    @Delete("delete from system_menu where id =#{id}")
    public void deleteMenu(Long id);

    @Select("select * from system_menu where parentId =#{id} ")
    public List<Menu> queryMenuByParentId(Long id);


}
