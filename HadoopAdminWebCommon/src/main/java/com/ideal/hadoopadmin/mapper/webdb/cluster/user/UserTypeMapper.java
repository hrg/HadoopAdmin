package com.ideal.hadoopadmin.mapper.webdb.cluster.user;

import com.ideal.hadoopadmin.entity.cluster.user.UserType;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by 袁颖 on 2016/2/29.
 */
public interface UserTypeMapper {
    @Select("select * from cluster_user_type")
    List<UserType> findUserType();
}
