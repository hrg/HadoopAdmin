package com.ideal.hadoopadmin.mapper.webdb.meta;

import com.ideal.hadoopadmin.entity.meta.hive.MetaHiveAccess;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * Created by fwj on 16-3-2.
 *   hive访问mapper
 */
public interface MetaHiveAccessMapper {
    @Insert("insert into meta_hive_access (clusterUserId,hiveInfoId,createTime) values (#{clusterUserId},#{hiveInfoId},#{createTime})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    public void saveMetaHiveAccess(MetaHiveAccess metaHiveAccess);
    //根据用户id进行删除
    @Delete("delete from meta_hive_access where clusterUserId=#{clusterUserId}")
    public void deleteByUserId(Long clusterUserId);
    //根据用户ids进行批量删除
    void deleteByUserIds(Long[] clusterUserId);
    @Delete("delete from meta_hive_access \n" +
            "where clusterUserId=#{clusterUserId} \n" +
            "or hiveInfoId in (select i.id from meta_hive_info i where i.clusterUserId = #{clusterUserId})")
    void deleteByUserIdOrHiveInfoId(Long clusterUserId);

    public List<Map<String,Object>> findPrivilege(Map<String,Object> map);
    //根据accessId进行删除
    @Delete("delete from meta_hive_access where id=#{id}")
    public void deleteById(Long id);
    @Select("select * from meta_hive_access where id=#{id}")
    MetaHiveAccess findById(Long id);
}
