package com.ideal.hadoopadmin.mapper.webdb.meta;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

/**
 * Created by 袁颖 on 2016/3/11.
 */
public interface MetaHiveSqlMapper {
    @Select("select hiveSql from meta_hive_sql where hiveInfoId=#{hiveInfoId}")
    String findSqlById(Long hiveInfoId);
    @Delete("delete from meta_hive_sql where hiveInfoId in(select id from meta_hive_info where clusterUserId= #{userId})")
    void deleteByUserId(Long userId);
}
