package com.ideal.hadoopadmin.mapper.db;

/**
 * Created by CC on 2016/2/17.
 * 这个没有别的作用只是为了在mybatis自动注入时，出现多个数据源
 * 只是一个区分作用 用接口可能不太帅 。有时间改成注解
 * 也可以不用这么复杂，直接使用不同的package来定义不同的数据源
 */
public interface WebDB {

}
