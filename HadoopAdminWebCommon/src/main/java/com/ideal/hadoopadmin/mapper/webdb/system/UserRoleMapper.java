package com.ideal.hadoopadmin.mapper.webdb.system;

import com.ideal.hadoopadmin.entity.main.RoleMenu;
import com.ideal.hadoopadmin.entity.system.Role;
import com.ideal.hadoopadmin.entity.system.UserRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * Created by fwj on 2016/3/8.
 * 角色管理
 */
public interface UserRoleMapper {


    @Select("select * from system_user_role where roleId=#{roleId}")
    public List<UserRole> queryUserRoleByRoleId(Long roleId);

    @Select("select  sr.name from system_user_role sur left join system_role sr on sur.roleId=sr.id where userId=#{userId}")
    public List<String> queryUserRoleByUserId(Long userId);

    /**
     * 新增
     * */
     @Insert("insert into system_user_role (userId,roleId) values (#{userId},#{roleId})")
     public void saveUserRole(UserRole userRole);

     @Delete("delete from system_user_role where userId = #{id}")
     public void deleteUserRoleByUserIds(Long id);

    @Select("select roleId from system_user_role where userId=#{userId}")
    public List<Long> queryRoleIdByUserId(Long userId);
    //根据用户id删除
    @Delete("delete from system_user_role where userId=#{userId}")
    public void deleteUserRoleByUserId(Long userId);

}
