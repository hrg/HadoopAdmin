package com.ideal.hadoopadmin.mapper.webdb.cluster.user;

import com.ideal.hadoopadmin.entity.cluster.user.Wizard;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by 袁颖 on 2016/3/17.
 */
public interface WizardMapper {
    @Select("select * from cluster_user_wizard order by sort")
    List<Wizard> findAllWizard();
}
