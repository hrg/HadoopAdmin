package com.ideal.hadoopadmin.mapper.webdb.cluster;

import com.ideal.hadoopadmin.entity.cluster.ClusterType;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;


import java.util.List;

/**
 * Created by 袁颖 on 2016/2/22.
 */
public interface ClusterTypeMapper {
    @Select("select * from cluster_cluster_type")
    List<ClusterType> findAllClusterType();
    //根据id删除
    @Delete("delete from cluster_cluster_type where id=#{id}")
    void deleteById(Long id);
    //根据id进行更新
    @Update("UPDATE cluster_cluster_type SET clusterTypeName = #{clusterTypeName}, note = #{note} WHERE  id=#{id}; ")
    void updateById(ClusterType clusterType);
    //保存
    @Insert("insert into cluster_cluster_type (clusterTypeId, clusterTypeName, note) VALUES (#{clusterTypeId},#{clusterTypeName},#{note})")
    void insert(ClusterType clusterType);

    //通过id查找
    @Select("select * from cluster_cluster_type where id=#{id}")
    ClusterType findById(Long id);

    //查找出最大的clusterTypeId
    @Select("select max(clusterTypeId) from cluster_cluster_type")
    Long findMaxClusterTypeId();

    //查出另外的集群
    @Select("select * from cluster_cluster_type where id!=#{id}")
    List<ClusterType> findOtherClusterType(Long id);
}
