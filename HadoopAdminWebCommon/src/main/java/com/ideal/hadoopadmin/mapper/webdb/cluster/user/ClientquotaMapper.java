package com.ideal.hadoopadmin.mapper.webdb.cluster.user;

import com.ideal.hadoopadmin.entity.cluster.user.Clientquota;
import com.ideal.hadoopadmin.entity.cluster.user.Hdfsquota;
import org.apache.ibatis.annotations.*;

import java.util.Map;

/**
 * Created by qinfengxia on 2016/7/14.
 */
public interface ClientquotaMapper {
    @Select("select * from cluster_user_clientquota where userId=#{userId}")
    public Clientquota findByUserId(Long userId);
    @Insert("insert into cluster_user_clientquota(userId,clientCatalog,clientCatalogSize,clientCatalogUnit,machineId) " +
            "values(#{userId},#{clientCatalog},#{clientCatalogSize},#{clientCatalogUnit},#{machineId})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    public void save(Clientquota clientquota);
    @Update("update cluster_user_clientquota set clientCatalog=#{clientCatalog},clientCatalogSize=#{clientCatalogSize},clientCatalogUnit=#{clientCatalogUnit},machineId=#{machineId}" +
            "where id=#{id}")
    public void update(Clientquota clientquota);
    @Delete("delete from cluster_user_clientquota where userId=#{userId}")
    public void deleteByUserId(Long userId);
}
