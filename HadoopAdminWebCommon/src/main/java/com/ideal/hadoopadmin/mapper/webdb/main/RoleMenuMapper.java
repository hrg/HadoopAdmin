package com.ideal.hadoopadmin.mapper.webdb.main;

import com.ideal.hadoopadmin.entity.main.Menu;
import com.ideal.hadoopadmin.entity.main.RoleMenu;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by 袁颖 on 2016/2/18.
 */
public interface RoleMenuMapper {
    //通过角色查找对应的menuId
    List<Long> getMenuById(List<Long> ids);

    @Delete("delete from system_role_menu where menuId=#{menuId}")
    public void deleteRoleMenu(Long menuId);

    @Delete("delete from system_role_menu where roleId=#{roleId} and  menuId=#{menuId}")
    public void deleteRoleMenuByRoleIdAndMeuId(@Param("roleId")Long roleId, @Param("menuId")Long menuId);

    @Select("select * from system_role_menu where roleId=#{roleId} and menuId=#{menuId};")
    public RoleMenu queryRoleMenuByRoleIdAndMenuId(@Param("roleId") Long roleId, @Param("menuId") Long menuId);

    @Select("select * from system_role_menu where roleId=#{roleId}")
    public List<RoleMenu> queryRoleMenuByRoleId(Long roleId);

    @Insert("insert into system_role_menu (roleId,menuId) values (#{roleId},#{menuId})")
    public void saveRoleMenu(RoleMenu roleMenu);

}
