package com.ideal.hadoopadmin.mapper.webdb.system;

import com.ideal.hadoopadmin.entity.report.FairSchedulerAllocation;
import com.ideal.hadoopadmin.entity.system.Role;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by fwj on 2016/3/8.
 * 角色管理
 */
public interface RoleMapper {


    @Select("select * from system_role")
    public List<Role> queryRoleList();

    /**
     * 根据id查询
     */
    @Select("select * from system_role where id =#{id}")
    public Role queryRoleById(Long id);

    @Insert("insert into system_role (name,status,createTime) values (#{name},#{status},#{createTime})")
    public void saveRole(Role role);

    @Update("update system_role set name=#{name},status=#{status},createTime=#{createTime} where id =#{id}")
    public void updateRole(Role role);

    @Delete("delete from  system_role where id =#{id}")
    public void deleteeRoleByID(Long id);
}
