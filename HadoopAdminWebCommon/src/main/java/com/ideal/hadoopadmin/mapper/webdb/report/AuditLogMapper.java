package com.ideal.hadoopadmin.mapper.webdb.report;

import com.ideal.hadoopadmin.entity.report.AuditLog;
import com.ideal.hadoopadmin.entity.report.AuditLogCustom;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by 袁颖 on 2016/5/3.
 */
public interface AuditLogMapper {
    /*根据时间查找用户,目录及对应的次数*/
    @Select("select count(1) num,ugi,src from report_audit_log where time like '%${time}%' group by ugi,src")
    List<AuditLogCustom> findAccessPattern(@Param("time")String time);
}
