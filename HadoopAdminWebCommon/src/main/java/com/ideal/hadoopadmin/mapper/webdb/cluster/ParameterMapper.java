package com.ideal.hadoopadmin.mapper.webdb.cluster;

import com.ideal.hadoopadmin.entity.cluster.Parameter;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * Created by 袁颖 on 2016/2/22.
 */
public interface ParameterMapper {
    //查找所有的参数及每个父参数下对应的所有子参数update20160715qinfengxia
    //@Select("select * from cluster_parameter")
    List<Parameter> findAllParameter();
    //查找所有的父参数
    //@Select("select * from cluster_parameter where parentId=0")
    List<Parameter> findAllParentParameter();
    @Select("select * from cluster_parameter where id=#{id}")
    Parameter findById(Long id);

    //更新数据
    @Update("update cluster_parameter set parameterKey=#{parameterKey},parameterVal=#{parameterVal},parentId=#{parentId},note=#{note},clusterTypeId=#{clusterType.clusterTypeId} where id=#{id}")
    void updateParameter(Parameter parameter);

    //保存数据
    @Insert("insert into cluster_parameter(parameterKey,parameterVal,parentId,note,clusterTypeId) values(#{parameterKey},#{parameterVal},#{parentId},#{note},#{clusterType.clusterTypeId})")
    void saveParameter(Parameter parameter);

    @Delete("delete from cluster_parameter where id=#{id}")
    void deleteById(Long id);

    @Delete("delete from cluster_parameter where parentId=#{parentId}")
    void deleteByParentId(Long parentId);

    @Select("select * from cluster_parameter where parameterKey=#{parameterKey}")
    Parameter findByKey(String parameterKey);

    /**
     * 根据key值自身关联查询
     */
    @Select("select p1.* from cluster_parameter p1 left join  cluster_parameter p2 on p1.parentId=p2.id where p1.parameterKey+'.'+p2.parameterKey=#{parameterKey}")
    Parameter findParameterByKey(String parameterKey);
//    @Select("select * from cluster_parameter where parameterKey = 'HDFS_GROUP_SUFFIX' and parentId= (select id from cluster_parameter where parameterKey = 'HDFS_SUPER_USER')")

    @Select("select * from cluster_parameter where parameterKey like concat('%',#{parameterKey},'%')")
    List<Parameter> findParameterByParam(String parameterKey);
}
