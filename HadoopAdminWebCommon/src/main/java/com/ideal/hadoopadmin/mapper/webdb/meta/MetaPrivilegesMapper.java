package com.ideal.hadoopadmin.mapper.webdb.meta;

import com.ideal.hadoopadmin.entity.meta.privilege.MetaAccessPrivilege;
import com.ideal.hadoopadmin.entity.meta.privilege.MetaPrivileges;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by qinfengxia on 2016/8/4.
 */
public interface MetaPrivilegesMapper {
    /**
     * 查询所有的权限类型
     * @return
     */
    @Select("select * from meta_privileges")
    List<MetaPrivileges> queryMetaPrivileges();

    @Select("select * from meta_privileges where accessId=#{accessId}")
    List<MetaPrivileges> findByAccessId(Long accessId);

    //保存数据
    @Insert("insert into meta_privileges(accessId,privilegeId) values(#{accessId},#{privilegeId})")
    void saveMetaAccessType(MetaPrivileges metaPrivileges);

    @Delete("delete from meta_privileges where accessId=#{accessId}")
    void deleteByAccessId(Long accessId);

    @Delete("delete from meta_privileges where id=#{id}")
    void deleteById(Long id);
    @Select("select id from meta_privileges where accessId=#{accessId}")
    List<Long> findIdByAccessId(Long accessId);

    @Select("select * from meta_privileges where id=#{id}")
    MetaPrivileges findIdById(Long id);
}
