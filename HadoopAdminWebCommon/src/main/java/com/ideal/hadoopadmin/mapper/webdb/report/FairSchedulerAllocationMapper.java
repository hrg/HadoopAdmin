package com.ideal.hadoopadmin.mapper.webdb.report;

import com.ideal.hadoopadmin.entity.report.FairSchedulerAllocation;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by fwj on 2016/3/3.
 * 队列资源访问列表
 */
public interface FairSchedulerAllocationMapper {

    public List<FairSchedulerAllocation> queryFairSchedulerAllocationList();
    public List<FairSchedulerAllocation> queryFairSchedulerAllocationListBySearchParam(@Param("cuId")Long cuId,@Param("queue")String queue);


    //根据用户名获取队列名
    @Select("select f.queue from fair_scheduler_allocation f where f.clusterUser = #{clusterUser}")
    public List<String> findQueueByUsername(Long clusterId);

    //获取所有的队列名
    @Select("select f.queue from fair_scheduler_allocation f")
    public List<String> findAllQueue();
}
