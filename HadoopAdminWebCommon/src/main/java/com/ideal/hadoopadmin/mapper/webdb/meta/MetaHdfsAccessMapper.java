package com.ideal.hadoopadmin.mapper.webdb.meta;

import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsAccess;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by fwj on 16-2-26.
 *   hdfs访问mapper
 */
public interface MetaHdfsAccessMapper {
    /**
     * 根据集群用户id、hdfsId穿
     * */
    @Select("select * from meta_hdfs_access where clusterUserId=#{clusterUserId} and hdfsInfoId=#{hdfsInfoId}")
    public MetaHdfsAccess queryMetaHdfsAccessByClusterUserIdAndHdfsInfoId(MetaHdfsAccess metaHdfsAccess);



    @Insert("insert into meta_hdfs_access (clusterUserId,hdfsInfoId,createTime) values (#{clusterUserId},#{hdfsInfoId},#{createTime})")
    public void saveMetaHdfsAccess(MetaHdfsAccess metaHdfsAccess);

    /**
     *  根据集群用户id
     * */
    @Select("select * from meta_hdfs_access where hdfsInfoId=#{clusterUserId}")
    public List<MetaHdfsAccess> queryMetaHdfsAccessByClusterUserId(Long clusterUserId);
    /**
     *  删除
     * */
    @Delete("delete  from  meta_hdfs_access where clusterUserId=#{clusterUserId} and hdfsInfoId=#{hdfsInfoId} ")
    public void deleteMetaHdfsAccessByClusterUserId(@Param("clusterUserId")Long clusterUserId,@Param("hdfsInfoId")Long hdfsInfoId);
    //删除
    @Delete("delete from meta_hdfs_access where clusterUserId=#{clusterUserId}")
    public void deleteByUserId(Long clusterUserId);
    @Delete("delete from meta_hdfs_access \n" +
            "where clusterUserId=#{clusterUserId} \n" +
            "or hdfsInfoId in (select i.id from meta_hdfs_info i where i.clusterUserId = #{clusterUserId})")
    void deleteByUserIdOrHdfsInfoId(Long clusterUserId);

    //被移除的用户是否还能访问 目录所有者的其他目录
    @Select("select count(*) from meta_hdfs_access mha left join meta_hdfs_info mhi on mhi.id=mha.hdfsInfoId " +
            "where mha.clusterUserId= #{userId} and mhi.clusterUserId= (select clusterUserId from meta_hdfs_info where id=#{hdfsId})")
    public int findHdfsHasOtherAcess(@Param("userId")Long userId,@Param("hdfsId")Long hdfsId);

    //删除
    @Delete("delete from meta_hdfs_access where clusterUserId=#{clusterUserId} and hdfsInfoId=#{hdfsInfoId}")
    public void deleteByParam(@Param("clusterUserId")Long clusterUserId,@Param("hdfsInfoId")Long hdfsInfoId );
}
