package com.ideal.hadoopadmin.mapper.webdb.cluster.user;
import com.ideal.hadoopadmin.entity.cluster.user.Kbrconfig;
import org.apache.ibatis.annotations.*;
import com.ideal.hadoopadmin.common.util.plugins.handler.TimeTypeHandler;

import java.util.List;
import java.util.Map;

/**
 * Created by 袁颖 on 2016/2/26.
 */
public interface KbrconfigMapper {
    @Select("select * from cluster_user_kbrconfig where userId=#{userId}")
    public Kbrconfig findByUserId(Long userId);
    //更新有效时间,机器id,状态及修改时间
    @Update("update cluster_user_kbrconfig set validDay=#{validDay},machineIds=#{machineIds},status=#{status},changeTime=#{changeTime} " +
            "where id=#{id}")
    public void update(Kbrconfig kbrconfig);
    //插入数据
    @Insert("insert into cluster_user_kbrconfig(userId,validDay,machineIds,status,changeTime,createTime,startTime,endTime)" +
            " values(#{userId},#{validDay},#{machineIds},#{status},#{changeTime},#{createTime},#{startTime},#{endTime}) " )
    @Options(useGeneratedKeys = true,keyProperty = "id")
    public void save(Kbrconfig kbrconfig);
    //通过id进行 查找
    public Kbrconfig findById(Long id);
    @Delete("delete from cluster_user_kbrconfig where userId=#{userId}")
    public void deleteByUserId(Long userId);
    //写在xml里面
    void updateByUserIds(Map<String,Object> map);
    @Update("update cluster_user_kbrconfig set validDay=#{validDay},machineIds=#{machineIds},status=#{status},changeTime=#{changeTime},startTime=#{startTime},endTime=#{endTime} " +
            "where userId=#{userId}")
    void updateByUserId(Kbrconfig kbrconfig);

}
