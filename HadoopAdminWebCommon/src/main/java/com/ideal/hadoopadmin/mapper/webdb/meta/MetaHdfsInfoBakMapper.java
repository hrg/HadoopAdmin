package com.ideal.hadoopadmin.mapper.webdb.meta;

import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsInfo;
import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsInfoBak;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by fwj on 16-2-22.
 * hdfs元数据
 */
public interface MetaHdfsInfoBakMapper {


    //添加公司信息
    @Insert("insert into meta_hdfs_info_bak (hdfsPath,note,createTime,clusterUserId,hdfsGroup,hdfsOwner,hdfsPerm,properties)" +
            " values(#{hdfsPath},#{note},#{createTime},#{clusterUserId},#{hdfsGroup},#{hdfsOwner},#{hdfsPerm},#{properties})")
    public void saveMetaHdfsInfoBak(MetaHdfsInfoBak metaHdfsInfoBak);

    //根据id删除
    @Delete("delete from meta_hdfs_info_bak where id =#{id}")
    public void deleteMetaHdfsInfoBak(Long id);

    //根据id查找
    @Select("select * from meta_hdfs_info_bak where id =#{id}")
    public MetaHdfsInfoBak queryMetaHdfsInfoBakById(Long id);
    //根据用户id进行删除
    @Delete("delete from meta_hdfs_info_bak where ClusterUserId=#{userId}")
    public void deleteByUserId(Long userId);
    @Select("SELECT * FROM meta_hdfs_info_bak hdfsInfobak WHERE NOT EXISTS\n" +
            "(SELECT 1 FROM meta_hdfs_info WHERE meta_hdfs_info.hdfsPath=hdfsInfobak.hdfsPath\n" +
            "AND meta_hdfs_info.clusterUserId=hdfsInfobak.clusterUserId\n" +
            "AND meta_hdfs_info.hdfsGroup=hdfsInfobak.hdfsGroup\n" +
            "AND meta_hdfs_info.hdfsOwner=hdfsInfobak.hdfsOwner\n" +
            "AND meta_hdfs_info.hdfsPerm=hdfsInfobak.hdfsPerm)\n" +
            "AND hdfsInfobak.hdfsPath LIKE '%public%'")
    List<MetaHdfsInfoBak> compareWithHdfsInfo();
}
