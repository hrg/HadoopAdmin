package com.ideal.hadoopadmin.mapper.webdb.system;

import com.ideal.hadoopadmin.entity.system.WebLog;
import com.ideal.hadoopadmin.entity.system.WebOperations;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by fwj on 2016/3/15.
 * 系统日志管理
 */
public interface WebLogMapper {

    @Insert("insert into system_web_log (userName,operationId,createtime,parameter) values (#{userName},#{operationId},#{createtime},#{parameter})")
    public void saveWebLog(WebLog webLog);

    public List<WebLog> queryWebLog();
}
