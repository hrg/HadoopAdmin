package com.ideal.hadoopadmin.mapper.webdb.cluster;

import com.ideal.hadoopadmin.entity.cluster.ClusterUser;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * Created by CC on 2016/2/19.
 */
public interface ClusterUserMapper {
    public List<ClusterUser> findClusterUser();
    //通过id查询cluster_user
    @Select("select * from cluster_user where id =#{id}")
    public ClusterUser findById( Long id);
    //查询clusterUser
    @Select("select * from cluster_user order by userName")
    public List<ClusterUser> queryClusterUser();

    //根据公司id查询所有用户
    @Select("select * from cluster_user where companyId=#{sysCompanyId}")
    public List<ClusterUser> queryClusterUserBySystemId(Long sysCompanyId);

    //根据公司id和集群类型id查询所有用户add20160714qinfengxia
    @Select("select * from cluster_user where companyId=#{sysCompanyId} and clusterTypeId=#{clusterTypeId}")
    public List<ClusterUser> queryClusterUserByParams(@Param("sysCompanyId")Long sysCompanyId , @Param("clusterTypeId")Long clusterTypeId);

    //根据集群类型id查询符合的用户
    @Select("select * from cluster_user where clusterTypeId=#{clusterTypeId}")
    public List<ClusterUser> queryClusterUserByClusterTypeId(Long clusterTypeId);

    //查询clusterUser
    @Select("select * from cluster_user where id=#{id}")
    public ClusterUser queryClusterUserById(@Param("id") Long id);
    //新增cluster
    @Insert("insert into cluster_user(userName,clientPW,systemPW,companyId,contactPerson,phoneNumber,email,remark,userTypeId,status,clusterTypeId,changeTime,createTime,itsmNumber) " +
            "values(#{userName},#{clientPW},#{systemPW},#{companyId},#{contactPerson},#{phoneNumber},#{email},#{remark},#{userTypeId},#{status},#{clusterTypeId},#{changeTime},#{createTime},#{itsmNumber})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    public void save(ClusterUser clusterUser);
    @Update("update cluster_user set userName=#{userName},clientPW=#{clientPW},companyId=#{companyId},contactPerson=#{contactPerson},phoneNumber=#{phoneNumber}" +
            ",email=#{email},remark=#{remark},userTypeId=#{userTypeId},clusterTypeId=#{clusterTypeId},changeTime=#{changeTime},itsmNumber=#{itsmNumber} " +
            "where id=#{id}")
    public void update(ClusterUser clusterUser);

    //查询clusterUser除集群用户的id
    @Select("select * from cluster_user where id not in (select clusterUserId from meta_hdfs_access where hdfsInfoId=#{id})")
    public List<ClusterUser> queryClusterUserByIdNotIn(@Param("id")Long id);
    //查询clusterUser除集群用户的id
    @Select("select * from cluster_user where id not in (select clusterUserId from meta_hive_access where hiveInfoId=#{id})")
    public List<ClusterUser> queryClusterUserByHiveIdNotIn(@Param("id")Long id);
    //通过所属集群和用户名进行查找
    @Select("select * from cluster_user where userName=#{userName} and clusterTypeId=#{clusterTypeId}")
    public ClusterUser findByNameAndCluster(@Param("userName")String userName,@Param("clusterTypeId")Long clusterTypeId);
    @Delete("delete from cluster_user where id=#{id}")
    public void deleteById(Long id);
    //根据hive本身userId和hiveAccess里面根据hiveInfoId对应的clusterUserId
    List<ClusterUser> findInnerByGroupId(@Param("hiveInfoId")Long hiveInfoId,@Param("userId")Long userId);
    //
    List<ClusterUser> findOuterByGroupId(@Param("hiveInfoId")Long hiveInfoId,@Param("userId")Long userId);

//    @Select("select * from cluster_user where clusterTypeId =(select clusterTypeId from cluster_user where id=#{userId}) and id=#{userId} or id in(select clusterUserId from meta_hdfs_access where hdfsInfoId=#{hdfsInfoId})")
    List<ClusterUser> findInnerByHdfsInfo(@Param("userId")Long userId,@Param("hdfsInfoId")Long hdfsInfoId);

//    @Select("select * from cluster_user where clusterTypeId =(select clusterTypeId from cluster_user where id=#{userId}) and id!=#{userId} and id not in(select clusterUserId from meta_hdfs_access where hdfsInfoId=#{hdfsInfoId})")
    List<ClusterUser> findOuterByHdfsInfoId(@Param("userId")Long userId,@Param("hdfsInfoId")Long hdfsInfoId);
    @Select("select id,userName from cluster_user where clusterTypeId=#{clusterTypeId} order by userName")
    List<ClusterUser>findByClusterType(Long clusterTypeId);

    //更改状态
    @Update("update cluster_user set status = #{status} where id=#{userId}")
    void updateStatus(@Param("userId")Long userId,@Param("status") Integer status);
    Map<String ,Object> findCompanyParams(@Param("companyId")Long companyId);
    public List<Map<String,Object>> findClusterUserMap();
    /*查询cluster和hdfs信息*/
    public List<Map<String,Object>> findUserAndHdfsList(Map<String, Object> map);
    public List<Map<String,Object>> findOtherClusterUserMap();
}
