package com.ideal.hadoopadmin.mapper.webdb.meta;

import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsInfo;
import com.ideal.hadoopadmin.entity.meta.hive.MetaHiveInfo;
import com.ideal.hadoopadmin.entity.report.HiveInfoCustom;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * Created by admin on 16-3-1.
 * hive元数据管理
 */
public interface MetaHiveInfoMapper {

    /**
     *  查询数据库里面的数据库
     * */
    @Select("select distinct h.dbName from meta_hive_info h")
    public List<String> getDbNames();

    /**
     *  根据id查询
     * */
    public MetaHiveInfo findHiveInfoById(Long id);
    //根据id查询userId
    @Select("select clusterUserId from meta_hive_info where id=#{id}")
    Long findUserIdInfoById(Long id);
    public List<MetaHiveInfo> queryMetaHiveInfo();
    List<MetaHiveInfo> findHiveInfo();
    @Delete("delete from meta_hive_info where clusterUserId=#{clusterUserId}")
    public void deleteByUserId(Long clusterUserId);
    //根据userId查询HiveInfo
    @Select("select * from meta_hive_info where clusterUserId=#{clusterUserId}")
    MetaHiveInfo findHiveInfoByUserId(Long userId);
    //通过useId查找所有的tableNames
    @Select("select tableName from meta_hive_info where clusterUserId=#{userId}")
    List<String> findTableNamesByUserId(Long userId);

    /*查找hiveInfo中的表被允许访问的用户,和所属用户,关联hiveAccess和clusterUser表*/
    List<HiveInfoCustom> findAccessUsers();
    List<Map<String,Object>> findAccessUsersList(Map<String,Object> param);
    /*查找hive第一级数据add20160721qinfengxia*/
    List<MetaHiveInfo> hiveParentList();
    List<Map<String,Object>> findUserByHiveId(Map<String,Object> param);
    /*查找hive（用户主导）第一级数据add20160918qinfengxia*/
    List<Map<String,Object>> hiveUserParentList(Map<String, Object> paraMap);
    /*查找hive（用户主导）第一级数据add20160918qinfengxia*/
    Long hiveUserParentCount(Map<String, Object> paraMap);
    /*查找hive（用户主导）第二级数据add20160918qinfengxia*/
    List<Map<String,Object>> hiveUserChildList(Map<String,Object> param);
}
