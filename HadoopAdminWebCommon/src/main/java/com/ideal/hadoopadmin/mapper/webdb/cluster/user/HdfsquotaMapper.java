package com.ideal.hadoopadmin.mapper.webdb.cluster.user;

import com.ideal.hadoopadmin.entity.cluster.user.Hdfsquota;
import org.apache.ibatis.annotations.*;

import java.util.Map;

/**
 * Created by 袁颖 on 2016/2/26.
 */
public interface HdfsquotaMapper {
    @Select("select * from cluster_user_hdfsquota where userId=#{userId}")
    public Hdfsquota findByUserId(Long userId);
    @Insert("insert into cluster_user_hdfsquota(userId,hdfsSpace,hdfsFileCount,hdfsSpaceUnit,hdfsPath) " +
            "values(#{userId},#{hdfsSpace},#{hdfsFileCount},#{hdfsSpaceUnit},#{hdfsPath})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    public void save(Hdfsquota hdfsquota);
    @Update("update cluster_user_hdfsquota set hdfsSpace=#{hdfsSpace},hdfsFileCount=#{hdfsFileCount},hdfsSpaceUnit=#{hdfsSpaceUnit},hdfsPath=#{hdfsPath}" +
            "where id=#{id}")
    public void update(Hdfsquota hdfsquota);
    @Delete("delete from cluster_user_hdfsquota where userId=#{userId}")
    public void deleteByUserId(Long userId);

    void updateByUserIds(Map<String,Object> map);
}
