package com.ideal.hadoopadmin.mapper.webdb.meta;

import com.ideal.hadoopadmin.entity.meta.privilege.MetaAccessPrivilege;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * Created by qinfengxia on 2016/8/4.
 */
public interface MetaAccessPrivilegeMapper {
    /**
     * 查询所有的权限类型
     * @return
     */
    @Select("select * from meta_access_privilege where isHidden=0")
    List<MetaAccessPrivilege> queryMetaAccessType();

    @Select("select * from meta_access_privilege where type=#{type} and isHidden=0 order by id asc")
    List<MetaAccessPrivilege> queryMetaAccessPrivilegeByType(String type);

    @Select("select * from meta_access_privilege where id=#{id}")
    MetaAccessPrivilege findById(Long id);

    //更新数据
    @Update("update meta_access_privilege set type=#{type},privilegeType=#{privilegeType},note=#{note},isHidden=#{isHidden} where id=#{id}")
    void updateMetaAccessType(MetaAccessPrivilege metaAccessType);

    //保存数据
    @Insert("insert into meta_access_privilege(type,privilegeType,note,isHidden) values(#{type},#{privilegeType},#{note},#{isHidden})")
    void saveMetaAccessType(MetaAccessPrivilege metaAccessType);

    @Delete("delete from meta_access_privilege where id=#{id}")
    void deleteById(Long id);

}
