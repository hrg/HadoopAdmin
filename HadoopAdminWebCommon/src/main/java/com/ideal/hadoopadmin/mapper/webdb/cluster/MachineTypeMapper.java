package com.ideal.hadoopadmin.mapper.webdb.cluster;

import com.ideal.hadoopadmin.entity.cluster.MachineType;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by 袁颖 on 2016/2/22.
 */
public interface MachineTypeMapper {
    @Select("select * from cluster_machine_type")
    List<MachineType> findAllMachineType();
}
