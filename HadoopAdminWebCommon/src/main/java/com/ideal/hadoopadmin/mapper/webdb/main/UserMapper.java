package com.ideal.hadoopadmin.mapper.webdb.main;

import com.ideal.hadoopadmin.entity.main.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by CC on 2016/2/18.
 */
public interface UserMapper {

    //根据用户id获取用户信息
    @Select("select * from system_user where id=#{id}")
    public User getUserById(@Param("id")Long id);

    //根据用户名获取用户信息
    @Select("select * from system_user where userName=#{userName}")
    public User getUserByUserName(@Param("userName")String userName);

    //用户登录验证
    @Select("select * from system_user where username=#{userName} and password=#{passWord}")
    public User getUserByNameAndPassWord(@Param("userName")String userName,
                                         @Param("passWord")String passWord) throws Exception;

    //根据用户的id获取用户角色
    @Select("select roleid from system_user_role where userid=#{id}")
    public List<Long> getUserRoles(@Param("id")Long id);
    //查询所有用户
    public List<User> queryAllUser();

    /**
     * 添加用户
     * */
    public Long saveUsers(User user);

    @Update("update system_user set email=#{email},phoneNumber=#{phoneNumber},changeTime=#{changeTime},systemCompanyId=#{systemCompanyId},remark=#{remark} where id=#{id}")
    public void updateUsers(User user);

    @Update("update system_user set passWord=#{passWord} where id =#{id}")
    public void updateUserPassword(User user);


    @Delete("delete from system_user where id = #{id}")
    public void deleteUserByIds(Long id);
}
