package com.ideal.hadoopadmin.mapper.webdb.cluster;

import com.ideal.hadoopadmin.entity.cluster.DataTransfer;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * Created by 袁颖 on 2016/4/19.
 */
public interface DataTransferMapper {
    //关联查询
    List<DataTransfer> findAll();

    //保存数据
    @Insert("INSERT INTO cluster_data_transfer (sourceClusterTypeId, targetClusterTypeId, clusterUserId, hdfsInfoId, hdfsPath, startTime, endTime, runTime, runCycle, status) " +
            "VALUES ( #{sourceClusterTypeId},  #{targetClusterTypeId}, #{clusterUserId}, #{hdfsInfoId}, #{hdfsPath}, #{startTime}, #{endTime}, #{runTime}, #{runCycle}, #{status})")
    void save(DataTransfer dataTransfer);
    @Update("update cluster_data_transfer set status =#{status} where id = #{id}")
    void updateStatus(@Param("id")Long id,@Param("status")Integer status);
    @Delete("delete from cluster_data_transfer where id=#{id}")
    void delete(Long id);
}
