package com.ideal.hadoopadmin.mapper.webdb.cluster;

import com.ideal.hadoopadmin.entity.cluster.ClusterCompanyQuota;
import com.ideal.hadoopadmin.entity.system.company.SystemCompany;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * Created by Administrator on 2016/8/4.
 */
public interface ClusterCompanyQuotaMapper {

    //添加公司配置信息
    @Insert("insert into cluster_company_quota (companyId, maxUserNum, maxResource, maxCpu, hdfsFileCount, hdfsSpace) values(#{companyId},#{maxUserNum},#{maxResource},#{maxCpu},#{hdfsFileCount},#{hdfsSpace})")
    //@Options(useGeneratedKeys = true,keyProperty = "id")
    public void saveClusterCompanyQuota(ClusterCompanyQuota clusterCompanyQuota);

    /**
     * 根据公司id查询
     * */
    @Select("select * from cluster_company_quota where companyId=#{companyId}")
    ClusterCompanyQuota findClusterCompanyQuotaById(Long companyId);


    //更新公司配置信息
    @Update("update cluster_company_quota set companyId=#{companyId},maxUserNum=#{maxUserNum},maxResource=#{maxResource},maxCpu=#{maxCpu},hdfsFileCount=#{hdfsFileCount},hdfsSpace=#{hdfsSpace} where id=#{id}")
    public void updateClusterCompanyQuotaById(ClusterCompanyQuota clusterCompanyQuota);

    //删除公司配置信息
    @Update("delete from cluster_company_quota  where companyId=#{companyId} ")
    public void deleteClusterCompanyQuotaByCompanyId(Long companyId);

}
