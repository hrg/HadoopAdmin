package com.ideal.hadoopadmin.mapper.webdb.meta;

import com.ideal.hadoopadmin.entity.meta.hdfs.MetaHdfsInfo;
import com.ideal.hadoopadmin.entity.report.HdfsInfoCustom;
import com.ideal.hadoopadmin.entity.system.company.SystemCompany;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * Created by fwj on 16-2-22.
 * hdfs元数据
 */
public interface MetaHdfsInfoMapper {

    public List<MetaHdfsInfo> queryMetaHdfsInfo();
    public List<MetaHdfsInfo> queryMetaHdfsPath();
    //根据id查询

    public MetaHdfsInfo queryMetaHdfsInfoById(Long id);

    //添加公司信息
    @Insert("insert into meta_hdfs_info (hdfsPath,note,createTime,clusterUserId,hdfsGroup,hdfsOwner,hdfsPerm)" +
            " values(#{hdfsPath},#{note},#{createTime},#{userId},#{hdfsGroup},#{hdfsOwner},#{hdfsPerm})")
    public void saveMetaHdfsInfo(MetaHdfsInfo metaHdfsInfo);

    //根据id修改备注
    @Insert("update meta_hdfs_info set note=#{note} where id = #{id}")
    public void updateMetaHdfsInfoById(MetaHdfsInfo metaHdfsInfo);
    //根据id删除
    @Delete("delete from meta_hdfs_info where id =#{id}")
    public void deleteMetaHdfsInfoById(Long id);
    @Delete("delete from meta_hdfs_info where clusterUserId=#{userId}")
    public void deleteByUserId(Long userId);
    @Select("select clusterUserId from meta_hdfs_info where id =#{id}")
    public Long findUserIdByHdfsInfoId(Long id);
    @Select("select id,hdfsPath from meta_hdfs_info where clusterUserId=#{userId} order by hdfsPath")
    List<MetaHdfsInfo>findPathByUserId(Long userId);
    @Select("SELECT * FROM meta_hdfs_info hdfsInfo \n" +
            "WHERE NOT EXISTS( SELECT 1 FROM meta_hdfs_info_bak \n" +
            "WHERE hdfsInfo.hdfsPath=meta_hdfs_info_bak.hdfsPath\n" +
            "AND hdfsInfo.clusterUserId=meta_hdfs_info_bak.clusterUserId\n" +
            "AND hdfsInfo.hdfsGroup=meta_hdfs_info_bak.hdfsGroup\n" +
            "AND hdfsInfo.hdfsOwner=meta_hdfs_info_bak.hdfsOwner\n" +
            "AND hdfsInfo.hdfsPerm=meta_hdfs_info_bak.hdfsPerm )")
    List<MetaHdfsInfo> compareWithHdfsInfoBak();
    @Select("select id from meta_hdfs_info where hdfsPath=#{hdfsPath} and clusterUserId=#{clusterUserId}")
    List<MetaHdfsInfo>findPathByUserAndTbl(@Param("hdfsPath") String hdfsPath, @Param("clusterUserId") Long clusterUserId);
    /*@Select("SELECT\n" +
            "    id\n" +
            "FROM\n" +
            "    meta_hdfs_info\n" +
            "WHERE\n" +
            "    EXISTS\n" +
            "    (\n" +
            "        SELECT\n" +
            "            1\n" +
            "        FROM\n" +
            "            meta_hdfs_info_bak \n" +
            "        WHERE\n" +
            "            meta_hdfs_info.hdfsPath=meta_hdfs_info_bak.hdfsPath\n" +
            "           \n" +
            "        AND meta_hdfs_info.clusterUserId=meta_hdfs_info_bak.clusterUserId\n" +
            "        AND meta_hdfs_info.hdfsGroup=meta_hdfs_info_bak.hdfsGroup\n" +
            "        AND meta_hdfs_info.hdfsOwner=meta_hdfs_info_bak.hdfsOwner\n" +
            "        AND meta_hdfs_info.hdfsPerm=meta_hdfs_info_bak.hdfsPerm \n" +
            "\t\t  AND meta_hdfs_info_bak.id=(select meta_hive_info.hdfsInfoBakId from meta_hive_info where meta_hive_info.id =3))")*/
    //注释掉上面的代码，是因为代码中有写固定参数meta_hive_info.id =3且结果不正确update20160809
    @Select("select info.id from meta_hdfs_info_bak bak left join meta_hdfs_info info\n" +
            "on bak.hdfsPath = info.hdfsPath and bak.clusterUserId = info.clusterUserId\n" +
            "and bak.hdfsGroup = info.hdfsGroup and bak.hdfsOwner = info.hdfsOwner\n" +
            "and bak.hdfsPerm = info.hdfsPerm where bak.id=#{hdfsInfoBakId}")
    Long findHdfsInfoIdByHiveInfoId(Long hdfsInfoBakId);
    /*哪些hdfs地址可以被用户访问,查找hdfsInfo表并且对meta_hdfs_access和cluster_user表进行关联*/
    List<HdfsInfoCustom> hdfsPathAccess();

    /*hdfs基础数据和权限数据add20160721qinfengxia*/
    List<Map<String,Object>> hdfsPathAccessList(Map<String,Object> param);

    /*hdfs第一级数据add20160721qinfengxia*/
    List<MetaHdfsInfo> hdfsParentList();

    /*hdfs（用户主导）第一级数据add20160918qinfengxia*/
    List<Map<String,Object>> hdfsUserParentList();

    /*hdfs（用户主导）第二级数据add20160918qinfengxia*/
    List<Map<String,Object>> hdfsUserChildList(Map<String,Object> param);
}
