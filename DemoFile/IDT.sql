DROP TABLE IF EXISTS `cluster_user`;
CREATE TABLE
    cluster_user
    (
        id INT NOT NULL AUTO_INCREMENT COMMENT '自增id',
        userName VARCHAR(45) COMMENT '用户名',
        clientPW VARCHAR(45) COMMENT '用户客户端密码',
        systemPW VARCHAR(45) COMMENT '用户服务器密码',
        companyId INT COMMENT '用户所属公司',
        contactPerson VARCHAR(45) COMMENT '联系人名字',
        phoneNumber VARCHAR(11) COMMENT '联系电话',
        email VARCHAR(45) COMMENT '用户邮箱',
        remark VARCHAR(45) COMMENT '用户备注',
        userTypeId INT COMMENT '用户类型',
        status INT(1) COMMENT '用户状态:0-启用,1-停用',
        clusterTypeId INT COMMENT '用户所属集群',
        changeTime bigint COMMENT '修改时间',
        createTime bigint COMMENT '创建时间',
        PRIMARY KEY (id)
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='集群用户表';

-- 导出  表 import.meta_hdfs_info_bak 结构
DROP TABLE IF EXISTS `meta_hdfs_info_bak`;
CREATE TABLE IF NOT EXISTS `meta_hdfs_info_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hdfsPath` varchar(100) NOT NULL COMMENT 'hdfs地址',
  `clusterUserId` int(11) NOT NULL COMMENT '根据地址截取的用户',
  `hdfsGroup` varchar(45) NOT NULL COMMENT '地址所属组',
  `hdfsOwner` varchar(45) NOT NULL COMMENT '地址的权限所属人',
  `hdfsPerm` varchar(45) NOT NULL COMMENT '地址所属权限',
  `note` varchar(45) NOT NULL COMMENT '备注',
  `createTime` bigint(20) NOT NULL COMMENT '创建时间',
  `properties` int(1) NOT NULL COMMENT '0 公有 1 私有 ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='hdfs元数据信息';

-- 导出  表 import.meta_hive_info 结构
DROP TABLE IF EXISTS `meta_hive_info`;
CREATE TABLE IF NOT EXISTS `meta_hive_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dbName` varchar(45) NOT NULL COMMENT 'HIVE数据库名称',
  `tableName` varchar(45) NOT NULL COMMENT 'HIVE表名',
  `hdfsInfoBakId` int(11) NOT NULL COMMENT 'hdfs_bak表信息',
  `createTime` bigint(20) NOT NULL COMMENT '创建时间',
  `clusterUserId` int(11) NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='集群公有的hive元数据信息';

-- 导出  表 import.meta_hive_sql 结构
DROP TABLE IF EXISTS `meta_hive_sql`;
CREATE TABLE IF NOT EXISTS `meta_hive_sql` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hiveSql` text NOT NULL COMMENT 'hive建表sql',
  `hiveInfoId` int(11) NOT NULL COMMENT 'hiveInfo表id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='hive的建表sql语句';

-- 导出  表 import.cluster_user_kbrauth 结构
DROP TABLE IF EXISTS `cluster_user_kbrauth`;
CREATE TABLE IF NOT EXISTS `cluster_user_kbrauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '集群用户标识',
  `machineId` int(11) NOT NULL COMMENT '机器id',
  `startTime` bigint(20) NOT NULL COMMENT '票据生效时间',
  `endTime` bigint(20) NOT NULL COMMENT '票据失效时间',
  `principal` varchar(50) NOT NULL COMMENT '认证主体',
  `ticketPath` varchar(50) NOT NULL COMMENT 'kerberos票据具体地址',
  `status` int(11) NOT NULL COMMENT 'kerberos认证状态:0-成功,1-失败',
  `createTime` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='集群用户kerberos认证情况';

-- 导出  表 import.cluster_user_kbrconfig 结构
DROP TABLE IF EXISTS `cluster_user_kbrconfig`;
CREATE TABLE IF NOT EXISTS `cluster_user_kbrconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '集群用户标识',
  `validDay` int(10) unsigned zerofill NOT NULL COMMENT 'kerberos有效期',
  `machineIds` varchar(100) NOT NULL COMMENT '机器列表(存储机器id)',
  `status` int(1) unsigned zerofill NOT NULL COMMENT 'kerberos状态:0-启用,1-停用',
  `changeTime` bigint(20) NOT NULL COMMENT '修改时间',
  `createTime` bigint(20) NOT NULL COMMENT '创建时间',
  `startTime` bigint(20) NOT NULL COMMENT '有效时间开始日期',
  `endTime` bigint(20) NOT NULL COMMENT '有效时间结束日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='集群用户kerberos配置表';

-- 导出  表 import.cluster_parameter 结构
DROP TABLE IF EXISTS `cluster_parameter`;
CREATE TABLE IF NOT EXISTS `cluster_parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameterKey` varchar(50) NOT NULL DEFAULT '' COMMENT '参数key值',
  `parameterVal` varchar(200) NOT NULL DEFAULT '' COMMENT '参数val值',
  `parentId` int(11) NOT NULL COMMENT '父参数Id',
  `note` varchar(50) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `parameterKey` (`parameterKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 导出  表 import.cluster_machine 结构
DROP TABLE IF EXISTS `cluster_machine`;
CREATE TABLE IF NOT EXISTS `cluster_machine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `machineIp` varchar(15) NOT NULL COMMENT '机器ip地址',
  `loginUserName` varchar(45) NOT NULL COMMENT '机器登陆用户',
  `loginPassWord` varchar(45) NOT NULL COMMENT '机器登陆密码',
  `clusterTypeId` int(11) NOT NULL COMMENT '机器所属集群',
  `machineTypeId` int(11) unsigned NOT NULL COMMENT '机器类型',
  `isAddUser` int(11) NOT NULL COMMENT '是否添加用户:0-添加,1-不添加',
  `status` int(11) NOT NULL COMMENT '机器状态:0-正常,1-停机',
  `note` varchar(45) NOT NULL COMMENT '机器备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='集群机器信息';