﻿--miyue database bak
-- --------------------------------------------------------
-- 主机:                           172.29.0.7
-- 服务器版本:                        5.7.11 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  8.3.0.4795
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出 hadoopadminweb_db 的数据库结构
CREATE DATABASE IF NOT EXISTS `hadoopadminweb_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `hadoopadminweb_db`;


-- 导出  表 hadoopadminweb_db.cluster_cluster_type 结构
CREATE TABLE IF NOT EXISTS `cluster_cluster_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '集群标识',
  `clusterTypeId` varchar(45) NOT NULL,
  `clusterTypeName` varchar(45) NOT NULL COMMENT '集群类型名称',
  `note` varchar(45) NOT NULL COMMENT '集群类型备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `clusterTypeId_UNIQUE` (`clusterTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='集群类型表';

-- 正在导出表  hadoopadminweb_db.cluster_cluster_type 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `cluster_cluster_type` DISABLE KEYS */;
INSERT INTO `cluster_cluster_type` (`id`, `clusterTypeId`, `clusterTypeName`, `note`) VALUES
	(1, '1', 'XX大数据平台', 'null'),
	(2, '2', 'YY大数据平台', 'null');
/*!40000 ALTER TABLE `cluster_cluster_type` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.cluster_machine 结构
CREATE TABLE IF NOT EXISTS `cluster_machine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `machineIp` varchar(15) NOT NULL COMMENT '机器ip地址',
  `loginUserName` varchar(45) NOT NULL COMMENT '机器登陆用户',
  `loginPassWord` varchar(45) NOT NULL COMMENT '机器登陆密码',
  `clusterTypeId` int(11) NOT NULL COMMENT '机器所属集群',
  `machineTypeId` int(11) unsigned NOT NULL COMMENT '机器类型',
  `isAddUser` int(11) NOT NULL COMMENT '是否添加用户:0-添加,1-不添加',
  `status` int(11) NOT NULL COMMENT '机器状态:0-正常,1-停机',
  `note` varchar(45) NOT NULL COMMENT '机器备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='集群机器信息';

-- 正在导出表  hadoopadminweb_db.cluster_machine 的数据：~9 rows (大约)
/*!40000 ALTER TABLE `cluster_machine` DISABLE KEYS */;
INSERT INTO `cluster_machine` (`id`, `machineIp`, `loginUserName`, `loginPassWord`, `clusterTypeId`, `machineTypeId`, `isAddUser`, `status`, `note`) VALUES
	(1, '172.29.0.7', 'hadoop', 'had0op1&cyrf%nbtv77ldb', 1, 1, 0, 1, 'nn'),
	(2, '172.29.0.8', 'hadoop', 'had0op1&bbtf%nbtv77laa', 1, 1, 0, 1, 'nn'),
	(3, '0.0.0.0', 'hadoop', 'iiui', 1, 2, 0, 1, 'dn'),
	(4, '172.29.0.7', 'hadoop', 'had0op1&cyrf%nbtv77ldb', 1, 5, 0, 1, 'Hive'),
	(5, '172.29.0.14', 'hadoop', 't~6HZD+5xqxXBPjs2cfg', 1, 6, 0, 1, 'clustshell'),
	(6, '172.29.0.25', 'hadoop', '!)jv46DOj%dWQPb%', 1, 3, 0, 1, 'Client'),
	(7, '172.29.0.25', 'hadoop', '!)jv46DOj%dWQPb%', 1, 8, 0, 1, 'WebApp'),
	(8, '172.29.0.7', 'hadoop', 'had0op1&cyrf%nbtv77ldb', 1, 4, 0, 1, 'kdc'),
	(9, '172.29.0.8', 'hadoop', 'had0op1&bbtf%nbtv77laa', 1, 7, 0, 1, 'rm');
/*!40000 ALTER TABLE `cluster_machine` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.cluster_machine_type 结构
CREATE TABLE IF NOT EXISTS `cluster_machine_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `machineTypeId` varchar(45) NOT NULL COMMENT '机器类型id',
  `machineTypeName` varchar(45) NOT NULL COMMENT '机器类型名称',
  `note` varchar(45) NOT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `machineTypeId_UNIQUE` (`machineTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='集群机器类型';

-- 正在导出表  hadoopadminweb_db.cluster_machine_type 的数据：~8 rows (大约)
/*!40000 ALTER TABLE `cluster_machine_type` DISABLE KEYS */;
INSERT INTO `cluster_machine_type` (`id`, `machineTypeId`, `machineTypeName`, `note`) VALUES
	(1, '1', 'NN', 'NN'),
	(2, '2', 'DN', 'DN'),
	(3, '3', 'Client', 'Client'),
	(4, '4', 'KDC', 'KDC'),
	(5, '5', 'Hive', 'Hive'),
	(6, '6', 'Clushell', 'Clushell'),
	(7, '7', 'RM', 'RM'),
	(8, '8', 'WebApp', 'WebApp');
/*!40000 ALTER TABLE `cluster_machine_type` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.cluster_parameter 结构
CREATE TABLE IF NOT EXISTS `cluster_parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameterKey` varchar(50) NOT NULL DEFAULT '' COMMENT '参数key值',
  `parameterVal` varchar(200) NOT NULL DEFAULT '' COMMENT '参数val值',
  `parentId` int(11) NOT NULL COMMENT '父参数Id',
  `note` varchar(50) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `parameterKey` (`parameterKey`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- 正在导出表  hadoopadminweb_db.cluster_parameter 的数据：~28 rows (大约)
/*!40000 ALTER TABLE `cluster_parameter` DISABLE KEYS */;
INSERT INTO `cluster_parameter` (`id`, `parameterKey`, `parameterVal`, `parentId`, `note`) VALUES
	(1, 'WEBAPP_INIT_PATH', '/home/hadoop/hadoopadmin/source/init', 22, '初始系统参数地址'),
	(2, 'HIVE_HDFSPATH_PREFIX', 'hdfs://ns1', 20, ''),
	(4, 'HDFS_SUPER_USER', 'hdfs', 21, ''),
	(5, 'HDFS_CLUSH_DN_RANGE', 'cdhdatanode', 21, ''),
	(6, 'HDFS_GROUP_SUFFIX', '_group', 21, ''),
	(7, 'HDFS_HIVE_TEMP_DIR', '/tmp/hive-', 21, ''),
	(8, 'HDFS_PATH_PREFIX', '/user/', 21, ''),
	(9, 'HDFS_PUBLIC_PATH_SUFFIX', '/public', 21, ''),
	(10, 'HDFS_TRASH_PATH_SUFFIX', '/.Trash', 21, ''),
	(11, 'HDFS_PRIVATE_PATH_SUFFIX', '/private', 21, ''),
	(12, 'HIVE_CLIENT_LOG_DIR', '/tmp/', 20, ''),
	(13, 'HIVE_SENTRY_PRINCIPAL', 'principal=hive/dmp-nn-001@EXAMPLE.COM', 20, ''),
	(14, 'HIVE_SENTRY_JDBC', 'jdbc:hive2://localhost:10000/', 20, ''),
	(15, 'HIVE_SENTRY_ROLE_TEMPLET', 'role_@item@_@principle@_@path@', 20, ''),
	(16, 'HIVE_SUPER_USER', 'hive', 20, ''),
	(17, 'KDC_REALM', '@EXAMPLE.COM', 21, 'KDC认证'),
	(18, 'Des_Ticket_Cache_Path', '/home/hadoop/hadoopadmin/source/ticket_cache/', 22, 'kerberos ticket cache 存放地址'),
	(19, 'Linux_Group_Path', '/etc/group', 22, 'Linux组信息_初始hdfsvisiter信息'),
	(20, 'Hive', 'Hive parameters', 0, 'hive参数'),
	(21, 'HADOOP', 'Hadoop parameters', 0, 'hadoop参数'),
	(22, 'SYSTEM', 'System parameter', 0, '系统配置参数'),
	(23, 'RM_SCHEDULE_XML_DES_PATH', '/etc/hadoop/conf/fair-scheduler.xml', 21, ''),
	(24, 'RM_SCHEDULE_XML_FILE_NAME', 'fair-scheduler.xml', 21, ''),
	(25, 'RM_SCHEDULE_XML_SRC_PATH', '/home/hadoop/hadoopadmin/source/init/fair-scheduler.xml', 21, ''),
	(28, 'WEBAPP_SHELL_PATH', '/home/hadoop/hadoopadmin/source/sh/', 22, ''),
	(29, 'PublicPw', '12345', 22, '公共的验证密码'),
	(30, 'Is_Use_BeeLine', '1', 22, '1:启动beeline;0:关闭beeline'),
	(31, 'HIVE_USERNAME', 'hadoop', 20, 'hive用户');
/*!40000 ALTER TABLE `cluster_parameter` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.cluster_user 结构
CREATE TABLE IF NOT EXISTS `cluster_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `userName` varchar(45) DEFAULT NULL COMMENT '用户名',
  `clientPW` varchar(45) DEFAULT NULL COMMENT '用户客户端密码',
  `systemPW` varchar(45) DEFAULT NULL COMMENT '用户服务器密码',
  `companyId` int(11) DEFAULT NULL COMMENT '用户所属公司',
  `contactPerson` varchar(45) DEFAULT NULL COMMENT '联系人名字',
  `phoneNumber` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(45) DEFAULT NULL COMMENT '用户邮箱',
  `remark` varchar(45) DEFAULT NULL COMMENT '用户备注',
  `userTypeId` int(11) DEFAULT NULL COMMENT '用户类型',
  `status` int(1) DEFAULT NULL COMMENT '用户状态:0-启用,1-停用',
  `clusterTypeId` int(11) DEFAULT NULL COMMENT '用户所属集群',
  `changeTime` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `createTime` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='集群用户表';

-- 正在导出表  hadoopadminweb_db.cluster_user 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `cluster_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `cluster_user` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.cluster_user_hdfsquota 结构
CREATE TABLE IF NOT EXISTS `cluster_user_hdfsquota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '集群用户标识',
  `hdfsSpace` int(11) NOT NULL COMMENT 'hdfs用户空间大小',
  `hdfsFileCount` int(11) NOT NULL COMMENT 'hdfs文件数量',
  `hdfsSpaceUnit` varchar(1) NOT NULL COMMENT 'hdfs空间单位',
  `hdfsPath` varchar(100) NOT NULL COMMENT 'hdfs地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- 正在导出表  hadoopadminweb_db.cluster_user_hdfsquota 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `cluster_user_hdfsquota` DISABLE KEYS */;
/*!40000 ALTER TABLE `cluster_user_hdfsquota` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.cluster_user_kbrauth 结构
CREATE TABLE IF NOT EXISTS `cluster_user_kbrauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '集群用户标识',
  `machineId` int(11) NOT NULL COMMENT '机器id',
  `startTime` bigint(20) NOT NULL COMMENT '票据生效时间',
  `endTime` bigint(20) NOT NULL COMMENT '票据失效时间',
  `principal` varchar(50) NOT NULL COMMENT '认证主体',
  `ticketPath` varchar(50) NOT NULL COMMENT 'kerberos票据具体地址',
  `status` int(11) NOT NULL COMMENT 'kerberos认证状态:0-成功,1-失败',
  `createTime` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='集群用户kerberos认证情况';

-- 正在导出表  hadoopadminweb_db.cluster_user_kbrauth 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `cluster_user_kbrauth` DISABLE KEYS */;
/*!40000 ALTER TABLE `cluster_user_kbrauth` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.cluster_user_kbrconfig 结构
CREATE TABLE IF NOT EXISTS `cluster_user_kbrconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '集群用户标识',
  `validDay` int(10) unsigned zerofill NOT NULL COMMENT 'kerberos有效期',
  `machineIds` varchar(100) NOT NULL COMMENT '机器列表(存储机器id)',
  `status` int(1) unsigned zerofill NOT NULL COMMENT 'kerberos状态:0-启用,1-停用',
  `changeTime` bigint(20) NOT NULL COMMENT '修改时间',
  `createTime` bigint(20) NOT NULL COMMENT '创建时间',
  `startTime` bigint(20) NOT NULL COMMENT '有效时间开始日期',
  `endTime` bigint(20) NOT NULL COMMENT '有效时间结束日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=727 DEFAULT CHARSET=utf8 COMMENT='集群用户kerberos配置表';

-- 正在导出表  hadoopadminweb_db.cluster_user_kbrconfig 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `cluster_user_kbrconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `cluster_user_kbrconfig` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.cluster_user_queue 结构
CREATE TABLE IF NOT EXISTS `cluster_user_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '用户id',
  `queueName` varchar(50) NOT NULL DEFAULT '' COMMENT '队列名称',
  `minResource` int(11) NOT NULL COMMENT '最小资源数',
  `maxResource` int(11) NOT NULL COMMENT '最大资源数',
  `maxApp` int(11) NOT NULL COMMENT '最大app数',
  `weight` float NOT NULL COMMENT '权重值',
  `acl` varchar(50) NOT NULL DEFAULT '' COMMENT 'acl值',
  `minCpu` int(11) NOT NULL COMMENT '最小cpu值',
  `maxCpu` int(11) NOT NULL COMMENT '最大cpu值',
  `modifyTime` bigint(20) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8;

-- 正在导出表  hadoopadminweb_db.cluster_user_queue 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `cluster_user_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `cluster_user_queue` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.cluster_user_type 结构
CREATE TABLE IF NOT EXISTS `cluster_user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户类型标识',
  `userTypeId` varchar(45) NOT NULL,
  `userTypeName` varchar(45) NOT NULL COMMENT '用户类型名称',
  `note` varchar(45) NOT NULL COMMENT '用户类型备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userTypeId_UNIQUE` (`userTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='集群用户类型';

-- 正在导出表  hadoopadminweb_db.cluster_user_type 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `cluster_user_type` DISABLE KEYS */;
INSERT INTO `cluster_user_type` (`id`, `userTypeId`, `userTypeName`, `note`) VALUES
	(1, '1', '厂商用户', ''),
	(2, '2', '内部用户', ''),
	(3, '3', '其他用户', ' ');
/*!40000 ALTER TABLE `cluster_user_type` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.meta_hdfs_access 结构
CREATE TABLE IF NOT EXISTS `meta_hdfs_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clusterUserId` int(11) NOT NULL COMMENT '集群用户',
  `hdfsInfoId` int(11) NOT NULL COMMENT 'hdfspath信息',
  `createTime` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7976 DEFAULT CHARSET=utf8 COMMENT='hdsf访问权限表';

-- 正在导出表  hadoopadminweb_db.meta_hdfs_access 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `meta_hdfs_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_hdfs_access` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.meta_hdfs_info 结构
CREATE TABLE IF NOT EXISTS `meta_hdfs_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hdfsPath` varchar(100) NOT NULL COMMENT 'hdfs地址',
  `clusterUserId` int(11) NOT NULL COMMENT '根据地址截取的用户',
  `hdfsGroup` varchar(45) NOT NULL COMMENT '地址所属组',
  `hdfsOwner` varchar(45) NOT NULL COMMENT '地址的权限所属人',
  `hdfsPerm` varchar(45) NOT NULL COMMENT '地址所属权限',
  `note` varchar(45) NOT NULL COMMENT '备注',
  `createTime` bigint(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12170 DEFAULT CHARSET=utf8 COMMENT='hdfs公有的元数据信息';

-- 正在导出表  hadoopadminweb_db.meta_hdfs_info 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `meta_hdfs_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_hdfs_info` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.meta_hdfs_info_bak 结构
CREATE TABLE IF NOT EXISTS `meta_hdfs_info_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hdfsPath` varchar(100) NOT NULL COMMENT 'hdfs地址',
  `clusterUserId` int(11) NOT NULL COMMENT '根据地址截取的用户',
  `hdfsGroup` varchar(45) NOT NULL COMMENT '地址所属组',
  `hdfsOwner` varchar(45) NOT NULL COMMENT '地址的权限所属人',
  `hdfsPerm` varchar(45) NOT NULL COMMENT '地址所属权限',
  `note` varchar(45) NOT NULL COMMENT '备注',
  `createTime` bigint(20) NOT NULL COMMENT '创建时间',
  `properties` int(1) NOT NULL COMMENT '0 公有 1 私有 ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='hdfs元数据信息';

-- 正在导出表  hadoopadminweb_db.meta_hdfs_info_bak 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `meta_hdfs_info_bak` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_hdfs_info_bak` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.meta_hive_access 结构
CREATE TABLE IF NOT EXISTS `meta_hive_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clusterUserId` int(11) NOT NULL COMMENT '集群用户标识',
  `hiveInfoId` int(11) NOT NULL COMMENT 'hive数据标识',
  `createTime` bigint(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='集群hive数据访问权限';

-- 正在导出表  hadoopadminweb_db.meta_hive_access 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `meta_hive_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_hive_access` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.meta_hive_info 结构
CREATE TABLE IF NOT EXISTS `meta_hive_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dbName` varchar(45) NOT NULL COMMENT 'HIVE数据库名称',
  `tableName` varchar(45) NOT NULL COMMENT 'HIVE表名',
  `hdfsInfoBakId` int(11) NOT NULL COMMENT 'hdfs_bak表信息',
  `createTime` bigint(20) NOT NULL COMMENT '创建时间',
  `clusterUserId` int(11) NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='集群公有的hive元数据信息';

-- 正在导出表  hadoopadminweb_db.meta_hive_info 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `meta_hive_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_hive_info` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.meta_hive_sql 结构
CREATE TABLE IF NOT EXISTS `meta_hive_sql` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hiveSql` text NOT NULL COMMENT 'hive建表sql',
  `hiveInfoId` int(11) NOT NULL COMMENT 'hiveInfo表id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='hive的建表sql语句';

-- 正在导出表  hadoopadminweb_db.meta_hive_sql 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `meta_hive_sql` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_hive_sql` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.system_company 结构
CREATE TABLE IF NOT EXISTS `system_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '公司唯一标识',
  `companyName` varchar(45) NOT NULL COMMENT '公司名称',
  `note` varchar(45) NOT NULL,
  `contactName` varchar(255) NOT NULL,
  `contactMobile` varchar(255) NOT NULL,
  `delTag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8 COMMENT='集群公司表';

-- 正在导出表  hadoopadminweb_db.system_company 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `system_company` DISABLE KEYS */;
INSERT INTO `system_company` (`id`, `companyName`, `note`, `contactName`, `contactMobile`, `delTag`) VALUES
	(61, '上海理想信息产业（集团）有限公司', '0', '张弛', '18918726543', 0),
	(199, '东方国信', '', '王林', '18917263546', 0),
	(200, '信息中心', '测试', 'wanghuiren', '18910000000', 0);
/*!40000 ALTER TABLE `system_company` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.system_menu 结构
CREATE TABLE IF NOT EXISTS `system_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL COMMENT '菜单名',
  `url` varchar(100) NOT NULL DEFAULT ' ' COMMENT '菜单路径',
  `cssClass` varchar(45) NOT NULL DEFAULT '' COMMENT '菜单样式',
  `parentId` int(11) NOT NULL COMMENT '父菜单id',
  `code` varchar(50) NOT NULL DEFAULT '',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `createTime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `isParent` int(1) NOT NULL COMMENT '0是父菜单 1不是父菜单',
  `type` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COMMENT='系统菜单';

-- 正在导出表  hadoopadminweb_db.system_menu 的数据：~64 rows (大约)
/*!40000 ALTER TABLE `system_menu` DISABLE KEYS */;
INSERT INTO `system_menu` (`id`, `title`, `url`, `cssClass`, `parentId`, `code`, `sort`, `createTime`, `isParent`, `type`) VALUES
	(1, '系统管理', '', 'ace-icon fa fa-cogs   ', 0, '', 0, 0, 0, 1),
	(2, '集群管理', '', 'ace-icon fa fa-users', 0, '', 1, 0, 0, 1),
	(3, '元数据管理', '', 'ace-icon fa fa-adjust', 0, '', 2, 0, 0, 1),
	(4, '报表管理', '', 'ace-icon fa fa-bar-chart-o', 0, '', 3, 0, 0, 1),
	(5, '用户管理', '/cluster/user/query_clusterUser_list.do', ' ', 2, '', 2, 0, 1, 1),
	(6, '机器管理', '/cluster/clusterMachine/clusterMachine_list.do', ' ', 2, '', 3, 0, 1, 1),
	(7, '参数管理', '/cluster/parameter/parameter_list.do', ' ', 2, '', 4, 0, 1, 1),
	(8, '参数管理父参数新增按钮', '', '', 7, 'parameter_parent_add', 0, 0, 1, 2),
	(9, '参数管理父参数修改按钮', ' ', '', 7, 'parameter_parent_modify', 0, 0, 1, 2),
	(10, '参数管理父参数删除按钮', ' ', '', 7, 'parameter_parent_delete', 0, 0, 1, 2),
	(11, '参数管理子参数新增按钮', ' ', '', 7, 'parameter_children_add', 0, 0, 1, 2),
	(12, '参数管理子参数修改按钮', ' ', '', 7, 'parameter_children_modify', 0, 0, 1, 2),
	(13, '参数管理子参数删除按钮', ' ', '', 7, 'parameter_children_delete', 0, 0, 1, 2),
	(14, ' hive元数据管理', '/meta/hive/hive_metadata_management.do', '', 3, '', 0, 0, 1, 1),
	(15, 'hdfs元数据管理', '/meta/hdfs/hdfs_metadata_management.do', '', 3, '', 0, 0, 1, 1),
	(16, '租户管理', '/system/company/system_company.do', '', 2, '', 1, 0, 1, 1),
	(17, '租户管理新建按钮', ' ', '', 16, 'customerAdd', 0, 0, 1, 2),
	(18, '租户管理修改按钮', ' ', '', 16, 'customer_modify', 0, 0, 1, 2),
	(19, '租户管理删除按钮', ' ', '', 16, 'customer_delete', 0, 0, 1, 2),
	(21, '菜单管理', '/system/menu/menu_manage.do', '', 1, '', 1, 0, 1, 1),
	(22, '角色管理', '/system/role/role_manage.do', '', 1, '', 2, 0, 1, 1),
	(23, '账户管理', '/system/user/user_manage.do', '', 1, '', 3, 0, 1, 1),
	(24, '新增用户', '', '', 23, 'user_add', 0, 1457925235087, 1, 2),
	(25, '新增角色', '', '', 22, 'role_add', 0, 1457925337772, 1, 2),
	(26, '修改角色', '', '', 22, 'role_modify', 0, 1457925482748, 1, 2),
	(27, '删除角色', '', '', 22, 'role_delete', 0, 1457925573489, 1, 2),
	(28, '授权角色', '', '', 22, 'role_authority', 0, 1457925638923, 1, 2),
	(29, '修改用户', '', '', 23, 'user_modify', 0, 1457925911431, 1, 2),
	(30, '重置用户密码', '', '', 23, 'user_resetPassword', 0, 1457926031603, 1, 2),
	(31, '删除用户', '', '', 23, 'user_delete', 0, 1457943996299, 1, 2),
	(32, '日志管理', '/system/log/view_daily.do', '', 1, '', 4, 1458018880039, 1, 1),
	(33, '用户报表', '/report/userFormReport/user_report_form.do', '', 4, '', 1, 0, 1, 1),
	(34, '队列资源报表', '/report/queue/queue_list.do', '', 4, '', 2, 0, 1, 1),
	(35, '用户认证信息报表', '/report/userApprove/userApprove_list.do', '', 4, '', 3, 0, 1, 1),
	(45, '用户管理新增用户按钮', '', '', 5, 'addClusterUser', 0, 1458191937156, 1, 2),
	(46, '用户管理修改用户按钮', '', '', 5, 'editClusterUser', 0, 1458192008355, 1, 2),
	(47, '用户管理查看用户按钮', '', '', 5, 'seeClusterUser', 0, 1458192064354, 1, 2),
	(48, '用户管理删除用户按钮', '', '', 5, 'deleteClusterUser', 0, 1458192136659, 1, 2),
	(49, '用户管理重置用户密码', '', '', 5, 'resetClusterUserPassword', 0, 1458192247599, 1, 2),
	(50, '用户管理用户启用按钮', '', '', 5, 'openClusterUser', 0, 1458192339491, 1, 2),
	(51, '用户管理用户暂停按钮', '', '', 5, 'closeClusterUser', 0, 1458192378089, 1, 2),
	(52, '机器管理增加机器按钮', '', '', 6, 'addClusterMachine', 0, 1458192434587, 1, 2),
	(53, '机器管理删除机器按钮', '', '', 6, 'deleteClusterMachine', 0, 1458192463794, 1, 2),
	(54, '机器管理修改机器按钮', '', '', 6, 'editeClusterMachine', 0, 1458192497692, 1, 2),
	(55, '用户管理批量新增用户按钮', '', '', 5, 'batchProcess', 0, 1458193829990, 1, 2),
	(56, 'hive元数据管理查看hive按钮', '', '', 14, 'seeHive', 0, 1458195728293, 1, 2),
	(57, 'hive元数据管理配置用户按钮', '', '', 14, 'hiveConfigureUser', 0, 1458195799384, 1, 2),
	(58, 'hive元数据管理刷新列表按钮', '', '', 14, 'pushHive', 0, 1458195960763, 1, 2),
	(59, 'hdfs元数据管理新增目录按钮', '', '', 15, 'addHdfs', 0, 1458196062150, 1, 2),
	(60, 'hdfs元数据管理刷新列表按钮', '', '', 15, 'pushHdfs', 0, 1458196094965, 1, 2),
	(61, 'hdfs元数据管理修改hdfs按钮', '', '', 15, 'editHdfs', 0, 1458196133397, 1, 2),
	(62, 'hdfs元数据管理配置用户按钮', '', '', 15, 'hdfsConfigureUser', 0, 1458196184394, 1, 2),
	(63, 'hdfs元数据管理删除hdfs按钮', '', '', 15, 'deleteHdfs', 0, 1458196465642, 1, 2),
	(64, '队列资源报表下载按钮', '', '', 34, 'downloadQueueResource', 0, 1458196874689, 1, 2),
	(65, '用户认证信息报表下载按钮', '', '', 35, 'downloadUserAuthentication', 0, 1458196954235, 1, 2),
	(66, '集群管理', '/cluster/type/cluster_list.do', '', 2, '', 0, 0, 1, 1),
	(67, '集群管理新增集群按钮', '', '', 66, 'addCluster', 0, 0, 1, 2),
	(68, '集群管理修改集群按钮', '', '', 66, 'editCluster', 0, 0, 1, 2),
	(69, '集群管理删除集群按钮', '', '', 66, 'deleteCluster', 0, 0, 1, 2),
	(70, '数据迁移', '/cluster/transfer/transfer_list.do', '', 2, '', 5, 0, 1, 1),
	(71, '数据迁移开启按钮', '', '', 70, 'tranferOpen', 0, 1461135363142, 1, 2),
	(72, '数据迁移暂停按钮', '', '', 70, 'tranferPause', 1, 1461135445092, 1, 2),
	(73, '数据迁移删除按钮', '', '', 70, 'tranferDelete', 0, 1461135472065, 1, 2),
	(75, '权限报表(HDFS地址主导)', '/report/hdfsPathAccess/hdfsPathAccess_list.do', '', 4, '', 4, 0, 1, 1),
	(76, '权限报表(HIVE表名主导)', '/report/hiveTableAccess/hiveTableAccess_list.do', '', 4, '', 5, 
/*!40000 ALTER TABLE `system_menu` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.system_role 结构
CREATE TABLE IF NOT EXISTS `system_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL COMMENT '角色名',
  `status` varchar(45) NOT NULL COMMENT '生效0 无效1',
  `createTime` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- 正在导出表  hadoopadminweb_db.system_role 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `system_role` DISABLE KEYS */;
INSERT INTO `system_role` (`id`, `name`, `status`, `createTime`) VALUES
	(1, 'admin', '0', 0);
/*!40000 ALTER TABLE `system_role` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.system_role_menu 结构
CREATE TABLE IF NOT EXISTS `system_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleId` int(11) NOT NULL,
  `menuId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=289 DEFAULT CHARSET=utf8 COMMENT='角色菜单对照表';

-- 正在导出表  hadoopadminweb_db.system_role_menu 的数据：~64 rows (大约)
/*!40000 ALTER TABLE `system_role_menu` DISABLE KEYS */;
INSERT INTO `system_role_menu` (`id`, `roleId`, `menuId`) VALUES
	(182, 1, 1),
	(183, 1, 16),
	(184, 1, 17),
	(185, 1, 18),
	(186, 1, 19),
	(187, 1, 21),
	(188, 1, 22),
	(189, 1, 25),
	(190, 1, 26),
	(191, 1, 27),
	(192, 1, 28),
	(193, 1, 23),
	(194, 1, 24),
	(195, 1, 29),
	(196, 1, 30),
	(197, 1, 31),
	(198, 1, 32),
	(199, 1, 2),
	(200, 1, 5),
	(201, 1, 45),
	(202, 1, 46),
	(203, 1, 47),
	(204, 1, 48),
	(205, 1, 49),
	(206, 1, 50),
	(207, 1, 51),
	(208, 1, 55),
	(209, 1, 6),
	(210, 1, 52),
	(211, 1, 53),
	(212, 1, 54),
	(213, 1, 7),
	(214, 1, 8),
	(215, 1, 9),
	(216, 1, 10),
	(217, 1, 11),
	(218, 1, 12),
	(219, 1, 13),
	(220, 1, 3),
	(221, 1, 14),
	(222, 1, 56),
	(223, 1, 57),
	(224, 1, 58),
	(225, 1, 15),
	(226, 1, 59),
	(227, 1, 60),
	(228, 1, 61),
	(229, 1, 62),
	(230, 1, 63),
	(231, 1, 4),
	(232, 1, 33),
	(233, 1, 34),
	(234, 1, 64),
	(235, 1, 35),
	(236, 1, 65),
	(237, 1, 66),
	(238, 1, 67),
	(239, 1, 68),
	(240, 1, 69),
	(241, 1, 70),
	(285, 1, 71),
	(286, 1, 72),
	(287, 1, 73),
	(354, 1, 75),
	(355, 1, 76);
/*!40000 ALTER TABLE `system_role_menu` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.system_user 结构
CREATE TABLE IF NOT EXISTS `system_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(45) NOT NULL,
  `passWord` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `phoneNumber` varchar(45) NOT NULL,
  `remark` varchar(45) NOT NULL,
  `createTime` bigint(20) NOT NULL,
  `changeTime` bigint(20) NOT NULL,
  `systemCompanyId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='webapp用户';

-- 正在导出表  hadoopadminweb_db.system_user 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `system_user` DISABLE KEYS */;
INSERT INTO `system_user` (`id`, `userName`, `passWord`, `email`, `phoneNumber`, `remark`, `createTime`, `changeTime`, `systemCompanyId`) VALUES
	(1, 'admin', '202cb962ac59075b964b07152d234b70', '', '', '', 0, 0, 0);
/*!40000 ALTER TABLE `system_user` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.system_user_role 结构
CREATE TABLE IF NOT EXISTS `system_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户角色对照表';

-- 正在导出表  hadoopadminweb_db.system_user_role 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `system_user_role` DISABLE KEYS */;
INSERT INTO `system_user_role` (`id`, `userId`, `roleId`) VALUES
	(1, 1, 1);
/*!40000 ALTER TABLE `system_user_role` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.system_web_log 结构
CREATE TABLE IF NOT EXISTS `system_web_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) NOT NULL,
  `operationId` int(11) NOT NULL,
  `createtime` bigint(20) NOT NULL,
  `parameter` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- 正在导出表  hadoopadminweb_db.system_web_log 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `system_web_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_web_log` ENABLE KEYS */;


-- 导出  表 hadoopadminweb_db.system_web_operations 结构
CREATE TABLE IF NOT EXISTS `system_web_operations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `operationName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- 正在导出表  hadoopadminweb_db.system_web_operations 的数据：~13 rows (大约)
/*!40000 ALTER TABLE `system_web_operations` DISABLE KEYS */;
INSERT INTO `system_web_operations` (`id`, `url`, `operationName`) VALUES
	(1, '/main/home_page.do', NULL),
	(2, '/system/company/system_company.do', NULL),
	(3, '/cluster/user/query_clusterUser_list.do', NULL),
	(4, '/cluster/user/add_update_see_clusterUser_wss.do', NULL),
	(5, '/cluster/user/save_clusterUser.do', NULL),
	(6, '/cluster/user/pause_user.do', NULL),
	(7, '/cluster/user/open_user.do', NULL),
	(8, '/meta/hdfs/hdfs_metadata_management.do', NULL),
	(9, '/meta/hdfsCompare/compareHdfs.do', NULL),
	(10, '/meta/hdfsCompare/query_dbList_pop.do', NULL),
	(11, '/meta/hdfsCompare/query_hdfsList_pop.do', NULL),
	(12, '/meta/hive/hive_metadata_management.do', NULL),
	(13, '/meta/hive/flush_hive.do', NULL);
/*!40000 ALTER TABLE `system_web_operations` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
