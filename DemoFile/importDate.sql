-- --------------------------------------------------------
-- 主机:                           localhost
-- 服务器版本:                        5.7.11 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  8.2.0.4675
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出  表 import.cluster_cluster_type 结构
DROP TABLE IF EXISTS `cluster_cluster_type`;
CREATE TABLE IF NOT EXISTS `cluster_cluster_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '集群标识',
  `clusterTypeId` varchar(45) NOT NULL,
  `clusterTypeName` varchar(45) NOT NULL COMMENT '集群类型名称',
  `note` varchar(45) NOT NULL COMMENT '集群类型备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `clusterTypeId_UNIQUE` (`clusterTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='集群类型表';

-- 导出  表 import.cluster_machine_type 结构
DROP TABLE IF EXISTS `cluster_machine_type`;
CREATE TABLE IF NOT EXISTS `cluster_machine_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `machineTypeId` varchar(45) NOT NULL COMMENT '机器类型id',
  `machineTypeName` varchar(45) NOT NULL COMMENT '机器类型名称',
  `note` varchar(45) NOT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `machineTypeId_UNIQUE` (`machineTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='集群机器类型';

-- 导出  表 import.cluster_user_hdfsquota 结构
DROP TABLE IF EXISTS `cluster_user_hdfsquota`;
CREATE TABLE IF NOT EXISTS `cluster_user_hdfsquota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '集群用户标识',
  `hdfsSpace` int(11) NOT NULL COMMENT 'hdfs用户空间大小',
  `hdfsFileCount` int(11) NOT NULL COMMENT 'hdfs文件数量',
  `hdfsSpaceUnit` varchar(1) NOT NULL COMMENT 'hdfs空间单位',
  `hdfsPath` varchar(100) NOT NULL COMMENT 'hdfs地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




-- 正在导出表  import.cluster_user_kbrconfig 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `cluster_user_kbrconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `cluster_user_kbrconfig` ENABLE KEYS */;


-- 导出  表 import.cluster_user_queue 结构
DROP TABLE IF EXISTS `cluster_user_queue`;
CREATE TABLE IF NOT EXISTS `cluster_user_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '用户id',
  `queueName` varchar(50) NOT NULL DEFAULT '' COMMENT '队列名称',
  `minResource` int(11) NOT NULL COMMENT '最小资源数',
  `maxResource` int(11) NOT NULL COMMENT '最大资源数',
  `maxApp` int(11) NOT NULL COMMENT '最大app数',
  `weight` float NOT NULL COMMENT '权重值',
  `acl` varchar(50) NOT NULL DEFAULT '' COMMENT 'acl值',
  `minCpu` int(11) NOT NULL COMMENT '最小cpu值',
  `maxCpu` int(11) NOT NULL COMMENT '最大cpu值',
  `modifyTime` bigint(20) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  import.cluster_user_queue 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `cluster_user_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `cluster_user_queue` ENABLE KEYS */;


-- 导出  表 import.cluster_user_type 结构
DROP TABLE IF EXISTS `cluster_user_type`;
CREATE TABLE IF NOT EXISTS `cluster_user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户类型标识',
  `userTypeId` varchar(45) NOT NULL,
  `userTypeName` varchar(45) NOT NULL COMMENT '用户类型名称',
  `note` varchar(45) NOT NULL COMMENT '用户类型备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userTypeId_UNIQUE` (`userTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='集群用户类型';
-- 导出  表 import.meta_hdfs_access 结构
DROP TABLE IF EXISTS `meta_hdfs_access`;
CREATE TABLE IF NOT EXISTS `meta_hdfs_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clusterUserId` int(11) NOT NULL COMMENT '集群用户',
  `hdfsInfoId` int(11) NOT NULL COMMENT 'hdfspath信息',
  `createTime` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='hdsf访问权限表';

-- 正在导出表  import.meta_hdfs_access 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `meta_hdfs_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_hdfs_access` ENABLE KEYS */;


-- 导出  表 import.meta_hdfs_info 结构
DROP TABLE IF EXISTS `meta_hdfs_info`;
CREATE TABLE IF NOT EXISTS `meta_hdfs_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hdfsPath` varchar(100) NOT NULL COMMENT 'hdfs地址',
  `clusterUserId` int(11) NOT NULL COMMENT '根据地址截取的用户',
  `hdfsGroup` varchar(45) NOT NULL COMMENT '地址所属组',
  `hdfsOwner` varchar(45) NOT NULL COMMENT '地址的权限所属人',
  `hdfsPerm` varchar(45) NOT NULL COMMENT '地址所属权限',
  `note` varchar(45) NOT NULL COMMENT '备注',
  `createTime` bigint(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='hdfs公有的元数据信息';

-- 正在导出表  import.meta_hdfs_info 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `meta_hdfs_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_hdfs_info` ENABLE KEYS */;




-- 正在导出表  import.meta_hdfs_info_bak 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `meta_hdfs_info_bak` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_hdfs_info_bak` ENABLE KEYS */;


-- 导出  表 import.meta_hive_access 结构
DROP TABLE IF EXISTS `meta_hive_access`;
CREATE TABLE IF NOT EXISTS `meta_hive_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clusterUserId` int(11) NOT NULL COMMENT '集群用户标识',
  `hiveInfoId` int(11) NOT NULL COMMENT 'hive数据标识',
  `createTime` bigint(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='集群hive数据访问权限';


-- 正在导出表  import.meta_hive_sql 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `meta_hive_sql` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_hive_sql` ENABLE KEYS */;


-- 导出  表 import.system_company 结构
DROP TABLE IF EXISTS `system_company`;
CREATE TABLE IF NOT EXISTS `system_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '公司唯一标识',
  `companyName` varchar(45) NOT NULL COMMENT '公司名称',
  `note` varchar(45) NOT NULL,
  `contactName` varchar(255) NOT NULL,
  `contactMobile` varchar(255) NOT NULL,
  `delTag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='集群公司表';

-- 正在导出表  import.system_company 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `system_company` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_company` ENABLE KEYS */;


-- 导出  表 import.system_menu 结构
DROP TABLE IF EXISTS `system_menu`;
CREATE TABLE IF NOT EXISTS `system_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL COMMENT '菜单名',
  `url` varchar(100) NOT NULL DEFAULT ' ' COMMENT '菜单路径',
  `cssClass` varchar(45) NOT NULL DEFAULT '' COMMENT '菜单样式',
  `parentId` int(11) NOT NULL COMMENT '父菜单id',
  `code` varchar(50) NOT NULL DEFAULT '',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `createTime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `isParent` int(1) NOT NULL COMMENT '0是父菜单 1不是父菜单',
  `type` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COMMENT='系统菜单';

-- 正在导出表  import.system_menu 的数据：~55 rows (大约)
/*!40000 ALTER TABLE `system_menu` DISABLE KEYS */;
INSERT INTO `system_menu` (`id`, `title`, `url`, `cssClass`, `parentId`, `code`, `sort`, `createTime`, `isParent`, `type`) VALUES
	(1, '系统管理', '', 'ace-icon fa fa-cogs   ', 0, '', 0, 0, 0, 1),
	(2, '集群管理', '', 'ace-icon fa fa-users', 0, '', 1, 0, 0, 1),
	(3, '元数据管理', '', 'ace-icon fa fa-adjust', 0, '', 2, 0, 0, 1),
	(4, '报表管理', '', 'ace-icon fa fa-bar-chart-o', 0, '', 3, 0, 0, 1),
	(5, '用户管理', '/cluster/user/query_clusterUser_list.do', ' ', 2, '', 2, 0, 1, 1),
	(6, '机器管理', '/cluster/clusterMachine/clusterMachine_list.do', ' ', 2, '', 3, 0, 1, 1),
	(7, '参数管理', '/cluster/parameter/parameter_list.do', ' ', 2, '', 4, 0, 1, 1),
	(8, '参数管理父参数新增按钮', '', '', 7, 'parameter_parent_add', 0, 0, 1, 2),
	(9, '参数管理父参数修改按钮', ' ', '', 7, 'parameter_parent_modify', 0, 0, 1, 2),
	(10, '参数管理父参数删除按钮', ' ', '', 7, 'parameter_parent_delete', 0, 0, 1, 2),
	(11, '参数管理子参数新增按钮', ' ', '', 7, 'parameter_children_add', 0, 0, 1, 2),
	(12, '参数管理子参数修改按钮', ' ', '', 7, 'parameter_children_modify', 0, 0, 1, 2),
	(13, '参数管理子参数删除按钮', ' ', '', 7, 'parameter_children_delete', 0, 0, 1, 2),
	(14, ' hive元数据管理', '/meta/hive/hive_metadata_management.do', '', 3, '', 0, 0, 1, 1),
	(15, 'hdfs元数据管理', '/meta/hdfs/hdfs_metadata_management.do', '', 3, '', 0, 0, 1, 1),
	(16, '租户管理', '/system/company/system_company.do', '', 2, '', 1, 0, 1, 1),
	(17, '租户管理新建按钮', ' ', '', 16, 'customerAdd', 0, 0, 1, 2),
	(18, '租户管理修改按钮', ' ', '', 16, 'customer_modify', 0, 0, 1, 2),
	(19, '租户管理删除按钮', ' ', '', 16, 'customer_delete', 0, 0, 1, 2),
	(21, '菜单管理', '/system/menu/menu_manage.do', '', 1, '', 0, 0, 1, 1),
	(22, '角色管理', '/system/role/role_manage.do', '', 1, '', 0, 0, 1, 1),
	(23, '账户管理', '/system/user/user_manage.do', '', 1, '', 0, 0, 1, 1),
	(24, '新增用户', '', '', 23, 'user_add', 0, 1457925235087, 1, 2),
	(25, '新增角色', '', '', 22, 'role_add', 0, 1457925337772, 1, 2),
	(26, '修改角色', '', '', 22, 'role_modify', 0, 1457925482748, 1, 2),
	(27, '删除角色', '', '', 22, 'role_delete', 0, 1457925573489, 1, 2),
	(28, '授权角色', '', '', 22, 'role_authority', 0, 1457925638923, 1, 2),
	(29, '修改用户', '', '', 23, 'user_modify', 0, 1457925911431, 1, 2),
	(30, '重置用户密码', '', '', 23, 'user_resetPassword', 0, 1457926031603, 1, 2),
	(31, '删除用户', '', '', 23, 'user_delete', 0, 1457943996299, 1, 2),
	(32, '日志管理', '/system/log/view_daily.do', '', 1, '', 0, 1458018880039, 1, 1),
	(33, '用户报表', '/report/userFormReport/user_report_form.do', '', 4, '', 0, 0, 1, 1),
	(34, '队列资源报表', '/report/queue/queue_list.do', '', 4, '', 0, 0, 1, 1),
	(35, '用户认证信息报表', '/report/userApprove/userApprove_list.do', '', 4, '', 0, 0, 1, 1),
	(45, '用户管理新增用户按钮', '', '', 5, 'addClusterUser', 0, 1458191937156, 1, 2),
	(46, '用户管理修改用户按钮', '', '', 5, 'editClusterUser', 0, 1458192008355, 1, 2),
	(47, '用户管理查看用户按钮', '', '', 5, 'seeClusterUser', 0, 1458192064354, 1, 2),
	(48, '用户管理删除用户按钮', '', '', 5, 'deleteClusterUser', 0, 1458192136659, 1, 2),
	(49, '用户管理重置用户密码', '', '', 5, 'resetClusterUserPassword', 0, 1458192247599, 1, 2),
	(50, '用户管理用户启用按钮', '', '', 5, 'openClusterUser', 0, 1458192339491, 1, 2),
	(51, '用户管理用户暂停按钮', '', '', 5, 'closeClusterUser', 0, 1458192378089, 1, 2),
	(52, '机器管理增加机器按钮', '', '', 6, 'addClusterMachine', 0, 1458192434587, 1, 2),
	(53, '机器管理删除机器按钮', '', '', 6, 'deleteClusterMachine', 0, 1458192463794, 1, 2),
	(54, '机器管理修改机器按钮', '', '', 6, 'editeClusterMachine', 0, 1458192497692, 1, 2),
	(55, '用户管理批量新增用户按钮', '', '', 5, 'batchProcess', 0, 1458193829990, 1, 2),
	(56, 'hive元数据管理查看hive按钮', '', '', 14, 'seeHive', 0, 1458195728293, 1, 2),
	(57, 'hive元数据管理配置用户按钮', '', '', 14, 'hiveConfigureUser', 0, 1458195799384, 1, 2),
	(58, 'hive元数据管理刷新列表按钮', '', '', 14, 'pushHive', 0, 1458195960763, 1, 2),
	(59, 'hdfs元数据管理新增目录按钮', '', '', 15, 'addHdfs', 0, 1458196062150, 1, 2),
	(60, 'hdfs元数据管理刷新列表按钮', '', '', 15, 'pushHdfs', 0, 1458196094965, 1, 2),
	(61, 'hdfs元数据管理修改hdfs按钮', '', '', 15, 'editHdfs', 0, 1458196133397, 1, 2),
	(62, 'hdfs元数据管理配置用户按钮', '', '', 15, 'hdfsConfigureUser', 0, 1458196184394, 1, 2),
	(63, 'hdfs元数据管理删除hdfs按钮', '', '', 15, 'deleteHdfs', 0, 1458196465642, 1, 2),
	(64, '队列资源报表下载按钮', '', '', 34, 'downloadQueueResource', 0, 1458196874689, 1, 2),
	(65, '用户认证信息报表下载按钮', '', '', 35, 'downloadUserAuthentication', 0, 1458196954235, 1, 2),
	(66, '集群管理', '/cluster/type/cluster_list.do', '', 2, '', 0, 0, 1, 1),
	(67, '集群管理新增集群按钮', '', '', 66, 'addCluster', 0, 0, 1, 2),
	(68, '集群管理修改集群按钮', '', '', 66, 'editCluster', 0, 0, 1, 2),
	(69, '集群管理删除集群按钮', '', '', 66, 'deleteCluster', 0, 0, 1, 2),
	(70, '数据迁移', '/cluster/transfer/transfer_list.do', '', 2, '', 5, 0, 1, 1),
	(71, '数据迁移开启按钮', '', '', 70, 'tranferOpen', 0, 1461135363142, 1, 2),
	(72, '数据迁移暂停按钮', '', '', 70, 'tranferPause', 1, 1461135445092, 1, 2),
	(73, '数据迁移删除按钮', '', '', 70, 'tranferDelete', 0, 1461135472065, 1, 2),
	(75, '权限报表(HDFS地址主导)', '/report/hdfsPathAccess/hdfsPathAccess_list.do', '', 4, '', 4, 0, 1, 1),
	(76, '权限报表(HIVE表名主导)', '/report/hiveTableAccess/hiveTableAccess_list.do', '', 4, '', 5, 1462861295244, 1, 1);

/*!40000 ALTER TABLE `system_menu` ENABLE KEYS */;


-- 导出  表 import.system_role 结构
DROP TABLE IF EXISTS `system_role`;
CREATE TABLE IF NOT EXISTS `system_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL COMMENT '角色名',
  `status` varchar(45) NOT NULL COMMENT '生效0 无效1',
  `createTime` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- 正在导出表  import.system_role 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `system_role` DISABLE KEYS */;
INSERT INTO `system_role` (`id`, `name`, `status`, `createTime`) VALUES
	(1, 'admin', '0', 0);
/*!40000 ALTER TABLE `system_role` ENABLE KEYS */;


-- 导出  表 import.system_role_menu 结构
DROP TABLE IF EXISTS `system_role_menu`;
CREATE TABLE IF NOT EXISTS `system_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleId` int(11) NOT NULL,
  `menuId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=237 DEFAULT CHARSET=utf8 COMMENT='角色菜单对照表';

-- 正在导出表  import.system_role_menu 的数据：~55 rows (大约)
/*!40000 ALTER TABLE `system_role_menu` DISABLE KEYS */;
INSERT INTO `system_role_menu` (`id`, `roleId`, `menuId`) VALUES
	(182, 1, 1),
	(183, 1, 16),
	(184, 1, 17),
	(185, 1, 18),
	(186, 1, 19),
	(187, 1, 21),
	(188, 1, 22),
	(189, 1, 25),
	(190, 1, 26),
	(191, 1, 27),
	(192, 1, 28),
	(193, 1, 23),
	(194, 1, 24),
	(195, 1, 29),
	(196, 1, 30),
	(197, 1, 31),
	(198, 1, 32),
	(199, 1, 2),
	(200, 1, 5),
	(201, 1, 45),
	(202, 1, 46),
	(203, 1, 47),
	(204, 1, 48),
	(205, 1, 49),
	(206, 1, 50),
	(207, 1, 51),
	(208, 1, 55),
	(209, 1, 6),
	(210, 1, 52),
	(211, 1, 53),
	(212, 1, 54),
	(213, 1, 7),
	(214, 1, 8),
	(215, 1, 9),
	(216, 1, 10),
	(217, 1, 11),
	(218, 1, 12),
	(219, 1, 13),
	(220, 1, 3),
	(221, 1, 14),
	(222, 1, 56),
	(223, 1, 57),
	(224, 1, 58),
	(225, 1, 15),
	(226, 1, 59),
	(227, 1, 60),
	(228, 1, 61),
	(229, 1, 62),
	(230, 1, 63),
	(231, 1, 4),
	(232, 1, 33),
	(233, 1, 34),
	(234, 1, 64),
	(235, 1, 35),
	(236, 1, 65),
	(237, 1, 66),
	(238, 1, 67),
	(239, 1, 68),
	(240, 1, 69),
	(241, 1, 70),
	(285, 1, 71),
	(286, 1, 72),
	(287, 1, 73),
	(354, 1, 75),
	(355, 1, 76);
/*!40000 ALTER TABLE `system_role_menu` ENABLE KEYS */;


-- 导出  表 import.system_user 结构
DROP TABLE IF EXISTS `system_user`;
CREATE TABLE IF NOT EXISTS `system_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(45) NOT NULL,
  `passWord` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `phoneNumber` varchar(45) NOT NULL,
  `remark` varchar(45) NOT NULL,
  `createTime` bigint(20) NOT NULL,
  `changeTime` bigint(20) NOT NULL,
  `systemCompanyId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='webapp用户';

-- 正在导出表  import.system_user 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `system_user` DISABLE KEYS */;
INSERT INTO `system_user` (`id`, `userName`, `passWord`, `email`, `phoneNumber`, `remark`, `createTime`, `changeTime`, `systemCompanyId`) VALUES
	(1, 'admin', '202cb962ac59075b964b07152d234b70', '', '', '', 0, 0, 0);
/*!40000 ALTER TABLE `system_user` ENABLE KEYS */;


-- 导出  表 import.system_user_role 结构
DROP TABLE IF EXISTS `system_user_role`;
CREATE TABLE IF NOT EXISTS `system_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户角色对照表';

-- 正在导出表  import.system_user_role 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `system_user_role` DISABLE KEYS */;
INSERT INTO `system_user_role` (`id`, `userId`, `roleId`) VALUES
	(1, 1, 1);
/*!40000 ALTER TABLE `system_user_role` ENABLE KEYS */;


-- 导出  表 import.system_web_log 结构
DROP TABLE IF EXISTS `system_web_log`;
CREATE TABLE IF NOT EXISTS `system_web_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) NOT NULL,
  `operationId` int(11) NOT NULL,
  `createtime` bigint(20) NOT NULL,
  `parameter` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  import.system_web_log 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `system_web_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_web_log` ENABLE KEYS */;


-- 导出  表 import.system_web_operations 结构
DROP TABLE IF EXISTS `system_web_operations`;
CREATE TABLE IF NOT EXISTS `system_web_operations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `operationName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `cluster_data_transfer`;
CREATE TABLE `cluster_data_transfer` (
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `sourceClusterTypeId` INT(11) NOT NULL COMMENT '源集群',
        `targetClusterTypeId` INT(11) NOT NULL COMMENT '目标集群',
        `clusterUserId` VARCHAR(50) NOT NULL COMMENT '用户',
        `hdfsInfoId` VARCHAR(50) NOT NULL COMMENT '源hdfs',
        `hdfsPath` VARCHAR(50) NOT NULL COMMENT 'hdfs路径',
        `startTime` BIGINT(20) NOT NULL COMMENT '开始时间',
        `endTime` BIGINT(20) NOT NULL COMMENT '结束时间',
        `runTime` BIGINT(20) NOT NULL COMMENT '运行时间',
        `runCycle` INT(11) NOT NULL COMMENT '运行周期(月)',
        `status` INT(11) NOT NULL COMMENT '0启动 1暂停',
        PRIMARY KEY (`id`)
    );
-- 正在导出表  import.system_web_operations 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `system_web_operations` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_web_operations` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
