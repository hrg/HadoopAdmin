package com.ideal.boots;

import com.ideal.service.hdfs.HDFSService;
import com.ideal.service.hive.HiveService;
import com.ideal.service.krb.KerberosService;
import com.ideal.tools.db.ChangeDataBase;
import com.ideal.tools.ssh.context.ClusterContext;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by CC on 2016/8/4.
 */
public class Boots {

    public static void main(String[] args){

        if(args==null||args.length<1){
            System.out.println("please input below number:");
            System.out.println("    1.hdfs refesh");
            System.out.println("    2.hive refesh");
            System.out.println("    3.kerberos refesh");
            return ;
        }
        String flag =  args[0];


        if(flag.equals("1")){
            HDFSService hdfs = new HDFSService();
            hdfs.refreshHDFS(null);
        }else if(flag.equals("2")){
            HiveService hiveService = new HiveService();
            hiveService.refreshHive(null);
        }else if(flag.equals("3")){
            KerberosService kerberosService = new KerberosService();
            String username ="";
            if(args.length==2)
                username = args[1];
            Map<String,Object> params = new HashMap<String, Object>();
            params.put("Cluster_User_Name",username);
            ClusterContext context=new ClusterContext(params);
            kerberosService.KerberosFresh_new(context);
        }

//        ChangeDataBase temp = new ChangeDataBase();
//        temp.changeDataBase("hadoopadmin_shaxiang");
    }
}
