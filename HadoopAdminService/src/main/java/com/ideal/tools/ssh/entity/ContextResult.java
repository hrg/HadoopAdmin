package com.ideal.tools.ssh.entity;

import com.ideal.tools.ssh.result.LinuxResult;

import java.util.*;

/**
 * Created by CC on 2016/4/14.
 */
public class ContextResult {

    Map<String,List<LinuxResult>> resultMap = new HashMap<String, List<LinuxResult>>();


    public void setConextResult(String curStep,List<LinuxResult> results){
        if(resultMap==null){
            resultMap = new HashMap<String, List<LinuxResult>>();
        }
        if (resultMap.get(curStep)==null){
            List<LinuxResult> linuxResults = new ArrayList<LinuxResult>();
            linuxResults.addAll(results);
            resultMap.put(curStep,linuxResults);
        }else{
            resultMap.get(curStep).addAll(results);
        }
    }

    public Map<String,List<LinuxResult>>  getAllResult(){
        return resultMap;
    }


    public boolean isSuccess(String step){
        List<LinuxResult> results = resultMap.get(step);
        if(results==null)return false;
        for(LinuxResult result:results){
            if(result.getExitCode()==LinuxResult.DEFAULT_FAILD_CODE)
                return false;
        }
        return true;
    }

    public List<LinuxResult> getResultByName(String name){
        return resultMap.get(name);
    }

    public List<LinuxResult> getLastResult(){
        List<List<LinuxResult>> list = new ArrayList<List<LinuxResult>>();
        Set<Map.Entry<String, List<LinuxResult>>> set = resultMap.entrySet();
        Iterator<Map.Entry<String, List<LinuxResult>>> it = set.iterator();
        while(it.hasNext()){
            Map.Entry<String, List<LinuxResult>> entry = it.next();
            list.add(entry.getValue());
        }
      return list.get(0);
    }

}
