package com.ideal.tools.ssh.entity;

/**
 * Created by CC on 2016/3/1.
 */
public class SSHAuthor {

    String host;
    String username;
    String passwd;
    String pubToken;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getPubToken() {
        return pubToken;
    }

    public void setPubToken(String pubToken) {
        this.pubToken = pubToken;
    }
}
