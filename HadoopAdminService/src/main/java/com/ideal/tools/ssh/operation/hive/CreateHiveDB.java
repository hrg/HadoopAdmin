package com.ideal.tools.ssh.operation.hive;

import com.ideal.tools.ssh.common.CommonProperties;
import com.ideal.tools.ssh.common.CommonTools;
import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxCommand;
import com.ideal.tools.ssh.operation.LinuxOperation;
import com.ideal.tools.ssh.result.LinuxResult;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by CC on 2016/3/7.
 */
public class CreateHiveDB extends LinuxOperation {

    String superUser;
    String jdbc;
    String principal;
    String db;
    String db_role;
    String userName;
    String hdfsPath;
    String is_Use_BeeLine;
    String hiveUsername;


    public CreateHiveDB(String db,String userName){
        this.db=db;
        this.userName=userName;
    }

    @Override
    public void execLinuxCMD(ClusterContext context) {
        this.superUser = context.getCommonProperties().getProperty(CommonProperties.HIVE_SUPER_USER,"hive");
        this.jdbc = context.getCommonProperties().getProperty(CommonProperties.HIVE_SENTRY_JDBC,"");
        this.principal = context.getCommonProperties().getProperty(CommonProperties.HIVE_SENTRY_PRINCIPAL,"");
        this.db_role  = CommonTools.getHiveRole("db","all",this.userName,context.getCommonProperties());
        this.hdfsPath = CommonTools.getHiveSentryPrincipalPath(this.userName,context.getCommonProperties());
        this.is_Use_BeeLine = context.getCommonProperties().getProperty(CommonProperties.Is_Use_BeeLine,"");
        this.hiveUsername = context.getCommonProperties().getProperty(CommonProperties.HIVE_USERNAME,"");
        StringBuffer cmd = new StringBuffer();
        if(is_Use_BeeLine!=null&&is_Use_BeeLine.equals("0")){
            cmd.append("sudo -u " + superUser + " hive -e ");
            cmd.append("\"");
            cmd.append("CREATE DATABASE IF NOT EXISTS " + db + ";");
            cmd.append("GRANT all ON DATABASE " + db + " TO GROUP " + userName+",GROUP "+hiveUsername);
            //结束
            cmd.append("\"");
        }else {
            //具体去运行的命令  这个地方 可能不灵活 如果需要修改命令 会需要修改代码
            //这里后面可以移动到从数据库中载入数据
            cmd.append("sudo -u " + superUser + " beeline -u ");
            cmd.append("\"" + jdbc + ";" + principal + "\" -e ");
            cmd.append("\"");
            cmd.append("create database IF NOT EXISTS " + db + ";");
            cmd.append("use " + db + ";");
            cmd.append("create role " + db_role + ";");
            //角色赋权
            cmd.append("grant all on database " + db + " to role " + db_role + ";");
            //给组 赋权
            cmd.append("grant role " + db_role + " to group " + userName + ";");
            //赋权目录地址
            cmd.append("grant ALL ON URI \\\"" + hdfsPath + "\\\" TO ROLE " + db_role + "; ");
            //结束
            cmd.append("\"");
        }
        //创建命令 创建命令的时候 需要根据不同的OperationType 初始化不同的参数
        setLinuxCommand(new LinuxCommand().initCMD(cmd.toString()));
        /**这个分割线是为了说明下面的是基本不用变的，如果要实现新的功能，只需要写上面**/

        //执行
        super.execOperation(context);
    }

    @Override
    public void setResultNote(LinuxResult linuxResult) {
//        String info = getResultInfo();
        int exitcode = linuxResult.getExitCode();
        String exitCodeMean="";
        String host=getHost();
        if(exitcode == 0){
            exitCodeMean = "SUCCESS MAKE HIVE DB! ["+db+"] ["+host+"]\n";
        }else {
            exitCodeMean = "FAILD MAKE HIVE DB! ["+db+"] ["+host+"]\n";
        }
        //设置note
        linuxResult.setNote(exitCodeMean+(StringUtils.isBlank(linuxResult.getNote())?"":linuxResult.getNote()));
    }

}
