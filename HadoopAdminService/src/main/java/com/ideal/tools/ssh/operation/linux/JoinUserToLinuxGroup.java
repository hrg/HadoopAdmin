package com.ideal.tools.ssh.operation.linux;

import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxCommand;
import com.ideal.tools.ssh.operation.LinuxOperation;
import com.ideal.tools.ssh.result.LinuxResult;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by CC on 2016/3/4.
 */
public class JoinUserToLinuxGroup extends LinuxOperation{

    String group;
    String userName;

    public JoinUserToLinuxGroup(String userName,String group){
        this.userName=userName;
        this.group=group;
    }


    @Override
    public void execLinuxCMD(ClusterContext context) {
        String cmd="sudo usermod -a -G "+group+" "+userName;

        setLinuxCommand(new LinuxCommand().initCMD(cmd));

        //执行
        super.execOperation(context);
    }

    @Override
    public void setResultNote(LinuxResult linuxResult) {
//        String info = getResultInfo();
        int exitcode = linuxResult.getExitCode();
        String exitCodeMean="";
        String host=getHost();
        if(exitcode == 0){
            exitCodeMean = "SUCCESS JOIN USER ["+userName+"] TO GROUP ["+group+"]! ["+host+"]\n";
        }else {
            exitCodeMean = "FAILD JOIN USER ["+userName+"] TO GROUP ["+group+"]! ["+host+"]\n";
        }
        //设置note
        linuxResult.setNote(exitCodeMean+(StringUtils.isBlank(linuxResult.getNote())?"":linuxResult.getNote()));
    }
}
