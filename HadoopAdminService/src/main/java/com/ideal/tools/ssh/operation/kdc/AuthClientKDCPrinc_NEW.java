package com.ideal.tools.ssh.operation.kdc;

import com.ideal.tools.ssh.common.CommonProperties;
import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxCommand;
import com.ideal.tools.ssh.operation.LinuxOperation;
import com.ideal.tools.ssh.result.LinuxResult;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by CC on 2016/3/4.
 */
public class AuthClientKDCPrinc_NEW extends LinuxOperation{

    String username;
    String passWord;

    String kdcPrinc;

    public AuthClientKDCPrinc_NEW(String username, String passWord){
        this.username=username;
        this.passWord=passWord;
    }

    @Override
    public void execLinuxCMD(ClusterContext context) {
        String realm = context.getCommonProperties().getProperty(CommonProperties.KDC_PRINC_REALM,"@EXAMPLE.COM");
        this.kdcPrinc=username+realm;
        String cmd="echo '"+passWord+"'|sudo -u "+username+" kinit "+this.kdcPrinc;

        setLinuxCommand(new LinuxCommand().initCMD(cmd));
        //执行
        super.execOperation(context);
    }

    @Override
    public void setResultNote(LinuxResult linuxResult) {
//        String info = getResultInfo();
        int exitcode = linuxResult.getExitCode();
        String exitCodeMean="";
        String host=getHost();
        if(exitcode == 0){
            exitCodeMean = "SUCCESS kinit client["+this.kdcPrinc+"]! ["+host+"]\n";
        }else {
            exitCodeMean = "FAILD kinit client["+this.kdcPrinc+"]! ["+host+"]\n";
        }
        //设置note
        linuxResult.setNote(exitCodeMean+(StringUtils.isBlank(linuxResult.getNote())?"":linuxResult.getNote()));
    }

}
