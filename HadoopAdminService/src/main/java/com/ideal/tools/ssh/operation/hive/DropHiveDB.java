package com.ideal.tools.ssh.operation.hive;

import com.ideal.tools.ssh.common.CommonProperties;
import com.ideal.tools.ssh.common.CommonTools;
import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxCommand;
import com.ideal.tools.ssh.operation.LinuxOperation;
import com.ideal.tools.ssh.result.LinuxResult;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by CC on 2016/3/7.
 */
public class DropHiveDB extends LinuxOperation {

    String superUser;
    String jdbc;
    String principal;
    String db;
    String db_role;
    List<String> tbls; //库中存在的表
    String is_Use_BeeLine;
    String hiveUsername;

    public DropHiveDB(String db, List<String> tbls){
        this.db=db;
        this.tbls = tbls;
    }

    @Override
    public void execLinuxCMD(ClusterContext context) {
        this.superUser = context.getCommonProperties().getProperty(CommonProperties.HIVE_SUPER_USER,"hive");
        this.jdbc = context.getCommonProperties().getProperty(CommonProperties.HIVE_SENTRY_JDBC,"");
        this.principal = context.getCommonProperties().getProperty(CommonProperties.HIVE_SENTRY_PRINCIPAL,"");
        this.db_role  = CommonTools.getHiveRole("db", "all", this.db, context.getCommonProperties());
        this.is_Use_BeeLine = context.getCommonProperties().getProperty(CommonProperties.Is_Use_BeeLine,"");
        this.hiveUsername = context.getCommonProperties().getProperty(CommonProperties.HIVE_USERNAME,"");
        StringBuffer cmd = new StringBuffer();

        if(is_Use_BeeLine!=null&&is_Use_BeeLine.equals("0")){
            cmd.append("sudo -u " + superUser + " hive -e ");
            cmd.append("\"");
            //先删除hive 库中存在的表
            if (tbls != null) {
                for (String tbl : tbls) {
                    if (!StringUtils.isBlank(tbl))
                        cmd.append("drop table if exists ").append(tbl).append(";");
                }
            }
            cmd.append("drop database if exists " + db + ";");
            cmd.append("\"");
        }else {
            //具体去运行的命令  这个地方 可能不灵活 如果需要修改命令 会需要修改代码
            //这里后面可以移动到从数据库中载入数据
            cmd.append("sudo -u " + superUser + " beeline -u ");
            cmd.append("\"" + jdbc + ";" + principal + "\" -e ");
            cmd.append("\"");
            cmd.append("use " + db + ";");
            //先删除hive 库中存在的表
            if (tbls != null) {
                for (String tbl : tbls) {
                    if (!StringUtils.isBlank(tbl))
                        cmd.append("drop table if exists ").append(tbl).append(";");
                }
            }
            cmd.append("drop database if exists " + db + ";");
            //角色赋权
            cmd.append("drop role " + db_role + ";");
            cmd.append("\"");
        }

        //创建命令 创建命令的时候 需要根据不同的OperationType 初始化不同的参数
        setLinuxCommand(new LinuxCommand().initCMD(cmd.toString()));

        /**这个分割线是为了说明下面的是基本不用变的，如果要实现新的功能，只需要写上面**/

        //执行
        super.execOperation(context);
    }

    @Override
    public void setResultNote(LinuxResult linuxResult) {
//        String info = getResultInfo();
        int exitcode = linuxResult.getExitCode();
        String exitCodeMean="";
        String host=getHost();
        String tables = tbls==null?"":tbls.toString();
        if(exitcode == 0){
            exitCodeMean = "SUCCESS DROP HIVE DB! ["+db+"] "+tables+" ["+host+"]\n";
        } else if(exitcode == 1){
            exitCodeMean = "Database does not exist! ["+db+"] "+tables+" ["+host+"]\n" ;
            linuxResult.setSuccess(true);
        }else {
            exitCodeMean = "FAILD DROP HIVE DB! ["+db+"] "+tables+" ["+host+"]\n";
        }
        //设置note
        linuxResult.setNote(exitCodeMean+(StringUtils.isBlank(linuxResult.getNote())?"":linuxResult.getNote()));
    }
}
