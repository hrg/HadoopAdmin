package com.ideal.tools.ssh.operation.linux;

import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxCommand;
import com.ideal.tools.ssh.operation.LinuxOperation;
import com.ideal.tools.ssh.result.LinuxResult;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by CC on 2016/3/4.
 */
public class ChangeUserPassWord extends LinuxOperation {
    String userName;
    String passWord;

    public ChangeUserPassWord(String userName,String passWord){
        this.userName=userName;
        this.passWord=passWord;
    }


    @Override
    public void execLinuxCMD(ClusterContext context) {
        String cmd="echo '"+passWord+"' | sudo passwd --stdin  "+userName;

        //设置linux命令
        setLinuxCommand(new LinuxCommand().initCMD(cmd));

        //执行
        super.execOperation(context);
    }


    @Override
    public void setResultNote(LinuxResult linuxResult) {
//        String info = getResultInfo();
        int exitcode = linuxResult.getExitCode();
        String exitCodeMean="";
        String host=getHost();
        if(exitcode == 0){
            exitCodeMean = "SUCCESS CHANGE USER PASSWORD! ["+userName+"] ["+host+"]\n";
        }else {
            exitCodeMean = "FAILD CHANGE USER PASSWORD! ["+userName+"] ["+host+"]\n";
        }
        //设置note
        linuxResult.setNote(exitCodeMean+(StringUtils.isBlank(linuxResult.getNote())?"":linuxResult.getNote()));
    }


}
