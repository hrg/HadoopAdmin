package com.ideal.tools.ssh.operation.linux;

import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxCommand;
import com.ideal.tools.ssh.operation.LinuxOperation;
import com.ideal.tools.ssh.result.LinuxResult;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by CC on 2016/3/4.
 */
public class ExeOneShellCMD extends LinuxOperation{

    String cmd;

    public ExeOneShellCMD(String cmd){
        this.cmd=cmd;
    }

    @Override
    public void execLinuxCMD(ClusterContext context) {

        setLinuxCommand(new LinuxCommand().initCMD(cmd));

        //执行
        super.execOperation(context);
    }

    @Override
    public void setResultNote(LinuxResult linuxResult) {
//        String info = getResultInfo();
        int exitcode = linuxResult.getExitCode();
        String exitCodeMean="";
        String host=getHost();
        if(exitcode == 0){
            exitCodeMean = "SUCCESS EXE ONE SHELL CMD["+cmd+"]! ["+host+"]\n";
        }else {
            exitCodeMean = "FAILD EXE ONE SHELL CMD["+cmd+"]! ["+host+"]\n";
        }
        //设置note
        linuxResult.setNote(exitCodeMean+(StringUtils.isBlank(linuxResult.getNote())?"":linuxResult.getNote()));
    }
}
