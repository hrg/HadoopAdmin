package com.ideal.tools.ssh.executor;

import com.ideal.tools.ssh.entity.ExeCommand;
import com.ideal.tools.ssh.result.ExecutorResult;

/**
 * Created by CC on 2016/2/29.
 */
public interface Executor {

    ExecutorResult exec(ExeCommand command);


}
