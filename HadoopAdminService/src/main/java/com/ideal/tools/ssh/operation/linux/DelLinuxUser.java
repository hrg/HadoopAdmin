package com.ideal.tools.ssh.operation.linux;

import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxCommand;
import com.ideal.tools.ssh.operation.LinuxOperation;
import com.ideal.tools.ssh.result.LinuxResult;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by CC on 2016/3/2.
 */
public class DelLinuxUser extends LinuxOperation {

    String userName;

    public DelLinuxUser(String userName){
        this.userName=userName;
    }

    @Override
    public void execLinuxCMD(ClusterContext context) {
        //具体去运行的命令  这个地方 可能不灵活 如果需要修改命令 会需要修改代码
        //这里后面可以移动到从数据库中载入数据
        String cmd="sudo userdel -r -f "+userName;

        //创建命令 创建命令的时候 需要根据不同的OperationType 初始化不同的参数
        setLinuxCommand(new LinuxCommand().initCMD(cmd));

        /**这个分割线是为了说明下面的是基本不用变的，如果要实现新的功能，只需要写上面**/

        //执行
        super.execOperation(context);
    }

    @Override
    public void setResultNote(LinuxResult linuxResult) {
//        String info = getResultInfo();
        int exitcode = linuxResult.getExitCode();
        String exitCodeMean="";
        String host=getHost();
        if(exitcode == 0){
            exitCodeMean = "SUCCESS DEL USER! ["+userName+"] ["+host+"]\n";
            linuxResult.setSuccess(true);
        }else if(exitcode==6){
            exitCodeMean = "USER ["+userName+"] DOES NOT EXIT! ["+host+"]\n";
            linuxResult.setSuccess(true);
        }else{
            exitCodeMean = "FAILD DEL USER! ["+userName+"] ["+host+"]\n";
            linuxResult.setSuccess(false);
        }
        //设置note
        linuxResult.setNote(exitCodeMean+(StringUtils.isBlank(linuxResult.getNote())?"":linuxResult.getNote()));

    }
}
