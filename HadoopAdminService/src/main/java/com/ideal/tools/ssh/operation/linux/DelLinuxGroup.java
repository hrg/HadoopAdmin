package com.ideal.tools.ssh.operation.linux;

import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxCommand;
import com.ideal.tools.ssh.operation.LinuxOperation;
import com.ideal.tools.ssh.result.LinuxResult;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by CC on 2016/3/4.
 */
public class DelLinuxGroup extends LinuxOperation{

    String group;

    public DelLinuxGroup(String group){
        this.group=group;
    }

    @Override
    public void execLinuxCMD(ClusterContext context) {
        String cmd="sudo groupdel "+group;

        setLinuxCommand(new LinuxCommand().initCMD(cmd));

        //执行
        super.execOperation(context);
    }

    @Override
    public void setResultNote(LinuxResult linuxResult) {
//        String info = getResultInfo();
        int exitcode = linuxResult.getExitCode();
        String exitCodeMean="";
        String host=getHost();
        if(exitcode == 0){
            exitCodeMean = "SUCCESS DEL GROUP["+group+"]! ["+host+"]\n";
        }else if(exitcode==6){
            exitCodeMean = "Group ["+group+"] DOES NOT EXIT! ["+host+"]\n";
            linuxResult.setSuccess(true);
        }else {
            exitCodeMean = "FAILD DEL GROUP["+group+"]! ["+host+"]\n";
        }
        //设置note
        linuxResult.setNote(exitCodeMean+(StringUtils.isBlank(linuxResult.getNote())?"":linuxResult.getNote()));
    }

}
