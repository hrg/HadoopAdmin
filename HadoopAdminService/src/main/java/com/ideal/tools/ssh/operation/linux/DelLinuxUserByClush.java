package com.ideal.tools.ssh.operation.linux;

import com.ideal.tools.ssh.common.CommonProperties;
import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxCommand;
import com.ideal.tools.ssh.operation.LinuxOperation;
import com.ideal.tools.ssh.result.LinuxResult;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by CC on 2016/3/7.
 */
public class DelLinuxUserByClush extends LinuxOperation {

    String userName;
    String group_range;

    public DelLinuxUserByClush(String userName, String group_range){
        this.userName=userName;
        this.group_range=group_range;
    }

    public DelLinuxUserByClush(String userName){
        this.userName=userName;
    }


    @Override
    public void execLinuxCMD(ClusterContext context) {
        this.group_range= context.getCommonProperties()
                .getProperty(CommonProperties.HDFS_CLUSH_DN_RANGE,"cdhdatanode");
        //具体去运行的命令  这个地方 可能不灵活 如果需要修改命令 会需要修改代码
        //这里后面可以移动到从数据库中载入数据
        String cmd="sudo clush -g "+group_range+" \"userdel -r -f "+userName+"\"";

        //创建命令 创建命令的时候 需要根据不同的OperationType 初始化不同的参数
        setLinuxCommand(new LinuxCommand().initCMD(cmd));

        /**这个分割线是为了说明下面的是基本不用变的，如果要实现新的功能，只需要写上面**/

        //执行
        super.execOperation(context);
    }

    @Override
    public void setResultNote(LinuxResult linuxResult) {
//        String info = getResultInfo();
        int exitcode = linuxResult.getExitCode();
        String exitCodeMean="";
        String host=getHost();
        if(exitcode == 0){
            exitCodeMean = "SUCCESS Del USER! ["+userName+"] ["+host+"]\n";
        }else {
            exitCodeMean = "FAILD Del USER! ["+userName+"] ["+host+"]\n";
        }
        //设置note
        linuxResult.setNote(exitCodeMean+(StringUtils.isBlank(linuxResult.getNote())?"":linuxResult.getNote()));
    }
}
