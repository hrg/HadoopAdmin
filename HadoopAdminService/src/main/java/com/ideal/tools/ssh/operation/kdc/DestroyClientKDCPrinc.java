package com.ideal.tools.ssh.operation.kdc;

import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxCommand;
import com.ideal.tools.ssh.operation.LinuxOperation;
import com.ideal.tools.ssh.result.LinuxResult;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by CC on 2016/3/4.
 */
public class DestroyClientKDCPrinc extends LinuxOperation{

    String username;

    public DestroyClientKDCPrinc(String username){
        this.username=username;
    }

    @Override
    public void execLinuxCMD(ClusterContext context) {
        String cmd="sudo -u "+username+" kdestroy";

        setLinuxCommand(new LinuxCommand().initCMD(cmd));
        //执行
        super.execOperation(context);
    }

    @Override
    public void setResultNote(LinuxResult linuxResult) {
//        String info = getResultInfo();
        int exitcode = linuxResult.getExitCode();
        String exitCodeMean="";
        String host=getHost();
        if(exitcode == 0){
            exitCodeMean = "SUCCESS destroy client["+username+"]! ["+host+"]\n";
        }else {
            exitCodeMean = "FAILD destroy client["+username+"]! ["+host+"]\n";
        }
        //设置note
        linuxResult.setNote(exitCodeMean+(StringUtils.isBlank(linuxResult.getNote())?"":linuxResult.getNote()));
    }
}
