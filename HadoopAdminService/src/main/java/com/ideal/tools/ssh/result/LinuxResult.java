package com.ideal.tools.ssh.result;

import com.ideal.tools.ssh.executor.Executor;
import com.ideal.tools.ssh.executor.SSHExecutor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by CC on 2016/2/29.
 */
public class LinuxResult implements ExecutorResult {

    public static final int DEFAULT_SUCCESS_CODE = 0;
    public static final int DEFAULT_FAILD_CODE = -1;


    private String errOut;

    private String stdOut;

//    private String ip;
    private Map<String,Object> objectMap;

    private String exception;

    private int exitCode = 0;

    private String note;//这个字段自己书写，方便前台提示

    private Executor sshExecutor; //执行器

    private String cmd;

    private Boolean isSuccess;//这个值是为了 不仅仅只是因为exitcode来判断成功与否

    public LinuxResult(Executor executor) {
        this.sshExecutor = executor;
    }

    public LinuxResult(String std, String err, int exitCode) {
        this.errOut = err;
        this.stdOut = std;
        this.exitCode = exitCode;
    }

    public String getErrOut() {
        return errOut;
    }

    public void setErrOut(String errOut) {
        this.errOut = errOut;
    }

    public String getStdOut() {
        return stdOut;
    }

    public void setStdOut(String stdOut) {
        this.stdOut = stdOut;
    }

    public int getExitCode() {
        return exitCode;
    }

    public void setExitCode(int exitCode) {
        this.exitCode = exitCode;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public Executor getSshExecutor() {
        return sshExecutor;
    }

    public void setSshExecutor(Executor sshExecutor) {
        this.sshExecutor = sshExecutor;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public Map<String, Object> getObjectMap() {
        if(objectMap==null){
            objectMap=new HashMap<String, Object>();
        }
        return objectMap;
    }

    public void setObjectMap(Map<String, Object> objectMap) {
        this.objectMap = objectMap;
    }

    public boolean isSuccess() {
        //这里默认是非零 全部取false
        if(isSuccess==null){
            isSuccess=exitCode==0;
        }
        return isSuccess;
    }

    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }
}
