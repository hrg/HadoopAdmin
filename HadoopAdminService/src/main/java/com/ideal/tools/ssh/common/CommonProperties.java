package com.ideal.tools.ssh.common;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by CC on 2016/3/9.
 *
 * 这个类用来接受 web 端传递过来的 公用值
 */
public class CommonProperties {

    public CommonProperties(Map<String,String> map){
        this.propertiesMap = map;
    }


    /**存储共有参数**/
    Map<String,String> propertiesMap=new HashMap<String, String>();
    /**这个是用来 接受用户给需要调用的方法 传递参数 **/
    Map<String,String> argumentMap= new HashMap<String, String>();
    /**放置非String的参数**/
    Map<String,Object> objParamterMap = new HashMap<String, Object>();


    public String getProperty(String key,String def){
        String val = propertiesMap.get(key);
        if(val == null){
            val = def;
        }
        return val;
    }

    public void setArgument(String key,String val){
        argumentMap.put(key,val);
    }

    public void setAllArgument(Map<String,String> map){
        this.argumentMap = map;
    }

    public String getArgument(String key,String del){
        String val =argumentMap.get(key);
        if(val==null){
            val=del;
        }
        return val;
    }

    public void setObjParamter(String key,Object val){
        objParamterMap.put(key,val);
    }

    public void setAllObjtParamter(Map<String,Object> map){
        objParamterMap=map;
    }

    public Object getObjectParamter(String key,Object del){
        Object val = objParamterMap.get(key);
        if(val==null){
            val = del;
        }
        return val;
    }

    /***hdfs paramter*******/
    public static String HDFS_GROUP_SUFFIX = "HDFS_GROUP_SUFFIX";
    public static String HDFS_PATH_PREFIX = "HDFS_PATH_PREFIX";
    public static String HDFS_PUBLIC_PATH_SUFFIX ="HDFS_PUBLIC_PATH_SUFFIX";
    public static String HDFS_PRIVATE_PATH_SUFFIX ="HDFS_PRIVATE_PATH_SUFFIX";
    public static String HDFS_TRASH_PATH_SUFFIX ="HDFS_TRASH_PATH_SUFFIX";
    public static String HDFS_HIVE_TEMP_DIR ="HDFS_HIVE_TEMP_DIR";
    public static String HDFS_SUPER_USER = "HDFS_SUPER_USER";
    public static String HDFS_CLUSH_DN_RANGE = "HDFS_CLUSH_DN_RANGE";

    /***hive paramter***/
    public static String HIVE_SUPER_USER ="HIVE_SUPER_USER";
    public static String HIVE_SENTRY_JDBC="HIVE_SENTRY_JDBC";
    public static String HIVE_SENTRY_PRINCIPAL="HIVE_SENTRY_PRINCIPAL";
    public static String HIVE_SENTRY_ROLE_TEMPLET = "HIVE_SENTRY_ROLE_TEMPLET";//角色定义规则模板
    public static String HIVE_HDFSPATH_PREFIX="HIVE_HDFSPATH_PREFIX";
    public static String HIVE_CLIENT_LOG_DIR="HIVE_CLIENT_LOG_DIR";
//    public static String HIVE_HDFSPATH_PRIFIX="HIVE_HDFSPATH_PRIFIX";
    public static final String Is_Use_BeeLine="Is_Use_BeeLine";//是否使用beeline
    public static final String HIVE_USERNAME="HIVE_USERNAME";
    /**kdc paramter**/
    public static String KDC_PRINC_REALM = "KDC_REALM";
    public static String KDC_REALM = "KDC_REALM";
    public static String START_KERBEROS_REFRESH = "start_kerberos_refresh";
    public static String END_KERBEROS_REFRESH = "end_kerberos_refresh";
    /**web app**/
    public static String WEBAPP_SHELL_PATH="WEBAPP_SHELL_PATH";
    public static String WEBAPP_DAILY_JOB_PATH="WEBAPP_DAILY_JOB_PATH";
    public static String WEBAPP_INIT_PATH="WEBAPP_INIT_PATH";
    public static String Current_Cluster="Current_Cluster";//系统所属集群
    public static String Ticket_Cache_Path="Des_Ticket_Cache_Path";
    /**yarn fair schedule **/
    public static String RM_SCHEDULE_XML_SRC_PATH = "RM_SCHEDULE_XML_SRC_PATH";//mr公平调度配置文件地址
    public static String RM_SCHEDULE_XML_FILE_NAME = "RM_SCHEDULE_XML_FILE_NAME";//公平调度文件名称
    public static String FAIR_SCHEDULER_ROOT_ACL = "FAIR_SCHEDULER_ROOT_ACL"; //公平调度root acl
    public static String RM_SCHEDULE_XML_DES_PATH = "RM_SCHEDULE_XML_DES_PATH";//公平调度 文件目标地址
}
