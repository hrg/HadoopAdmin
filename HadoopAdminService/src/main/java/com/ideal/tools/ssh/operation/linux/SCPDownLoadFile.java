package com.ideal.tools.ssh.operation.linux;

import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxCommand;
import com.ideal.tools.ssh.operation.LinuxOperation;
import com.ideal.tools.ssh.result.LinuxResult;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by CC on 2016/3/4.
 */
public class SCPDownLoadFile extends LinuxOperation{

    String fromPath;
    String toPath;

    public SCPDownLoadFile(String fromHost, String fromUser, String fromPass,String pubkey, String fromPath, String toPath){
        //初始化 一个下载的地址
        initFromSSHAuth(getOneSSHAuth(fromHost,fromUser,fromPass,pubkey));
        this.fromPath=fromPath;
        this.toPath=toPath;
    }

    @Override
    public void execLinuxCMD(ClusterContext context) {

        setLinuxCommand(new LinuxCommand().initFromPath(this.fromPath)
                .initToPath(this.toPath).initOperationType(OperationType.SCP_DOWNLOAD));
        //执行
        super.execOperation(context);
    }

    @Override
    public void setResultNote(LinuxResult linuxResult) {
//        String info = getResultInfo();
        int exitcode = linuxResult.getExitCode();
        String exitCodeMean="";
        if(exitcode == 0){
            exitCodeMean = "SUCCESS SCP file from ["+fromPath+"|"+getFromSSHAuthor().getHost()+"] to ["+toPath+"]! \n";
        }else {
            exitCodeMean = "FAILD SCP file from ["+fromPath+"|"+getFromSSHAuthor().getHost()+"] to ["+toPath+"]!\n";
        }
        //设置note
        linuxResult.setNote(exitCodeMean+(StringUtils.isBlank(linuxResult.getNote())?"":linuxResult.getNote()));
    }

    @Override
    public OperationType getOperatType() {
        return OperationType.SCP_DOWNLOAD;
    }
}
