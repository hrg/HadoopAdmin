package com.ideal.tools.ssh.example;

import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxMachine;
import com.ideal.tools.ssh.operation.LinuxOperation;
import com.ideal.tools.ssh.operation.kdc.AddKDCPrinc;
import com.ideal.tools.ssh.operation.kdc.AuthClientKDCPrinc;
import com.ideal.tools.ssh.operation.linux.*;
import com.ideal.tools.ssh.result.LinuxResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CC on 2016/2/26.
 */
public class SSHExample {


//    public static void main(String[] args){
//
//        SSHExample sshExample=new SSHExample();
//
//
//        try {
////            sshExample.ExecExample();
//
////            sshExample.keepAlive();
////            sshExample.test();
//
//
//        } finally {
//
//        }
//    }
//
//
//    public void test(){
//        ClusterContext context=new ClusterContext();
//        String ip="10.5.24.151";
//        String user="I-Hadoop";
//        String passwd="ideal123";
//        String pubKey="01:53:d1:3e:c7:ff:7e:93:6b:c5:b2:e2:14:e8:e2:65";
//        List<LinuxOperation> linuxOperations = new ArrayList<LinuxOperation>();
//        //添加用户操作
//        LinuxOperation adduser =new AddLinuxUser("testSSHUser");   //添加用户
//        LinuxOperation changePw = new ChangeUserPassWord("testSSHUser","asdf#$@!wer");  //修改密码
//        LinuxOperation addGroup = new AddLinuxGroup("testSSHUser_group");     //添加linux 组
//        LinuxOperation addUserToGroup = new AddUserToGroup("testSSHUser","testSSHUser_group");     //添加linux 组
//        LinuxOperation addKDCPrinc = new AddKDCPrinc("10.5.24.148","hadoop","ShTb,Ts.hd","testSSHUser","test111");
//        LinuxOperation SCPDownLoadFile = new SCPDownLoadFile(ip,user,passwd,pubKey,"/home/I-Hadoop/cc/test/shell/sh/ ","D:\\");
//        LinuxOperation authClientKDCPrinc = new AuthClientKDCPrinc("10.5.24.151","hadoop","ShTb,Ts.hd","testSSHUser","test111");
//
//
////        linuxOperations.add(adduser);
////        linuxOperations.add(changePw);
////        linuxOperations.add(addGroup);
////        linuxOperations.add(addUserToGroup);
////        linuxOperations.add(addKDCPrinc);
////        linuxOperations.add(SCPDownLoadFile);
//        linuxOperations.add(authClientKDCPrinc);
//
//        //执行
//        context.initMachine(ip,user,passwd,pubKey,linuxOperations);
//        context.doTheThing();
//
//        //获取结果集
//        List<LinuxMachine> machines=context.getMachineList();
//        for(LinuxMachine machine:machines){
//            List<LinuxResult> results=machine.getResult();
//            for(LinuxResult result:results){
//                System.out.println("std:"+result.getStdOut());
//                System.out.println("err:"+result.getErrOut());
//                System.out.println("exception:"+result.getException());
//                System.out.println("node:"+result.getNote());
//                System.out.println("exitCode:"+result.getExitCode());
//            }
//        }
//
//        System.out.println("end");
//    }
//


//    public void ExecExample() throws IOException {
//        final SSHClient ssh=new SSHClient();
//        ssh.loadKnownHosts();
//        ssh.addHostKeyVerifier("01:53:d1:3e:c7:ff:7e:93:6b:c5:b2:e2:14:e8:e2:65");
//
//
//        ssh.connect("10.5.24.1");
//
//        ssh.authPassword("I-Hadoop","ideal123");
//
//        final Session session=ssh.startSession();
//
//        final Session.Command command=session.exec("ls -ls");
//
//        System.out.println(IOUtils.readFully(command.getInputStream()).toString());
//
//        session.close();
//
//        ssh.close();
//
////        session.exec("ls ");
//
////        System.out.println(IOUtils.readFully(command.getInputStream()).toString());
//    }
//
//
//    public void keepAlive() throws IOException, InterruptedException {
//        DefaultConfig defaultConfig = new DefaultConfig();
//        defaultConfig.setKeepAliveProvider(KeepAliveProvider.KEEP_ALIVE);
//        final SSHClient ssh = new SSHClient(defaultConfig);
//        try {
//            ssh.addHostKeyVerifier("01:53:d1:3e:c7:ff:7e:93:6b:c5:b2:e2:14:e8:e2:65");
//            ssh.addHostKeyVerifier(new PromiscuousVerifier());
//            ssh.connect("10.5.24.15");
//            ssh.getConnection().getKeepAlive().setKeepAliveInterval(5); //every 60sec
//            ssh.authPassword("I-Hadoop","ideal123");
//            Session session = ssh.startSession();
////            session.allocateDefaultPTY();
////            new CountDownLatch(1).await();
//            try {
////                session.allocateDefaultPTY();
//
//                Session.Command command = session.exec("ls -las");
//                System.out.println("std:\n" + IOUtils.readFully(command.getInputStream()).toString());
//                System.out.println("err:\n"+IOUtils.readFully(command.getErrorStream()).toString());
////                command.getOutputStream().write("ls -l\n".getBytes());
////                System.out.println("std:\n" + IOUtils.readFully(command.getInputStream()).toString());
////                System.out.println("err:\n"+IOUtils.readFully(command.getErrorStream()).toString());
////                session.close();
//
//                System.out.println(command.getExitStatus() );
//
////                session= ssh.startSession();
////                command = session.exec("pwd") ;
////
////                System.out.println(IOUtils.readFully(command.getInputStream()).toString());
//
//
//            } finally {
//                session.close();
//            }
//        } finally {
//            ssh.disconnect();
//        }
//    }


}
