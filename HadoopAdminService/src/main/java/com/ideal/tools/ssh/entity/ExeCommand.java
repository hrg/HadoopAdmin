package com.ideal.tools.ssh.entity;

import com.ideal.tools.ssh.operation.LinuxOperation;

/**
 * Created by CC on 2016/3/1.
 */
public interface ExeCommand {

    /**
     * 存在这个接口主要是 在获取命令的时候可以对 这个命令做自己的特定处理
     * @return
     */
    String getCommand();

    LinuxOperation.OperationType getCommandOperationType();

    String getFromPath();

    String getToPath();

    SSHAuthor getSSHAuth();
}
