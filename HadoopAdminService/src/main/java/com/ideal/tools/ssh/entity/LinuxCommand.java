package com.ideal.tools.ssh.entity;

import com.ideal.tools.ssh.operation.LinuxOperation;

/**
 * Created by CC on 2016/3/1.
 */
public class LinuxCommand implements ExeCommand{

    String cmd;
    String fromPath;
    String toPath;
    LinuxOperation.OperationType operationType;
    SSHAuthor sshAuthor;
    SSHAuthor fromSSHAuthor;

    public LinuxCommand initCMD(String cmd){
        this.cmd=cmd;
        return this;
    }

    public LinuxCommand initFromPath(String fromPath){
        this.fromPath=fromPath;
        return this;
    }

    public LinuxCommand initToPath(String toPath){
        this.toPath=toPath;
        return this;
    }

    public LinuxCommand initOperationType(LinuxOperation.OperationType operationType){
        this.operationType=operationType;
        return this;
    }

    public LinuxCommand initSSHAuth(SSHAuthor sshAuthor){
        this.sshAuthor=sshAuthor;
        return this;
    }

    public LinuxCommand initFromSSHAuth(SSHAuthor fromsshAuthor){
        this.fromSSHAuthor= fromsshAuthor;
        return this;
    }

    @Override
    /**
     *
     */
    public String getCommand() {
        /**这里可以对特殊的命令做一些转换**/
        return this.cmd;
    }

    @Override
    public LinuxOperation.OperationType getCommandOperationType() {
        //默认都是 cmd
        if(operationType==null){
            operationType = LinuxOperation.OperationType.EXE_CMD;
        }
        return operationType;
    }

    @Override
    public String getFromPath() {
        return this.fromPath;
    }

    @Override
    public String getToPath() {
        return this.toPath;
    }

    @Override
    public SSHAuthor getSSHAuth() {
        return sshAuthor;
    }



}
