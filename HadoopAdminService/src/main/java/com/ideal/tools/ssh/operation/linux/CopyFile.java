package com.ideal.tools.ssh.operation.linux;

import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxCommand;
import com.ideal.tools.ssh.operation.LinuxOperation;
import com.ideal.tools.ssh.result.LinuxResult;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by CC on 2016/3/4.
 */
public class CopyFile extends LinuxOperation{

    String src;
    String des;
    boolean isSUDO;

    public CopyFile(String src,String des,boolean isSUDO){
        this.src = src;
        this.des = des;
        this.isSUDO = isSUDO;
    }

    @Override
    public void execLinuxCMD(ClusterContext context) {
        String cmd="cp -r "+src+" "+des;
        if(isSUDO)
            cmd = "sudo "+cmd;

        setLinuxCommand(new LinuxCommand().initCMD(cmd));

        //执行
        super.execOperation(context);
    }

    @Override
    public void setResultNote(LinuxResult linuxResult) {
//        String info = getResultInfo();
        int exitcode = linuxResult.getExitCode();
        String exitCodeMean="";
        String host=getHost();
        if(exitcode == 0){
            exitCodeMean = "SUCCESS CP["+src+"--->"+des+"]! ["+host+"]\n";
        }else {
            exitCodeMean = "FAILD ADD GROUP["+src+"--->"+des+"]! ["+host+"]\n";
            linuxResult.setSuccess(false);
        }
        //设置note
        linuxResult.setNote(exitCodeMean+(StringUtils.isBlank(linuxResult.getNote())?"":linuxResult.getNote()));
    }

}
