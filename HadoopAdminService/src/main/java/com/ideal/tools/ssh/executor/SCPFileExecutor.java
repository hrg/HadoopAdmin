package com.ideal.tools.ssh.executor;

import com.ideal.tools.ssh.entity.ExeCommand;
import com.ideal.tools.ssh.entity.SSHAuthor;
import com.ideal.tools.ssh.result.ExecutorResult;
import com.ideal.tools.ssh.result.LinuxResult;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;
import net.schmizz.sshj.xfer.FileSystemFile;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 * Created by CC on 2016/2/29.
 *
 * 以ssh的方式与linux交互
 *
 * 一个执行器持有 一个长连接
 * 调用exec方法是新起一个会话
 *
 */
public class SCPFileExecutor implements Executor{

    final SSHClient ssh;

    SSHAuthor sshAuthor;

    int SCP_DIRECTION;
    public static final int UPLOAD=0;
    public static final int DOWNLOAD=1;

    public SCPFileExecutor( SSHAuthor sshAuthor){
        this.sshAuthor = sshAuthor;
        //初始化一个长连接ssh
        ssh = new SSHClient();
        initSSHConnect();
    }

    public LinuxResult initSSHConnect()  {
        LinuxResult linuxResult = null;
        if(sshAuthor==null){
            String exceptionStr= "The SSHAuthor is null!please init login in info!";
            linuxResult = initFaultResult(exceptionStr,exceptionStr);
            return linuxResult;
        }
        try {
            ssh.loadKnownHosts();
            //添加公钥
            String pubToken=sshAuthor.getPubToken();
            if(pubToken!=null&& !StringUtils.isBlank(pubToken)){
                ssh.addHostKeyVerifier(pubToken);
            }
            ssh.addHostKeyVerifier(new PromiscuousVerifier());
            ssh.connect(sshAuthor.getHost());
            ssh.authPassword(sshAuthor.getUsername(),sshAuthor.getPasswd());

        } catch (IOException e) {
            String exceptionStr = e.getMessage();
            linuxResult = initFaultResult(exceptionStr,"connect to host["+sshAuthor.getHost()+"] faild!");;
            closeConnection();
            return linuxResult;
        }
        linuxResult=initSuccResult("USER["+sshAuthor.getUsername()+"] login in HOST["+sshAuthor.getHost()+"] SUCCESS!");
        return linuxResult;
    }


    public SSHAuthor getSshAuthor() {
        return sshAuthor;
    }

    public void setSshAuthor(SSHAuthor sshAuthor) {
        this.sshAuthor = sshAuthor;
    }

    public int getSCP_DIRECTION() {
        return SCP_DIRECTION;
    }

    public void setSCP_DIRECTION(int SCP_DIRECTION) {
        this.SCP_DIRECTION = SCP_DIRECTION;
    }

    @Override
    public ExecutorResult exec(ExeCommand exeCommand) {
        LinuxResult linuxResult=new LinuxResult(this);
        Integer exitCode;
        String errOut;
        String stdOut;
        Session session = null;
        try {

            if(SCP_DIRECTION==DOWNLOAD) {
                ssh.newSCPFileTransfer().download(exeCommand.getFromPath(),
                        new FileSystemFile(exeCommand.getToPath()));
            }else if(SCP_DIRECTION == UPLOAD){
                ssh.newSCPFileTransfer().upload(new FileSystemFile(exeCommand.getFromPath()),
                        exeCommand.getToPath());
            }

            linuxResult.setExitCode(LinuxResult.DEFAULT_SUCCESS_CODE);

        } catch (Exception e) {
            e.printStackTrace();
            linuxResult.setExitCode(LinuxResult.DEFAULT_FAILD_CODE);
            linuxResult.setException(e.getMessage());
        } finally {
            //放入执行命令
            linuxResult.setCmd("scp from [" + exeCommand.getFromPath() + "] to [" + exeCommand.getToPath() + "]");
            try {
                ssh.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return linuxResult;
    }

    /**
     * 关闭连接
     */
    public void closeConnection(){
        try {
            if(ssh!=null) {
//                if(ssh.getConnection() !=null){
//                    ssh.getConnection().getKeepAlive().interrupt();
//                }
                ssh.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public LinuxResult initFaultResult(String exception,String note){
        LinuxResult linuxResult = new LinuxResult(null,null,LinuxResult.DEFAULT_FAILD_CODE);
        linuxResult.setException(exception);
        linuxResult.setNote(note);
        linuxResult.setSshExecutor(this);
        return linuxResult;
    }

    public LinuxResult initSuccResult(String note){
        LinuxResult linuxResult = new LinuxResult(null,null,LinuxResult.DEFAULT_SUCCESS_CODE);
        linuxResult.setNote(note);
        linuxResult.setSshExecutor(this);
        return linuxResult;
    }

}
