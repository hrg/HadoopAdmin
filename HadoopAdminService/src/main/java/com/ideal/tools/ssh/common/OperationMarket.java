package com.ideal.tools.ssh.common;

import com.ideal.tools.ssh.entity.LinuxMachine;
import com.ideal.tools.ssh.operation.LinuxOperation;
import com.ideal.tools.ssh.operation.hdfs.MakeHDFSDir;
import com.ideal.tools.ssh.operation.hdfs.QuotaHDFS;
import com.ideal.tools.ssh.operation.hdfs.RMHDFSDir;
import com.ideal.tools.ssh.operation.hive.*;
import com.ideal.tools.ssh.operation.kdc.*;
import com.ideal.tools.ssh.operation.linux.*;

import java.util.List;

/**
 * Created by CC on 2016/3/10.
 */
public class OperationMarket {


    /**
     * 添加一个linux 用户
     * @return
     */
    public static LinuxOperation AddLinuxUser(String userName){
        LinuxOperation adduser =new AddLinuxUser(userName);
        return adduser;
    }

    public static LinuxOperation AddLinuxUser(String userName,String homedir){
        LinuxOperation adduser =new AddLinuxUser(userName,homedir);
        return adduser;
    }

    public static LinuxOperation ChangeUserPassWord(String userName,String passWord){
        ChangeUserPassWord changeUserPassWord =new ChangeUserPassWord(userName,passWord);
        return changeUserPassWord;
    }

    public static LinuxOperation AddLinuxGroup(String group){
        AddLinuxGroup addLinuxGroup=new AddLinuxGroup(group);
        return addLinuxGroup;
    }

    public static LinuxOperation AddUserToGroup(String userName,String group){
        AddUserToGroup addUserToGroup = new AddUserToGroup(userName,group);
        return addUserToGroup;
    }

    public static LinuxOperation MakeHDFSDir(String dir,
                                             String owner, String group, String mode){
        MakeHDFSDir makeHDFSDir=new MakeHDFSDir(dir,owner,group,mode);
        return makeHDFSDir;
    }

    public static LinuxOperation MakeLinuxDir(String dir, String owner,
                                              String group, String mode){
        MakeLinuxDir makeLinuxDir = new MakeLinuxDir(dir,owner,group,mode);
        return makeLinuxDir;
    }

    public static LinuxOperation AuthClientKDCPrinc(String cltHost, String cltUser, String cltPass,
                                                    String username, String passWord){
        AuthClientKDCPrinc authClientKDCPrinc=
                new AuthClientKDCPrinc(cltHost,cltUser,cltPass,username,passWord);
        return authClientKDCPrinc;
    }

    public static LinuxOperation DestroyClientKDCPrinc(String username){
        DestroyClientKDCPrinc destroyClientKDCPrinc =new DestroyClientKDCPrinc(username);
        return destroyClientKDCPrinc;
    }

    public static LinuxOperation AddKDCPrinc(String username,String passWord){
        AddKDCPrinc addKDCPrinc = new AddKDCPrinc(username,passWord);
        return addKDCPrinc;
    }

    public static LinuxOperation CreateHiveDB(String db,String userName){
        CreateHiveDB createHiveDB = new CreateHiveDB(db,userName);
        return createHiveDB;
    }

    public static LinuxOperation AddLinuxUserByClush(String userName,String passWD){
        AddLinuxUserByClush addLinuxUserByClush=new AddLinuxUserByClush(userName,passWD);
        return addLinuxUserByClush;
    }

    public static LinuxOperation DelLinuxUser(String userName){
        DelLinuxUser delLinuxUser = new DelLinuxUser(userName);
        return delLinuxUser;
    }

    public static LinuxOperation DelLinuxGroup(String group){
        DelLinuxGroup delLinuxGroup = new DelLinuxGroup(group);
        return delLinuxGroup;
    }

    public static LinuxOperation RMHDFSDir(String dir){
        RMHDFSDir rmhdfsDir =new RMHDFSDir(dir);
        return rmhdfsDir;
    }

    public static LinuxOperation DelLinuxUserByClush(String userName){
        DelLinuxUserByClush delLinuxUserByClush = new DelLinuxUserByClush(userName);
        return delLinuxUserByClush;
    }

    public static LinuxOperation DropHiveDB(String db, List<String> tbls){
        DropHiveDB dropHiveDB = new DropHiveDB(db,tbls);
        return dropHiveDB;
    }

    public static LinuxOperation DelKDCPrinc( String username){
        DelKDCPrinc delKDCPrinc = new DelKDCPrinc(username);
        return delKDCPrinc;
    }

    public static LinuxOperation RMLinuxDir(String dir){
        RMLinuxDir rmLinuxDir = new RMLinuxDir(dir);
        return rmLinuxDir;
    }

    public static LinuxOperation QuotaHDFS(String userName,String spaceSize,String dirNum){
        QuotaHDFS quotaHDFS=new QuotaHDFS(userName,spaceSize,dirNum);
        return quotaHDFS;
    }

    public static LinuxOperation CreateSentryRole(String db_role){
        CreateSentryRole createSentryRole = new CreateSentryRole(db_role);
        return createSentryRole;
    }

    public static LinuxOperation JoinUserToGroup(String userName,String group){
        JoinUserToLinuxGroup joinUserToLinuxGroup =new JoinUserToLinuxGroup(userName,group);
        return joinUserToLinuxGroup;
    }

    public static LinuxOperation RemoveUserFromGroup(String userName,String group){
        RemoveUserFromLinuxGroup removeUserFromLinuxGroup =new RemoveUserFromLinuxGroup(userName,group);
        return removeUserFromLinuxGroup;
    }

    public static LinuxOperation GrantSentryPrivile(String db,String table,String group,String db_role){
        GrantSentryPrivile grantSentryPrivile =new GrantSentryPrivile(db,table,group,db_role);
        return grantSentryPrivile;
    }

    public static LinuxOperation RevokeSentryPrivile(String db,String table,String group,String db_role){
        RevokeSentryPrivile revokeSentryPrivile = new RevokeSentryPrivile(db,table,group,db_role);
        return revokeSentryPrivile;
    }

    public static LinuxOperation ExeOneShellCMD(String cmd){
        ExeOneShellCMD exeOneShellCMD = new ExeOneShellCMD(cmd);
        return exeOneShellCMD;
    }

    public static LinuxOperation AuthClientKDCPrinc_New(String username,String syspasswd){
        AuthClientKDCPrinc_NEW exeOneShellCMD = new AuthClientKDCPrinc_NEW(username,syspasswd);
        return exeOneShellCMD;
    }

    public static LinuxOperation ListClientKDCPrinc(String username){
        ListClientKDCPrinc exeOneShellCMD = new ListClientKDCPrinc(username);
        return exeOneShellCMD;
    }














}
