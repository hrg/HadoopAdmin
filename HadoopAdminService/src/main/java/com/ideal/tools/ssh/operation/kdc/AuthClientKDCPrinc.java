package com.ideal.tools.ssh.operation.kdc;

import com.ideal.tools.ssh.common.CommonProperties;
import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxCommand;
import com.ideal.tools.ssh.operation.LinuxOperation;
import com.ideal.tools.ssh.result.LinuxResult;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by CC on 2016/3/4.
 */
public class AuthClientKDCPrinc extends LinuxOperation{

    String username;
    String passWord;

    String cltHost;
    String cltUser;
    String cltPass;

    String shellPath;
    String kdcPrinc;

    public AuthClientKDCPrinc(String cltHost, String cltUser, String cltPass, String username, String passWord){
        this.cltHost=cltHost;
        this.cltUser=cltUser;
        this.cltPass=cltPass;
        this.username=username;
        this.passWord=passWord;
    }

    @Override
    public void execLinuxCMD(ClusterContext context) {
        this.shellPath=context.getCommonProperties().
                getProperty(CommonProperties.WEBAPP_SHELL_PATH,"")+ "authClientPrinc.exp";
        String realm = context.getCommonProperties().getProperty(CommonProperties.KDC_PRINC_REALM,"@EXAMPLE.COM");
        this.kdcPrinc=username+realm;
        String cmd="expect "+shellPath+" "+cltHost+" "+cltUser+" '"+cltPass+"' '"+username+"' "+kdcPrinc+" '"+passWord+"'";

        setLinuxCommand(new LinuxCommand().initCMD(cmd));
        //执行
        super.execOperation(context);
    }

    @Override
    public void setResultNote(LinuxResult linuxResult) {
//        String info = getResultInfo();
        int exitcode = linuxResult.getExitCode();
        String exitCodeMean="";
        String host=getHost();
        if(exitcode == 0){
            exitCodeMean = "SUCCESS kinit client["+this.cltHost+","+this.kdcPrinc+"]! ["+host+"]\n";
        }else {
            exitCodeMean = "FAILD kinit client["+this.cltHost+","+this.kdcPrinc+"]! ["+host+"]\n";
        }
        //设置note
        linuxResult.setNote(exitCodeMean+(StringUtils.isBlank(linuxResult.getNote())?"":linuxResult.getNote()));
    }

}
