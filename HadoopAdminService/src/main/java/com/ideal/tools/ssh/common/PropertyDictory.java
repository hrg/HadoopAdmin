package com.ideal.tools.ssh.common;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CC on 2016/7/25.
 * 这里存放 所有的 需要使用的 项目参数名称
 * 不在这个里面配置的不能使用
 *
 */
public class PropertyDictory {

    private static List<String> keys=new ArrayList<String>();

    private static PropertyDictory dictory = new PropertyDictory();

    public static List<String> getKeys() {
        return keys;
    }

    public static boolean hasKey(String key){
        return keys.contains(key);
    }

    PropertyDictory config(String key){
        if (!keys.contains(key))
            keys.add(key);
        return dictory;
    }


    //webapp
    public static final String WEB_APP_DB_IP="webapp.ip";
    public static final String WEB_APP_DB_DB="webapp.database";
    public static final String WEB_APP_DB_USER="webapp.username";
    public static final String WEB_APP_DB_PW="webapp.password";
    public static final String WEB_APP_DB_PORT="webapp.port";
    //hivemeta
    public static final String HIVE_META_IP = "hive.ip";
    public static final String HIVE_META_DB = "hive.database";
    public static final String HIVE_META_PW = "hive.password";
    public static final String HIVE_META_USER = "hive.username";
    public static final String HIVE_META_PORT="hive.port";

    //sentry
    public static final String SENTRY_META_IP = "sentry.ip";
    public static final String SENTRY_META_DB = "sentry.database";
    public static final String SENTRY_META_PW = "sentry.password";
    public static final String SENTRY_META_USER = "sentry.username";
    public static final String SENTRY_META_PORT="sentry.port";

    //remote syn db
    public static final String REMOTE_SYN_IP = "syn.ip";
    public static final String REMOTE_SYN_DB = "syn.database";
    public static final String REMOTE_SYN_PW = "syn.password";
    public static final String REMOTE_SYN_USER = "syn.username";
    public static final String REMOTE_SYN_PORT="syn.port";

    /** param from common property start **/
    /***hdfs paramter*******/
    public static String HDFS_GROUP_SUFFIX = "HDFS_GROUP_SUFFIX";
    public static String HDFS_PATH_PREFIX = "HDFS_PATH_PREFIX";
    public static String HDFS_PUBLIC_PATH_SUFFIX ="HDFS_PUBLIC_PATH_SUFFIX";
    public static String HDFS_PRIVATE_PATH_SUFFIX ="HDFS_PRIVATE_PATH_SUFFIX";
    public static String HDFS_TRASH_PATH_SUFFIX ="HDFS_TRASH_PATH_SUFFIX";
    public static String HDFS_HIVE_TEMP_DIR ="HDFS_HIVE_TEMP_DIR";
    public static String HDFS_SUPER_USER = "HDFS_SUPER_USER";
    public static String HDFS_CLUSH_DN_RANGE = "HDFS_CLUSH_DN_RANGE";

    /***hive paramter***/
    public static String HIVE_SUPER_USER ="HIVE_SUPER_USER";
    public static String HIVE_SENTRY_JDBC="HIVE_SENTRY_JDBC";
    public static String HIVE_SENTRY_PRINCIPAL="HIVE_SENTRY_PRINCIPAL";
    public static String HIVE_SENTRY_ROLE_TEMPLET = "HIVE_SENTRY_ROLE_TEMPLET";//角色定义规则模板
    public static String HIVE_CLIENT_LOG_DIR="HIVE_CLIENT_LOG_DIR";
    //    public static String HIVE_HDFSPATH_PRIFIX="HIVE_HDFSPATH_PRIFIX";
    public static final String Is_Use_BeeLine="Is_Use_BeeLine";//是否使用beeline
    public static final String HIVE_USERNAME="HIVE_USERNAME";
    /**kdc paramter**/
//    public static String KDC_PRINC_REALM = "KDC_REALM";
    /**web app**/
//    public static String WEBAPP_DAILY_JOB_PATH="WEBAPP_DAILY_JOB_PATH";
    public static String Current_Cluster="Current_Cluster";//系统所属集群
    /**yarn fair schedule **/
    public static String RM_SCHEDULE_XML_SRC_PATH = "RM_SCHEDULE_XML_SRC_PATH";//mr公平调度配置文件地址
    public static String RM_SCHEDULE_XML_FILE_NAME = "RM_SCHEDULE_XML_FILE_NAME";//公平调度文件名称
    public static String FAIR_SCHEDULER_ROOT_ACL = "FAIR_SCHEDULER_ROOT_ACL"; //公平调度root acl
    public static String RM_SCHEDULE_XML_DES_PATH = "RM_SCHEDULE_XML_DES_PATH";//公平调度 文件目标地址

    /** param from common property end **/


    //kdc
    public static final String KDC_REALM = "KDC_REALM";


    //system
    public static final String WEBAPP_SHELL_PATH="WEBAPP_SHELL_PATH";
    public static final String WEBAPP_INIT_PATH="WEBAPP_INIT_PATH";  //初始化脚本放置地址
    public static final String Ticket_Cache_Path="Des_Ticket_Cache_Path";
    public static final String HIVE_HDFSPATH_PREFIX="HIVE_HDFSPATH_PREFIX";//hdfs地址前缀
    public static final String CLIENT_USER_HOME_1 = "CLIENT_USER_HOME_1";
    public static final String CLIENT_USER_HOME_2 ="CLIENT_USER_HOME_2";



    //propertyw文件中的key 值
    //用来初始 property 的sql语句
    public static final String PROPERTY_INIT_MYSQL_PARAMETER_SQL="property.init.mysql.parameter.sql";

    static {
        dictory.config(WEB_APP_DB_IP).config(WEB_APP_DB_DB).config(WEB_APP_DB_USER).config(WEB_APP_DB_PW).config(WEB_APP_DB_PORT)
                .config(HIVE_META_IP).config(HIVE_META_DB).config(HIVE_META_USER).config(HIVE_META_PW).config(HIVE_META_PORT)
                .config(SENTRY_META_IP).config(SENTRY_META_DB).config(SENTRY_META_PW).config(SENTRY_META_USER).config(REMOTE_SYN_PORT)
                .config(REMOTE_SYN_IP).config(REMOTE_SYN_DB).config(REMOTE_SYN_USER).config(REMOTE_SYN_PW).config(SENTRY_META_PORT)
                .config(KDC_REALM)
                .config(WEBAPP_INIT_PATH).config(WEBAPP_SHELL_PATH).config(Ticket_Cache_Path).config(HIVE_HDFSPATH_PREFIX)
                .config(CLIENT_USER_HOME_1).config(CLIENT_USER_HOME_2)
                .config(HDFS_CLUSH_DN_RANGE).config(HDFS_SUPER_USER)
                .config(HIVE_SUPER_USER).config(HIVE_SENTRY_JDBC).config(HIVE_SENTRY_PRINCIPAL).config(HIVE_SENTRY_ROLE_TEMPLET).config(HIVE_CLIENT_LOG_DIR)
                .config(Is_Use_BeeLine).config(HIVE_USERNAME)
                .config(RM_SCHEDULE_XML_SRC_PATH).config(RM_SCHEDULE_XML_FILE_NAME).config(RM_SCHEDULE_XML_DES_PATH);


    }



}
