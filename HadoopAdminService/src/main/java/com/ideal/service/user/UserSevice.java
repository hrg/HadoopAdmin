package com.ideal.service.user;

import com.ideal.service.Utils;
import com.ideal.tools.db.MysqlDBUtils;

import java.util.Map;

/**
 * Created by CC on 2016/8/30.
 */
public class UserSevice {

    public static String getClientUserHome(String cltStr,String ip){
        String home="";
        String[] homes = cltStr.split(";");
        Map<String,String> map = Utils.getCltReservMap();
        String id = map.get(ip);
        for(String h:homes){
            if(h.contains(","+id+",")){
                home = h.split(",")[1];
            }
        }
        return home;
    }
}
