

rename table cluster_machine to cluster_machine_old;
-- ----------------------------
-- Table structure for cluster_cluster_type
-- ----------------------------
DROP TABLE IF EXISTS `cluster_cluster_type`;
CREATE TABLE `cluster_cluster_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '集群标识',
  `clusterTypeId` varchar(45) NOT NULL,
  `clusterTypeName` varchar(45) NOT NULL COMMENT '集群类型名称',
  `note` varchar(45) NOT NULL COMMENT '集群类型备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `clusterTypeId_UNIQUE` (`clusterTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='集群类型表';


-- ----------------------------
-- Table structure for cluster_machine
-- ----------------------------
DROP TABLE IF EXISTS `cluster_machine`;
CREATE TABLE `cluster_machine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `machineIp` varchar(15) NOT NULL COMMENT '机器ip地址',
  `loginUserName` varchar(45) NOT NULL COMMENT '机器登陆用户',
  `loginPassWord` varchar(45) NOT NULL COMMENT '机器登陆密码',
  `clusterTypeId` int(11) NOT NULL COMMENT '机器所属集群',
  `machineTypeId` int(11) unsigned NOT NULL COMMENT '机器类型',
  `isAddUser` int(11) NOT NULL COMMENT '是否添加用户:0-添加,1-不添加',
  `status` int(11) NOT NULL COMMENT '机器状态:0-正常,1-停机',
  `note` varchar(45) NOT NULL COMMENT '机器备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='集群机器信息';

-- ----------------------------
-- Table structure for cluster_machine_type
-- ----------------------------
DROP TABLE IF EXISTS `cluster_machine_type`;
CREATE TABLE `cluster_machine_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `machineTypeId` varchar(45) NOT NULL COMMENT '机器类型id',
  `machineTypeName` varchar(45) NOT NULL COMMENT '机器类型名称',
  `note` varchar(45) NOT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `machineTypeId_UNIQUE` (`machineTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='集群机器类型';



-- ----------------------------
-- Table structure for cluster_user
-- ----------------------------
DROP TABLE IF EXISTS `cluster_user`;
CREATE TABLE `cluster_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `userName` varchar(45) DEFAULT NULL COMMENT '用户名',
  `clientPW` varchar(45) DEFAULT NULL COMMENT '用户客户端密码',
  `systemPW` varchar(45) DEFAULT NULL COMMENT '用户服务器密码',
  `companyId` int(11) DEFAULT NULL COMMENT '用户所属公司',
  `contactPerson` varchar(45) DEFAULT NULL COMMENT '联系人名字',
  `phoneNumber` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(45) DEFAULT NULL COMMENT '用户邮箱',
  `remark` varchar(45) DEFAULT NULL COMMENT '用户备注',
  `userTypeId` int(11) DEFAULT NULL COMMENT '用户类型',
  `status` int(1) DEFAULT NULL COMMENT '用户状态:0-启用,1-停用',
  `clusterTypeId` int(11) DEFAULT NULL COMMENT '用户所属集群',
  `changeTime` bigint(20) DEFAULT NULL COMMENT '修改时间',
  `createTime` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='集群用户表';


-- ----------------------------
-- Table structure for cluster_user_clientquota
-- ----------------------------
DROP TABLE IF EXISTS `cluster_user_clientquota`;
CREATE TABLE `cluster_user_clientquota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientCatalog` varchar(45) NOT NULL COMMENT '用户目录',
  `clientCatalogSize` int(11) NOT NULL COMMENT '用户目录配置额',
  `userId` int(11) NOT NULL COMMENT '集群用户标识',
  `clientCatalogUnit` varchar(1) NOT NULL COMMENT '用户目录配置额单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='client配置';

-- ----------------------------
-- Table structure for cluster_user_hdfsquota
-- ----------------------------
DROP TABLE IF EXISTS `cluster_user_hdfsquota`;
CREATE TABLE `cluster_user_hdfsquota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '集群用户标识',
  `hdfsSpace` int(11) NOT NULL COMMENT 'hdfs用户空间大小',
  `hdfsFileCount` int(11) NOT NULL COMMENT 'hdfs文件数量',
  `hdfsSpaceUnit` varchar(1) NOT NULL COMMENT 'hdfs空间单位',
  `hdfsPath` varchar(100) NOT NULL COMMENT 'hdfs地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for cluster_user_kbrauth
-- ----------------------------
DROP TABLE IF EXISTS `cluster_user_kbrauth`;
CREATE TABLE `cluster_user_kbrauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '集群用户标识',
  `machineId` int(11) NOT NULL COMMENT '机器id',
  `startTime` bigint(20) NOT NULL COMMENT '票据生效时间',
  `endTime` bigint(20) NOT NULL COMMENT '票据失效时间',
  `principal` varchar(50) NOT NULL COMMENT '认证主体',
  `ticketPath` varchar(50) NOT NULL COMMENT 'kerberos票据具体地址',
  `status` int(11) NOT NULL COMMENT 'kerberos认证状态:0-成功,1-失败',
  `createTime` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='集群用户kerberos认证情况';


-- ----------------------------
-- Table structure for cluster_user_kbrconfig
-- ----------------------------
DROP TABLE IF EXISTS `cluster_user_kbrconfig`;
CREATE TABLE `cluster_user_kbrconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '集群用户标识',
  `validDay` int(10) unsigned zerofill NOT NULL COMMENT 'kerberos有效期',
  `machineIds` varchar(100) NOT NULL COMMENT '机器列表(存储机器id)',
  `status` int(1) unsigned zerofill NOT NULL COMMENT 'kerberos状态:0-启用,1-停用',
  `changeTime` bigint(20) NOT NULL COMMENT '修改时间',
  `createTime` bigint(20) NOT NULL COMMENT '创建时间',
  `startTime` bigint(20) NOT NULL COMMENT '有效时间开始日期',
  `endTime` bigint(20) NOT NULL COMMENT '有效时间结束日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=749 DEFAULT CHARSET=utf8 COMMENT='集群用户kerberos配置表';


-- ----------------------------
-- Table structure for cluster_user_queue
-- ----------------------------
DROP TABLE IF EXISTS `cluster_user_queue`;
CREATE TABLE `cluster_user_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL COMMENT '用户id',
  `queueName` varchar(50) NOT NULL DEFAULT '' COMMENT '队列名称',
  `minResource` int(11) NOT NULL COMMENT '最小资源数',
  `maxResource` int(11) NOT NULL COMMENT '最大资源数',
  `maxApp` int(11) NOT NULL COMMENT '最大app数',
  `weight` float NOT NULL COMMENT '权重值',
  `acl` varchar(50) NOT NULL DEFAULT '' COMMENT 'acl值',
  `minCpu` int(11) NOT NULL COMMENT '最小cpu值',
  `maxCpu` int(11) NOT NULL COMMENT '最大cpu值',
  `modifyTime` bigint(20) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for cluster_user_type
-- ----------------------------
DROP TABLE IF EXISTS `cluster_user_type`;
CREATE TABLE `cluster_user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户类型标识',
  `userTypeId` varchar(45) NOT NULL,
  `userTypeName` varchar(45) NOT NULL COMMENT '用户类型名称',
  `note` varchar(45) NOT NULL COMMENT '用户类型备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userTypeId_UNIQUE` (`userTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='集群用户类型';


-- ----------------------------
-- Table structure for meta_hdfs_access
-- ----------------------------
DROP TABLE IF EXISTS `meta_hdfs_access`;
CREATE TABLE `meta_hdfs_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clusterUserId` int(11) NOT NULL COMMENT '集群用户',
  `hdfsInfoId` int(11) NOT NULL COMMENT 'hdfspath信息',
  `createTime` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8032 DEFAULT CHARSET=utf8 COMMENT='hdsf访问权限表';

-- ----------------------------
-- Table structure for meta_hdfs_info
-- ----------------------------
DROP TABLE IF EXISTS `meta_hdfs_info`;
CREATE TABLE `meta_hdfs_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hdfsPath` varchar(100) NOT NULL COMMENT 'hdfs地址',
  `clusterUserId` int(11) NOT NULL COMMENT '根据地址截取的用户',
  `hdfsGroup` varchar(45) NOT NULL COMMENT '地址所属组',
  `hdfsOwner` varchar(45) NOT NULL COMMENT '地址的权限所属人',
  `hdfsPerm` varchar(45) NOT NULL COMMENT '地址所属权限',
  `note` varchar(45) NOT NULL COMMENT '备注',
  `createTime` bigint(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12208 DEFAULT CHARSET=utf8 COMMENT='hdfs公有的元数据信息';


-- ----------------------------
-- Table structure for meta_hdfs_info_bak
-- ----------------------------
DROP TABLE IF EXISTS `meta_hdfs_info_bak`;
CREATE TABLE `meta_hdfs_info_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hdfsPath` varchar(100) NOT NULL COMMENT 'hdfs地址',
  `clusterUserId` int(11) NOT NULL COMMENT '根据地址截取的用户',
  `hdfsGroup` varchar(128) NOT NULL COMMENT '地址所属组',
  `hdfsOwner` varchar(45) NOT NULL COMMENT '地址的权限所属人',
  `hdfsPerm` varchar(45) NOT NULL COMMENT '地址所属权限',
  `note` varchar(45) NOT NULL COMMENT '备注',
  `createTime` bigint(20) NOT NULL COMMENT '创建时间',
  `properties` int(1) NOT NULL COMMENT '0 公有 1 私有 ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COMMENT='hdfs元数据信息';


-- ----------------------------
-- Table structure for meta_hive_access
-- ----------------------------
DROP TABLE IF EXISTS `meta_hive_access`;
CREATE TABLE `meta_hive_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clusterUserId` int(11) NOT NULL COMMENT '集群用户标识',
  `hiveInfoId` int(11) NOT NULL COMMENT 'hive数据标识',
  `createTime` bigint(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='集群hive数据访问权限';


-- ----------------------------
-- Table structure for meta_hive_info
-- ----------------------------
DROP TABLE IF EXISTS `meta_hive_info`;
CREATE TABLE `meta_hive_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dbName` varchar(45) NOT NULL COMMENT 'HIVE数据库名称',
  `tableName` varchar(45) NOT NULL COMMENT 'HIVE表名',
  `hdfsInfoBakId` int(11) NOT NULL COMMENT 'hdfs_bak表信息',
  `createTime` bigint(20) NOT NULL COMMENT '创建时间',
  `clusterUserId` int(11) NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='集群公有的hive元数据信息';

-- ----------------------------
-- Table structure for meta_hive_sql
-- ----------------------------
DROP TABLE IF EXISTS `meta_hive_sql`;
CREATE TABLE `meta_hive_sql` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hiveSql` text NOT NULL COMMENT 'hive建表sql',
  `hiveInfoId` int(11) NOT NULL COMMENT 'hiveInfo表id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='hive的建表sql语句';


-- ----------------------------
-- Table structure for system_company
-- ----------------------------
DROP TABLE IF EXISTS `system_company`;
CREATE TABLE `system_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '公司唯一标识',
  `companyName` varchar(45) NOT NULL COMMENT '公司名称',
  `note` varchar(45) NOT NULL,
  `contactName` varchar(255) NOT NULL,
  `contactMobile` varchar(255) NOT NULL,
  `delTag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8 COMMENT='集群公司表';


-- ----------------------------
-- Table structure for cluster_company_quota
-- ----------------------------
DROP TABLE IF EXISTS `cluster_company_quota`;
CREATE TABLE `cluster_company_quota` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `companyId` int(11) NOT NULL COMMENT '公司标识',
  `maxUserNum` int(11) DEFAULT '0',
  `maxResource` int(11) DEFAULT '0',
  `maxCpu` int(11) DEFAULT '0',
  `hdfsFileCount` int(11) DEFAULT '0',
  `hdfsSpace` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8 COMMENT='集群公司配额表';

-- ----------------------------
-- Table structure for system_menu
-- ----------------------------
DROP TABLE IF EXISTS `system_menu`;
CREATE TABLE `system_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL COMMENT '菜单名',
  `url` varchar(100) NOT NULL DEFAULT ' ' COMMENT '菜单路径',
  `cssClass` varchar(45) NOT NULL DEFAULT '' COMMENT '菜单样式',
  `parentId` int(11) NOT NULL COMMENT '父菜单id',
  `code` varchar(50) NOT NULL DEFAULT '',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `createTime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `isParent` int(1) NOT NULL COMMENT '0是父菜单 1不是父菜单',
  `type` int(1) NOT NULL,
  `isHidden` int(1) NOT NULL DEFAULT '0' COMMENT '0是显示1是隐藏',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COMMENT='系统菜单';


-- ----------------------------
-- Table structure for system_role
-- ----------------------------
DROP TABLE IF EXISTS `system_role`;
CREATE TABLE `system_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL COMMENT '角色名',
  `status` varchar(45) NOT NULL COMMENT '生效0 无效1',
  `createTime` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='角色表';


-- ----------------------------
-- Table structure for system_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `system_role_menu`;
CREATE TABLE `system_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleId` int(11) NOT NULL,
  `menuId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=625 DEFAULT CHARSET=utf8 COMMENT='角色菜单对照表';


-- ----------------------------
-- Table structure for system_user
-- ----------------------------
DROP TABLE IF EXISTS `system_user`;
CREATE TABLE `system_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(45) NOT NULL,
  `passWord` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `phoneNumber` varchar(45) NOT NULL,
  `remark` varchar(45) NOT NULL,
  `createTime` bigint(20) NOT NULL,
  `changeTime` bigint(20) NOT NULL,
  `systemCompanyId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='webapp用户';


-- ----------------------------
-- Table structure for system_user_role
-- ----------------------------
DROP TABLE IF EXISTS `system_user_role`;
CREATE TABLE `system_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户角色对照表';


-- ----------------------------
-- Table structure for system_web_log
-- ----------------------------
DROP TABLE IF EXISTS `system_web_log`;
CREATE TABLE `system_web_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) NOT NULL,
  `operationId` int(11) NOT NULL,
  `createtime` bigint(20) NOT NULL,
  `parameter` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2783 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for system_web_operations
-- ----------------------------
DROP TABLE IF EXISTS `system_web_operations`;
CREATE TABLE `system_web_operations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `operationName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `relationship`;
CREATE TABLE `relationship` (
  `newId` int(11)  NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `oldId` varchar(50) NOT NULL,
  PRIMARY KEY (`newId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='新表旧表关系';

-- ----------------------------
-- Table structure for d_machin_type
-- ----------------------------
DROP TABLE IF EXISTS `d_machin_type`;
CREATE TABLE `d_machin_type` (
  `id` int(100) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `note` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for d_cluster
-- ----------------------------
DROP TABLE IF EXISTS `d_cluster`;
CREATE TABLE `d_cluster` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `note` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of d_cluster
-- ----------------------------
INSERT INTO `d_cluster` VALUES ('1', '沙箱集群', null);
INSERT INTO `d_cluster` VALUES ('2', '正式集群', null);

-- ----------------------------
-- Records of d_machin_type
-- ----------------------------
INSERT INTO `d_machin_type` VALUES ('1', 'NN', null);
INSERT INTO `d_machin_type` VALUES ('2', 'DN', null);
INSERT INTO `d_machin_type` VALUES ('3', 'Client', null);
INSERT INTO `d_machin_type` VALUES ('4', 'KDC', null);
INSERT INTO `d_machin_type` VALUES ('5', 'Hive', null);
INSERT INTO `d_machin_type` VALUES ('6', 'Clushell', null);
INSERT INTO `d_machin_type` VALUES ('7', 'RM', null);
INSERT INTO `d_machin_type` VALUES ('9', 'WebApp', null);



-- cluster_cluster_type
delete from cluster_cluster_type;
INSERT INTO cluster_cluster_type
SELECT id,id,name,IFNULL(note,'')  
FROM d_cluster;

-- cluster_machine_type
delete from cluster_machine_type;
INSERT into cluster_machine_type
select id,id,name,IFNULL(note,'') 
from d_machin_type;

-- cluster_user_type
delete from cluster_user_type;
INSERT INTO `cluster_user_type` VALUES ('1', '1', '厂商用户', '');
INSERT INTO `cluster_user_type` VALUES ('2', '2', '内部用户', '');
INSERT INTO `cluster_user_type` VALUES ('3', '3', '其他用户', ' ');


-- cluster_machine
delete from cluster_machine;
INSERT into cluster_machine
select id,mc_ip,mc_user,mc_pw,cluster_type,mc_type,add_user,mc_status,mc_remarks
from machine_info;

-- system_company
delete from system_company;
INSERT into system_company
select id,NAME,REMARK,CONTACT_NAME,CONTACT_MOBILE,DEL_FLAG
from t_customer;

-- relationship
delete from relationship;
INSERT into relationship (oldId)
SELECT hp_user_id
from hadoopuser;

-- cluster_user
delete from cluster_user;
INSERT into cluster_user
select 
				IFNULL(newId,0),
				hp_user_name,hp_user_pw,hp_user_syspw,
				hp_company_id,hp_contacts,hp_phone,hp_email,
				hp_remarks,hp_user_type,hp_user_status,hp_culster,
				0,IFNULL(UNIX_TIMESTAMP(hp_createtime),0)
from hadoopuser
left join relationship 
on hp_user_id = oldId;

-- cluster_user_hdfsquota
delete from cluster_user_hdfsquota;
INSERT into cluster_user_hdfsquota 
SELECT id,IFNULL(newId,0),quota_space,IFNULL(file_count,0),unit,IFNULL(path,'')
from hadoopuser_hdfs
LEFT JOIN relationship 
on username = oldId;

-- cluster_user_kbrauth   注释掉：20160808这张表不需要初始数据
-- delete from cluster_user_kbrauth;
-- INSERT into cluster_user_kbrauth
-- select
-- 			kdc_auth.id,IFNULL(newId,0),mi.id machineId,
-- 			case when ker_start_time is not null and LENGTH(ker_start_time)>16 then round(UNIX_TIMESTAMP(CONCAT('20',SUBSTRING(ker_start_time,7,2),'/',SUBSTRING(ker_start_time,1,5),SUBSTRING(ker_start_time,9))),0)
-- 			else 0
-- 			end,
-- 			case when ker_end_time is not null and LENGTH(ker_end_time)>16 then round(UNIX_TIMESTAMP(CONCAT('20',SUBSTRING(ker_end_time,7,2),'/',SUBSTRING(ker_end_time,1,5),SUBSTRING(ker_end_time,9))),0)
-- 			else 0
-- 			end,
-- 			ker_princ,ticket_cahce_path,ker_status,
-- 			UNIX_TIMESTAMP(create_time)
-- from kdc_auth
-- LEFT JOIN relationship
-- on hp_user_id = oldId
-- left join machine_info mi
-- on kdc_auth.mc_ip = mi.mc_ip and mi.mc_type=3;

-- cluster_user_kbrconfig   20160809这张表需要初始数据
delete from cluster_user_kbrconfig;
INSERT into cluster_user_kbrconfig
select kdc_config.id,IFNULL(newId,0),valid_day,IFNULL(temp.ips,''),
status,0,IFNULL(UNIX_TIMESTAMP(create_time),0),IFNULL(UNIX_TIMESTAMP(start_time),0),IFNULL(UNIX_TIMESTAMP(end_time),0)
from kdc_config
left join (
	select configId,GROUP_CONCAT(id) ips
	from (select t.id configId,machine_info.id
				from (select a.ID,substring_index(substring_index(a.mc_ip,',',b.help_topic_id),',',-1) ip
				from kdc_config a join mysql.help_topic b
				on b.help_topic_id <= (length(a.mc_ip) - length(replace(a.mc_ip,',',''))+1)
				order by a.ID) t
left join machine_info
on t.ip = machine_info.mc_ip and machine_info.mc_type=3
order by t.id) b
GROUP BY
    configId) temp
on kdc_config.id = temp.configId
left join relationship
on hp_user_id = oldId;

-- cluster_user_queue
delete from cluster_user_queue;
INSERT into cluster_user_queue
select id,IFNULL(newId,0),queue,min_memory,max_memory,max_apps,weight,acl,min_cpu,max_cpu,UNIX_TIMESTAMP(update_time)
from fair_scheduler_allocation
LEFT JOIN relationship
on username = oldId;


-- meta_hdfs_info
delete from meta_hdfs_info;
INSERT into meta_hdfs_info
select id,hp_folder_name,IFNULL(newId,0),
hp_folder_group,hp_folder_owner,hp_folder_auth,
IFNULL(hp_remarks,''),
IFNULL(UNIX_TIMESTAMP(hp_createtime),0)
from hdfs_info
left join relationship
on hp_user_id = oldId;