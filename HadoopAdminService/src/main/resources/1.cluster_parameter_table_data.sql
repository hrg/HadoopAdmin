
DROP TABLE IF EXISTS `cluster_parameter`;
CREATE TABLE `cluster_parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameterKey` varchar(50) NOT NULL DEFAULT '' COMMENT '参数key值',
  `parameterVal` varchar(200) NOT NULL DEFAULT '' COMMENT '参数val值',
  `parentId` int(11) NOT NULL COMMENT '父参数Id',
  `note` varchar(50) NOT NULL DEFAULT '' COMMENT '备注',
  `clusterTypeId` int(11) NOT NULL COMMENT '机器所属集群',
  PRIMARY KEY (`id`),
  UNIQUE KEY `parameterKey` (`parameterKey`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
-- cluster_parameter
delete from cluster_parameter;
-- INSERT into cluster_parameter
-- select id,parameter_key,parameter_val,parent_id,IFNULL(note,''),encrypt
-- from t_parameter_new;
INSERT INTO `cluster_parameter` VALUES (1,'WEBAPP_INIT_PATH','/home/hadoop/hadoopadmin/source/init',22,'初始系统参数地址','1'),(2,'HIVE_HDFSPATH_PRIFIX','hdfs://ns1',20,'','1'),(4,'HDFS_SUPER_USER','hdfs',21,'','1'),(5,'HDFS_CLUSH_DN_RANGE','dnall',21,'','1'),(6,'HDFS_GROUP_SUFFIX','_group',21,'','1'),(7,'HDFS_HIVE_TEMP_DIR','/tmp/hive-',21,'','1'),(8,'HDFS_PATH_PREFIX','/user/',21,'','1'),(9,'HDFS_PUBLIC_PATH_SUFFIX','/public',21,'','1'),(10,'HDFS_TRASH_PATH_SUFFIX','/.Trash',21,'','1'),(11,'HDFS_PRIVATE_PATH_SUFFIX','/private',21,'','1'),(12,'HIVE_CLIENT_LOG_DIR','/tmp/',20,'','1'),(13,'HIVE_SENTRY_PRINCIPAL','',20,'','1'),(14,'HIVE_SENTRY_JDBC','jdbc:hive2://localhost:10000/',20,'','1'),(15,'HIVE_SENTRY_ROLE_TEMPLET','role_@item@_@principle@_@path@',20,'','1'),(16,'HIVE_SUPER_USER','hive',20,'','1'),(17,'KDC_REALM','@EXAMPLE.COM',21,'KDC认证','1'),(18,'Des_Ticket_Cache_Path','/home/hadoop/hadoopadmin/source/ticket_cache/',22,'kerberos ticket cache 存放地址','1'),(19,'Linux_Group_Path','/etc/group',22,'Linux组>信息_初始hdfsvisiter信息','1'),(20,'Hive','Hive parameters',0,'hive参数','1'),(21,'HADOOP','Hadoop parameters',0,'hadoop参数','1'),(22,'SYSTEM','System parameter',0,'系统配置参数','1'),(23,'RM_SCHEDULE_XML_DES_PATH','/etc/hadoop/conf/fair-scheduler.xml',21,'','1'),(24,'RM_SCHEDULE_XML_FILE_NAME','fair-scheduler.xml',21,'','1'),(25,'RM_SCHEDULE_XML_SRC_PATH','/home/hadoop/hadoopadmin/source/init/fair-scheduler.xml',21,'','1'),(28,'WEBAPP_SHELL_PATH','/home/hadoop/hadoopadmin/source/sh/',0,'','1'),(29,'PublicPw','12345',22,'公共的验证密码','1'),(30,'Is_Use_BeeLine','0',22,'1:启动beeline;0:关闭beeline','1'),(31,'HIVE_USERNAME','hadoop',20,'hive用户','1');