drop table if exists audit_log;
drop table if exists cluster_machine_old;
drop table if exists d_cluster;
drop table if exists d_machin_type;
drop table if exists dictionary_common;
drop table if exists event_info;
drop table if exists fair_scheduler_allocation;
drop table if exists fair_scheduler_user;
drop table if exists hadoopuser;
drop table if exists hadoopuser_hdfs;
drop table if exists hdfs_info;
drop table if exists hdfs_info_bak;
drop table if exists hdfs_visiters;
drop table if exists hive_info;
drop table if exists hive_visiters;
drop table if exists info_sync;
drop table if exists kdc_auth;
drop table if exists kdc_config;
drop table if exists machine_info;
drop table if exists mc_type_name;
drop table if exists scheduler_yarn_configuration;
drop table if exists t_customer;
drop table if exists t_menu;
drop table if exists t_operations;
drop table if exists t_parameter;
drop table if exists t_parameter_new;
drop table if exists t_role;
drop table if exists t_role_menu;
drop table if exists t_schedule;
drop table if exists t_tmp;
drop table if exists t_user;
drop table if exists t_user_role;
drop table if exists t_webapp_log;
drop table if exists relationship;
