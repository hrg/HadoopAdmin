

-- meta_hdfs_access
delete from meta_hdfs_access;
INSERT into meta_hdfs_access
select hdfs_visiters.id,IFNULL(newId,0),IFNULL(hdfs_info.id,0),IFNULL(UNIX_TIMESTAMP(modify_time),0)
from hdfs_visiters
LEFT JOIN relationship
on hdfs_visit_user = oldId
LEFT JOIN hdfs_info
on folder_name = hp_folder_name;



-- meta_hive_info           直接取此数据库meta_hdfs_info_bak的新数据作为关联数据表
delete from meta_hive_info;
INSERT into meta_hive_info
SELECT hive_info.id,hp_db_name,hp_tb_name
,IFNULL(meta_hdfs_info_bak.id,0) ,IFNULL(UNIX_TIMESTAMP(hive_info.hp_createtime),0),newId
from hive_info
LEFT JOIN meta_hdfs_info_bak
on hive_info.hp_hdfs_loc = meta_hdfs_info_bak.hdfsPath
left JOIN relationship
on hive_info.hadoop_user_id = oldId 
;


-- meta_hive_access									
delete from meta_hive_access;
INSERT into meta_hive_access
SELECT hive_visiters.id,IFNULL(newId,0),IFNULL(hive_info.id,0),IFNULL(UNIX_TIMESTAMP(modify_time),0)
from hive_visiters
left JOIN relationship
on hive_visit_user = oldId
LEFT JOIN hive_info 
on hive_visiters.hp_tab_name = hive_info.hp_db_name
and hive_visiters.hp_user_id = hive_info.hadoop_user_id