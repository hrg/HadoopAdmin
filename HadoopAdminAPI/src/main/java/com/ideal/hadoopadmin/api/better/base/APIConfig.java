package com.ideal.hadoopadmin.api.better.base;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by CC on 2016/7/29.
 * 需要包含所有的
 */
public class APIConfig {


    public static Map<String,ConfigDef> api_method_config=new HashMap<String, ConfigDef>();

    /**
     * 增加集群用户
     */
    public static final String CLUSTER_USER_ADD_CLUSTER_USER = "CLUSTER_USER_ADD_CLUSTER_USER";
    public static final String CLUSTER_USER_DROP_CLUSTER_USER = "CLUSTER_USER_DROP_CLUSTER_USER";
    public static final String CLUSTER_USER_RESET_USER_PASSWORD="CLUSTER_USER_RESET_USER_PASSWORD";
    public static final String CLUSTER_USER_ADD_CLIENT_USER = "CLUSTER_USER_ADD_CLIENT_USER";



    /**
     * 增加HDFS
     */
    public static final String HADOOP_HDFS_QUOTA_HADOOP_HDFS = "HADOOP_HDFS_QUOTA_HADOOP_HDFS";
    public static final String HADOOP_HDFS_CREATE_HDFS_PUB_DIR = "HADOOP_HDFS_CREATE_HDFS_PUB_DIR";
    public static final String HADOOP_HDFS_REMOVE_HDFS_PUB_DIR = "HADOOP_HDFS_REMOVE_HDFS_PUB_DIR";
    public static final String HADOOP_HDFS_ACCESS_USER_TO_GROUP="HADOOP_HDFS_ACCESS_USER_TO_GROUP";
    public static final String HADDOP_HDFS_REMOVE_USER_FROM_GROUP = "HADDOP_HDFS_REMOVE_USER_FROM_GROUP";
    public static final String HADDOP_HDFS_FLUSH_HDFS_INFO_BAK="HADDOP_HDFS_FLUSH_HDFS_INFO_BAK";

    /**
     * 增加hive
     */
    public static final String HIVE_GRANT_HIVE_PRIVILEGE="HIVE_GRANT_HIVE_PRIVILEGE";
    public static final String HIVE_REVOKE_HIVE_PRIVILEGE="HIVE_REVOKE_HIVE_PRIVILEGE";
    public static final String HIVE_FLUSH_HIVE_INFO = "HIVE_FLUSH_HIVE_INFO";

    /**
     *增加kerberos
     */
    public static final String KERBEROS_AUTH_CLIIENT_KERBEROS="KERBEROS_AUTH_CLIIENT_KERBEROS";
    public static final String KERBEROS_DESTROY_CLIENT_KERBEROS="KERBEROS_DESTROY_CLIENT_KERBEROS";
//    public static final String KERBEROS_

    /**
     * 增加Yarn
     */
    public static final String YARN_FAIR_SCHEDULER="YARN_FAIR_SCHEDULER";


    static{
        //定义 添加用户 参数
        ConfigDef configDef = new ConfigDef(CLUSTER_USER_ADD_CLUSTER_USER);
        NotNulValidation validation = new NotNulValidation();
        configDef.config(new ConsumerParam("Cluster_User_Name", ConfigDef.ParamType.STRING,validation))
                 .config(new ConsumerParam("Cluster_User_PW", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("Cluster_User_SysPW", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("Cluster_User_HomeDir", ConfigDef.ParamType.STRING, validation));
//                .config(new ConsumerParam("Cluster_User_CltQuota", ConfigDef.ParamType.STRING, validation));
        api_method_config.put(CLUSTER_USER_ADD_CLUSTER_USER,configDef);

        //定义 删除用户 参数
        configDef = new ConfigDef(CLUSTER_USER_DROP_CLUSTER_USER);
        configDef.config(new ConsumerParam("Cluster_User_Name", ConfigDef.ParamType.STRING, validation));
        api_method_config.put(CLUSTER_USER_DROP_CLUSTER_USER,configDef);

        //定义 重新设置密码 参数
        configDef = new ConfigDef(CLUSTER_USER_RESET_USER_PASSWORD);
        configDef.config(new ConsumerParam("Cluster_User_Name", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("Cluster_User_PW", ConfigDef.ParamType.STRING, validation));
        api_method_config.put(CLUSTER_USER_RESET_USER_PASSWORD,configDef);

        //定义 添加client 参数
        configDef = new ConfigDef(CLUSTER_USER_ADD_CLIENT_USER);
        configDef.config(new ConsumerParam("Cluster_User_Name", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("Cluster_User_PW", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("Cluster_User_Client", ConfigDef.ParamType.STRING, validation));
        api_method_config.put(CLUSTER_USER_ADD_CLIENT_USER,configDef);

        //定义 HDFS配额 参数
        configDef = new ConfigDef(HADOOP_HDFS_QUOTA_HADOOP_HDFS);
        configDef.config(new ConsumerParam("Cluster_User_Name", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("HDFS_QUOTA_SPACE_SIZE", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("HDFS_QUOTA_DIR_NUMBER", ConfigDef.ParamType.STRING, validation));
        api_method_config.put(HADOOP_HDFS_QUOTA_HADOOP_HDFS,configDef);

        //定义 创建HDFS目录 参数
        configDef = new ConfigDef(HADOOP_HDFS_CREATE_HDFS_PUB_DIR);
        configDef.config(new ConsumerParam("Cluster_User_Name", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("HDFS_PUB_TABLE_DIR", ConfigDef.ParamType.STRING, validation));
        api_method_config.put(HADOOP_HDFS_CREATE_HDFS_PUB_DIR,configDef);

        //定义 移除HDFS目录 参数
        configDef = new ConfigDef(HADOOP_HDFS_REMOVE_HDFS_PUB_DIR);
        configDef.config(new ConsumerParam("Cluster_User_Name", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("HDFS_PUB_TABLE_DIR", ConfigDef.ParamType.STRING, validation));
        api_method_config.put(HADOOP_HDFS_REMOVE_HDFS_PUB_DIR,configDef);

        //定义 用户加入组 参数
        configDef = new ConfigDef(HADOOP_HDFS_ACCESS_USER_TO_GROUP);
        configDef.config(new ConsumerParam("HDFS_PUB_TABLE_DIR", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("Cluster_User_Name", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("Cluster_HDFS_PATH_OWNER", ConfigDef.ParamType.STRING, validation));
        api_method_config.put(HADOOP_HDFS_ACCESS_USER_TO_GROUP,configDef);

        //定义 用户移出组 参数
        configDef = new ConfigDef(HADDOP_HDFS_REMOVE_USER_FROM_GROUP);
        configDef.config(new ConsumerParam("HDFS_PUB_TABLE_DIR", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("Cluster_User_Name", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("Cluster_HDFS_PATH_OWNER", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("HDFS_HAS_OTHER_ACCESS", ConfigDef.ParamType.STRING, validation));
        api_method_config.put(HADDOP_HDFS_REMOVE_USER_FROM_GROUP,configDef);

        //定义 刷新HDFSInfoBak 参数
        configDef = new ConfigDef(HADDOP_HDFS_FLUSH_HDFS_INFO_BAK);
        api_method_config.put(HADDOP_HDFS_FLUSH_HDFS_INFO_BAK,configDef);

        //定义 授予hive特权 参数
        configDef = new ConfigDef(HIVE_GRANT_HIVE_PRIVILEGE);
        configDef.config(new ConsumerParam("HDFS_PUB_TABLE_DIR", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("Cluster_User_Name", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("HIVE_TABLE_NAME", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("HIVE_PRIVILE_USER", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("HIVE_PRIVILE_TYPE",ConfigDef.ParamType.STRING, validation));
        api_method_config.put(HIVE_GRANT_HIVE_PRIVILEGE,configDef);

        //定义 撤销hive特权
        configDef = new ConfigDef(HIVE_REVOKE_HIVE_PRIVILEGE);
        configDef.config(new ConsumerParam("HDFS_PUB_TABLE_DIR", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("Cluster_User_Name", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("HIVE_TABLE_NAME", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("HIVE_PRIVILE_USER", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("HDFS_HAS_OTHER_ACCESS", ConfigDef.ParamType.STRING, validation));
        api_method_config.put(HIVE_REVOKE_HIVE_PRIVILEGE,configDef);

        //定义 刷新hive 参数
        configDef = new ConfigDef(HIVE_FLUSH_HIVE_INFO);
        api_method_config.put(HIVE_FLUSH_HIVE_INFO,configDef);

        //定义  客户认证 参数
        configDef = new ConfigDef(KERBEROS_AUTH_CLIIENT_KERBEROS);
        configDef.config(new ConsumerParam("Cluster_User_Name", ConfigDef.ParamType.STRING, validation));
//                .config(new ConsumerParam("Cluster_User_SysPW", ConfigDef.ParamType.STRING, validation))
//                .config(new ConsumerParam("Cluster_KBR_Client", ConfigDef.ParamType.STRING, validation));
        api_method_config.put(KERBEROS_AUTH_CLIIENT_KERBEROS,configDef);

        //定义  销毁客户认证 参数
        configDef = new ConfigDef(KERBEROS_DESTROY_CLIENT_KERBEROS);
        configDef.config(new ConsumerParam("Cluster_User_Name", ConfigDef.ParamType.STRING, validation))
                .config(new ConsumerParam("Cluster_KBR_Client", ConfigDef.ParamType.STRING, validation));
        api_method_config.put(KERBEROS_DESTROY_CLIENT_KERBEROS,configDef);

        //定义  跟新调度文件 参数
        configDef = new ConfigDef(YARN_FAIR_SCHEDULER);
        api_method_config.put(YARN_FAIR_SCHEDULER,configDef);

    }

    /**
     *
     * @param key
     * @return
     */
    public static ConfigDef getConfigDefaul(String key){
        return api_method_config.get(key);
    }


















}
