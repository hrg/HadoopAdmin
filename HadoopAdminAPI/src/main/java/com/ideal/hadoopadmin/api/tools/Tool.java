package com.ideal.hadoopadmin.api.tools;


import java.util.Random;

public class Tool {
    /** 大写 */
    private static String[] UpperCase=new String[]{"A","B","C","D","E","F","G","H","I","J","K","L",
            "M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l",
            "m","n","o","p","q","r","s","t","u","v","w","x","y","z","1","2","3","4","5","6","7","8","9","0"};

    public static String GeneratRandomPW(){
        String password="";
        password = getRandomPartFromArray(10,UpperCase);

        return password;
    }

    private static String getRandomPartFromArray(int len,String[] array){
        StringBuffer sb=new StringBuffer();

        Random random=new Random(System.currentTimeMillis());

        int range=array.length;

        for(int i=0;i<len;i++){
            sb.append(array[random.nextInt(range)]);
        }

        return sb.toString();
    }

    public static void main(String[] a){
//        System.out.println(GeneratRandomPW());
    }
}
