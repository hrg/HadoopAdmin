package com.ideal.hadoopadmin.api.better.hdfs;

import com.ideal.hadoopadmin.api.better.base.APIConfig;
import com.ideal.hadoopadmin.api.better.base.ConfigDef;
import com.ideal.hadoopadmin.api.hdfs.HDFSAPI;
import com.ideal.hadoopadmin.api.linux.UserAPI;
import com.ideal.hadoopadmin.crontab.hdfs.FlushHDFSInfo;
import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.ContextResult;

import java.util.Map;

/**
 * Created by CC on 2016/7/30.
 */
public class HadoopHDFSAPI {


    /**
     * hdfs 配额
     * @param params
     * @return
     */
    public static ContextResult QuotaHadoopHDFS(Map<String,Object> params){
        ConfigDef configDef= APIConfig.getConfigDefaul(APIConfig.HADOOP_HDFS_QUOTA_HADOOP_HDFS);
        if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        HDFSAPI.QuotaHDFS(context);
        return context.getContextResult();
    }

    /**
     * 创建hdfs dir
     * @param params
     * @return
     */
    public static ContextResult CreateHDFSPubDir(Map<String,Object> params){
        ConfigDef configDef= APIConfig.getConfigDefaul(APIConfig.HADOOP_HDFS_CREATE_HDFS_PUB_DIR);
        if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        HDFSAPI.MakeHDFSPubDir(context);
        return context.getContextResult();
    }

    /**
     * 删除hdfs dir
     * @param params
     * @return
     */
    public static ContextResult RemoveHDFSPubDir(Map<String,Object> params){
        ConfigDef configDef= APIConfig.getConfigDefaul(APIConfig.HADOOP_HDFS_REMOVE_HDFS_PUB_DIR);
        if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        HDFSAPI.RMHDFSPubDir(context);
        return context.getContextResult();
    }

    /**
     * 用户加入组
     * @param params
     * @return
     */
    public static ContextResult AccessUserToGroup(Map<String,Object> params){
        ConfigDef configDef= APIConfig.getConfigDefaul(APIConfig.HADOOP_HDFS_ACCESS_USER_TO_GROUP);
        if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        HDFSAPI.accessUserToGroup(context);
        return context.getContextResult();
    }

    /**
     * 用户移出组
     * @param params
     * @return
     */
    public static ContextResult RemoveUserFromGroup(Map<String,Object> params){
        ConfigDef configDef= APIConfig.getConfigDefaul(APIConfig.HADDOP_HDFS_REMOVE_USER_FROM_GROUP);
        if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        HDFSAPI.refuseUserToGroup(context);
        return context.getContextResult();
    }

    /**
     * 刷新HDFSInfoBak
     * @param params
     * @return
     */
    public static ContextResult FlushHDFSInfoBak(Map<String,Object> params){
        ConfigDef configDef= APIConfig.getConfigDefaul(APIConfig.HADDOP_HDFS_FLUSH_HDFS_INFO_BAK);
        //if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        HDFSAPI.flushHdfsInfoBak(context);
        return context.getContextResult();
    }

}
