package com.ideal.hadoopadmin.api.better.kerberos;

import com.ideal.hadoopadmin.api.better.base.APIConfig;
import com.ideal.hadoopadmin.api.better.base.ConfigDef;
import com.ideal.hadoopadmin.api.kerberos.KDCAPI;
import com.ideal.hadoopadmin.api.linux.UserAPI;
import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.ContextResult;

import java.util.Map;

/**
 * Created by CC on 2016/7/30.
 */
public class KerberosAPI {


    /**
     * 客户认证
     * @param params
     * @return
     */
    public static ContextResult AuthClientKerberos(Map<String,Object> params){
        ConfigDef configDef= APIConfig.getConfigDefaul(APIConfig.KERBEROS_AUTH_CLIIENT_KERBEROS);
        if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        KDCAPI.authClientKerberos(context);
        return context.getContextResult();
    }

    /**
     * 撤销客户认证
     * @param params
     * @return
     */
    public static ContextResult DestroyClientKerberos(Map<String,Object> params){
        ConfigDef configDef= APIConfig.getConfigDefaul(APIConfig.KERBEROS_DESTROY_CLIENT_KERBEROS);
        if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        KDCAPI.destroyClientKerberos(context);
        return context.getContextResult();
    }
}
