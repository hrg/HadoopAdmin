package com.ideal.hadoopadmin.api.kerberos;

import com.ideal.hadoopadmin.api.linux.UserAPI;
import com.ideal.hadoopadmin.crontab.kerberos.KerberosAPI;
import com.ideal.service.krb.KerberosService;
import com.ideal.tools.ssh.common.CommonProperties;
import com.ideal.tools.ssh.common.CommonTools;
import com.ideal.tools.ssh.common.OperationMarket;
import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.LinuxMachine;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by CC on 2016/3/12.
 */
public class KDCAPI {

    public static final String Cluster_KBR_Client ="Cluster_KBR_Client";

    /**
     * UserAPI.Cluster_User_Name 需要认证的用户
     * UserAPI.Cluster_User_SysPW 需要认真用户的 系统密码
     * KDCAPI.Cluster_KBR_Client 配置的机器ip,ip用都好分割
     * 需要载入 client 和  webapp 机器
     * 只处理前台传入的机器
     *
     * 认证kdc 这个 逻辑修改下：
     * 1.先添加用户
     * 2.然后认证
     * 3.最后复制票据
     * @param context
     */
    public static void authClientKerberos(ClusterContext context){

        KerberosService service= new KerberosService();
        service.KerberosFresh_new(context);

//        List<LinuxMachine> machineList = context.getOriginalList();
//        CommonProperties commonProperties =context.getCommonProperties();
////
//        String userName = commonProperties.getArgument(UserAPI.Cluster_User_Name,"");
//        String sysPW = commonProperties.getArgument(UserAPI.Cluster_User_SysPW,"");
//        String machineIPs = commonProperties.getArgument(KDCAPI.Cluster_KBR_Client,"");
//        if (machineIPs==null)return;
//        String[] ips = machineIPs.split(",");
//
//        if(StringUtils.isBlank(userName)){
//            return ;
//        }
//
//        //webapp 机器是脚本机器 放置执行脚本 所以是执行机器
//        List<LinuxMachine> finalMachines = new ArrayList<LinuxMachine>() ;
//        LinuxMachine webappMachine =  CommonTools.getMachineListByType(machineList,context,
//                LinuxMachine.MachineType.WebAPP).get(0);
//
//        for(LinuxMachine linuxMachine:machineList) {
//            if(linuxMachine.getMachineType() == LinuxMachine.MachineType.Client) {
//                if(ArrayUtils.contains(ips, linuxMachine.getSshAuthor().getHost())) {
//
//                    //添加用户
//                    linuxMachine.initOperation(OperationMarket.AddLinuxUser(userName));
//                    finalMachines.add(linuxMachine);
//
//                    //webapp 只会有一台 这里只需要 得到第一个就可以了
//                    webappMachine.initOperation(OperationMarket.AuthClientKDCPrinc(
//                            linuxMachine.getSshAuthor().getHost(), linuxMachine.getSshAuthor().getUsername(),
//                            linuxMachine.getSshAuthor().getPasswd(), userName, sysPW));
//                }
//            }
//        }
//
//        if(finalMachines.size()>0){
//            finalMachines.add(webappMachine);
//        }
//
//        //重新设置 机器列表
//        context.setMachineList(finalMachines);
//        //执行
//        context.doTheThing();
//
//        //这里调用时为了 复制
//        KerberosService kerberosService = new KerberosService();
//        kerberosService.KerberosFresh(" and userinfo.userName ='"+userName+"'");
    }

    /**
     * Cluster_User_Name 需要认证的用户
     * machineList 需要放入client 端就行
     * @param context
     */
    public static void destroyClientKerberos(ClusterContext context){
        List<LinuxMachine> machineList = context.getOriginalList();
        CommonProperties commonProperties =context.getCommonProperties();

        String userName = commonProperties.getArgument(UserAPI.Cluster_User_Name,"");
        String machineIPs = commonProperties.getArgument(KDCAPI.Cluster_KBR_Client,"");
        String[] ips =null;
        if (machineIPs!=null) {
            ips = machineIPs.split(",");

        }
        if(StringUtils.isBlank(userName)){
            return ;
        }

        HashSet finalList = new HashSet();
        //webapp 机器是脚本机器 放置执行脚本 所以是执行机器
        List<LinuxMachine> finalMachines = CommonTools.getMachineListByType(machineList,context,
                LinuxMachine.MachineType.Client);

        for(LinuxMachine machine: finalMachines){
            if(ips==null||ips.length==0) {
                machine.initOperation(OperationMarket.DestroyClientKDCPrinc(userName));
            }else if(ArrayUtils.contains(ips, machine.getSshAuthor().getHost())){
                machine.initOperation(OperationMarket.DestroyClientKDCPrinc(userName));
            }

        }

        //重新设置 机器列表
        context.setMachineList(finalMachines);
        //执行
        context.doTheThing();

    }

}
