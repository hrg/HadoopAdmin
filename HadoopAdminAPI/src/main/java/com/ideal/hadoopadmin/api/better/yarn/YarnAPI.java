package com.ideal.hadoopadmin.api.better.yarn;

import com.ideal.hadoopadmin.api.better.base.APIConfig;
import com.ideal.hadoopadmin.api.better.base.ConfigDef;
import com.ideal.hadoopadmin.api.hdfs.HDFSAPI;
import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.ContextResult;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/8/2.
 */
public class YarnAPI {
    /**
     * 跟新调度文件
     * @param params
     * @return
     */
    public static ContextResult FairScheduler(Map<String,Object> params){
        ConfigDef configDef= APIConfig.getConfigDefaul(APIConfig.YARN_FAIR_SCHEDULER);
        if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        com.ideal.hadoopadmin.api.yarn.YarnAPI.dbFairScheduler(context);
        return context.getContextResult();
    }


    public static void main(String[] args){
        YarnAPI.FairScheduler(new HashMap<String, Object>());
    }
}

