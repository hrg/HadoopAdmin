package com.ideal.hadoopadmin.api.better.base;

/**
 * Created by CC on 2016/7/29.
 */
public class NotNulValidation implements ConfigDef.Validation{

        ConsumerParam param;

        public boolean validate(ConsumerParam param){
            this.param=param;
            return this.param.getVal()==null;
        }

        public String getDescription(){
            return "["+param.getName()+"] must have valuse!";
        }
}

