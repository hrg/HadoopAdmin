package com.ideal.hadoopadmin.api.better.base;

/**
 * Created by CC on 2016/7/29.
 */
public class ConsumerParam {
    String name;
    Object val;
    ConfigDef.ParamType type;
    ConfigDef.Validation validation;

    public ConsumerParam(String name, ConfigDef.ParamType type, ConfigDef.Validation validation) {
        this.name = name;
        this.type = type;
        this.validation = validation;
    }

    public String getName() {
        return name;
    }

    public Object getVal() {
        return val;
    }

    public ConfigDef.ParamType getType() {
        return type;
    }

    public ConfigDef.Validation getValidation() {
        return validation;
    }

    public void setVal(Object val) {
        this.val = val;
    }
}
