package com.ideal.hadoopadmin.api.better.linux;

import com.ideal.hadoopadmin.api.better.base.APIConfig;
import com.ideal.hadoopadmin.api.better.base.ConfigDef;
import com.ideal.hadoopadmin.api.linux.UserAPI;
import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.ContextResult;
import com.ideal.tools.ssh.entity.LinuxMachine;

import java.util.Map;

/**
 * Created by CC on 2016/7/29.
 *
 * 对外提供服务
 * 想包含两张形式  ：
 * 1.本地直接调用
 * 2.外部可以通过http 请求调用 两种混写在一起不太好，因为这样对外就暴露了servier
 *
 */
public class ClusterUserAPI {


    /**
     * 添加集群用户
     * 只需要放入 用户传递的参数
     *
     * 这个clustercontext  由这边api组织
     *
     * @param params
     *
     */
    public static ContextResult AddClusterUser(Map<String,Object> params){
        ConfigDef configDef=APIConfig.getConfigDefaul(APIConfig.CLUSTER_USER_ADD_CLUSTER_USER);
        if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        UserAPI.addClusterUser(context);
        return context.getContextResult();
    }

    /**
     * 删除集群用户
     * @param params
     * @return
     */
    public static ContextResult DropClusterUser(Map<String,Object> params){
        ConfigDef configDef=APIConfig.getConfigDefaul(APIConfig.CLUSTER_USER_DROP_CLUSTER_USER);
        if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        UserAPI.delClusterUser(context);
        return context.getContextResult();
    }


    /**
     * 同步客户端用户
     * @param params
     * @return
     */
    public static ContextResult SynClientUser(Map<String,Object> params){

        return null;
    }

    /**
     * 这个方法 使用clush 同步用户
     * 这里只同步
     * @param params
     * @return
     */
    public static ContextResult SynUserWithClush(Map<String,Object> params){
        return null;
    }


    /**
     * 使用 机器信息同步用户
     * @param params
     * @return
     */
    public static ContextResult SynUserWithMachineInfo(Map<String,Object> params){
        return null;
    }


    /**
     * 集群客户机 配额
     * @param params
     * @return
     */
    public static ContextResult QuotaClientMachine(Map<String,Object> params){
        return null;
    }



    public static ContextResult ResetUserPassword(Map<String,Object> params){
        ConfigDef configDef=APIConfig.getConfigDefaul(APIConfig.CLUSTER_USER_RESET_USER_PASSWORD);
        if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        UserAPI.resetUserPasswd(context);
        return context.getContextResult();
    }

    public static ContextResult AddClientUser(Map<String,Object> params){
        ConfigDef configDef=APIConfig.getConfigDefaul(APIConfig.CLUSTER_USER_ADD_CLIENT_USER);
        if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        UserAPI.addClientUser(context);
        return context.getContextResult();
    }
}
