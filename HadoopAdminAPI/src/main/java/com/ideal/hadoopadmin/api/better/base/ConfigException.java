package com.ideal.hadoopadmin.api.better.base;

/**
 * Created by CC on 2016/7/29.
 */
public class ConfigException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public ConfigException(String message) {
        super(message);
    }

    public ConfigException(String name, Object value) {
        this(name, value, null);
    }

    public ConfigException(String name, Object value, String message) {
        super("Invalid value " + value + " for configuration " + name + (message == null ? "" : ": " + message));
    }


}
