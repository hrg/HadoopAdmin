package com.ideal.hadoopadmin.api.better.hive;

import com.ideal.hadoopadmin.api.better.base.APIConfig;
import com.ideal.hadoopadmin.api.better.base.ConfigDef;
import com.ideal.hadoopadmin.api.linux.UserAPI;
import com.ideal.tools.ssh.context.ClusterContext;
import com.ideal.tools.ssh.entity.ContextResult;

import java.util.Map;

/**
 * Created by CC on 2016/7/30.
 */
public class HiveAPI {


    /**
     *授予hive特权
     * @param params
     * @return
     */
    public static ContextResult GrantHivePrivilege(Map<String,Object> params){
        ConfigDef configDef= APIConfig.getConfigDefaul(APIConfig.HIVE_GRANT_HIVE_PRIVILEGE);
        if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        com.ideal.hadoopadmin.api.hive.HiveAPI.grantHivePrivilege(context);
        return context.getContextResult();
    }

    /**
     * 撤销hive特权
     * @param params
     * @return
     */
    public static ContextResult RevokeHivePrivilege(Map<String,Object> params){
        ConfigDef configDef= APIConfig.getConfigDefaul(APIConfig.HIVE_REVOKE_HIVE_PRIVILEGE);
        if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        com.ideal.hadoopadmin.api.hive.HiveAPI.revokeHivePrivilege(context);
        return context.getContextResult();
    }

    /**
     * 刷新hive
     * @param params
     * @return
     */
    public static ContextResult FlushHiveInfo(Map<String,Object> params){
        ConfigDef configDef= APIConfig.getConfigDefaul(APIConfig.HIVE_FLUSH_HIVE_INFO);
        //if (configDef==null)return null;
        ClusterContext context=new ClusterContext(configDef.parseParam(params));
        com.ideal.hadoopadmin.api.hive.HiveAPI.flushHive(context);
        return context.getContextResult();
    }

}
